#include "mainwindow.h"
#include <QApplication>
#include <QSurfaceFormat>
#include <QTime>
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include "shared.h"
#include "sim/Support/neighbourfinder.h"
#include "tools/include.h"
#include "sim/kernels.h"

#include <random>

void perf_test_1() 
{
	int test_size = 800 * 1000 * 1000;
	Timer t1, t2, t3;

	std::vector<int> a1(test_size);
	std::vector<int> a2(test_size);

	int total = 0;

	for (int j = 0; j < 80; j++) 
	{
		t1.start();
		for (auto it = a1.begin(), end = a1.end(); it != end; it++) {
			total += *it;
		}
		t1.stop();

		t2.start();
		for (auto i : a2) {
			total += i;
		}
		t2.stop();

		t3.start();
		for (int* it = a1.data(), *end = it + a1.size(); it != end; it++) {
			total += *it;
		}
		t3.stop();
	}

	qInfo() << "TEST 1" << t1.elapsed_ms() << "ms";
	qInfo() << "TEST 2" << t2.elapsed_ms() << "ms";
	qInfo() << "TEST 3" << t3.elapsed_ms() << "ms";

	qInfo() << total;
}

void test_obst_dist() {
	std::vector<Vector2f> vertices {
		{0,0},
		{0,1},
		{1,1},
		{1,0}
	};
	Obstacle x(0.1, vertices);

	x.agent_collider().distanceTo(Vector2f(0.5, 0.5));
	x.agent_collider().distanceTo(Vector2f(-0.5, 0.5));
	x.agent_collider().distanceTo(Vector2f(1.5, 1.5));
}

void rand_test() {
	std::mt19937						m_rgen;
	std::normal_distribution<float>		m_dis{ 0.0f, 0.00f }; // mean and variance

	for (int i = 0; i < 1000; i++) {
		auto v = m_dis(m_rgen);
		qInfo() << v;
	}
}

int main(int argc, char *argv[])
{
	//perf_test_1();
	//rand_test();

	QApplication app(argc, argv);

	QSurfaceFormat format = QSurfaceFormat::defaultFormat();
	format.setVersion(3, 2);
	format.setProfile(QSurfaceFormat::CoreProfile);
	QSurfaceFormat::setDefaultFormat(format);
	
	/*QSurfaceFormat fmt;
	fmt.setDepthBufferSize(24);
	fmt.setSamples(8);
	fmt.setVersion(4, 3);
	fmt.setOption(QSurfaceFormat::StereoBuffers, true);
	fmt.setProfile(QSurfaceFormat::CoreProfile);
	QSurfaceFormat::setDefaultFormat(fmt);*/

	//QSurfaceFormat  format;
	//format.setSamples(8);
	//format.setDepthBufferSize(24);
	//format.setStencilBufferSize(8);
	//format.setProfile(QSurfaceFormat::OpenGLContextProfile::CoreProfile);
	//QSurfaceFormat::setDefaultFormat(format);

	auto& w = MainWindow::Create();
	w.show();
	//w.adept();
	return app.exec();
}

/*
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QSurfaceFormat fmt;
	fmt.setDepthBufferSize(24);
	fmt.setSamples(4);
	fmt.setVersion(3, 2);
	fmt.setProfile(QSurfaceFormat::CoreProfile);
	QSurfaceFormat::setDefaultFormat(fmt);

	//SimulationSettings s;
	//if (write_json("resources/test_settings1.json", s.to_json()))   qInfo() << "Succes!";
	//else  qInfo() << "Fail!";


	MainWindow w;
	w.show();

	return app.exec();
}*/
