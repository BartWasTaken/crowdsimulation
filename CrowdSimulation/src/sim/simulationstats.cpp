#include "simulationstats.h"
#include <functional>

template<typename T, typename L, typename A, typename V>
T get_avg_element(L& list, A accessor, V validator)
{
	T total{ 0 };
	int count = 0;
	for (auto& entry : list) {
		if (validator(entry)) {
			total += (T) accessor(entry);
			count++;
		}
	}
	if (count == 0) return T(0);
	else return T(total / (T) count);
}

template<typename T>
bool default_true(T) {
	return true;
}

template<typename T, typename L, typename A>
float get_avg_element(L& list, A accessor)
{
	using DataType = typename L::value_type;
	return get_avg_element<T>(list, accessor, &default_true<DataType>);
}

SimulationStats::SimulationStats()
{

}

SimulationStats::FrameData SimulationStats::avg()
{
	FrameData result;

	for (int i = 0; i < StatVars::NR_VARS; i++) {
		result.var(i) = get_avg_element<float>(m_memory, [i](FrameData& fd) -> float { return fd.var(i); });
	}


	int n_timers = last().timers().size();

	// loop over current timers
	for (int i = 0; i < n_timers; i++) {
		// create fake timer with elapsed time from the average
		auto elapsed = get_avg_element<std::chrono::nanoseconds>(m_memory,
			[i](FrameData& fd) -> std::chrono::nanoseconds {
				auto& timer = fd.timers()[i];
				Q_ASSERT(!timer.is_running());
				return timer.elapsed(); 
			},
			[i, this](FrameData& fd) -> bool {
				if (fd.timers().size() > i) {
					return fd.timers()[i].name.compare(last().timers()[i].name) == 0;
				}
				else return false;
			}
		);
		result.timers().push_back({ elapsed });
		result.timers().back().name = last().timers()[i].name;
	}

	return result;
}

void SimulationStats::push(std::chrono::time_point<Timer::my_clock> time_stamp)
{
	while (m_memory.size() > 0 && m_memory.front().age_seconds(time_stamp) > m_trail_time)
	{
		m_memory.pop_front();
	}
	Q_ASSERT(m_memory.size() == 0 || m_memory.back().timestamp < time_stamp);

	m_memory.push_back(m_working_data);
	m_working_data = FrameData();
	m_working_data.timestamp = time_stamp;
}

SimulationStats::FrameData::TimerAcces SimulationStats::FrameData::push_timer(QString name, bool auto_start)
{
	m_work_timers.push_back({ auto_start }); // true for start the timer
	m_work_timers.back().name = name;
	return TimerAcces(&m_work_timers, m_work_timers.size() - 1);
}

float SimulationStats::FrameData::pop_timer()
{
	// make sure its stopped
	m_work_timers.back().stop();
	// get the elapsed time
	float dt = m_work_timers.back().elapsed_ms();
	m_timers.push_back(m_work_timers.back());
	m_work_timers.pop_back();
	return dt;
}

void SimulationStats::FrameData::TimerAcces::start()
{
	(*m_pTimers)[timer_idx].start();
}

void SimulationStats::FrameData::TimerAcces::stop()
{
	(*m_pTimers)[timer_idx].stop();
}
