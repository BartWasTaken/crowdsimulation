#include "simulationsettings.h"
#include "tools/geomfunc.h"
#include <QDebug>
#include <QFormLayout>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QHeaderView>
#include <QDialogButtonBox>
#include <QMessageBox>

void write_interpret_value(QJsonObject& json, QString key, QString str_val) 
{
	{ // check integer
		bool is_type;
		int value = str_val.toInt(&is_type);
		if (is_type) {
			json[key] = value;
			return;
		}
	}
	{ // check double
		bool is_double;
		double val_double = str_val.toDouble(&is_double);
		if (is_double) {
			json[key] = val_double;
			return;
		}
	}
	{ // check bool
		if (str_val.toLower().compare("true") == 0) {
			json[key] = true;
			return;
		}
		if (str_val.toLower().compare("false") == 0) {
			json[key] = false;
			return;
		}
	}
	json[key] = str_val;
}

QString to_string(QJsonValue val) {
	if (val.isString()) return val.toString();
	if (val.isBool()) return val.toBool();
	if (val.isDouble()) return QString::number(val.toDouble());
	if (val.isUndefined()) return "Undefined";
	if (val.isDouble()) return QString::number(val.toDouble());
	return "???";
}

bool load_json(const QJsonObject& json, QString name, qint32& out_value, bool warn)
{
	bool succes = false;

	auto result = json.find(name);
	if (result != json.end()) {
		out_value = (*result).toInt();
		succes = true;
	}
	else if (warn) {
		qDebug() << "Unable to find key: " << name;
	}

	return succes;
}

bool load_json(const QJsonObject& json, QString name, float& out_value, bool warn)
{
	bool succes = false;

	auto result = json.find(name);
	if (result != json.end()) {
		out_value = (float) (*result).toDouble();
		succes = true;
	}
	else if (warn) {
		qDebug() << "Unable to find key: " << name;
	}

	return succes;
}

SimulationSettings::SimulationSettings()
{
	m_agent_count = 100;
	m_agent_rad = 0.3f;
	m_agent_speed = 0.9f;

	m_mde_max_iter = 6;

	m_sph_kernel_rad = 0.0f; // 0 will automatically set it
	m_sph_max_dens_coef = 0.4f;
	m_sph_stiffness = 0.3f;
	m_sph_max_iter_dens = 5;

	m_cnt_discomfort_project_dt = 0.1f;
	m_cnt_weight_discomfort_agent = 1;
	m_cnt_weight_discomfort_smoke = 10;
	m_cnt_weight_discomfort_obst = 1;
	m_cnt_weight_dist = 1;
	m_cnt_weight_time = 1;
}

SimulationSettings::SimulationSettings(QJsonObject json)
{
	from_json(json);
}

SimulationSettings::~SimulationSettings()
{

}

QJsonObject SimulationSettings::to_json() const
{
	// result object:
	QJsonObject json;

	// add all the data from "extra" entries
	auto keys = m_json_extra.keys();
	QJsonObject::const_iterator it_val = m_json_extra.begin();
	QJsonObject::const_iterator val_end = m_json_extra.end();
	QList<QString>::const_iterator it_key = keys.begin();
	QList<QString>::const_iterator key_end = keys.end();

	for (; it_val != val_end 
		&& it_key != key_end; 
		it_val++, it_key++)
	{
		json[*it_key] = *it_val;
	}

	// write base values (override extra data)
	json["agent_count"] = m_agent_count;
	json["agent_rad"] = m_agent_rad;
	json["agent_speed"] = m_agent_speed;

	json["mde_max_iter"] = m_mde_max_iter;

	json["sph_kernel_rad"] = m_sph_kernel_rad;
	json["sph_max_dens_coef"] = m_sph_max_dens_coef;
	json["sph_stiffness"] = m_sph_stiffness;
	json["sph_max_iter_dens"] = m_sph_max_iter_dens;

	json["cnt_discomfort_project_dt"] = m_cnt_discomfort_project_dt;
	json["cnt_weight_discomfort_smoke"] = m_cnt_weight_discomfort_smoke;
	json["cnt_weight_discomfort_agent"] = m_cnt_weight_discomfort_agent;
	json["cnt_weight_discomfort_obst"] = m_cnt_weight_discomfort_obst;
	json["cnt_weight_dist"] = m_cnt_weight_dist;
	json["cnt_weight_time"] = m_cnt_weight_time;

	return json;
}

void SimulationSettings::open_dialog()
{
	if (m_pDialog == nullptr) {
		m_pDialog = new QDialog();
		m_pDialog->setMinimumSize(300, 400);

		QLabel* queryLabel = new QLabel("Empty");
		QLineEdit* queryEdit = new QLineEdit();
		m_ui_table = new QTableView();

		QHBoxLayout* queryLayout = new QHBoxLayout();
		queryLayout->addWidget(queryLabel);
		queryLayout->addWidget(queryEdit);

		QVBoxLayout* mainLayout = new QVBoxLayout();
		mainLayout->addLayout(queryLayout);
		mainLayout->addWidget(m_ui_table);
		m_pDialog->setLayout(mainLayout);

		///////////////////

		m_dialog_items_ui = new QStandardItemModel();


		auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
			| QDialogButtonBox::Cancel);
		QObject::connect(buttonBox, &QDialogButtonBox::accepted, this, &SimulationSettings::accept);
		QObject::connect(buttonBox, &QDialogButtonBox::rejected, this, &SimulationSettings::reject);

		mainLayout->addWidget(buttonBox);
	}

	if (m_refill_dialog) {
		m_refill_dialog = false;
		///////////////////////
		m_dialog_items.clear();
		m_dialog_items_ui->clear();
		m_dialog_items_ui->setHorizontalHeaderLabels(QStringList() << ("Name") << ("Value"));

		auto json = to_json();
		auto keys = json.keys();
		QJsonObject::const_iterator it_val = json.begin();
		QJsonObject::const_iterator val_end = json.end();
		QList<QString>::const_iterator it_key = keys.begin();
		QList<QString>::const_iterator key_end = keys.end();

		for (; it_val != val_end
			&& it_key != key_end;
			it_val++, it_key++)
		{
			QString kstr = *it_key;
			QString vstr = to_string(*it_val);
			QList<QStandardItem *> items{ new QStandardItem(kstr), new QStandardItem(vstr) };
			m_dialog_items.push_back({ items[0], items[1] });
			m_dialog_items_ui->appendRow(items);
		}

		m_ui_table->setModel(m_dialog_items_ui);
		m_ui_table->resizeColumnsToContents();
		m_ui_table->verticalHeader()->hide();
		m_ui_table->horizontalHeader()->setStretchLastSection(true);
	}

	m_pDialog->exec();
}

QSharedPointer<SimulationSettings> SimulationSettings::apply_changes()
{
	QJsonObject old = this->to_json();
	if (m_dialog_json.length() > 0) {
		this->from_json(m_dialog_json);
	}
	return SimulationSettings_ptr::create(old);
}


SimulationSettings_ptr SimulationSettings::copy()
{
	return SimulationSettings_ptr::create(this->to_json());
}

void SimulationSettings::accept()
{
	// get values:
	m_dialog_json = QJsonObject();
	for (auto& pair : m_dialog_items) {
		auto key = pair.first->text();
		auto value = pair.second->text();
		write_interpret_value(m_dialog_json, key, value);
	}

	if (SimulationSettings(m_dialog_json).is_valid()) {
		shout_change(this);
		m_pDialog->close();
	}
	else {
		QMessageBox msgBox;
		msgBox.setText("Failure.");
		msgBox.setInformativeText("Invalid values given");
		msgBox.setStandardButtons(QMessageBox::Ok);
		int ret = msgBox.exec();
	}
}

void SimulationSettings::reject()
{
	m_pDialog->close();
}

template<typename T>
bool in_range(T val, T low, T high) {
	return val >= low && val <= high;
}

float SimulationSettings::compute_sph_kernel_rad()
{
	float	infl_rad = sph_kernel_rad();
	if (infl_rad <= 0) infl_rad = agent_rad() * 6;
	return infl_rad;
}

bool SimulationSettings::is_valid()
{
	return in_range(m_agent_count, 1, 100000000)
		&& in_range(m_agent_rad, Geometry::Epsilon, 1e8f)
		&& in_range(m_agent_speed, Geometry::Epsilon, 1e8f)
		&& in_range(m_mde_max_iter, 0, 10000)
		&& in_range(m_sph_max_dens_coef, Geometry::Epsilon, 1.0f)
		&& in_range(m_sph_stiffness, 0.0f, 1.0f)
		&& in_range(m_sph_max_iter_dens, 0, 10000);
}

void SimulationSettings::from_json(const QJsonObject& json)
{
	m_json_extra = json;

	load_json(json, "agent_count", m_agent_count, true);
	load_json(json, "agent_rad", m_agent_rad, true);
	load_json(json, "agent_speed", m_agent_speed, true);

	load_json(json, "mde_max_iter", m_mde_max_iter, true);

	load_json(json, "sph_kernel_rad", m_sph_kernel_rad, true);
	load_json(json, "sph_max_dens_coef", m_sph_max_dens_coef, true);
	load_json(json, "sph_stiffness", m_sph_stiffness, true);
	load_json(json, "sph_max_iter_dens", m_sph_max_iter_dens, true);

	load_json(json, "cnt_discomfort_project_dt", m_cnt_discomfort_project_dt, true);
	load_json(json, "cnt_weight_discomfort_smoke", m_cnt_weight_discomfort_smoke, true);
	load_json(json, "cnt_weight_discomfort_agent", m_cnt_weight_discomfort_agent, true);
	load_json(json, "cnt_weight_discomfort_obst", m_cnt_weight_discomfort_obst, true);
	load_json(json, "cnt_weight_dist", m_cnt_weight_dist, true);
	load_json(json, "cnt_weight_time", m_cnt_weight_time, true);
}
