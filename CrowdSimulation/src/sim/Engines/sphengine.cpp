#include "sphengine.h"
#include "sim/simulationcore.h"
#include "sim/simulationsettings.h"
#include "tools/include.h"
#include "vis/GridDrawer2D.h"
#include "mainwindow.h"
#include "sim/kernels.h"
#include "sim/Support/neighbourfinder.h"
#include "sim/simulationcore.h"
#include "sim/IEnvironment.h"
#include "sim/Support/neighbourfinder.h"
#include "sim/collisionresolver.h"
#include <QTimer>
#include <QTime>
#include <QAction>

#define GATHER_INFO 1

SPHEngine::SPHEngine(OpenGLView&, bool secondary)
	: m_secondary(secondary)
{

}

void SPHEngine::step(SimulationCore& sc, float dt)
{
	sc.require_vector_size(m_dens, 1);
	sc.require_vector_size(m_press, 1);
	sc.require_vector_size(m_forces);
	sc.require_vector_size(m_pref_vels);

	m_pSc = &sc;
	auto nr_agents = sc.nr_active_agents();

	// collect constants:

	float	stiff = stiffness(sc.settings().sph_stiffness(), dt * dt);
	int		max_iter = sc.settings().sph_max_iter_dens();

	float	infl_rad = sc.settings().compute_sph_kernel_rad();
	//float	infl_rad_2 = infl_rad * infl_rad;

	// qInfo() << "SPH - influence radius:" << infl_rad;

	float	max_dens = max_density(2 * sc.settings().agent_rad(), sc.settings().sph_max_dens_coef());
	auto&	nbf = sc.collision_resolver().neighbourFinder();

	//float	dens_force_limit = sc.settings().agent_rad() * 1.0f / dt;

	sc.global_planner().set_pref_velocity(sc, m_pref_vels, dt);

	sc.frame_data().push_timer("SPH (ms)", true);
	auto mde_timer = sc.frame_data().push_timer("SPH - MDE (ms)", false);

	float full_change_t = 0.01f;
	float alpha = qMax(0.0f, 1 - dt / full_change_t); // std::pow(0.05f, dt); // how much of the original speed we keep
	float ialpha = 1 - alpha;
	float max_pressure = 100 * sc.settings().agent_rad();

	//sc.make_zero_vector(m_displacement);

	m_displacement = sc.positions(); // the displacement is the position memory, edited when moving through portals

	for (int i = 0; i < nr_agents; i++)
	{
		//sc.position(i) += sc.velocity(i) * dt;

		sc.velocity(i) = VEC2_ACCES(m_pref_vels, i) * ialpha + sc.velocity(i) * alpha;
		VEC2_ACCES(m_displacement, i) += sc.try_move(i, sc.velocity(i) * dt);
	}

	Poly6 kernel(infl_rad);

	if (m_neighbour_cache.size() < nr_agents) {
		m_neighbour_cache.resize(nr_agents);
		m_neighbour_cache_obst.resize(nr_agents);
	}
	if (m_neighbour_cache.size() > 2 * nr_agents) {
		m_neighbour_cache.resize(nr_agents);
		m_neighbour_cache.shrink_to_fit();
		m_neighbour_cache_obst.resize(nr_agents);
		m_neighbour_cache_obst.shrink_to_fit();
	}

	NeighbourFinder::NearestObstaclePointFinder obst_finder(sc, infl_rad);

	float	dens_err = max_dens + 1;
	int		iter = 0;
	for (; iter < max_iter && dens_err > max_dens; iter++)
	{
		nbf.update_agent_query_grid(sc, infl_rad);
		dens_err = 0;

		//float max_press_dbg = 0;

		// rho = dens
		// compute density and pressure

		for (int i = 0; i < nr_agents; i++)
		{
			// this fell the neighbour cache for later on aswell:
			nbf.find_nearby_agents_virtual(sc, sc.position(i), m_neighbour_cache[i]);

			m_neighbour_cache_obst[i] = Vector2f(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity());		
			obst_finder.nearest_obstacle_point(sc.position(i), m_neighbour_cache_obst[i]);

			//auto pos = sc.position(i);
			//Vector2f dir = pos - m_neighbour_cache_obst[i];
			//if (dir.squaredNorm() < qSqrt(0.001f)) qInfo() << "SMALL OBST DIR:" << dir(0) << dir(1);

			m_dens(i) = nbf.compute_dens(sc, sc.position(i), sc.velocity(i), dt, kernel, m_neighbour_cache[i], m_neighbour_cache_obst[i]);
			dens_err = qMax(dens_err, m_dens(i) - max_dens);
			m_press(i) = qMin(compute_pressure(m_dens(i), max_dens, stiff), max_pressure);
			//m_press(i) = compute_pressure(m_dens(i), max_dens, stiff);

			//if (m_press(i) > max_press_dbg) max_press_dbg = m_press(i);
		}

		//qInfo() << iter << max_press_dbg;

		// compute pressure force : F(i) = density(i)^(-1) * grad(pressure(i))

		for (int i = 0; i < nr_agents; i++)
		{
			auto pos = sc.position(i);

			Vector2f force{ Vector2f::Zero() };

			for (auto va : m_neighbour_cache[i].virtual_agents)
			{
				int j = va.agent_id;
				Vector2f dir = pos - va.virtual_location;

				//if (dir.squaredNorm() <= infl_rad_2) {
					// Equalize interaction
				force += ((m_press(i) + m_press(j)) / (m_dens(j) * 2)) * kernel.grad(dir);
				//}
			}
			 
			if (m_neighbour_cache_obst[i](0) != std::numeric_limits<float>::infinity()) 
			{
				Vector2f dir = pos - m_neighbour_cache_obst[i];
				force += m_press(i) / (max_dens) * kernel.grad(dir);
				
				// maybe add a small base force away from the wall
				// TODO: add a maximum if u want this
				//force += -0.01f * dir / (dir.squaredNorm() / (infl_rad * infl_rad));// m_press(i) / (max_dens)* kernel.grad(dir);
			}

			Vector2f dens_f = force / -m_dens(i); // here we make it negative (avoiding it in the loop)
			VEC2_ACCES(m_forces, i) = dens_f;
		}


		// update current velocities and positions using pressure force only

		for (int i = 0; i < nr_agents; i++)
		{
			Vector2f vel = VEC2_ACCES(m_forces, i) * dt;

			//sc.velocity(i) += vel;
			//sc.position(i) += vel * dt;

			VEC2_ACCES(m_displacement, i) += sc.try_move(i, vel * dt);
		}

		mde_timer.start();
		sc.collision_resolver().resolve_collisions(sc, m_displacement, 1); // might make this also effect the displacement (which only effects previous velocity)
		mde_timer.stop();

		//qInfo() << "mean density" << m_dens.mean() << " (goal maximum:) " << max_dens;
		//qInfo() << "mean pressure" << m_press.mean() << " (hard maximum:) " << max_pressure;
	}

	sc.frame_data().var(StatVars::SPH_ITER_COUNT) = iter;
	qInfo() << "SPH ITER:" << iter;

	mde_timer.start();
	sc.collision_resolver().resolve_collisions(sc, m_displacement);
	mde_timer.stop();

	sc.velocities() = (sc.positions() - m_displacement) / dt;

	sc.frame_data().pop_timer(); // SPH
	sc.frame_data().pop_timer(); // MDE
}