#ifndef SPHENGINE_H
#define SPHENGINE_H

#include <QSharedPointer>
#include <QtMath>
#include "vis/openglincl.h"
#include "vis/visdeclr.h"
#include "vis/CircleDrawer2D.h"
#include "tools/shared_eigen_incl.h"
#include "tools/Event.h"
#include "tools/parrallelforpool.h"
#include "tools/InputManager.h"
#include "tools/resizedlistener.h"
#include <tools/include.h>
#include "sim/ContinuumField.h"
#include "sim/interfaces.h"
#include "vis/irenderable.h"

#include <functional>

class SPHEngine
	: public QObject, public IAgentMover
{
public:
	SPHEngine(OpenGLView& f, bool secondary_engine = false);

	virtual void step(SimulationCore&, float dt) override;

	inline Vector2f debug_agent_nearest_obst(int i) {
		return m_neighbour_cache_obst[i];
	}

	inline VecX<float>& forces() {
		return m_forces;
	}

private:

	void refresh_grid(SimulationCore& sc);

	SimulationCore*										m_pSc;

	// per-frame recomputed:
	VecX<float>											m_dens;
	VecX<float>											m_press;
	VecX<float>											m_forces;
	
	VecX<float>											m_pref_vels;
	VecX<float>											m_displacement;

	bool												m_secondary;
	std::vector<NeighbourFinder::FindResultMultiCell>	m_neighbour_cache;
	std::vector<Vector2f>								m_neighbour_cache_obst;
	std::vector<Vector2f>								m_neighbour_cache_obst_out_vec;
};

using SPHEngine_ptr = QSharedPointer<SPHEngine>;

#endif // SPHENGINE_H
