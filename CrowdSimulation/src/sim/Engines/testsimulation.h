#ifndef TESTSIMULATION_H
#define TESTSIMULATION_H

#include <QSharedPointer>
#include "vis/openglincl.h"
#include "vis/visdeclr.h"
#include "tools/shared_eigen_incl.h"
#include "tools/Event.h"
#include "tools/parrallelforpool.h"
#include "tools/InputManager.h"
#include "tools/resizedlistener.h"
#include <tools/include.h>
#include "sim/interfaces.h"

class TestSimulation
	: public QObject, public IAgentMover
{
public:
	TestSimulation();
	~TestSimulation();

	virtual void step(SimulationCore&, float ms) override;
	virtual void reset(SimulationCore&) override;

};

using TestSimulation_ptr = QSharedPointer<TestSimulation>;

#endif // TESTSIMULATION_H