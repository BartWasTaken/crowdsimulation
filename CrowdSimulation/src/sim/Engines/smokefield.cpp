#include "smokefield.h"
#include "sim/simulationcore.h"
#include "sim/kernels.h"

SmokeField::SmokeField(OpenGLView& f)
	: m_drawer(f)
{
	m_diffusion_rate = 50.0f;
	m_smoke_gen_coef = 0.5f;
}

void SmokeField::setup(AABB2 region, Vector2i dim)
{
	m_original_raster = Raster(region, dim);
	m_bordered_raster = m_original_raster.make_raster_plus_1_boundary();
	m_edge_raster = m_bordered_raster.make_centers_raster();
	m_grid.clear();

	m_events.clear();
	m_events.insert(m_all_events.begin(), m_all_events.end());
}

void SmokeField::step(SimulationCore& sc, float dt)
{
	require_grid();

	execute_events(sc.simtime_sec()); // create new fires if there are new events

	// air flow field		: A
	// smoke field			: S
	// fire field			: F
	// smoke diffusivity	: D
	// fire smoke coef		: c

	// 1: update fire field (function based ? not computing grid ?)
	// 2: diverge smoke

	// SG			= gradient(smoke) (difference between neighbours : x2 - x0) | type s[0] : vec2
	// Div(D*SG)	= divergence(SG) (add components per cell of SG, each devided by the size of a cell)

	// Smoke propagation:
	// div(D * grad(S)) - div(A*S) + c*F

	// First term:
	// [1] = make_grad(D*S)	: vec2
	//			this shows the direction and speed in which smoke is moving based on diffusion
	// [2] = make_div([1])	: add derivatives of components
	//			first make the derivative of (D*S), this is the 

	////////////////////////

	// add fire directly to smoke

	evaluate_fire(dt);

	///////////////////////

	// require the correct size, resize doesnt resize if its already correct
	m_diff_gradient_edge.resize(m_edge_raster.cell_count());

	// fix border of requisite data
	set_smoke_border();

	// make the smoke gradient
	compute_gradient(sc, dt);

	// add divergence to smoke change
	FOR_EACH_INNER_CELL_IDX(m_bordered_raster, i, j, idx)
	{
		auto dxl = m_diff_gradient_edge[m_edge_raster.idx(i - 1, j - 1)](0);
		auto dyb = m_diff_gradient_edge[m_edge_raster.idx(i - 1, j - 1)](1);
		auto dxr = m_diff_gradient_edge[m_edge_raster.idx(i, j - 1)](0);
		auto dyt = m_diff_gradient_edge[m_edge_raster.idx(i - 1, j)](1);
		float dx = dxr - dxl;
		float dy = dyt - dyb;
		m_grid[idx].smoke += (dx + dy) / 4.0f;
	}

	// set color
	FOR_EACH_INNER_CELL_IDX(m_bordered_raster, i, j, idx) {
		float fire = m_grid[idx].fire / 5.0f;
		float smoke = m_grid[idx].smoke / 20.0f - fire;
		if (fire > 0.1) {
			float r = clamp(fire, 0.0f, 1.0f);
			float a = clamp(fire, 0.0f, 1.0f);
			m_drawer.set_block_color(i - 1, j - 1, ColorF{ r,0,0,a }.c());
		}
		else {
			float r = clamp(fire + smoke, 0.0f, 1.0f);
			float g = clamp(smoke - fire, 0.0f, 1.0f);
			float b = clamp(smoke - fire, 0.0f, 1.0f);
			float a = clamp(fire + smoke, 0.0f, 1.0f);
			m_drawer.set_block_color(i - 1, j - 1, ColorF{ r,g,b,a }.c());
		}
	}

	m_drawer.update();

	//FOR_EACH_CELL_IDX(m_corner_raster, i, j, idx) {
	//	m_drawer.set_arrow_dir(i, j, m_diffusion_gradient[idx].normalized(), 1);
	//}
}

float SmokeField::get_combined_at(int i, int j)
{
	auto& c = m_grid[m_bordered_raster.idx(i, j)];
	return c.smoke + c.fire;
}

float SmokeField::get_combined_at(Vector2f pos)
{
	BilinearInfo bi;
	bi.make(m_bordered_raster.clamp_inner(pos), m_bordered_raster);

	Q_ASSERT(bi.x_low && bi.x_high && bi.y_low && bi.y_high);

	auto bl = m_grid[m_bordered_raster.idx(bi.x_i, bi.y_i)].smoke;
	auto tl = m_grid[m_bordered_raster.idx(bi.x_i, bi.y_i + 1)].smoke;
	auto br = m_grid[m_bordered_raster.idx(bi.x_i + 1, bi.y_i)].smoke;
	auto tr = m_grid[m_bordered_raster.idx(bi.x_i + 1, bi.y_i + 1)].smoke;

	return (1 - bi.dx) * ((1 - bi.dy) * bl + bi.dy * tl)
		+ bi.dx * ((1 - bi.dy) * br + bi.dy * tr);

	//auto safe_coords = m_bordered_raster.safe_coords(pos);
	//return m_grid[m_bordered_raster.idx(safe_coords)].smoke;
}

void SmokeField::render(RenderContext& rc)
{
	if (m_do_render) {
		rc.f.glEnable(GL_BLEND);
		rc.f.glxBlendFuncBasic();
		m_drawer.render(rc);
		rc.f.glDisable(GL_BLEND);
	}
}

void SmokeField::add_event(FireEvent& fevent)
{
	fevent.event_id = m_event_id_gen++;
	m_events.insert(fevent);
	m_all_events.push_back(fevent);
}

void SmokeField::toggle_render()
{
	m_do_render = !m_do_render;
}

void SmokeField::execute_events(float time_sec)
{
	// retreive the first next event
	std::set<FireEvent, FireEventSorter>::iterator min_entry = m_events.begin();

	while (m_events.begin() != m_events.end())
	{
		min_entry = m_events.begin();
		if (time_sec >= min_entry->time_seconds) {
			execute_event(*min_entry);
			m_events.erase(min_entry);
		}
		else {
			break;
		}
	}

}

void SmokeField::execute_event(const FireEvent& e)
{
	// rasterize a circle from the event location and radius in the grid
	auto cell_bl = m_bordered_raster.safe_coords(e.coordinates - Vector2f{ e.radius, e.radius });
	auto cell_tr = m_bordered_raster.safe_coords(e.coordinates + Vector2f{ e.radius, e.radius });
	for (int iy = cell_bl(1); iy <= cell_tr(1); iy++) {
		for (int ix = cell_bl(0); ix <= cell_tr(0); ix++) {
			float dist2 = (m_bordered_raster.cell_center(ix, iy) - e.coordinates).squaredNorm();
			if (dist2 < e.radius * e.radius) {
				float dist = std::sqrt(dist2);
				float cell_intentity = falloff(dist, e.radius, e.intensity, e.falloff);
				m_grid[m_bordered_raster.idx(ix, iy)].fire += cell_intentity;
			}
		}
	}
}

void SmokeField::require_grid()
{
	if (m_grid.size() < m_bordered_raster.cell_count())
	{
		m_grid.resize(m_bordered_raster.cell_count());
		m_drawer.make_grid(m_original_raster.min(), m_original_raster.range(), m_original_raster.resolution(), true, false, false, 0.05f);
	}
}

void SmokeField::evaluate_fire(float dt)
{
	//Vector2f temp_fire_spot = { 0,0.55f };
	//int origin_idx = m_bordered_raster.safe_idx(temp_fire_spot);
	//m_grid[origin_idx].fire = 1;

	//// push grid to smoke
	float time_coef = m_smoke_gen_coef * dt;
	auto area = m_bordered_raster.cell_size().norm();
	FOR_EACH_INNER_CELL_IDX(m_bordered_raster, i, j, idx) {
		m_grid[idx].smoke += time_coef * m_grid[idx].fire / area;
	}
}

void SmokeField::compute_gradient(SimulationCore& sc, float dt)
{
	/*
		Gradient is defined on cell borders
		But not for the borders of the outer ring of cells (the outer layer of cells is there for ease of computation)
	*/

	float time_diffusion = qMin(dt * m_diffusion_rate, 1.0f);

	// first compute normally
	FOR_EACH_CELL_IDX(m_edge_raster, i, j, idx)
	{
		/* gradient has resolution - 1
		each cell has x : dx, and y : dy respective to the left bottom
		*/

		// left / botom / self
		float s = m_grid[m_bordered_raster.idx(i + 1, j + 1)].smoke;
		float l = m_grid[m_bordered_raster.idx(i, j + 1)].smoke;
		float b = m_grid[m_bordered_raster.idx(i + 1, j)].smoke;
		float dx = (s - l); // no division over distance is needed since we use density based values which are already relative to the area
		float dy = (s - b);
		m_diff_gradient_edge[idx] = time_diffusion * Vector2f{ dx, dy };
	}

	// then for each cell that is an obstacle, set all gradients to zero
	auto* pObst_field = sc.environment().obstacleFields().get_obstacle_field_bool(m_original_raster, false, sc.step_id());

	FOR_EACH_CELL_IDX(m_original_raster, i, j, idx) {
		bool is_obst = (*pObst_field)[idx];
		if (is_obst) {
			m_diff_gradient_edge[m_edge_raster.idx(i, j)] = { 0, 0 };
			m_diff_gradient_edge[m_edge_raster.idx(i + 1, j)](0) = 0;
			m_diff_gradient_edge[m_edge_raster.idx(i, j + 1)](1) = 0;
		}
	}
}

void SmokeField::set_smoke_border()
{
	// we need 0 gradient at the border
	// so the smoke density at borders is mirrored
	// corners dont really matter

	auto& r = m_bordered_raster;

	int max_x = r.resolution(0) - 1;
	int max_y = r.resolution(1) - 1;

	// loop without endpoints, (corners in the bordered grid are not needed)
	for (int i = 1, n = max_x; i < n; i++) {
		m_grid[r.idx(i, 0)].smoke = m_grid[r.idx(i, 1)].smoke;
		m_grid[r.idx(i, max_y)].smoke = m_grid[r.idx(i, max_y - 1)].smoke;
	}
	for (int j = 0, n = max_y; j < n; j++) {
		m_grid[r.idx(0, j)].smoke = m_grid[r.idx(1, j)].smoke;
		m_grid[r.idx(max_x, j)].smoke = m_grid[r.idx(max_x - 1, j)].smoke;
	}
}
