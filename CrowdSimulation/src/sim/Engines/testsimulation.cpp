#include "testsimulation.h"
#include "vis/CircleDrawer2D.h"
#include "vis/GridDrawer2D.h"
#include "sim/simulationcore.h"
#include "vis/openglincl.h"
#include "tools/InputManager.h"
#include "vis/openglmaster.h"
#include "tools/include.h"
#include "mainwindow.h"
#include "sim/Support/neighbourfinder.h"
#include "sim/collisionresolver.h"

#include <QtMath> 
#include <QRunnable>
#include <QThreadPool>
#include <QDebug>
#include <QtGlobal>
#include <QElapsedTimer>
#include <limits>
#include <QMenu>
#include <QAction>

TestSimulation::TestSimulation()
{

}

TestSimulation::~TestSimulation()
{

}

void TestSimulation::step(SimulationCore& sc, float ms)
{
	sc.collision_resolver().neighbourFinder().require_obstacle_register(sc);

	for (int i = 0, n = sc.nr_active_agents(); i < n; i++) {
		sc.try_move(i, {0.1 * ms, 0});
	}
}

void TestSimulation::reset(SimulationCore&)
{

}
