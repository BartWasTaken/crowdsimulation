#include "continuumengine.h"
#include "sim/simulationcore.h"
#include "sim/IEnvironment.h"
#include "tools/include.h"
#include "sim/Support/neighbourfinder.h"

ContinuumEngine::ContinuumEngine(OpenGLView& f)
	: m_continuum_field(f) //, m_nbfinder(f)
{

}

void ContinuumEngine::set_pref_velocity(SimulationCore& sc, VecX<float>& pref_velocities, float dt)
{
	sc.frame_data().push_timer("Continuum (ms)", true);

	m_continuum_field.update(sc, dt);

	int nr_agents = sc.nr_active_agents();

	for (int i = 0, n = sc.nr_active_agents(); i < n; i++)
	{
		VEC2_ACCES(pref_velocities, i) = m_continuum_field.retreive_velocity(sc.position(i), sc.goal_picker().goal(i));
		Q_ASSERT(is_valid_vec2(VEC2_ACCES(pref_velocities, i)));
	}

	m_continuum_field.step_end(sc);

	sc.frame_data().pop_timer();
}


void ContinuumEngine::render_gp(RenderContext rc)
{
	m_continuum_field.render(rc);
}

bool ContinuumEngine::settings_changed(SimulationCore& sc, SimulationSettings_ptr old_settings)
{
	reset(sc);
	return true;
}

void ContinuumEngine::make_ui(SimulationCore& sc)
{
	m_continuum_field.make_ui(sc);
}

void ContinuumEngine::MinPotGoalPicker::choose_goals(SimulationCore& sc)
{
	m_to_update = true;
	m_pSC = &sc;
	m_pSC->require_vector_size(m_goals, 1);
}

void ContinuumEngine::reset(SimulationCore & sc)
{
	m_continuum_field.setup(sc, sc.environment().bbox(), sc.environment().goals());
}

QString ContinuumEngine::MinPotGoalPicker::goal_picker_name()
{
	if (m_type == PickerType::MIN) {
		return "Continuum engine | MIN";
	}
	else if (m_type == PickerType::PROBABILISTIC) {
		return "Continuum engine | Probabilistic";
	}
	else return "Continuum engine | ?";
}

bool ContinuumEngine::MinPotGoalPicker::object_changed(ContinuumField* pObj)
{
	if (m_ref_timer.elapsed_sec() > m_refresh_timer_sec) {
		m_to_update = true;
	}

	if (m_to_update && m_pSC)
	{
		m_to_update = false;

		int nr_goals = pObj->goal_count();
		int nr_agents = m_pSC->nr_active_agents();

		if (nr_goals < 2)
		{
			m_pSC->make_zero_vector(m_goals, 1);
		}
		else {

			m_pSC->require_vector_size(m_goals, 1);

			if (m_type == PickerType::MIN) {
				for (int i = 0; i < nr_agents; i++)
				{
					auto posi = m_pSC->position(i);

					int min_g = 0;
					float min_pot = m_continuum_field.retreive_potential(posi, 0);

					for (int g = 1, n = nr_goals; g < nr_goals; g++) {
						float next_pot = m_continuum_field.retreive_potential(posi, g);
						if (next_pot < min_pot) {
							min_pot = next_pot;
							min_g = g;
						}
					}

					m_goals[i] = min_g;
				}
			}
			if (m_type == PickerType::PROBABILISTIC)
			{
				if (m_alterations.size() != nr_goals * nr_agents)
				{
					m_alterations.resize(nr_goals * nr_agents);

					for (int i = 0; i < nr_agents; i++) {
						for (int j = 0; j < nr_goals; j++)
						{
							int idx = i * nr_goals + j;
							float random_value = m_dis(m_rgen);
							if (random_value < 0) m_alterations[idx] = 1 / (1 - random_value);
							else m_alterations[idx] = 1 + random_value;
						}
					}
				}
				for (int i = 0; i < nr_agents; i++)
				{
					auto posi = m_pSC->position(i);

					int min_g = 0;
					float min_pot = m_continuum_field.retreive_potential(posi, 0) * m_alterations[i * nr_goals + 0];

					for (int g = 1, n = nr_goals; g < nr_goals; g++) 
					{
						float next_pot = m_continuum_field.retreive_potential(posi, g) * m_alterations[i * nr_goals + g];
						if (next_pot < min_pot) {
							min_pot = next_pot;
							min_g = g;
						}
					}

					m_goals[i] = min_g;
				}

				/*std::vector<float>					values(nr_goals);
				float								value_total;

				for (int i = 0; i < nr_agents; i++)
				{
					value_total = 0;

					auto posi = m_pSC->position(i);

					// fill array
					for (int g = 0, n = nr_goals; g < nr_goals; g++) {
						float next_value = 1 / (0.1f + m_continuum_field.retreive_potential(posi, g));
						values[g] = next_value;
						value_total += next_value;
					}

					// propagate values
					for (int v = 1; v < nr_goals; v++) {
						values[v] += values[v - 1];
					}

					// random choice
					float target_value = value_total * m_dis(m_rgen);

					// find corresponding target to random choice
					int gi_low = 0;
					int gi_high = nr_goals - 1;

					while (gi_low != gi_high) {
						int mid = (gi_low + gi_high) / 2; // no way this overflows
						if (target_value < values[mid]) {
							gi_high = mid;
						}
						else {
							gi_low = mid;
							if (gi_low + 1 == gi_high) {
								break;
							}
						}
					}

					m_goals[i] = gi_high;
				}*/
			}
		}

		m_pSC->colorize_agents();

		m_ref_timer.restart();
	}


	return true;
}

ContinuumEngine::MinPotGoalPicker::MinPotGoalPicker(ContinuumField& cf, PickerType t)
	: IGoalPicker(m_goals), m_continuum_field(cf)
{
	m_refresh_timer_sec = 2;
	m_type = t;
	cf.add_listener(this);
}
