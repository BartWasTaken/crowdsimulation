#ifndef CONTINUUMENGINE_H
#define CONTINUUMENGINE_H

#include "sim/ContinuumField.h"
#include "sphengine.h"
#include "sim/interfaces.h"
#include <QObject>
#include <random>

class ContinuumEngine
	: public QObject, public virtual IGlobalPanner
{
	Q_OBJECT
public:
	ContinuumEngine(OpenGLView&);

	class MinPotGoalPicker 
		: public IGoalPicker, public ChangedListener<ContinuumField>
	{
	public:
		enum class PickerType { MIN, PROBABILISTIC };

		MinPotGoalPicker(ContinuumField&, PickerType);
		virtual void choose_goals(SimulationCore& sc) override;
		virtual QString goal_picker_name() override;

		virtual bool object_changed(ContinuumField* pObj) override;

	private:
		SimulationCore*		m_pSC = nullptr;
		ContinuumField&		m_continuum_field;
		VecX<int>			m_goals;
		bool				m_to_update = false;
		PickerType			m_type;

		std::mt19937						m_rgen;
		std::normal_distribution<float>		m_dis { 0.0f, 0.1f }; // mean and variance

		Timer				m_ref_timer;
		float				m_refresh_timer_sec;

		std::vector<float>	m_alterations;
	};
	using MinPotGoalPicker_ptr = QSharedPointer<MinPotGoalPicker>;

	virtual void set_pref_velocity(SimulationCore& sc, VecX<float>& pref_velocities, float dt) override;

	virtual void render_gp(RenderContext) override;

	virtual bool settings_changed(SimulationCore&, SimulationSettings_ptr old_settings) override;

	virtual void make_ui(SimulationCore& sc) override;

	virtual void reset(SimulationCore & sc) override;

	inline ContinuumField& get_continuum_field() { return m_continuum_field; }

private:
	ContinuumField	m_continuum_field;

};

using ContinuumEngine_ptr = QSharedPointer<ContinuumEngine>;


#endif // CONTINUUMENGINE_H