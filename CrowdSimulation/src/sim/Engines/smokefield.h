#pragma once

#include "tools/include.h"
#include "sim/simdeclr.h"
#include "vis/openglincl.h"
#include "vis/GridDrawer2D.h"
#include <vector>
#include <set>

class SmokeField
{
public:
	struct FireEvent {
		Vector2f	coordinates;
		float		intensity;
		float		radius;
		float		falloff;
		float		time_seconds;
	protected:
		friend class SmokeField;
		friend class FireEventSorter;
		unsigned int	event_id;
	};

	SmokeField(OpenGLView& f);

	void setup(AABB2 region, Vector2i dim);
	void step(SimulationCore& sc, float dt);

	float get_combined_at(int i, int j);
	float get_combined_at(Vector2f pos);

	void render(RenderContext& f);
	void add_event(FireEvent&);

	void toggle_render();

private:
	struct Cell {
		float		smoke = 0;
		float		fire = 0;
		Vector2f	flow = { 0,0 };
	};

	
	struct FireEventSorter {
		bool operator()(const FireEvent& a, const FireEvent& b) const
		{
			float pa = a.time_seconds;
			float pb = b.time_seconds;
			if (pa != pb) return pa < pb;
			else return a.event_id < b.event_id;
		}
	};

	void execute_events(float time_sec);
	void execute_event(const FireEvent&);

	void require_grid();
	void diffuse(std::vector<float>& in, std::vector<float>& out);

	void evaluate_fire(float dt);
	void compute_gradient(SimulationCore& sc, float dt);


	void set_smoke_border();
	void set_grad_border();

	GridDrawer2D			m_drawer;
	bool					m_do_render = true;

	Raster					m_original_raster;
	Raster					m_bordered_raster;
	Raster					m_edge_raster;

	std::vector<Cell>		m_grid;
	std::vector<Vector2f>	m_diff_gradient_edge;
	float					m_diffusion_rate;
	float					m_smoke_gen_coef;

	std::set<FireEvent, FireEventSorter> m_events;
	std::vector<FireEvent>  m_all_events;
	unsigned int			m_event_id_gen = 0;
};
