#ifndef KERNELS_H
#define KERNELS_H

#include "tools/include.h"

class Poly6
{
public:

	Poly6(float h);

	float		get(Vector2f r);
	Vector2f	grad(Vector2f r);

private:
	float c_grad;
	float c_base;
	float h2;
};

Vector2f gradSpiky(const Vector2f &r, float h);

inline float stiffness(float stiff_mp, float dt_sq) {
	return stiff_mp / dt_sq;
}

inline float compute_pressure(float dens, float dens_max, float stiff) {
	//return stiff * qMax(0.0f, ipow(dens / dens_max, 2) - 1.0f);
	return stiff * qMax(0.0f, dens - dens_max);
}

inline float max_density(float min_dist, float alpha) {
	return 2.0f * alpha / (c_sqrt3f * min_dist * min_dist);
}

inline float falloff_inrange(float x, float range, float limit, float curved)
{
	// x in [0, range], output [0, limit], curved in [0,1]

	// example: 
	// range 3, limit 10, curved 0.9
	// 0.1 * 10 * (3 - x)) / (0.1 * (3 - x) + x

	float d = (1.0f - 0.9999999f * curved);
	return (d * limit * (range - x)) / (d * (range - x) + x);
}

inline float falloff(float x, float range, float max, float steepness) {
	if (x > range) return 0;
	return falloff_inrange(x, range, max, steepness);
}


#endif // KERNELS_H


