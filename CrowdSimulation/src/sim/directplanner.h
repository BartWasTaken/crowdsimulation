#ifndef DIRECTPLANNER_H
#define DIRECTPLANNER_H

#include "interfaces.h"
#include "simdeclr.h"

class DirectPlanner
	: public virtual IGlobalPanner, public virtual IGoalPicker
{
public:

	DirectPlanner();

	virtual void set_pref_velocity(SimulationCore& sc, VecX<float>& pref_velocities, float dt) override;

	virtual void choose_goals(SimulationCore& sc) override;

	virtual QString goal_picker_name() override;

	virtual bool settings_changed(SimulationCore&, SimulationSettings_ptr old_settings) override;

private:

	VecX<int>		m_goals;
};

using DirectPlanner_ptr = QSharedPointer<DirectPlanner>;


class GoalPickerDynamic
	: public virtual IGoalPicker
{
public:
	GoalPickerDynamic(QString name, std::function<void(SimulationCore&, VecX<int>&)> goal_pick_func);

	virtual void choose_goals(SimulationCore& sc) override;

	virtual QString goal_picker_name() override;

	virtual bool settings_changed(SimulationCore&, SimulationSettings_ptr old_settings) override;

private:
	VecX<int>											m_goals;
	std::function<void(SimulationCore&, VecX<int>&)>	m_func;
	QString												m_name;
};

using GoalPickerDynamic_ptr = QSharedPointer<GoalPickerDynamic>;



#endif // DIRECTPLANNER_H