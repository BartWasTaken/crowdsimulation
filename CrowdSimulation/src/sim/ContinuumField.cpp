#include "ContinuumField.h"
#include "simulationcore.h"
#include "mainwindow.h"
#include "kernels.h"
#include "tools/include.h"
#include "tools/macros.h"
#include "obstacle.h"
#include "collisionresolver.h"

#include <unordered_set>
#include <assert.h>

#include <QtMath>
#include <QDebug>
#include <QMenu>

#define LINEAR_EVAL // LINEAR_EVAL | NEAREST_EVAL

// Since we have too loop so much, 2 macros to make it easier:


ContinuumField::ContinuumField(OpenGLView& f)
{
	m_draw_grid = false;
	m_draw_arrow = false;
	m_draw_dir = false;
	m_pGridDrawer = new GridDrawer2D(f);

	m_enable_debug_vars = true;
	m_debug_step_varname = "Continuum field step";
	m_visualize_group_id_varname = "Visualize group id";
	m_visualize_group_id = 0;

	m_project_velocity_cell_dist = 2;
	m_very_large = 1e8f;
}

void ContinuumField::render(RenderContext rc)
{
	if (m_draw_grid || m_draw_dir || m_draw_arrow) {
		rc.f.glEnable(GL_BLEND);
		rc.f.glxBlendFuncBasic();
		m_pGridDrawer->render(rc, m_draw_grid, m_draw_dir, m_draw_arrow);
		rc.f.glDisable(GL_BLEND);

		/*for (int y = 0; y < m_raster.resolution(1); y++) {
			float py = (float)(y + 0.5f) / m_dim(1);
			for (int x = 0; x < m_dim(0); x++) {
				float px = (float)(x + 0.5f) / m_dim(0);

				rc.f.render_text(-1 + 2 * px, -1 + 2 * py, { 255,255,255,255 }, "XXXXX");
			}
		}*/
	}
}

void ContinuumField::setup(SimulationCore& sc, AABB2 region, std::vector<IGoal_ptr> goals)
{
	if (m_enable_debug_vars) {
		if (!sc.settings().exists_var(m_debug_step_varname)) {
			sc.settings().add_variable(m_debug_step_varname, 0);
		}
		if (!sc.settings().exists_var(m_visualize_group_id_varname)) {
			sc.settings().add_variable(m_visualize_group_id_varname, m_visualize_group_id);
		}
	}

	m_par_for_pool.reset();

	auto dim = SimulationCore::compute_grid_dimensions(sc.settings().agent_rad(), region);

	m_raster = Raster(region, dim);

	m_groups.clear();
	m_groups.resize(goals.size());

	// for each goal, create a goal with a group of target cells
	for (int i = (int) goals.size() - 1; i >= 0; i--)
	{
		// get the cells
		auto g = goals[i]->get_indices(region, dim);

		// make a list of the coordinates
		std::vector<Vector2i>& goal_coordinates = m_groups[i].goal;
		goal_coordinates.reserve(g.size());
		for (auto id : g) goal_coordinates.push_back(id.second);

		m_groups[i].field.resize(m_raster.cell_count());
		// m_groups[i].init(goal_coordinates, m_raster.cell_count());
	}

	m_grid.clear();
	m_grid.resize(m_raster.cell_count());

	if (m_pGridDrawer) m_pGridDrawer->make_grid(region.min(), region.sizes(), dim, true, true, true, 0.005f);

	// TODO: set the portal_cell_neighbour 
}

void ContinuumField::handle_smoke(SimulationCore& sc)
{
	auto& smoke = sc.environment().smoke();
	float smoke_discomfort_coef = sc.settings().cnt_weight_discomfort_smoke();
	FOR_EACH_CELL_IDX(m_raster, i, j, idx) {
		discomfort_at(idx) += smoke_discomfort_coef * smoke.get_combined_at(m_raster.cell_center(i, j));
	}
}

void ContinuumField::update(SimulationCore& sc, float dt)
{
	m_dist_weight = sc.settings().cnt_weight_dist();
	m_time_weight = sc.settings().cnt_weight_time();
	m_agent_discomfort_weight = sc.settings().cnt_weight_discomfort_agent();
	m_obst_discomfort_weight = sc.settings().cnt_weight_discomfort_obst();

	if (!m_step_end_was_called)
	{
		qWarning() << "Not calling step_end on Continuum field will disable visualization";
	}
	else m_step_end_was_called = false;

	update_reachability(sc);

	// accumulate variables:
	int debug_step_count = 0;
	if (m_enable_debug_vars) {
		debug_step_count = sc.settings().retreive_int(m_debug_step_varname);
	}
	auto do_debug_step = (debug_step_count > 0 ? true : false);

	bool keep_potential = false;
	auto nr_agents = sc.nr_active_agents();
	float pref_speed = sc.settings().agent_speed();

	reset_fields(keep_potential);

	// start building the fields shared by the groups
	if (true) {
		make_dens_vel_fields(nr_agents, sc.positions(), sc.velocities());
		make_flow_field(pref_speed);
		project_agent_discomfort(nr_agents, sc.settings().cnt_discomfort_project_dt(), sc.positions(), sc.velocities());
		project_obstacle_discomfort(sc);
		handle_smoke(sc);
		make_cost_field();
	}
	else {
		cost_field_test_1(nr_agents, sc.positions(), sc.velocities(), pref_speed);
	}

	if (!m_par_for_pool.is_setup()) {
		m_par_for_pool.setup(m_groups.size(), [this, &sc](int thread_id, TaskData td)
		{
			//auto dt = td.dt;
			auto g_i = td.g_i;
			auto& group = m_groups[g_i];
			auto& goal = sc.environment().goals()[g_i];
			Vector2f pref_vel = goal->goal_inner_direction() * 	sc.settings().agent_speed();

			// write goal cells potential 0 (and set to known)
			Q_ASSERT(group.goal.size() > 0);

			for (int gc_i = 0, n = (int) group.goal.size(); gc_i < n; gc_i++)
			{
				auto goal_idx = m_raster.idx(group.goal[gc_i]);
				if (!m_movegrid[goal_idx].inside_obj) {
					group.field[goal_idx].set_goal(pref_vel);
				}
			}

			// do marching method:
			std::map<int, Vector2i, Compare_potential> candidates{ Compare_potential(group.field) }; // considered cells
			{
				// evaluate at neighbours of goal cells and set them considered
				for (int gc_i = 0, n = (int) group.goal.size(); gc_i < n; gc_i++)
				{
					// one of the cells of this goal:
					Vector2i gc_loc = group.goal[gc_i];

					// for all its neighbours
					for (int d = 0; d < Dir4::DIR_COUNT; d++)
					{
						// neighbour location:
						int nx = gc_loc(0) + Dir4::dir_x[d];
						int ny = gc_loc(1) + Dir4::dir_y[d];

						// check neighbour location is inside the grid
						if (valid_x(nx) && valid_y(ny))
						{
							// neighbouring cell index:
							int nidx = m_raster.idx(nx, ny);
							// neighbouring cell itself:
							GCell& ncell = group.field[nidx];
							// if not a goal cell by itself, set it as candidate:
							// (We dont have to consider the case that it is already a candidate,
							// since we would not be able to lower the potential 
							// as it would already border a goal cell directly (this is not hte marching loop!))
							if (ncell.state == GCell::UNKNOWN) {
								ncell.state = GCell::CANDIDATE;
								ncell.potential = evaluate_potential(g_i, nx, ny);
								candidates[nidx] = { nx,ny };
							}
						}
					}
				}

				int iter = 0;
				while (candidates.size() > 0)
				{
					if (thread_id == 0)
					{
						if (td.do_debug_step && (iter % td.debug_step_count) == 0) 
						{
							visualize_update(g_i);
							if (sc.has_stable_record()) {
								sc.opengl().opengl_master().snap_screenshot();
								sc.opengl().opengl_master().wait_till_screenshot_done();
							}
							else sc.debug_wait_step();
							qDebug() << "Iteration: " << iter;
						}
					}
					Q_ASSERT(iter <= m_raster.cell_count());

					auto min_entry = candidates.begin();
					auto min_idx = min_entry->first;
					auto min_loc = min_entry->second;
					candidates.erase(min_entry);
					group.field[min_idx].state = GCell::KNOWN;

					for (int d = 0; d < Dir4::DIR_COUNT; d++)
					{
						// neighbour location:
						int nx = min_loc(0) + Dir4::dir_x[d];
						int ny = min_loc(1) + Dir4::dir_y[d];
						int nidx = m_raster.idx(nx, ny);
						// check if cell is on grid:
						if (m_raster.is_coord_valid(nx, ny)) {
							consider_next_candidate(group, g_i, nidx, nx, ny, candidates, group.field[min_idx].unreachable);
						}
					}

					// + portal direciton
					if (m_movegrid[min_idx].can_portal_move())
					{
						int pdir = m_movegrid[min_idx].portal_move_dir();
						int pidx = m_movegrid[min_idx].portal_move_dest_idx();
						auto pcoords = m_raster.coords(pidx);
						GCell& pcell = group.field[pidx];

						//int g, int i, int j, int portal_dir, int portal_idx)

						if (pcell.state == GCell::UNKNOWN) {
							pcell.state = GCell::CANDIDATE;
							pcell.potential = evaluate_potential_trough_portal(g_i, pcoords[0], pcoords[1], Dir4::opposed(pdir), min_idx);
							candidates[pidx] = { pcoords(0), pcoords(1) };
						}
						else if (pcell.state == GCell::CANDIDATE) {
							float new_pot = evaluate_potential_trough_portal(g_i, pcoords[0], pcoords[1], Dir4::opposed(pdir), min_idx);
							if (new_pot < pcell.potential) {
								auto nodeHandler = candidates.extract(pidx);
								// nodeHandler.key() = ...; key doesnt actually change, but its evaluation does
								pcell.potential = new_pot;
								candidates.insert(std::move(nodeHandler));
							}
						}
					}

					//if (!is_valid_float(g.field[min_idx].potential)) {
					//	DebugBreak();
					//}

					iter++;
				}

				// 
				for (int i = 0, n = m_raster.cell_count(); i < n; i++) {
					if (group.field[i].state != GCell::KNOWN) {
						//DebugBreak();
					}
				}
			}

			// write potential gradients
			compute_grad_vel(sc, g_i);
		});
	}

	for (int g_i = (int) m_groups.size() - 1; g_i >= 0; g_i--) {
		m_par_for_pool.set_task(g_i, {
			g_i, dt, do_debug_step, debug_step_count
			});
	}

	m_par_for_pool.run_loop();

	shout_change(this);
}

void ContinuumField::make_ui(SimulationCore& sc)
{
	QMenu& menu = sc.get_vismenu();
	{
		auto act = new QAction("Show continuum grid", &sc.get_window());
		act->setCheckable(true);
		act->setChecked(m_draw_grid);
		menu.addAction(act);
		QObject::connect(act, &QAction::triggered, this, &ContinuumField::act_vis_grid);
	}
	{
		auto act = new QAction("Show continuum dir", &sc.get_window());
		act->setCheckable(true);
		act->setChecked(m_draw_dir);
		menu.addAction(act);
		QObject::connect(act, &QAction::triggered, this, &ContinuumField::act_vis_dir);
	}
	{
		auto act = new QAction("Show continuum arrow", &sc.get_window());
		act->setCheckable(true);
		act->setChecked(m_draw_arrow);
		menu.addAction(act);
		QObject::connect(act, &QAction::triggered, this, &ContinuumField::act_vis_arrow);
	}
}

void ContinuumField::act_vis_grid()
{
	m_draw_grid = !m_draw_grid;
}

void ContinuumField::act_vis_arrow()
{
	m_draw_arrow = !m_draw_arrow;
}

void ContinuumField::act_vis_dir()
{
	m_draw_dir = !m_draw_dir;
}

Vector2f ContinuumField::retreive_velocity(Vector2f pos, int group)
{
	auto& field = m_groups[group].field;
#if defined NEAREST_EVAL
	return field[idx(pos)].velocity;
#elif defined LINEAR_EVAL
	auto posn = m_raster.to_unit_space(pos);

	BilinearInfo bi;
	bi.make(posn, m_raster.resolution());

	Vector2f bl{ 0,0 }, br{ 0,0 }, tl{ 0,0 }, tr{ 0,0 };

	if (bi.x_low)
	{
		if (bi.y_low) {
			bl = field[m_raster.idx(bi.x_i, bi.y_i)].pref_vel;
		}
		else bi.dy = 1;
		if (bi.y_high) tl = field[m_raster.idx(bi.x_i, bi.y_i + 1)].pref_vel;
		else bi.dy = 0;
	}
	else bi.dx = 1;

	if (bi.x_high)
	{
		if (bi.y_low) br = field[m_raster.idx(bi.x_i + 1, bi.y_i)].pref_vel;
		else bi.dy = 1;
		if (bi.y_high) tr = field[m_raster.idx(bi.x_i + 1, bi.y_i + 1)].pref_vel;
		else bi.dy = 0;
	}
	else bi.dx = 0;

	// bilinear interpolate:
	Vector2f total = (1 - bi.dx) * ((1 - bi.dy) * bl + bi.dy * tl) + bi.dx * ((1 - bi.dy) * br + bi.dy * tr);
	return total;
#endif

	// TODO:
	// if inside an obstacle
	// make an exepction
	// then when retreiving preferred velocity
	// use only the ones that ARE working

	// if a portal is nearby, use it if we are on the right side
	// if a portal is nearby, dont use the cell behind it!
	// best: dont use ANY cell behind an obstacle!
	// requires: preset sets of cells for each unique region
}

Vector2f ContinuumField::retreive_avg_velocity(Vector2f pos)
{
	auto& field = m_grid;
#if defined NEAREST_EVAL
	return field[idx(pos)].velocity;
#elif defined LINEAR_EVAL
	auto posn = m_raster.to_unit_space(pos);

	BilinearInfo bi;
	bi.make(posn, m_raster.resolution());

	Vector2f bl{ 0,0 }, br{ 0,0 }, tl{ 0,0 }, tr{ 0,0 };

	if (bi.x_low)
	{
		if (bi.y_low) {
			bl = field[m_raster.idx(bi.x_i, bi.y_i)].velocity;
		}
		else bi.dy = 1;
		if (bi.y_high) tl = field[m_raster.idx(bi.x_i, bi.y_i + 1)].velocity;
		else bi.dy = 0;
	}
	else bi.dx = 1;

	if (bi.x_high)
	{
		if (bi.y_low) br = field[m_raster.idx(bi.x_i + 1, bi.y_i)].velocity;
		else bi.dy = 1;
		if (bi.y_high) tr = field[m_raster.idx(bi.x_i + 1, bi.y_i + 1)].velocity;
		else bi.dy = 0;
	}
	else bi.dx = 0;

	// bilinear interpolate:
	Vector2f total = (1 - bi.dx) * ((1 - bi.dy) * bl + bi.dy * tl) + bi.dx * ((1 - bi.dy) * br + bi.dy * tr);
	return total;
#endif
}

float ContinuumField::retreive_potential(Vector2f pos, int group)
{
	auto& field = m_groups[group].field;
#if defined NEAREST_EVAL
	return field[idx(pos)].velocity;
#elif defined LINEAR_EVAL
	auto posn = m_raster.to_unit_space(pos);

	BilinearInfo bi;
	bi.make(posn, m_raster.resolution());

	float bl{ 0 }, br{ 0 }, tl{ 0 }, tr{ 0 };

	if (bi.x_low)
	{
		if (bi.y_low) {
			bl = field[m_raster.idx(bi.x_i, bi.y_i)].potential;
		}
		else bi.dy = 1;
		if (bi.y_high) tl = field[m_raster.idx(bi.x_i, bi.y_i + 1)].potential;
		else bi.dy = 0;
	}
	else bi.dx = 1;

	if (bi.x_high)
	{
		if (bi.y_low) br = field[m_raster.idx(bi.x_i + 1, bi.y_i)].potential;
		else bi.dy = 1;
		if (bi.y_high) tr = field[m_raster.idx(bi.x_i + 1, bi.y_i + 1)].potential;
		else bi.dy = 0;
	}
	else bi.dx = 0;

	// bilinear interpolate:
	float total = (1 - bi.dx) * ((1 - bi.dy) * bl + bi.dy * tl) + bi.dx * ((1 - bi.dy) * br + bi.dy * tr);
	return total;
#endif
}

int ContinuumField::goal_count()
{
	return (int) m_groups.size();
}

void ContinuumField::update_reachability(SimulationCore& sc)
{
	m_movegrid = sc.environment().obstacleFields().get_movable_field(sc, m_raster);
}

void ContinuumField::reset_fields(bool keep_potential)
{
	for (int y = 0; y < m_raster.resolution(1); y++) {
		for (int x = 0; x < m_raster.resolution(0); x++) {
			int id = m_raster.idx(x, y);
			m_grid[id].reset(true);
			for (auto& group_grid : m_groups) {
				group_grid.field[id].reset(keep_potential);
			}
		}
	}
}

void ContinuumField::make_dens_vel_fields(int agent_count, VecX<float>& positions, VecX<float>& velocities)
{
	for (int i = 0; i < agent_count; i++) {
		auto pos = VEC2_ACCES(positions, i);
		auto vel = VEC2_ACCES(velocities, i);
		// bring position in [0:1] domain
		write_agent_to_field(m_raster.to_unit_space(pos), vel);
	}

	for (int y = 0; y < m_raster.resolution(1); y++) {
		for (int x = 0; x < m_raster.resolution(0); x++) {
			// device by sum of weights
			avg_velocity_at(x, y) /= Geometry::Epsilon + density_at(x, y);
		}
	}
}

void ContinuumField::make_flow_field(float max_velocity)
{
	// TODO flow in portal directdion

	// make speed field : this density is from the bilinear interpolation
	float speed_dens_min = 0.1f;
	float speed_dens_max = 0.4f;
	float max_speed = max_velocity;
	float offsetX = m_project_velocity_cell_dist / m_raster.resolution(0);
	float offsetY = m_project_velocity_cell_dist / m_raster.resolution(1);
	for (int y = 0; y < m_raster.resolution(1); y++)
	{
		float py = (float)(y + 0.5f) / m_raster.resolution(1);
		for (int x = 0; x < m_raster.resolution(0); x++)
		{
			float px = (float)(x + 0.5f) / m_raster.resolution(0);
			Vector2f pos_01 = { px, py };

			int id = m_raster.idx(x, y);

			flow_at(id, Dir4::N) = eval_flow_N(pos_01, offsetY, max_speed, speed_dens_min, speed_dens_max);
			flow_at(id, Dir4::E) = eval_flow_E(pos_01, offsetX, max_speed, speed_dens_min, speed_dens_max);
			flow_at(id, Dir4::S) = eval_flow_S(pos_01, offsetY, max_speed, speed_dens_min, speed_dens_max);
			flow_at(id, Dir4::W) = eval_flow_W(pos_01, offsetX, max_speed, speed_dens_min, speed_dens_max);
		}
	}
}

void ContinuumField::project_agent_discomfort(int agent_count, float dt, VecX<float>& positions, VecX<float>& velocities)
{
	// velicity multiplier
	Vector2f vel_mp = dt * m_raster.range().cwiseInverse();

	for (int i = 0; i < agent_count; i++)
	{
		// normalized future position:
		auto posn = m_raster.to_unit_space(VEC2_ACCES(positions, i));
		auto veln = vel_mp.cwiseProduct(VEC2_ACCES(velocities, i));
		write_agent_discomfort(posn + veln, m_agent_discomfort_weight);
		// ...
		// discomfort_at(idx_01(posn + veln));
	}
}

float smooth(float x) {
	return (x - 1) * (x - 1) * (2 * x + 1);
}

void ContinuumField::project_obstacle_discomfort(SimulationCore& sc)
{
	float infl_rad = sc.settings().agent_rad() * 6.0f;
	Vector2f dimf = sc.environment().bbox().sizes() / (infl_rad * 2);
	dimf(0) = qMax(1.0f, dimf(0));
	dimf(1) = qMax(1.0f, dimf(1));
	Vector2i dim = dimf.cast<int>();

	Raster obstacle_raster = { sc.environment().bbox(), dim };

	bool is_changed;
	auto& obstacle_register_field = *sc.environment().obstacleFields().get_obstacle_field_list(
		obstacle_raster, true, sc.step_id(), &is_changed);

	if (is_changed)
	{
		m_obstacle_discomfort_cache.clear();
		m_obstacle_discomfort_cache.resize(m_raster.cell_count(), 0);

		// in each cell of the continuum field
		// add a discomfort based on the obstacles within range

		std::unordered_set<int> obstacle_set;

		auto& obstacle_list = sc.environment().obstacles();

		FOR_EACH_CELL_IDX(m_raster, i, j, idx)
		{
			auto cell_center = m_raster.cell_center(i, j);

			SurroundingCells info;
			info.make(cell_center, obstacle_raster);
			float cost = 0;

			for (int obst_j = info.y1; obst_j <= info.y2; obst_j++)
			{
				for (int obst_i = info.x1; obst_i <= info.x2; obst_i++)
				{
					auto id_list = obstacle_register_field[obstacle_raster.idx(obst_i, obst_j)];

					for (int list_idx = 0, n = (int) id_list.size(); list_idx < n; list_idx++)
					{
						int obstacle_id = id_list[list_idx];

						float dist = obstacle_list[obstacle_id].agent_collider().distanceTo(cell_center);

						if (dist < infl_rad)
						{
							float temp = smooth(dist / infl_rad);
							if (temp > cost) cost = temp;
						}
					}
				}
			}

			m_obstacle_discomfort_cache[idx] = cost;
		}
	}

	FOR_EACH_CELL_IDX(m_raster, i, j, idx)
	{
		discomfort_at(idx) += m_obstacle_discomfort_cache[idx] * m_obst_discomfort_weight;
	}
}

void ContinuumField::make_cost_field()
{
	FOR_EACH_CELL_IDX(m_raster, i, j, idx) {
		for (int d = 0; d < Dir4::DIR_COUNT; d++) {
			compute_cost(i, j, (Dir4::DIR) d);
			auto& move_cell = m_movegrid[idx];
			if (move_cell.can_portal_move()) compute_portal_cost(idx, move_cell.portal_move_dest_idx(), move_cell.portal_move_dir());
		}
	}
}

template<typename FVAL, typename FMOD>
float avg(Raster r, FVAL f, FMOD modifier, float cutoff) {
	float t = 0;
	int c = 0;
	FOR_EACH_CELL(r, i, j) {
		float e = f(i, j);
		if (e < cutoff) {
			t += modifier(e);
			c++;
		}
	}
	return t / c;
}

float ContinuumField::max_potential(int g)
{
	float m = 0;
	FOR_EACH_CELL(m_raster, i, j) {
		float e = LINEAR_EVAL(g, i, j);
		if (e != std::numeric_limits<float>::infinity()) {
			if (e > m) m = e;
		}
	}
	return m;
}

void ContinuumField::compute_grad_vel(SimulationCore& sc, int group_id)
{
	thread_local static std::vector<Vector2i> edge_cache;
	edge_cache.clear();
	Q_ASSERT(edge_cache.size() == 0);

	FOR_EACH_CELL(m_raster, x, y) {
		write_grad_vel(group_id, x, y, edge_cache); // this fills the edge cache
	}

	auto& nf = sc.collision_resolver().neighbourFinder();
	auto& obst = sc.environment().obstacles();
	auto& g = m_groups[group_id];

	NeighbourFinder::FindResult4Cell fr4c;

	for (int i = 0, n = (int) edge_cache.size(); i < n; i++)
	{
		Q_ASSERT(i < edge_cache.size());
		auto coord = edge_cache[i];
		auto center = m_raster.cell_center(coord);
		Vector2f smooth = { 0, 0 };

		/*
		// smooth it out if obstructed
		nf.find_nearby_obstacles_single_floor(center, fr4c);

		bool in_obstacle = false;

		Q_ASSERT(fr4c.validate((int)obst.size()));

		
		LOOP_4CELL(fr4c, j) {
			Vector2f smooth = { 0, 0 };
			if (!obst[j].is_staircase() && obst[j].agent_collider().contains(center)) {
				in_obstacle = true;
				goto END_FIND_IF_IN_OBSTACLE;
			}
		} END_FIND_IF_IN_OBSTACLE:
		*/

		//if (in_obstacle) {
		int c = 0;
		for (int d = 0; d < Dir4::DIR_COUNT; d++) {
			int nx = coord(0) + Dir4::dir_x[d];
			int ny = coord(1) + Dir4::dir_y[d];
			if (m_raster.is_coord_valid(nx, ny)) {
				int nidx = m_raster.idx(nx, ny);
				if (potential_at(group_id, nidx) <= m_very_large / 2) {
					smooth += g.field[nidx].pref_vel;
					c++;
				}
			}
		}
		if (c) g.field[m_raster.idx(coord)].pref_vel = smooth / c;
		//}

	}
}

void ContinuumField::cost_field_test_1(int agent_count, VecX<float>& positions, VecX<float>& velocities, float pref_speed)
{
	// set all costs to 1
	FOR_EACH_CELL_IDX(m_raster, x, y, idx) {
		for (int d = 0; d < Dir4::DIR_COUNT; d++) {
			cost_at(idx, (Dir4::DIR) d) = 1;
		}
	}

	FOR_EACH_CELL_IDX(m_raster, x, y, idx) {
		for (int d = 0; d < Dir4::DIR_COUNT; d++) {
			cost_at(idx, (Dir4::DIR) d) = 1;
		}
	}

	// create a hallway in the center
	int inner_offset = 3;
	int w = m_raster.resolution(0);
	int h = m_raster.resolution(1);

	int w_center = w / 2;

	int l = w_center - inner_offset;
	int r = w_center + inner_offset;
	int b = 1;
	int t = h - 2;

	constexpr auto big = 1e6f;
	constexpr auto gate = 1e3f;

	// sideway cost:
	for (int y = b; y <= t; y++) {
		int idl = m_raster.idx(l, y);
		int idr = m_raster.idx(r, y);
		int idll = m_raster.idx(l - 1, y);
		int idlr = m_raster.idx(l + 1, y);
		int idrl = m_raster.idx(r - 1, y);
		int idrr = m_raster.idx(r + 1, y);

		m_grid[idll].cost[Dir4::E] = big;
		m_grid[idlr].cost[Dir4::W] = big;
		m_grid[idrl].cost[Dir4::E] = big;
		m_grid[idrr].cost[Dir4::W] = big;

		m_grid[idl].cost[Dir4::E] = big;
		m_grid[idl].cost[Dir4::W] = big;
		m_grid[idr].cost[Dir4::E] = big;
		m_grid[idr].cost[Dir4::W] = big;
	}

	// up down:
	m_grid[m_raster.idx(l, t + 1)].cost[Dir4::S] = big;
	m_grid[m_raster.idx(r, t + 1)].cost[Dir4::S] = big;
	m_grid[m_raster.idx(l, b - 1)].cost[Dir4::N] = big;
	m_grid[m_raster.idx(r, b - 1)].cost[Dir4::N] = big;

	m_grid[m_raster.idx(l, t)].cost[Dir4::N] = big;
	m_grid[m_raster.idx(r, t)].cost[Dir4::N] = big;
	m_grid[m_raster.idx(l, b)].cost[Dir4::S] = big;
	m_grid[m_raster.idx(r, b)].cost[Dir4::S] = big;

	// gate cost
	for (int offset = 1; offset < 2 * inner_offset; offset++) {
		int id = m_raster.idx(l + offset, t);
		m_grid[id].cost[Dir4::S] = gate;
	}

	FOR_EACH_CELL_IDX(m_raster, x, y, idx) {
		for (int d = 0; d < Dir4::DIR_COUNT; d++) {
			Q_ASSERT(cost_at(idx, (Dir4::DIR) d) != std::numeric_limits<float>::infinity());
		}
	}
}

void ContinuumField::visualize_update(int g_id)
{
	if (m_draw_grid) {

		auto value_f = [this, g_id](int i, int j) -> float {
			//return m_obstacle_discomfort_cache[m_raster.idx(i, j)];

			return potential_at(g_id, i, j);
		};

		float r = 1 + 0.1f * log10f(1 + 0.1f *
			avg(m_raster, value_f, [](float f) -> float { return f; }, m_very_large / 2)
		);

		auto modifier = [r](float v) { return std::pow(v, r); };

		float avg_modified = avg(m_raster, value_f, modifier, m_very_large / 2);

		float grid_max_scale = qMax(0.0001f, 2.0f * avg_modified);

		FOR_EACH_CELL_IDX(m_raster, i, j, idx) {
			auto state = m_groups[g_id].field[idx].state;

			if (state == GCell::CANDIDATE) {
				m_pGridDrawer->set_block_color(i, j, { 255,255,255,255 });
			}
			else {
				float p = value_f(i, j);

				if (p == std::numeric_limits<float>::infinity()) {
					m_pGridDrawer->set_block_color(i, j, { 0,0,0,160 });
				}
				else if (p >= m_very_large) {
					m_pGridDrawer->set_block_color(i, j, { 160,160,160,160 });
				}
				else {
					float value = modifier(p) / grid_max_scale;
					auto color = ColorF::heatmap(value, ColorF::color_5, 1.0f);
					color.a = 0.5f;
					m_pGridDrawer->set_block_color(i, j, color.c());
				}
			}
		}
	}
	if (m_draw_arrow || m_draw_dir)
	{

		FOR_EACH_CELL_IDX(m_raster, i, j, idx) {
			for (int d = 0; d < Dir4::DIR_COUNT; d++) {


				// portal?
				auto can_move = m_movegrid[idx].can_normal_move(d);
				auto can_pmove = m_movegrid[idx].can_portal_move(d);

				m_pGridDrawer->set_dir_color(i, j,
					(Dir4::DIR) d,
					{ can_move ? 0.0f : 1.0f,
					can_pmove ? 1.0f : 0.0f,
					0.0f,
					(!can_move || can_pmove) ? 1.0f : 0.0f }
				);

				/* cost?
				float dir_val = cost_modifier(cost_at(i, j, (Dir4::DIR) d));
				m_pGridDrawer->set_dir_alpha(i, j,
					(Dir4::DIR) d,
					std::sqrt(dir_val / max_dir_val)
				);*/
			}

			//auto pot = potential_at(0, m_raster.idx(i, j));
			//if (pot < m_very_large) {
			auto vel = pref_vel_at(0, i, j); // pref_vel_at(0, i, j); //avg_velocity_at(i, j);

			if (vel(0) == std::numeric_limits<float>::infinity()
				|| vel(1) == std::numeric_limits<float>::infinity()) {
				m_pGridDrawer->set_arrow_dir(i, j, { 0,1 }, 0.5f);
			}
			else {
				float vel_size = vel.norm();
				auto vel_n = vel / vel_size;
				if (vel_size > Geometry::Epsilon) {
					m_pGridDrawer->set_arrow_dir(i, j, vel_n, 1);
				}
				else {
					m_pGridDrawer->set_arrow_dir(i, j, { 1,0 }, 0.5f);
				}
			}


			//}
			/*else {
				Vector2f vel = {0,0};
				for (int i2 = i - 1; i2 <= i + 1; i2++) {
					for (int j2 = j - 1; j2 <= j + 1; j2++) {
						if (!(i2 == 1 && j2 == 1)) {
							if (m_raster.is_coord_valid({ i2, j2 })) {
								auto pot = potential_at(0, m_raster.idx(i, j));
								if (pot < m_very_large) {
									vel += velocity_at(0, i2, j2);
								}
							}
						}
					}
				}
				float vel_size = vel.norm();
				auto vel_n = vel / vel_size;
				m_pGridDrawer->set_arrow_dir(i, j, vel_n, vel_size > 0 ? 1 : 0);
			}*/
		}
	}

	m_pGridDrawer->update(m_draw_grid, m_draw_dir, m_draw_arrow);
}

float ContinuumField::density_at_01(Vector2f posn)
{
#if defined NEAREST_EVAL
	return density_at(idx_01(posn));
#elif defined LINEAR_EVAL

	BilinearInfo bi;
	bi.make(posn, m_raster.resolution());

	float bl{ 0.0f }, br{ 0.0f }, tl{ 0.0f }, tr{ 0.0f };

	if (bi.x_low)
	{
		if (bi.y_low) bl = density_at(bi.x_i, bi.y_i);
		else bi.dy = 1;
		if (bi.y_high) tl = density_at(bi.x_i, bi.y_i + 1);
		else bi.dy = 0;
	}
	else bi.dx = 1;

	if (bi.x_high)
	{
		if (bi.y_low) br = density_at(bi.x_i + 1, bi.y_i);
		else bi.dy = 1;
		if (bi.y_high) tr = density_at(bi.x_i + 1, bi.y_i + 1);
		else bi.dy = 0;
	}
	else bi.dx = 0;

	// bilinear interpolate:
	float total = (1 - bi.dx) * ((1 - bi.dy) * bl + bi.dy * tl) + bi.dx * ((1 - bi.dy) * br + bi.dy * tr);
	return total;
#endif
}

Vector2f ContinuumField::avg_velocity_at_01(Vector2f posn)
{
#if defined NEAREST_EVAL
	return avg_velocity_at(idx_01(posn));
#elif defined LINEAR_EVAL
	BilinearInfo bi;
	bi.make(posn, m_raster.resolution());

	Vector2f bl{ 0,0 }, br{ 0,0 }, tl{ 0,0 }, tr{ 0,0 };

	if (bi.x_low)
	{
		if (bi.y_low) bl = avg_velocity_at(bi.x_i, bi.y_i);
		else bi.dy = 1;
		if (bi.y_high) tl = avg_velocity_at(bi.x_i, bi.y_i + 1);
		else bi.dy = 0;
	}
	else bi.dx = 1;

	if (bi.x_high)
	{
		if (bi.y_low) br = avg_velocity_at(bi.x_i + 1, bi.y_i);
		else bi.dy = 1;
		if (bi.y_high) tr = avg_velocity_at(bi.x_i + 1, bi.y_i + 1);
		else bi.dy = 0;
	}
	else bi.dx = 0;

	// bilinear interpolate:
	Vector2f total =
		(1 - bi.dx) * ((1 - bi.dy) * bl + bi.dy * tl)
		+ bi.dx * ((1 - bi.dy) * br + bi.dy * tr);
	return total;
#endif
}

void ContinuumField::compute_cost(int i, int j, Dir4::DIR d)
{
	int i2 = i + Dir4::dir_x[(int)d];
	int j2 = j + Dir4::dir_y[(int)d];
	int id = m_raster.idx(i, j);
	int id2 = m_raster.idx(i2, j2);
	float discomfort, speed;

	bool can_move = m_movegrid[id].can_normal_move(d);

	if (!can_move) cost_at(id, d) = m_very_large;
	else if (valid_x(i2) && valid_y(j2) && can_move)
	{
		discomfort = discomfort_at(id2); // is pre-weighted
		speed = flow_at(id, d);
		cost_at(id, d) = m_dist_weight + (m_time_weight + discomfort) / (speed);
	}
	else cost_at(id, d) = std::numeric_limits<float>::infinity();
}

void ContinuumField::compute_portal_cost(int from_idx, int portal_idx, Dir4::DIR portal_dir)
{
	//discomfort = portal_discomfort_at(id2);
	//speed = portal_flow_at(id, d);
	//portal_cost_at(id, d) = m_speed_weight + (m_time_weight + discomfort) / (speed);
	float discomfort = discomfort_at(portal_idx);
	portal_cost_at(from_idx) = 6 * (m_dist_weight + (m_time_weight + discomfort) / flow_at(from_idx, portal_dir));
}

void ContinuumField::write_agent_discomfort(Vector2f posn, float magnitude)
{
#ifdef NEAREST_EVAL

#elif defined LINEAR_EVAL
	BilinearInfo bi;
	bi.make(posn, m_raster.resolution());

	constexpr const float falloff_exp = 2.0f;

	int id;
	if (bi.x_low)
	{
		if (bi.y_low)
		{
			float a_dens = std::pow(qMin(1 - bi.dx, 1 - bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i, bi.y_i);
			discomfort_at(id) += a_dens * magnitude;
		}
		if (bi.y_high)
		{
			float d_dens = std::pow(qMin(1 - bi.dx, bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i, bi.y_i + 1);
			discomfort_at(id) += d_dens * magnitude;
		}
	}
	if (bi.x_high)
	{
		if (bi.y_low)
		{
			float b_dens = std::pow(qMin(bi.dx, 1 - bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i + 1, bi.y_i);
			discomfort_at(id) += b_dens * magnitude;
		}
		if (bi.y_high)
		{
			float c_dens = std::pow(qMin(bi.dx, bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i + 1, bi.y_i + 1);
			discomfort_at(id) += c_dens * magnitude;
		}
	}
#endif
}

float ContinuumField::eval_flow(Vector2f future_pos_01, float max_speed, float min_dens, float max_dens, int dimension, float sign)
{
	float min_speed = max_speed * 0.001f;
	// avoid infinite costs for simplification

	float future_dens = density_at_01(future_pos_01);

	if (future_dens < min_dens) {
		return max_speed;
	}

	float future_avg_speed = qMax(0.0f, sign * avg_velocity_at_01(future_pos_01)(dimension));

	if (future_dens > max_dens) {
		return qMax(min_speed, future_avg_speed);
	}
	else {
		float alpha = (future_dens - min_dens) / (max_dens - min_dens);
		return qMax(min_speed, (1 - alpha) * max_speed + (alpha) * future_avg_speed);
	}
}

void ContinuumField::write_agent_to_field(Vector2f pos_01, Vector2f vel)
{
	BilinearInfo bi;
	bi.make(pos_01, m_raster.resolution());

	constexpr const float falloff_exp = 2.0f;

	int id;
	if (bi.x_low)
	{
		if (bi.y_low) {
			float a_dens = std::pow(qMin(1 - bi.dx, 1 - bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i, bi.y_i);
			density_at(id) += a_dens;
			avg_velocity_at(id) += a_dens * vel;
		}
		if (bi.y_high) {
			float d_dens = std::pow(qMin(1 - bi.dx, bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i, bi.y_i + 1);
			density_at(id) += d_dens;
			avg_velocity_at(id) += d_dens * vel;
		}
	}
	if (bi.x_high) {
		if (bi.y_low) {
			float b_dens = std::pow(qMin(bi.dx, 1 - bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i + 1, bi.y_i);
			density_at(id) += b_dens;
			avg_velocity_at(id) += b_dens * vel;
		}
		if (bi.y_high) {
			float c_dens = std::pow(qMin(bi.dx, bi.dy), falloff_exp);
			id = m_raster.idx(bi.x_i + 1, bi.y_i + 1);
			density_at(id) += c_dens;
			avg_velocity_at(id) += c_dens * vel;
		}
	}
}

float ContinuumField::solve_equasion(double a, double b, double c, double d)
{
	double a2 = a * a;
	double b2 = b * b;
	double c2 = c * c;
	double d2 = d * d;


	float X1 = a + b;
	float X2 = c + d;

	if (X1 > 1e3 * X2) return X2;
	if (X2 > 1e3 * X1) return X1;

	double linear_result = qMin(X1, X2);

	// todo:
	// should we simply always return minimum of the function or either term dropped?
	double result;

	double inner_root = b2 * d2 * (-a2 + 2 * a * c + b2 - c2 + d2);

	// if there is a solution
	if (inner_root > 0) {
		result = (std::sqrt(inner_root) + a * d2 + b2 * c) / (b2 + d2);

		// might overflow...
		if (!is_valid_float(result)) {
			result = linear_result;
		}
		else {
			result = qMin(linear_result, result);
		}
	}
	else {
		result = linear_result;
	}

#ifdef _DEBUG
	if (!is_valid_float(result) || result < 0) {
		DebugBreak();
	}
#endif

	return result;
}

float ContinuumField::evaluate_potential(int g, int i, int j)
{
	float P[Dir4::DIR_COUNT]; // absolute potentials
	float C[Dir4::DIR_COUNT]; // costs (~potential derivative)

	constexpr const float inf = std::numeric_limits<float>::infinity();

	int id = m_raster.idx(i, j);

	for (int d = 0; d < Dir4::DIR_COUNT; d++) {
		int i2 = i + Dir4::dir_x[d];
		int j2 = j + Dir4::dir_y[d];
		if (m_raster.is_coord_valid({ i2, j2 })) {
			int id2 = m_raster.idx(i2, j2);
			P[d] = potential_at(g, id2);
			C[d] = cost_at(id, (Dir4::DIR) d);
		}
		else {
			P[d] = inf;
			C[d] = inf;
		}
	}

	return evaluate_potential(P, C);
}

float ContinuumField::evaluate_potential(float P[4], float C[4])
{
	float pcN = P[Dir4::N] + C[Dir4::N];
	float pcE = P[Dir4::E] + C[Dir4::E];
	float pcS = P[Dir4::S] + C[Dir4::S];
	float pcW = P[Dir4::W] + C[Dir4::W];

	// decide best direction in both dimensions
	Dir4::DIR mx = pcE < pcW ? Dir4::E : Dir4::W;
	Dir4::DIR my = pcN < pcS ? Dir4::N : Dir4::S;

	// check of on of the directions is unavailable
	constexpr const float inf = std::numeric_limits<float>::infinity();

	bool mx_inf = (pcE == inf) && (pcW == inf);
	bool my_inf = (pcN == inf) && (pcS == inf);

	float a = P[mx];
	float b = C[mx];
	float c = P[my];
	float d = C[my];

	float potential;

	if (!mx_inf && !my_inf) {
		potential = solve_equasion(a, b, c, d);
	}
	else if (mx_inf) { // drop 'mx' term
					   // ipow((P_ij - P[my]) / C[my], 2) = 1
					   //	   ((X	  - c)	   / d)   ^ 2) = 1, solve for X gives:
		potential = c + d;
	}
	else { // drop 'my' term, similar to above
		potential = a + b;
	}

	//#ifdef _DEBUG
	//	if (!is_valid_float(potential)) {
	//		DebugBreak();
	//	}
	//#endif

	return potential;
}

float ContinuumField::evaluate_potential_trough_portal(int g, int i, int j, int portal_origin_dir, int origin_idx)
{
	float P[Dir4::DIR_COUNT]; // absolute potentials
	float C[Dir4::DIR_COUNT]; // costs (~potential derivative)

	constexpr const float inf = std::numeric_limits<float>::infinity();

	int id = m_raster.idx(i, j);

	for (int d = 0; d < Dir4::DIR_COUNT; d++) {
		if (d == portal_origin_dir) {
			P[d] = potential_at(g, origin_idx);
			C[d] = portal_cost_at(id);
		}
		else {
			int i2 = i + Dir4::dir_x[d];
			int j2 = j + Dir4::dir_y[d];
			if (m_raster.is_coord_valid({ i2, j2 })) {
				int id2 = m_raster.idx(i2, j2);
				P[d] = potential_at(g, id2);
				C[d] = cost_at(id, (Dir4::DIR) d);
			}
			else {
				P[d] = inf;
				C[d] = inf;
			}
		}
	}

	return evaluate_potential(P, C);
}

void ContinuumField::write_grad_vel(int g_i, int i, int j, std::vector<Vector2i>& edges)
{
	int id = m_raster.idx(i, j);
	auto& g = m_groups[g_i];
	auto& gcell = g.field[id];
	Vector2f grad;

	float local_pot = potential_at(g_i, i, j);
	if (local_pot == 0) return; // this is a goal, the preffered velocity is already set
	
	bool is_obstructed = local_pot > m_very_large / 2;

	if (is_obstructed) {
		Q_ASSERT(m_raster.is_coord_valid(i, j));
		edges.push_back({ i,j });
		gcell.pref_vel = { 0, 0 };
		return;
	}

	constexpr float inf = std::numeric_limits<float>::infinity();

	// move in the direction of the lowest potential
	float P[4];
	// find neighbouring potentials:
	for (int d = 0; d < Dir4::DIR_COUNT; d++)
	{
		if (m_movegrid[id].can_portal_move(d))
		{
			P[d] = g.field[m_movegrid[id].portal_move_dest_idx()].potential;
		}
		else
		{
			int i2 = i + Dir4::dir_x[d];
			int j2 = j + Dir4::dir_y[d];

			if (m_raster.is_coord_valid({ i2, j2 }) && (is_obstructed || m_movegrid[id].can_normal_move(d)))
			{
				P[d] = g.field[m_raster.idx(i2, j2)].potential;
			}
			else P[d] = inf;
		}
	}

	// horizontal gradient:
	if (P[Dir4::E] < P[Dir4::W])
		grad(0) = qMax(0.0f, gcell.potential - P[Dir4::E]); // grad(0) >= 0
	else
		grad(0) = qMin(0.0f, P[Dir4::W] - gcell.potential); // grad(0) <= 0

	// vertical gradient:
	if (P[Dir4::N] < P[Dir4::S])
		grad(1) = qMax(0.0f, gcell.potential - P[Dir4::N]); // grad(1) >= 0
	else
		grad(1) = qMin(0.0f, P[Dir4::S] - gcell.potential); // grad(1) <= 0

	// normalize gradient:
	grad.normalize();

	// write velocity
	auto& flow = m_grid[id].flow;
	gcell.pref_vel = {
		qMax(0.0f, grad(0) * flow[Dir4::E]) + qMin(0.0f, grad(0) * flow[Dir4::W]),
		qMax(0.0f, grad(1) * flow[Dir4::N]) + qMin(0.0f, grad(1) * flow[Dir4::S]),
	};
}

void ContinuumField::consider_next_candidate(Group& g, int g_i, int nidx, int nx, int ny,
	std::map<int, Vector2i, Compare_potential>& candidates, bool origin_unreachable)
{
	GCell& ncell = g.field[nidx];
	if (ncell.state == GCell::UNKNOWN)
	{
		ncell.potential = evaluate_potential(g_i, nx, ny);

		if (!origin_unreachable)
		{
			if (ncell.potential >= m_very_large) {
				ncell.unreachable = true;
			}

			ncell.state = GCell::CANDIDATE;
			candidates[nidx] = { nx, ny };
		}
	}
	else if (ncell.state == GCell::CANDIDATE) {
		float new_pot = evaluate_potential(g_i, nx, ny);
		if (new_pot < ncell.potential) {
			auto nodeHandler = candidates.extract(nidx);
			// nodeHandler.key() = ...; key doesnt actually change, but its evaluation does
			ncell.potential = new_pot;
			candidates.insert(std::move(nodeHandler));
		}
	}
}

void ContinuumField::step_end(SimulationCore& sc)
{
	m_step_end_was_called = true;
	if (m_enable_debug_vars) {
		m_visualize_group_id = sc.settings().retreive_int(m_visualize_group_id_varname);
	}
	visualize_update(m_visualize_group_id);
}