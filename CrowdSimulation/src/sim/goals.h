#ifndef GOALS_H
#define GOALS_H

#include <vector>
#include <unordered_map>
#include <QSharedPointer>
#include "tools/include.h"
#include "obstacle.h"

class IGoal
{
public:
	std::unordered_map<int, Vector2i> get_indices(AABB2 region, Vector2i dim);

	virtual Vector2f closest_point(Vector2f pos) = 0;

	inline Vector2f& goal_inner_direction() {
		return inner_direction;
	}

protected:
	virtual void fill_indices_inner(std::unordered_map<int, Vector2i>&, AABB2 region, Vector2i dim) const = 0;

	Vector2i idx(Vector2f pos, AABB2 region, Vector2i dim) const;
	int	flat_idx(Vector2i id, Vector2i dim) const;

	std::vector<IGoal*> sub_goals;
	Vector2f			inner_direction = { 0,0 };
};

using IGoal_ptr = QSharedPointer<IGoal>;

class SquareGoal
	: public IGoal
{
public:
	SquareGoal(AABB2);

	Vector2f closest_point(Vector2f pos) override;

protected:
	virtual void fill_indices_inner(std::unordered_map<int, Vector2i>&, AABB2 region, Vector2i dim) const override;

private:
	AABB2 m_square;
};

class ObstacleGoal 
	: public IGoal, public Obstacle
{
public:
	ObstacleGoal(std::vector<Vector2f>& vertices);

	virtual Vector2f closest_point(Vector2f pos) override;

protected:
	virtual void fill_indices_inner(std::unordered_map<int, Vector2i>&, AABB2 region, Vector2i dim) const override;
};

#endif // GOALS_H