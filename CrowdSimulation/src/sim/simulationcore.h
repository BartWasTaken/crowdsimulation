#ifndef SIMULATIONCORE_H
#define SIMULATIONCORE_H
#include "simdeclr.h"

#include "simulationsettings.h"
#include "interfaces.h"
#include "QSharedPointer"
#include "simulationstats.h"
#include "IEnvironment.h"

#include "tools/InputManager.h"
#include "tools/Event.h"
#include "tools/shared_eigen_incl.h"

#include "ui/eventview.h"

#include "vis/renderer3d.h"
#include "vis/irenderable.h"
#include "vis/visdeclr.h"
#include "vis/CircleDrawer2D.h"

#include <QMenu>
#include <QSignalMapper>
#include <QMutex>
#include <queue>
#include <map>

/*
 * The SimulationCore holds the environment and the agents
 * it holds the information that is available to the simulation engines
 * which perform the update on environment+agent
 */

class MainWindow;
class StatsView;

struct EngineSetup
{
	IGlobalPanner_ptr	pGlobalPlanner = nullptr;	// given a goal -> give a local direction
	IAgentMover_ptr		pAgentMover = nullptr;	// given a local direction -> give a final direction

	QString				name = "nameless";

	void render(RenderContext);
	void step(SimulationCore& sc, float dt);
	bool is_complete();

	void hard_reset(SimulationCore& sc);
	void settings_changed(SimulationCore& sc, SimulationSettings_ptr old_settings);
	void make_ui(SimulationCore& sc);

	bool operator==(const EngineSetup& other) const;
};

class SimulationCore :
	public QObject,
	public IRenderable,
	public ChangedListener<SimulationSettings>,
	public InputListener,
	public Tools::INonCopyable
{
	Q_OBJECT
public:
	SimulationCore(OpenGLView& f, MainWindow& mw, SimulationSettings_ptr& pSettings);
	~SimulationCore();

	constexpr static const int dim = 2;

	void render(RenderContext) override;
	void update();

	bool has_goalpicker();
	bool has_engine();

	void set_engine(EngineSetup);
	void add_engine(EngineSetup);

	void set_goalpicker(IGoalPicker_ptr);
	void add_goalpicker(IGoalPicker_ptr);

	void set_environment(IEnvironment_ptr, bool delay_reset = false);
	void add_environment(IEnvironment_ptr);

	void colorize_agents();


	EngineSetup find_engine(QString);
	IGoalPicker_ptr find_goalpicker(QString);

	SimulationStats::FrameData& frame_data();

	Vector2f try_move(int agent_id, Vector2f displacement);

	// getters
	IGoalPicker&			goal_picker() { return *m_pGoalPicker.data(); }
	IGlobalPanner&			global_planner() { return *m_engine.pGlobalPlanner.data(); }
	IAgentMover&			agent_mover() { return *m_engine.pAgentMover.data(); }
	CollisionResolver&		collision_resolver() { return *m_pCollision_resolver; }
	CircleDrawer2D&			circleDrawer() { return *m_pCircleDrawer; }
	QMenu&					get_simmenu() { return *m_simmenu; }
	QMenu&					get_vismenu() { return *m_vismenu; }
	MainWindow&				get_window() { return m_window; }
	StatsView&				stat_view() { return *m_pStatsView; }

	inline int				nr_active_agents() { return m_active_agent_count; }
	// assumes: n >= 0, n <= nr_active_agents() 
	// reduces nr_active_agents() by 1
	void					disable_agent(int n);

	inline IEnvironment&	environment() { return *m_pEnvironment.data(); }

	inline VecX<float>&		positions() { return m_agent_positions; }
	inline Vec2Acces<float> position(int i) { return VEC2_ACCES(m_agent_positions, i); }
	inline VecX<float>&		velocities() { return m_agent_velocities; }
	inline Vec2Acces<float> velocity(int i) { return VEC2_ACCES(m_agent_velocities, i); }
	inline Vec3Acces<float>	pos_3d(int i) { return VEC3_ACCES(m_agent_pos_3d, i); }
	inline float&			height(int i) { return  VEC3_ACCES(m_agent_pos_3d, i)(1); }

	inline int64_t			step_id() { return m_step_count; }
	inline float			simtime_sec() { return m_sim_time_sec; }
	inline SimulationSettings& settings() { return *m_pSettings; }

	static Vector2i			compute_grid_dimensions(float agent_rad, AABB2 region);

	// actions from ui
	void act_reset();
	void act_settings();
	void act_show_part();
	void act_show_smoke();
	void act_show_lines();
	void act_switch_renderer();
	void act_set_engine(int i);
	void act_set_env(int i);
	void act_set_goalpicker(int i);

	// event from changed listener
	virtual bool object_changed(SimulationSettings* pObj) override;

	void debug_wait_step();

	template<typename T>
	inline void make_zero_vector(VecX<T>& v, int dim_per_agent = -1) {
		if (dim_per_agent == -1) dim_per_agent = this->dim;
		int size = settings().agent_count() * dim_per_agent;
		if (v.size() != size) v = VecX<T>::Zero(size);
		else v.setZero();
		//if (dim_per_agent == -1) dim_per_agent = this->dim;
		//int size = settings().agent_count() * dim_per_agent;
		//v = VecX<T>::Zero(size);
	}

	template<typename T>
	inline void require_vector_size(VecX<T>& v, int dim_per_agent = -1) {
		if (dim_per_agent == -1) dim_per_agent = this->dim;
		int size = settings().agent_count() * dim_per_agent;
		if (v.size() != size) v = VecX<T>::Zero(size);
	}

	inline OpenGLView& opengl() { return f; }
	bool has_stable_record() { return m_stable_record; }

private:

	void reset();
	void refresh_ui(bool true_reset);
	void setup_agents();
	void setup_agents_draw();
	void handle_settings_change(SimulationSettings_ptr old_settings, SimulationSettings_ptr new_settings, bool delay_reset = false);
	void update_agents_vis();
	void update_line_draw();
	void set_settings(QJsonObject&, bool delay_reset = false);

	void run();
	void sim_step();

	OpenGLView&							f;
	MainWindow&							m_window;
	CircleDrawer2D*						m_pCircleDrawer = nullptr;
	LineDrawer2D*						m_pLineDrawer = nullptr;
	GoalColorizer						m_pColorizer;
	bool								m_draw_agents = true;
	bool								m_draw_lines = true;
	Color								m_leading_color;
	VecX<float> position_buffer;

#pragma region Simulation
	int									m_active_agent_count = 0;

	VecX<float>							m_agent_positions;
	VecX<float>							m_agent_velocities;
	VecX<float>							m_agentpositions_initial;
	VecX<float>							m_agent_pos_3d;

	SimulationSettings_ptr				m_pSettings = nullptr;
	SimulationStats						m_frame_stats;

	IEnvironment_ptr					m_pEnvironment = nullptr;
	std::vector<IEnvironment_ptr>		m_environment_list;

	EngineSetup							m_engine;
	std::vector<EngineSetup>			m_engine_list;

	IGoalPicker_ptr						m_pGoalPicker;
	std::vector<IGoalPicker_ptr>		m_goal_picker_list;

	CollisionResolver*					m_pCollision_resolver;
#pragma endregion

#pragma region Time
	// timing
	//Timer			m_total_timer;
	Timer			m_step_timer;
	//std::chrono::time_point<my_clock> t0;
	//std::chrono::time_point<my_clock> t1;
	int64_t			m_step_count = 0;
	float			m_sim_time_sec = 0;
	bool			m_paused = false;
	bool			m_post_pause_timestep = true;
	int64_t			m_paused_step = 0;
	Tools::Event	m_debug_step_event;

	struct TimeAnalyse {
	public:
		double dt_max = 0;
		int stepRefresh = 6;
	private:
		// use 2 buffers so the local average stays stable on a refresh
		double temp_total[2] = { 0, 0 };
		int steps[2] = { 0,0 };
		int current_bufferid = 0;
	public:
		void reset();
		void step(double dt);
		double avg_dt();
	} m_time_analyser;
#pragma endregion

#pragma region UI
	QMutex		m_rendermutex;
	bool		m_render_enabled = true;
	StatsView*	m_pStatsView;
	EventView	m_eventview;

	// ui - owned by main thread
	QMenu* m_simmenu = nullptr;
	QMenu* m_goalmenu = nullptr;
	QMenu* m_envmenu = nullptr;
	QMenu* m_vismenu = nullptr;

	QAction* m_act_reset = nullptr;
	QAction* m_act_settings = nullptr;
	QAction* m_switch_renderer = nullptr;
	QAction* m_show_particles = nullptr;
	QAction* m_show_lines = nullptr;
	QAction* m_show_smoke = nullptr;

	std::map<QString, QAction*>				m_sim_actionmap;
	std::map<IGoalPicker_ptr, QAction*>		m_goalpicker_actionmap;
	std::map<IEnvironment_ptr, QAction*>	m_env_actionmap;

	Renderer3D							m_renderer3d;
	bool								m_stable_record = false;

#pragma endregion

#pragma region Threading
	struct ConcurrentTaskQueue {
		typedef std::function<void()> Task;
		std::queue<Task>		tasks;
		QMutex					mutex;

		void add(Task t) {
			QMutexLocker locker(&mutex);
			tasks.push(t);
		}
		void execute(int limit = -1) {
			int iteration = 0;
			QMutexLocker locker(&mutex);
			while (!tasks.empty() && (limit == -1 || iteration++ < limit)) {
				tasks.front()(); // execute task
				tasks.pop(); // remove from queue
			}
		}
	};

	ConcurrentTaskQueue		m_sim_tasks;
	ConcurrentTaskQueue		m_main_tasks;

	std::thread				m_simulation_thread;
	std::thread::id			m_simulation_thread_id;
	volatile bool			m_flag_alive = true;
#pragma endregion

	//struct void_cmpr {
	//	bool operator()(const void* a, const void* b) const { return a < b; }
	//};
	//std::vector<std::function<void(SimulationCore*)>> m_reset_responses;
	//std::vector<std::function<void(SimulationCore*)>> m_step_actions;

protected:
	virtual KeyEventReturnValue KeyClick(Qt::Key key) override;

};

#endif // SIMULATIONCORE_H
