#include "DirectPlanner.h"
#include "simulationcore.h"

DirectPlanner::DirectPlanner()
	: IGoalPicker(m_goals)
{

}

void DirectPlanner::set_pref_velocity(SimulationCore& sc, VecX<float>& pref_velocities, float dt)
{
	int nr_agents = sc.nr_active_agents();
	auto pref_speed = sc.settings().agent_speed();

	for (int i = 0; i < nr_agents; i++)
	{
		auto pos = sc.position(i);
		int gid = sc.goal_picker().goal(i);
		auto goal_point = sc.environment().goals()[gid]->closest_point(pos);

		Vector2f dir = (goal_point - pos);
		float dir_len = dir.norm();

		if (dir_len > pref_speed) {
			dir *= (pref_speed / dir_len);
		}

		VEC2_ACCES(pref_velocities, i) = dir;
	}
}

void DirectPlanner::choose_goals(SimulationCore& sc)
{
	int g_count = sc.environment().goals().size();
	int nr_agents = sc.nr_active_agents();

	if (g_count < 2) {
		sc.make_zero_vector(m_goals, 1);
	}
	else {
		sc.require_vector_size(m_goals, 1);

		int goal_id = 0;
		for (int i = 0; i < nr_agents; i++)
		{
			m_goals(i) = goal_id;
			goal_id = (goal_id + 1) % g_count;
		}
	}
}

QString DirectPlanner::goal_picker_name()
{
	return "Random";
}

bool DirectPlanner::settings_changed(SimulationCore& sc, SimulationSettings_ptr old_settings)
{
	return sc.settings().agent_count() != old_settings->agent_count();
}

GoalPickerDynamic::GoalPickerDynamic(QString n, std::function<void(SimulationCore&, VecX<int>&)> goal_pick_func)
	: IGoalPicker(m_goals)
{
	m_func = goal_pick_func;
	m_name = n;
}

void GoalPickerDynamic::choose_goals(SimulationCore& sc)
{
	sc.require_vector_size(m_goals, 1);
	m_func(sc, m_goals);
}

QString GoalPickerDynamic::goal_picker_name()
{
	return m_name;
}

bool GoalPickerDynamic::settings_changed(SimulationCore&, SimulationSettings_ptr old_settings)
{
	return true;
}
