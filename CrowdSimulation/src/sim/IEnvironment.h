#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "simdeclr.h"

#include "vis/openglincl.h"
#include "vis/GridDrawer2D.h"
#include "tools/shared_eigen_incl.h"
#include "sim/goals.h"
#include "vis/shapedrawer2d.h"

#include "obstacle.h"
#include "simulationsettings.h"
#include <unordered_map>
#include <vector>
#include "Support/obstaclegridprovider.h"
#include "Engines/smokefield.h"

#include <random>
#include <map>
#include <QObject>
#include <QSharedPointer>

class IEnvironment
{
public:

	class NewObstacleEvent : public ObstacleEvent
	{
	public:
		NewObstacleEvent(QString name, bool is_manual, float time_sec, Polygon2D shape);
		bool execute(SimulationCore& sc, std::vector<Obstacle>&);
		void reset(std::vector<Obstacle>& obstacles);
	private:
		Polygon2D m_polygon;
	};

	typedef void(*Distributor)(SimulationCore& sc, std::function<float()>);

	IEnvironment(OpenGLView& f, AABB2 bbox, QString name) : m_bbox(bbox), m_name(name), m_smoke(f), m_obstacle_field_provider(*this) {}

	inline PortalIterator get_portal_iterator() {
		return PortalIterator(&m_obstacles);
	}

	void update(SimulationCore& sc, float dt);
	void enable(SimulationCore& sc);

	void add_obstacle(const Obstacle& obstacle);
	void add_goal(const IGoal_ptr& goal);
	void add_staircase(StairCaseArea& staircase);
	void add_elevation(ElevationArea& area);
	void add_spawn_area(SpawnArea& area);
	void add_obstacle_event(NewObstacleEvent& e);

	void distribute_agents(SimulationCore& sc);
	void reset(SimulationCore& sc);

	void set_settings(QJsonObject&);
	QJsonObject& get_settings();

	ObstacleGridProvider& obstacleFields();

	int64_t last_obstacle_changed_step();

	void set_distributor(Distributor);
	void set_heights(SimulationCore& sc);

	std::vector<IGoal_ptr>& goals();
	std::vector<Obstacle>& obstacles();
	std::vector<StairCaseArea>& staircases();
	std::vector<ElevationArea>& elevation_areas();

	AABB2 bbox();
	Vector3f center3d();

	QString name();

	void render_env(RenderContext);

	SmokeField& smoke();

	void set_preferences(QString goal_pref, QString engine_pref, bool force_prefs);
	const QString& get_pref_goalpicker() const { return m_pref_goalpicker; }
	const QString& get_pref_engine() const { return m_pref_engine; }
	bool get_force_prefs() const { return m_force_prefs; }

	// should be used only before simulation
	void set_bounds(AABB2);

	std::vector<Obstacle> get_non_staircase_obstacles();
	std::vector<NewObstacleEvent>& get_obstacle_events();

private:

	inline void reset_random() { m_rgen.seed(m_rgen.default_seed); }
	inline float rand_float() { return m_dis(m_rgen); }
	void reset_smoke(SimulationSettings& sc);

	void spawn_from_areas(SimulationCore&);

	std::mt19937						m_rgen;
	std::uniform_real_distribution<>	m_dis{ 0.0f, 1.0f };


	Distributor							m_distr_func = nullptr;

	AABB2								m_bbox;
	QString								m_name;
	std::vector<IGoal_ptr>				m_goals;
	std::vector<Obstacle>				m_obstacles;
	std::vector<StairCaseArea>			m_staircases;
	std::vector<int>					m_spacial_span;

	std::vector<NewObstacleEvent>		m_obst_events;

	int64_t								m_obstacles_changed_frame = 0;

	std::vector<ShapeDrawer2D>			m_vis_shapes;
	bool								m_renew_vis_shapes = false;

	ObstacleGridProvider				m_obstacle_field_provider;
	SmokeField							m_smoke;

	QString								m_pref_goalpicker;
	QString								m_pref_engine;
	bool								m_force_prefs = false;

	QJsonObject m_settings;
	std::vector<ElevationArea>			m_elevations;
	std::vector<SpawnArea>				m_spawn_areas;
};

#endif // ENVIRONMENT_H
