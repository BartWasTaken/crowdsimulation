#pragma once
#include "src/tools/shared_eigen_incl.h"
#include "src/tools/griddecorators.h"
#include "simdeclr.h"
#include "tools/include.h"
#include <vector>
#include <QMutex>

/*
	An obstacle consists of a set of vertices.
	A standard obstacle have vertices be ccw ordered.
	The boundary of the map can be clockwise to (inside-out obstacle).
*/

template<typename T>
struct Label 
{
public:

	inline Label() {
		m_id = gen_id();
	}

	inline Label(const Label<T>& l) { // allow copy for usage in lists of objects
		m_id = l.m_id;
	}

	struct Ref
	{
	public:
		Ref(Label<T>& l) {
			m_refid = l.m_id;
		}
		Ref() {}
		void operator=(const Label<T>& l) {
			m_refid = l.raw_id();
		}
		bool operator==(const Label<T>& l) {
			return m_refid == l.raw_id();
		}
		bool operator!=(const Label<T>& l) {
			return !(*this == l);
		}
		void reset() {
			m_refid = 0;
		}

		// for use as a key:
		bool operator<(const Ref& other) const
		{
			return (other.m_refid < this->m_refid);
		}
	private:
		uint64_t m_refid = 0;
	};

	inline Ref make_ref() {
		return Ref { *this };
	}

	inline bool operator==(const Label<T>& other) {
		return this->m_id == other.m_id;
	}
	inline bool operator!=(const Label<T>& other) {
		return !(*this == other);
	}

protected:
	friend struct Label<T>::Ref;
	inline uint64_t raw_id() const { return m_id; }

private:
	inline uint64_t gen_id() {
		QMutexLocker lock(&s_id_gen_lock);
		s_id_gen += 1;
		return s_id_gen;
	}
	static QMutex		s_id_gen_lock;
	static uint64_t		s_id_gen;
	uint64_t			m_id;
};


template<typename T>
QMutex Label<T>::s_id_gen_lock;

template<typename T>
uint64_t Label<T>::s_id_gen = 1; // start at 1 as 0 means not-set

#define UNIQUE_LABEL_TYPE2(param) UNIQUE_LABEL_##param
#define UNIQUE_LABEL_TYPE1(param) UNIQUE_LABEL_TYPE2(param)
#define UNIQUE_LABEL_TYPE UNIQUE_LABEL_TYPE1(__COUNTER__)

typedef Label<struct UNIQUE_LABEL_TYPE> ObstacleID;
typedef Label<struct UNIQUE_LABEL_TYPE> ObstacleEventID;

class Obstacle;

enum class ObstacleGridType {
	FullCover, InnerCover
};

namespace ObstacleFlags
{
	enum Flag {
		NONE = 0,
		STAIRCASE = 1,
	};
}

struct ElevationArea {
	float height;
	Vector2f translation;
	Polygon2D polygon;
};

struct SpawnArea {
	Polygon2D polygon;
};

class ObstacleEvent {
public:
	ObstacleEvent(bool manual, float time, QString name);
	void activate_manual(bool);
	bool is_manually_activated();
	bool is_activated();
	inline QString get_name() { return m_name; }
	inline bool is_manual() { return m_is_manual; }

	ObstacleEventID id;

protected:
	QString m_name;
	bool m_active = false;
	bool m_is_manual = false;
	bool m_manual_activated = false;
	float m_event_time_sec;
	ObstacleID::Ref m_idref;
};

class Obstacle
{
public:
	struct PortalEndPoint {
		Vector2f a;
		Vector2f b;
		inline Vector2f line() { return b - a; }
	};

	struct HitInfo {
		bool has_hit;
		int obstacle_id;
		float collision_prog;
		Vector2f p1, p2;

		bool is_portal;
		bool any_staircase;
		Vector2f portal_p1, portal_p2; // order matters for orientation, must be same as p1, p2

		inline Vector2f move_through_portal(Vector2f a, Vector2f b) {
			Vector2f dummy;
			return move_through_portal(a, b, dummy);
		}
		
		inline Vector2f move_through_portal(Vector2f a, Vector2f b, Vector2f& out_teleportation_delta)
		{
			Vector2f start_portal_intersection = a + (b - a) * collision_prog;
			float progress_along_start_portal = (start_portal_intersection - p1).norm() / (p2 - p1).norm();
			Vector2f end_portal_intersection = portal_p2 + (portal_p1 - portal_p2) * progress_along_start_portal;
			Vector2f movement_left = (b - a) * (1 - collision_prog);
			out_teleportation_delta = end_portal_intersection - start_portal_intersection;
			// PORTALS SHOULD NOT ROTATE THE AGENTS!
			Q_ASSERT(are_colinear(p2 - p1, portal_p2 - portal_p1));
			Vector2f teleport_location = end_portal_intersection + movement_left;
			return teleport_location;
		}
		Vector2f calc_hit_normal() {
			return rotate_90_deg_ccw(p2 - p1).normalized();
		}
	};

	Obstacle(float inflate_rad, const std::vector<Vector2f>& vertices, bool persistent = true);

	void set_staircase(int up_id, int down_id, int up_vid, int down_vid);

	// avoids portal edges
	Vector2f closestPointOnEdge_base(const Vector2f& point) const;

	inline bool is_staircase() const {
		return type == ObstacleFlags::STAIRCASE;
	}

	inline bool is_persistent() const {
		return m_persistent;
	}

	// my_id is not in the array
	void bind_staircases(Obstacle* arr, int len); // bind indices

	void rebuild(float inflate_rad, float agent_rad);
	void move(Vector2f translation);
	
	const Polygon2D& agent_collider_bloat() const;
	const Polygon2D& agent_collider() const;
	const Polygon2D& base() const;

	static void hit_single_collider(SimulationCore& sc, Vector2f p1, Vector2f p2, int* obstacle_ids, int obstacle_id_count, HitInfo&, bool ignore_outwards);
	inline static void hit_single_collider(SimulationCore& sc, Vector2f p1, Vector2f p2, std::vector<int>& obstacle_ids, HitInfo& info, bool ignore_outwards) {
		hit_single_collider(sc, p1, p2, obstacle_ids.data(), (int) obstacle_ids.size(), info, ignore_outwards);
	}

	inline bool stairs_can_up() {
		Q_ASSERT(is_staircase());
		return up_id >= 0;
	}

	inline bool stairs_can_down() {
		Q_ASSERT(is_staircase());
		return down_id >= 0;
	}

	void get_up_line(PortalEndPoint& ep) {
		Q_ASSERT(up_id >= 0 && type == ObstacleFlags::STAIRCASE);
		ep.a = m_base.vertices()[up_vid];
		ep.b = m_base.vertices()[(up_vid + 1) % m_base.vertices().size()];
	}
	void get_up_line_end(PortalEndPoint& ep) {
		Q_ASSERT(up_id >= 0 && type == ObstacleFlags::STAIRCASE);
		ep = endpoint_up;
	}
	void get_down_line(PortalEndPoint& ep) {
		Q_ASSERT(down_id >= 0 && type == ObstacleFlags::STAIRCASE);
		ep.a = m_base.vertices()[down_vid];
		ep.b = m_base.vertices()[(down_vid + 1) % m_base.vertices().size()];
	}
	void get_down_line_end(PortalEndPoint& ep) {
		Q_ASSERT(down_id >= 0 && type == ObstacleFlags::STAIRCASE);
		ep = endpoint_down;
	}

	Vector2f project_through_portal_up(Vector2f p);
	Vector2f project_through_portal_down(Vector2f p);

	ObstacleID id; // auto generated unique typesafe idenity

protected:
	friend class StairCaseArea;

	void inflate(const std::vector<Vector2f>& vertices, float inflation, float agent_rad);
	
	ObstacleFlags::Flag type = ObstacleFlags::NONE;

	// if staircase:
	int up_id = -1;
	int down_id = -1;
	int up_vid = -1;
	int down_vid = -1;

	PortalEndPoint endpoint_up; 
	PortalEndPoint endpoint_down;

	Polygon2D m_initial;
	Polygon2D m_base;
	Polygon2D m_agent_collider;
	Polygon2D m_agent_collider_bloat;

	bool m_persistent;
};

class StairCaseArea {
public:
	StairCaseArea(std::vector<Vector2f>& vertices, float thickness, int up_id, int down_id, int up_vid, int down_vid, int opening_vid);

	void make_obstacles(std::vector<Obstacle>&, float agent_rad, float inflate_rad_override = -1) const;
	inline void bind_elevation(ElevationArea const* ptr) { m_elevation_area = ptr; }
	ElevationArea const* get_bound_elevation() const { return m_elevation_area; }

	float get_height_at(Vector2f point) const;
	void set_upper_height(std::vector<StairCaseArea>&);
	inline const Polygon2D& base_polygon() const { return m_polygon; }

	inline bool has_upwards() const { return up_portal_id >= 0; }

	//void find_pairs(int my_idx, std::vector<StairCaseArea>&);

	inline const std::vector<Vector2f>& vertices() const { return m_polygon.vertices(); };
	inline float base_height() const { return m_elevation_area == nullptr ? 0 : m_elevation_area->height; }

	// the area of a stairs is mapped to the lower floor

private:
	// identifiers
	int up_portal_id = -1;
	int down_portal_id = -1;

	// array indices
	//int up_portal_pair_idx;
	//int down_portal_pair_idx;

	// vertices
	int up_portal_vid = -1;
	int down_portal_vid = -1;

	// extra opening
	int opening_vid;

	//
	float thickness;
	Polygon2D m_polygon;

	/*
		If has an up wards part: Agents can move in the area.
		
	*/


	ElevationArea const*		m_elevation_area = nullptr;
	float						m_next_floor_height;
};

class PortalIterator 
{
public:
	PortalIterator(std::vector<Obstacle>*);

	struct PortalDescriptor {
		bool up;
		int obst_id = -1;
		Obstacle::PortalEndPoint start;
		Obstacle::PortalEndPoint end;
	};
	bool next();

	PortalDescriptor info;

private:
	bool check_obstacle();
	int reverse_id = 0;
	bool up;
	bool down;
	std::vector<Obstacle>*	m_pObstacles = nullptr;
};
