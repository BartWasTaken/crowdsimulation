#include "environment2d.h"
#include "simulationcore.h"


/*

Environment2D::Environment2D(AABB2 field)
{
	m_field = field;
}

AABB2 Environment2D::main_region()
{
	return m_field;
}

void Environment2D::hard_reset(SimulationCore& sc)
{
	for (auto& obst : m_obstacles) {
		obst.rebuild(sc);
	}
}

void Environment2D::settings_changed(SimulationCore& sc)
{
	hard_reset(sc);
}

const std::vector<IGoal_ptr>& Environment2D::get_goals(SimulationCore& sc)
{
	
}

void Environment2D::assign_goals_ids(SimulationCore& sc, VecX<int>& ids)
{
	// just assign something for now:

	int g_count = get_goals(sc).size();
	int agent_count = sc.settings().agent_count();

	int goal_id = 0;
	for (int i = 0; i < agent_count; i++)
	{
		ids(i) = goal_id;
		goal_id = (goal_id + 1) % g_count;
	}
}

void Environment2D::distribute_agents(SimulationCore& sc)
{
	// put just inside the main region:

	int agent_count = sc.settings().agent_count();

	Vector2f min = main_region().min();
	Vector2f range = main_region().sizes();

	for (int i = 0; i < agent_count; i++)
	{
		Vector2f rand = { rand_float(), rand_float() };
		sc.position(i) = min + rand * range;
	}	
}

void Environment2D::set_pref_velocity(SimulationCore& sc, VecX<float>& positions, VecX<float>& velocities)
{
	// requires global planning - not used for continuum
	throw std::logic_error("The method or operation is not implemented.");
}

void Environment2D::colorize(SimulationCore& sc)
{
	throw std::logic_error("The method or operation is not implemented.");
}

NeighbourFinder& Environment2D::get_neighbourfinder()
{
	throw std::logic_error("The method or operation is not implemented.");
}

float Environment2D::pref_speed()
{
	throw std::logic_error("The method or operation is not implemented.");
}

QString Environment2D::name()
{
	throw std::logic_error("The method or operation is not implemented.");
}

void Environment2D::resolve_collisions(SimulationCore& sc, int iter, bool inter_agent, bool obstacles)
{
	throw std::logic_error("The method or operation is not implemented.");
}

*/
