#pragma once

#include "tools/include.h"
#include "vis/openglincl.h"
#include "vis/GridDrawer2D.h"
#include "src/sim/kernels.h"
#include "src/sim/Support/obstaclegridprovider.h"
#include "obstaclegridprovider.h"
#include "src/sim/simulationcore.h"

#include <vector>
#include <QObject>

class SimulationCore;

struct AgentCell {
	std::vector<int> ids;
};

using ObstacleCell = std::vector<int>;

namespace Access
{

template<typename T> auto& get_ids(T&);

template<> inline auto& get_ids(AgentCell& c) {
	return c.ids;
}
template<> inline auto& get_ids(ObstacleCell& c) {
	return c;
}

}

class NeighbourFinder : Tools::INonCopyable
{
public:

	template<typename CELL>
	class Grid : public Raster
	{
	public:
		Grid() {}
		Grid(AABB2 bbox, Vector2i dim) : Raster(bbox, dim) {}

		void require(float infl_rad, bool clear = false) {
			Q_ASSERT(infl_rad > 0);

			Vector2f dimf = range() / (infl_rad * 2); // need double the influence radius to guarantee finding everything in range
			dimf(0) = qMax(1.0f, dimf(0));
			dimf(1) = qMax(1.0f, dimf(1));

			m_infl_rad = infl_rad;
			set_dim(dimf.cast<int>());

			int required_space = cell_count();
			int old_size = m_cells.size();

			if (clear) {
				int clear_range = (required_space < old_size) ? required_space : old_size;
				for (int i = 0; i < clear_range; i++) {
					Access::get_ids(m_cells[i]).clear();
				}
			}

			if (m_cells.size() < required_space) {
				m_cells.resize(required_space);
			}

			m_data = m_cells.data();
		}

		void reset() {
			m_cells.clear();
			m_register_step = -1;
		}

		CELL& cell(int i, int j) {
			return m_data[idx(i, j)];
		}
		CELL& cell(int idx) {
			return m_data[idx];
		}
		CELL& cell(Vector2i coord) {
			return m_data[idx(coord)];
		}

		int							m_register_step;
		float						m_infl_rad;

	protected:
		std::vector<CELL>			m_cells;
		CELL*						m_data = nullptr;
	};

	template<typename CELL>
	class RefGrid : public Grid<CELL> {
	public:
		void require(float infl_rad, bool clear = false) {
			if (infl_rad > 0)
			{
				// need double the influence radius to guarantee finding everything in range
				Vector2f dimf = range() / (infl_rad * 2);
				dimf(0) = qMax(1.0f, dimf(0));
				dimf(1) = qMax(1.0f, dimf(1));

				m_infl_rad = infl_rad;
				set_dim(dimf.cast<int>());
			}
		}
		void set_data_ptr(CELL* a) {
			m_data = a;
		}
	};

	struct FindResult4Cell {
		int infl_count[4]; // each cell has an amount of particles
		int* infl_ids[4]; // the particle id's are in this list
		inline int total_count() {
			return infl_count[0] + infl_count[1] + infl_count[2] + infl_count[3];
		}
		bool validate(int id_count);
	};

	struct FindResultMultiCell {
		struct VirtualAgent {
			int agent_id;
			Vector2f virtual_location;
			bool through_portal;
		};
		ReuseArray<VirtualAgent> virtual_agents;
	};

	NeighbourFinder(OpenGLView& f);
	~NeighbourFinder();

	void reset(AABB2 region);
	void render(RenderContext rc);

	void debug_neighbours(SimulationCore& sc);

	struct NearestObstaclePointFinder {
	public:
		inline NearestObstaclePointFinder(SimulationCore& sc, float max_dist)
			: m_obstacle_list(sc.environment().obstacles())
		{
			// compute required grid
			Vector2f dimf = sc.environment().bbox().sizes() / (max_dist * 2);
			dimf(0) = qMax(1.0f, dimf(0));
			dimf(1) = qMax(1.0f, dimf(1));
			Vector2i dim = dimf.cast<int>();

			m_raster = { sc.environment().bbox(), dim };
			m_max_dist2 = max_dist * max_dist;

			// build / request updated grid
			m_pObstacle_register_field = sc.environment().obstacleFields().get_obstacle_field_list(
				m_raster, true, sc.step_id(), nullptr);
		}

		bool nearest_obstacle_point(Vector2f p, Vector2f& out_point);

	private:
		Raster						m_raster;
		std::vector<ObstacleCell>*	m_pObstacle_register_field;
		std::vector<Obstacle>&		m_obstacle_list;
		float						m_max_dist2;
	};

	/*
		Attempt to remove all collisions between agents
		@positions, the positions to modify
		@max_iter, limit on the amount of iterations, if 0: the mde_min_iter setting will be used
		@keep_state, if the coarseness stays the same, and registration has been done, reuse that
	*/
	bool enforce_min_dist(
		SimulationCore& sc, VecX<float>& positions, VecX<float>& displacement,
		int max_iter = 0, bool keep_state = false
	);

	// getters
	int fullest_cell_elementcount();
	int agent_count(int i, int j);

	// direct control
	void update_agent_query_grid(SimulationCore& sc, float infl_rad);
	void update_agent_query_grid(SimulationCore& sc, VecX<float>& positions, float infl_rad);
	//bool push_outward(Vec2Acces<float> a, Vec2Acces<float> b, float min_dist);
	bool push_outward_collide(SimulationCore& sc,
		const Vector2f& a_pos, const Vector2f& b_pos,
		int out_ai, int out_bi,
		float min_dist, VecX<float>& displacement);
	bool push_outward_direct(const Vector2f& a_pos, const Vector2f& b_pos, float min_dist,
		Vec2Acces<float> out_a_pos, Vec2Acces<float> out_b_pos);

	void require_obstacle_register(SimulationCore& sc, float inf_rad = -1);

	void find_nearby_agents_virtual(SimulationCore& sc, Vector2f loc, FindResultMultiCell& frmc);

	void find_nearby_agents_inrange(SimulationCore& sc, Vector2f loc, FindResultMultiCell& frmc, float rad);

	inline float find_nearby_obstacles_range() {
		return m_obstacle_grid.influence_range();
	}

	void	trace_nearby_obstacles(std::vector<std::array<Vector2f, 2>>, std::vector<int>& out_ids);

	// returns only those particles inside the range
	void find_nearby_agents_rad(Vector2f loc, std::vector<int>& out_ids, VecX<float>& positions, float rad);
	// based on nearby particles
	float compute_dens(SimulationCore& sc, Vector2f pos, Vector2f vel, float dt, Poly6 kernel, FindResultMultiCell& cache, Vector2f near_obstacle_point);

	void toggle_render() {
		m_draw_grid = !m_draw_grid;
	}
	bool render_enabled() {
		return m_draw_grid;
	}

	Raster obstacle_raster() { return m_obstacle_grid; }

	inline ObstacleCell& obstacles_in_cell(Vector2i coord) {
		return m_obstacle_grid.cell(coord);
	}

	inline Raster::Block find_nearby_obstacles_single_floor(Vector2f loc, std::vector<int>& out_ids) {
		return find_nearby_single_floor(m_obstacle_grid, loc, out_ids);
	}
	inline Raster::Block find_nearby_obstacles_single_floor(Vector2f loc, FindResult4Cell& fr4c) {
		return find_nearby_single_floor(m_obstacle_grid, loc, fr4c);
	}
	inline Raster::Block find_nearby_obstacles_single_floor(Vector2f loc, int** out_4_id_arr, int* out_4_count) {
		return find_nearby_single_floor(m_obstacle_grid, loc, out_4_id_arr, out_4_count);
	}

	float pref_infl_rad_agents_collide(SimulationCore&);
	float pref_infl_rad_obstacles_collide(SimulationCore&);

private:

	inline Raster::Block find_nearby_agents_single_floor(Vector2f loc, FindResult4Cell& fr4c) { return find_nearby_single_floor(m_agent_grid, loc, fr4c); }
	inline Raster::Block find_nearby_agents_single_floor(Vector2f loc, std::vector<int>& out_ids) { return find_nearby_single_floor(m_agent_grid, loc, out_ids); }
	inline Raster::Block find_nearby_agents_single_floor(Vector2f loc, int** out_4_id_arr, int* out_4_count) { return find_nearby_single_floor(m_agent_grid, loc, out_4_id_arr, out_4_count); }

	enum MDE_TYPE {
		Delegate, Direct
	} mde_type;

	class IMde {
	public:
		IMde(NeighbourFinder& nf);
		virtual void start(int nr_agents, float rad) = 0;
		virtual void grid_updated(SimulationCore& sc, Grid<AgentCell>& agents, Grid<ObstacleCell>& obstacles, VecX<float>& positions) {};
		virtual int step(SimulationCore& sc, Grid<AgentCell>& agents, Grid<ObstacleCell>& obstacles, VecX<float>& positions, VecX<float>& displacement) = 0;
	protected:
		NeighbourFinder & nf;
	};

	class DirectMDE : public IMde
	{
	public:
		DirectMDE(NeighbourFinder& nf);
		virtual void start(int agent_count, float rad) override;
		virtual int step(SimulationCore& sc, Grid<AgentCell>& agents, Grid<ObstacleCell>& obstacles, VecX<float>& positions, VecX<float>& displacement) override;
	private:
		NeighbourFinder::FindResult4Cell fr_cache;
		int nr_agents;
		float rad;
	} m_direct_mde;

	// returns the 4 grid cells that are surrounding the location directly
	template<typename CELL>
	Raster::Block find_nearby_single_floor(Grid<CELL>& grid, Vector2f loc, int** out_4_id_arr, int* out_4_count)
	{
		Raster::Block block = grid.find_block(loc);

		for (int i = 0; i < 4; i++) out_4_count[i] = 0;

		// 4 regions with potential elements

		if (block.l >= 0)
		{
			// left bottom:
			if (block.b >= 0) {
				int idx = grid.idx(block.l, block.b);
				out_4_count[0] = (int)Access::get_ids(grid.cell(idx)).size();
				out_4_id_arr[0] = Access::get_ids(grid.cell(idx)).data();
			}

			// left top
			if (block.t < grid.resolution(1)) {
				int idx = grid.idx(block.l, block.t);
				out_4_count[1] = (int)Access::get_ids(grid.cell(idx)).size();
				out_4_id_arr[1] = Access::get_ids(grid.cell(idx)).data();
			}
		}

		if (block.r < grid.resolution(0)) {
			// right bottom
			if (block.b >= 0) {
				int idx = grid.idx(block.r, block.b);
				out_4_count[2] = (int)Access::get_ids(grid.cell(idx)).size();
				out_4_id_arr[2] = Access::get_ids(grid.cell(idx)).data();
			}

			// right top
			if (block.t < grid.resolution(1)) {
				int idx = grid.idx(block.r, block.t);
				out_4_count[3] = (int)Access::get_ids(grid.cell(idx)).size();
				out_4_id_arr[3] = Access::get_ids(grid.cell(idx)).data();
			}
		}

		return block;
	}

	//
	template<typename CELL>
	Raster::Block find_nearby_single_floor(Grid<CELL>& grid, Vector2f loc, FindResult4Cell& result)
	{
		return find_nearby_single_floor(grid, loc, (int**)(result.infl_ids), (int*)result.infl_count);
	}

	// fills the array with the entries in the 4 surrounding cells
	template<typename CELL>
	Raster::Block find_nearby_single_floor(Grid<CELL>& grid, Vector2f loc, std::vector<int>& out_ids)
	{
		out_ids.clear();

		int cell_counts[4]; // each cell has an amount of particles
		int* cell_entries[4]; // the particle id's are in these lists
		auto block = find_nearby_single_floor(grid, loc, cell_entries, cell_counts);

		for (int q = 0; q < 4; q++) {
			for (int o_idx = 0, n = cell_counts[q]; o_idx < n; o_idx++) {
				int pid = cell_entries[q][o_idx];
				out_ids.push_back(pid);
			}
		}

		return block;
	}

	void update_vis();
	int refresh_rate_agent(SimulationCore&, float cur_infl_rad);


	Grid<AgentCell>							m_agent_grid;
	std::unordered_map<int, PortalCel>*		m_pAgent_portal_grid;

	RefGrid<ObstacleCell>					m_obstacle_grid;
	//MovableGridRef					m_movegrid;

	// counter for random direction:
	int								m_dir_idx = 0;

	// random direction array:
	std::vector<Vector2f>			m_directions;

	inline Vector2f next_dir() {
		m_dir_idx = (m_dir_idx + 1) % m_directions.size();
		return m_directions[m_dir_idx];
	}

	// ui:
	bool			m_draw_grid = false;
	bool			m_need_vis_update = false;
	GridDrawer2D*	m_pGridDrawer;
};