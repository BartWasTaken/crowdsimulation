#include "sim/Support/neighbourfinder.h"
#include "mainwindow.h"
#include <QtMath>
#include <QDebug>
#include <unordered_set>

NeighbourFinder::NeighbourFinder(OpenGLView& f)
	: m_direct_mde(*this)
{
	m_pGridDrawer = new GridDrawer2D(f);

	m_directions = {
		Vector2f{ 0,1 }.normalized(),
		Vector2f{ 1,1 }.normalized(),
		Vector2f{ 1,0 }.normalized(),
		Vector2f{ 1,-1 }.normalized(),
		Vector2f{ 0,-1 }.normalized(),
		Vector2f{ -1,-1 }.normalized(),
		Vector2f{ -1,0 }.normalized(),
		Vector2f{ -1,1 }.normalized()
	};
}

NeighbourFinder::~NeighbourFinder()
{
	SafeDeletePtr(m_pGridDrawer);
}

void NeighbourFinder::reset(AABB2 region)
{
	m_agent_grid.reset();
	m_obstacle_grid.reset();

	m_agent_grid.set_region(region);
	m_obstacle_grid.set_region(region);
}

void NeighbourFinder::render(RenderContext rc)
{
	if (m_draw_grid)
	{
		if (m_need_vis_update) update_vis();
		rc.f.glEnable(GL_BLEND);
		rc.f.glxBlendFuncBasic();
		m_pGridDrawer->render(rc);
		rc.f.glDisable(GL_BLEND);
	}
}

void NeighbourFinder::debug_neighbours(SimulationCore& sc)
{
	update_agent_query_grid(sc, sc.settings().compute_sph_kernel_rad());

	std::unordered_map<int, std::vector<int>> pairs;

	int nr_agents = sc.nr_active_agents();
	FindResultMultiCell frmc;
	for (int i = 0; i < nr_agents; i++) {
		pairs[i] = {};

		find_nearby_agents_virtual(sc, sc.position(i), frmc);
		for (auto va : frmc.virtual_agents) {
			pairs[i].push_back(va.agent_id);
		}
	}

	for (int i = 0; i < nr_agents; i++) {
		for (int j : pairs[i]) {
			if (std::find(pairs[j].begin(), pairs[j].end(), i) == pairs[j].end()) {
				// wrong! not a bidirectional relation!
				find_nearby_agents_virtual(sc, sc.position(i), frmc);
				find_nearby_agents_virtual(sc, sc.position(j), frmc);
				sc.debug_wait_step();
			}
		}
	}
}


bool NeighbourFinder::NearestObstaclePointFinder::nearest_obstacle_point(Vector2f p, Vector2f& out_point)
{
	SurroundingCells info;
	info.make(p, m_raster);
	float best_dist2 = std::numeric_limits<float>::infinity();
	Vector2f best_point;

	for (int obst_j = info.y1; obst_j <= info.y2; obst_j++)
	{
		for (int obst_i = info.x1; obst_i <= info.x2; obst_i++)
		{
			auto id_list = (*m_pObstacle_register_field)[m_raster.idx(obst_i, obst_j)];

			for (int list_idx = 0, n = (int)id_list.size(); list_idx < n; list_idx++)
			{
				int obstacle_id = id_list[list_idx];

				Vector2f p_near = m_obstacle_list[obstacle_id].closestPointOnEdge_base(p);
				float temp_dist2 = (p - p_near).squaredNorm();

				if (temp_dist2 < best_dist2 && temp_dist2 < m_max_dist2)
				{
					best_dist2 = temp_dist2;
					best_point = p_near;
				}
			}
		}
	}

	if (best_dist2 != std::numeric_limits<float>::infinity()) {
		out_point = best_point;
		return true;
	}
	else return false;
}

void NeighbourFinder::trace_nearby_obstacles(std::vector<std::array<Vector2f, 2>> lines, std::vector<int>& out_ids)
{
	thread_local std::unordered_set<int> temp;
	temp.clear();

	for (auto l : lines) {
		LineTracer::rasterize_line(l[0], l[1], m_obstacle_grid, [this](Vector2i coord) -> bool {
			auto& obstacle_indices = m_obstacle_grid.cell(coord);
			temp.insert(obstacle_indices.begin(), obstacle_indices.end());
			return false;
		});
	}

	out_ids.clear();
	out_ids.insert(out_ids.end(), temp.begin(), temp.end());
}

void NeighbourFinder::find_nearby_agents_rad(Vector2f loc, std::vector<int>& out_ids, VecX<float>& positions, float rad)
{
	out_ids.clear();

	FindResult4Cell fr;
	find_nearby_single_floor(m_agent_grid, loc, fr);

	float radsqr = rad * rad;

	LOOP_4CELL(fr, i) {
		auto posi = VEC2_ACCES(positions, i);
		if ((posi - loc).squaredNorm() < radsqr) {
			out_ids.push_back(i);
		}
	}
}

void NeighbourFinder::update_agent_query_grid(SimulationCore& sc, float infl_rad)
{
	update_agent_query_grid(sc, sc.positions(), infl_rad);
}

void NeighbourFinder::update_agent_query_grid(SimulationCore& sc, VecX<float>& positions, float infl_rad)
{
	m_agent_grid.require(infl_rad, true);

#ifdef _DEBUG
	FOR_EACH_CELL_IDX(m_agent_grid, i, j, idx) {
		Q_ASSERT(m_agent_grid.cell(idx).ids.size() == 0);
	}
#endif

	m_agent_grid.m_register_step = sc.step_id();

	auto min = m_agent_grid.min();
	Vector2f range = m_agent_grid.range();

	auto nr_agents = sc.nr_active_agents();

	// enter agents:
	for (int i = 0; i < nr_agents; i++) {
		Vector2f pos = VEC2_ACCES(positions, i);
		Vector2i coord = m_agent_grid.safe_coords(pos);
		m_agent_grid.cell(coord).ids.push_back(i);
	}

	m_pAgent_portal_grid = sc.environment().obstacleFields().get_portal_field(m_agent_grid, sc.step_id());

#ifdef _DEBUG
	//FOR_EACH_CELL_IDX(m_agent_grid, i, j, idx) {
	//	auto& ids = m_agent_grid.cell(idx).ids;
	//	std::sort(ids.begin(), ids.end());
	//	Q_ASSERT(std::unique(ids.begin(), ids.end()) == ids.end());
	//}
#endif
}

bool NeighbourFinder::enforce_min_dist(
	SimulationCore& sc,
	VecX<float>& positions, VecX<float>& displacement,
	int in_max_iter,
	bool keep_state)
{
	// gather variables
	int max_iter = in_max_iter;
	if (max_iter <= 0) max_iter = sc.settings().mde_max_iter() - sc.frame_data().var(StatVars::MDE_ITER_COUNT);
	if (max_iter <= 0) max_iter = 1;

	//qInfo() << "MDE ITERATIONS :" << max_iter;

	auto nr_agents = sc.nr_active_agents();

	// check if we need to re-register
	bool has_data = sc.step_id() == m_agent_grid.m_register_step;

	if (!(keep_state && has_data)) {
		update_agent_query_grid(sc, positions, pref_infl_rad_agents_collide(sc));
	}

	int iter = 0;
	int collide_count = nr_agents; // need to initializee as max value
	bool grid_changed = true;

	IMde* mde = &m_direct_mde;
	mde->start(nr_agents, sc.settings().agent_rad());

	int rr = 1; // refresh_rate_agent(sc, m_agent_grid.m_infl_rad); // TODO put back real code

	for (iter = 0; iter < max_iter && collide_count > 0; iter++)
	{
		// check the grid needs to be update
		if ((iter % (int)rr == rr - 1))
		{
			grid_changed = true;
			update_agent_query_grid(sc, positions, pref_infl_rad_agents_collide(sc));
			// we modified the grid coarseness now : update the refresh rate
			//rr = refresh_rate_agent(sc, m_agent_grid.m_infl_rad);
		}

		if (grid_changed) {
			grid_changed = false;
			mde->grid_updated(sc, m_agent_grid, m_obstacle_grid, positions);
		}

		collide_count = mde->step(sc, m_agent_grid, m_obstacle_grid, positions, displacement);
	}

	sc.frame_data().var(StatVars::MDE_ITER_COUNT) += iter;
	sc.frame_data().var(StatVars::COLLISION_LEFT) = collide_count;

	return collide_count == 0;
}

void NeighbourFinder::update_vis()
{
	m_need_vis_update = false;
	
	/*{
		m_pGridDrawer->make_grid(m_obstacle_grid.min(), m_obstacle_grid.range(), m_obstacle_grid.resolution(), true, false, false, 0.02f);
		
		FOR_EACH_CELL(m_obstacle_grid, x, y) {
			if (m_obstacle_grid.cell(x, y).size() > 0) {
				m_pGridDrawer->set_block_color(x, y, { 255,0,0,128 });
			}
			else {
				m_pGridDrawer->set_block_color(x, y, { 0,255,0,128 });
			}
		}
	}*/

	{
		m_pGridDrawer->make_grid(m_agent_grid.min(), m_agent_grid.range(), m_agent_grid.resolution(), true, false, false, 0.02f);
		FOR_EACH_CELL_IDX(m_agent_grid, x, y, idx) {
			auto found = (*m_pAgent_portal_grid).find(idx);
			if (found != (*m_pAgent_portal_grid).end()) {
				m_pGridDrawer->set_block_color(x, y, { 0,0,255,128 });
			}
			else {
				m_pGridDrawer->set_block_color(x, y, { 0,0,0,50 });
			}
		}
	}

	m_pGridDrawer->update();
}


float NeighbourFinder::pref_infl_rad_agents_collide(SimulationCore& sc)
{
	return sc.settings().agent_rad() * 2;
}

float NeighbourFinder::pref_infl_rad_obstacles_collide(SimulationCore& sc)
{
	return sc.settings().agent_rad();
}

int NeighbourFinder::refresh_rate_agent(SimulationCore& sc, float cur_infl_rad)
{
	return qMax(1.0f, cur_infl_rad / (sc.settings().agent_rad() * 2));
}

/*bool NeighbourFinder::push_outward(Vec2Acces<float> a, Vec2Acces<float> b, float min_dist)
{
	Vector2f diff = (a - b);
	float dist = diff.norm();
	auto err = min_dist - dist;
	if (err > 0) {
		Vector2f offset;
		if (dist < Geometry::Epsilon) offset = next_dir() * err / 2.0f;
		else offset = diff * (err + 0.005f * min_dist) / (2.0f * dist);
		a += offset;
		b -= offset;
		return true;
	}
	else return false;
}*/

bool NeighbourFinder::push_outward_collide(SimulationCore& sc, const Vector2f& a, const Vector2f& b,
	int out_ai, int out_bi, float min_dist, VecX<float>& displacement)
{
	Vector2f diff = (a - b);
	float dist2 = diff.squaredNorm();
	auto err2 = (min_dist * min_dist) - dist2;

	if (err2 > 0)
	{
		float dist = qSqrt(dist2);
		float err = min_dist - dist;

		Vector2f offset;
		if (dist < Geometry::Epsilon) offset = 0.51f * err * next_dir();
		else offset = 0.51f * err * (diff / dist);// diff * (err + 0.005f * min_dist) / (2.0f * dist);
		VEC2_ACCES(displacement, out_ai) += sc.try_move(out_ai, offset);
		VEC2_ACCES(displacement, out_bi) += sc.try_move(out_bi, -offset);
		//a += m_push_offset;
		//b -= offset;
		return true;
	}
	else return false;
}

bool NeighbourFinder::push_outward_direct(const Vector2f& a_pos, const Vector2f& b_pos, float min_dist, Vec2Acces<float> out_a_pos, Vec2Acces<float> out_b_pos)
{
	Vector2f diff = (a_pos - b_pos);
	float dist2 = diff.squaredNorm();
	auto err2 = (min_dist * min_dist) - dist2;

	if (err2 > 0)
	{
		float dist = qSqrt(dist2);
		float err = min_dist - dist;

		Vector2f offset;
		if (dist < Geometry::Epsilon) offset = next_dir() * err / 2.0f;
		else offset = 0.51f * err * (diff / dist);// diff * (err + 0.005f * min_dist) / (2.0f * dist);
		out_a_pos += offset;
		out_b_pos -= offset;
		return true;
	}
	else return false;
}

void NeighbourFinder::require_obstacle_register(SimulationCore& sc, float infl_rad)
{
	if (infl_rad < 0) {
		infl_rad = pref_infl_rad_obstacles_collide(sc);
	}

	if (m_obstacle_grid.m_register_step < sc.step_id() || m_obstacle_grid.m_infl_rad != infl_rad)
	{
		m_obstacle_grid.m_register_step = sc.step_id();

		if (infl_rad < 0) {
			infl_rad = pref_infl_rad_obstacles_collide(sc);
		}

		m_obstacle_grid.require(infl_rad, true);

		auto& obstacles = sc.environment().obstacles();
		int obst_count = obstacles.size();

		auto* pVector = sc.environment().obstacleFields().get_obstacle_field_list(m_obstacle_grid, true, sc.step_id());
		m_obstacle_grid.set_data_ptr(pVector->data());

		m_need_vis_update = true;
	}
}

void NeighbourFinder::find_nearby_agents_virtual(SimulationCore& sc, Vector2f loc, FindResultMultiCell& frmc) //void find_nearby_agents_virtual(SimulationCore& sc, Vector2f loc, FindResultMultiCell& frmc)
{
	frmc.virtual_agents.clear();

	require_obstacle_register(sc, -1);

	bool debug_can_have_portal_agents = false;

	int nr_agents = sc.nr_active_agents();
	float infl_rad_2 = m_agent_grid.m_infl_rad * m_agent_grid.m_infl_rad;
	auto& grid = m_agent_grid;

	int idx_list[4];
	Raster::Block block = grid.find_block(loc);
	block.fill_idx_list(idx_list, grid);

	// the cell in which the location of interest lies
	auto origin_cell_id = grid.coords(loc);
	// if this cell is near a portal, a PortalCell exists, check if this is the case
	auto origin_portal_cell_it = (*m_pAgent_portal_grid).find(grid.idx(origin_cell_id));
	// create a variable for wether portals are nearby (a portal cell must exist, and it must verify that a portal is near)
	bool portals_near = (origin_portal_cell_it == (*m_pAgent_portal_grid).end()) ? false : origin_portal_cell_it->second.is_near_portal();

	Obstacle::HitInfo info;
	std::vector<int> obstacle_cache;
	PortalCel::ProjectInfo project_info;

	// in local space, for each of the 4 closest cells
	for (int corner = 0; corner < 4; corner++)
	{
		// the index of that corner cell
		int corner_idx = idx_list[corner];

		// if that corner is out of the defined grid, the value is -1, so check if its valid
		if (corner_idx >= 0)
		{
			// the cell representing the corner
			auto& corner_cell = grid.cell(corner_idx);

			// the cell representing the portal information at the corner
			PortalCel* p_corner_portal_cell = nullptr;
			bool corner_near_portal = false;
			auto corner_portal_ref = m_pAgent_portal_grid->find(corner_idx);
			if (corner_portal_ref != m_pAgent_portal_grid->end()) {
				p_corner_portal_cell = &corner_portal_ref->second;
				corner_near_portal = p_corner_portal_cell->is_near_portal();
			}

			// reserve space for the id's that might be added (a little too much isnt bad since the memory is reused every frame)
			frmc.virtual_agents.require_free_space((int)corner_cell.ids.size());

			// for each agent id found in that corner
			for (auto i : corner_cell.ids) {

				Q_ASSERT(i >= 0 && i < nr_agents);

				// avoid if on opposite sides of a portal (since they should be vertically different)
				if (corner_near_portal || portals_near) {
					//nf.find_nearby_obstacles_single_floor(sc.position(agent_i), obstacle_cache);
					trace_nearby_obstacles({ { loc, sc.position(i) } }, obstacle_cache);
					Obstacle::hit_single_collider(sc, loc, sc.position(i), obstacle_cache, info, false);

					if (info.has_hit) {
						continue;
					}
				}

				if ((sc.position(i) - loc).squaredNorm() <= infl_rad_2) {
					frmc.virtual_agents.push_back({ i, sc.position(i), false });
				}
			}

			if (project_info.portal.obst_id == -1 && p_corner_portal_cell != nullptr) {
				debug_can_have_portal_agents = true;
				p_corner_portal_cell->try_portal_project(sc, loc, project_info);
			}
		}
	}

	if (project_info.portal.obst_id != -1) {
		Q_ASSERT(debug_can_have_portal_agents);
		int idx_list_projected[4];
		Raster::Block projected_block = grid.find_block(project_info.projected_pos);
		projected_block.fill_idx_list(idx_list_projected, grid);

		for (int corner = 0; corner < 4; corner++)
		{
			int cell_idx = idx_list_projected[corner];

			if (cell_idx >= 0)
			{
				auto& projected_cell = grid.cell(cell_idx);

				frmc.virtual_agents.require_free_space((int)projected_cell.ids.size());

				// for each cell in the projected space
				for (auto& i : projected_cell.ids)
				{
					Q_ASSERT(i >= 0 && i < sc.nr_active_agents());
					if (project_info.can_connect_through_portal(project_info.projected_pos, sc.position(i))) {
						auto dist_2 = (sc.position(i) - project_info.projected_pos).squaredNorm();
						if (dist_2 <= infl_rad_2) {
							frmc.virtual_agents.push_back({ i, loc + (sc.position(i) - project_info.projected_pos), true });
						}
					}
				}
			}
		}

	}



	// for each of the corners
	//		look for a correctly oriented portal 
	//		if found, stop

	// for the portal
	//		project
	//		

	//auto& move_cell = m_movegrid[cell_idx];
}

void NeighbourFinder::find_nearby_agents_inrange(SimulationCore& sc, Vector2f loc, FindResultMultiCell& frmc, float rad)
{
	auto cell_bl = m_agent_grid.safe_coords(loc - Vector2f{ rad, rad });
	auto cell_tr = m_agent_grid.safe_coords(loc + Vector2f{ rad, rad });

	for (int iy = cell_bl(1); iy <= cell_tr(1); iy++)
	{
		for (int ix = cell_bl(0); ix <= cell_tr(0); ix++)
		{
			auto& cell = m_agent_grid.cell(ix, iy);
			frmc.virtual_agents.require_free_space(cell.ids.size());

			for (auto id : cell.ids)
			{
				auto loc2 = sc.position(id);

				if ((loc - loc2).squaredNorm() <= rad * rad) {
					frmc.virtual_agents.push_back(FindResultMultiCell::VirtualAgent{ id, loc2 });
				}
			}
		}
	}
}

float NeighbourFinder::compute_dens(SimulationCore& sc, Vector2f pos, Vector2f vel, 
	float dt, Poly6 kernel, FindResultMultiCell& fr_cache, Vector2f near_obstacle_point)
{
	Q_ASSERT(fr_cache.virtual_agents.size() > 0);

	float dens = 0;

	for (auto va : fr_cache.virtual_agents) {
		Vector2f dir = pos - va.virtual_location;
		//Vector2f dvel = vel - sc.velocity(va.agent_id);
		dens += kernel.get(dir);// + dt * dvel.dot(kernel.grad(dir));
	}

	if (near_obstacle_point(0) != std::numeric_limits<float>::infinity()) {
		dens += kernel.get(pos - near_obstacle_point);
	}

	return dens;
}

int NeighbourFinder::fullest_cell_elementcount()
{
	int max = 0;
	for (int j = 0; j < m_agent_grid.resolution(1); j++) {
		for (int i = 0; i < m_agent_grid.resolution(0); i++) {
			auto x = m_agent_grid.cell(i, j).ids.size();
			if (x > max) max = x;
		}
	}
	return max;
}

int NeighbourFinder::agent_count(int i, int j)
{
	return m_agent_grid.cell(i, j).ids.size();
}

NeighbourFinder::DirectMDE::DirectMDE(NeighbourFinder& nf)
	: IMde(nf)
{

}

void NeighbourFinder::DirectMDE::start(int agent_count, float rad)
{
	this->nr_agents = agent_count;
	this->rad = rad;
}

int NeighbourFinder::DirectMDE::step(SimulationCore& sc, Grid<AgentCell>& agents, Grid<ObstacleCell>& obstacles, VecX<float>& positions, VecX<float>& displacement)
{
	auto& obstacle_list = sc.environment().obstacles();

	FindResultMultiCell frmc;

	float edge_offset = sc.settings().agent_rad() * 0.0001f;

	int collisions = 0;
	for (int i = 0; i < nr_agents; i++)
	{
		auto posi = VEC2_ACCES(positions, i);

		nf.find_nearby_agents_virtual(sc, posi, frmc);

		for (auto va : frmc.virtual_agents)
		{
			int j = va.agent_id;

			if (i < j) {
				auto posj = VEC2_ACCES(positions, j);

				//if (nf.push_outward_direct(posi, posj, rad * 2, VEC2_ACCES(positions, i), posj)) collisions++;
				if (nf.push_outward_collide(sc, posi, posj, i, j, rad * 2, displacement)) collisions++;;

			}
		}
	}
	
	for (int i = 0; i < nr_agents; i++) 
	{
		auto posi = VEC2_ACCES(positions, i);

		nf.find_nearby_single_floor(obstacles, posi, fr_cache);
		LOOP_4CELL(fr_cache, j) {
			if (obstacle_list[j].agent_collider_bloat().contains(posi)) {
				Vector2f surface, normal;
				obstacle_list[j].agent_collider_bloat().project(posi, surface, normal);
				posi = surface - normal * edge_offset;
				collisions++;
			}
		}
	}
	
	return collisions;
}

NeighbourFinder::IMde::IMde(NeighbourFinder& n)
	: nf(n)
{

}

bool NeighbourFinder::FindResult4Cell::validate(int id_count)
{
	for (int i = 0; i < 4; i++) {
		if (infl_count[i] < 0) return false;
		for (int j = 0; j < infl_count[i]; j++) {
			int value = infl_ids[i][j];
			if (value < 0) return false;
			if (value >= id_count) return false;
		}
	}
	return true;
}