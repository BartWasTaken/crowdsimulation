#include "obstaclegridprovider.h"
#include "sim/IEnvironment.h"
#include "sim/simulationcore.h"
#include "sim/Support/neighbourfinder.h"
#include "sim/collisionresolver.h"

template<typename T, typename RG, typename Store>
std::vector<T>* ObstacleGridProvider::get_obstacle_field(Store& store, Raster r, RG& rg, bool full_cover, int step_id, bool* is_changed)
{
	// the local storage with info on the object
	StoredGrid<T>* pStoredGrid;
	// the key to identify the stored object
	auto key = Key_coverage{ full_cover, r };

	// try to see if the required grid already exists
	auto find_result = store.find(key);
	if (find_result != store.end()) {
		pStoredGrid = &find_result->second;
	}
	else {
		// if not, add it
		auto emplace_result = store.emplace(key, StoredGrid<T>());
		auto inserted_element_iterator = emplace_result.first;
		pStoredGrid = &inserted_element_iterator->second;
	}

	rg.set_grid(pStoredGrid->arr);

	// if the obstacle grind changed, update and return that it changed
	if (pStoredGrid->last_update < m_env.last_obstacle_changed_step()) {
		pStoredGrid->last_update = step_id;
		build_grid(rg, r, full_cover, m_env.obstacles());
		if (is_changed != nullptr) *is_changed = true;
	}
	// otherwise return that it changed if it did this frame
	else {
		if (is_changed != nullptr) {
			bool was_changed_on_this_step = (pStoredGrid->last_update == m_env.last_obstacle_changed_step());
			*is_changed = was_changed_on_this_step;
		}
	}

	return pStoredGrid->arr;
}

ObstacleGridProvider::ObstacleGridProvider(IEnvironment& env)
	: m_env(env)
{

}

std::vector<std::vector<int>>* ObstacleGridProvider::get_obstacle_field_list(Raster r, bool full_cover, int step_id, bool* is_changed)
{
	RegisterGrid_list rg(r.resolution());
	return get_obstacle_field<std::vector<int>, RegisterGrid_list>(m_store_list, r, rg, full_cover, step_id, is_changed);
}

std::vector<bool>* ObstacleGridProvider::get_obstacle_field_bool(Raster r, bool full_cover, int step_id, bool* is_changed)
{
	RegisterGrid_value<bool> rg(r.resolution(), true);
	return get_obstacle_field<bool, RegisterGrid_value<bool>>(m_store_bool, r, rg, full_cover, step_id, is_changed);
}

std::vector<int>* ObstacleGridProvider::get_obstacle_field_count(Raster r, bool full_cover, int step_id, bool* is_changed)
{
	RegisterGrid_count rg(r.resolution());
	return get_obstacle_field<int, RegisterGrid_count>(m_store_count, r, rg, full_cover, step_id, is_changed);
}

std::unordered_map<int, PortalCel>* ObstacleGridProvider::get_portal_field(Raster r, int step_id, bool* is_changed /*= nullptr*/)
{
	StoredData<std::unordered_map<int, PortalCel>>* result = nullptr;
	auto key = Key_portal{ r };

	auto find_result = m_store_portal.find(key);
	if (find_result != m_store_portal.end()) {
		result = &find_result->second;
	}
	else {
		auto emplace_result = m_store_portal.emplace(key, StoredData<std::unordered_map<int, PortalCel>>());
		auto inserted_element_iterator = emplace_result.first;
		result = &inserted_element_iterator->second;
	}

	if (result->last_update < m_env.last_obstacle_changed_step())
	{
		// write:
		result->last_update = step_id;
		result->data->clear();

		auto p_iter = m_env.get_portal_iterator();

		while (p_iter.next()) {
			LineTracer::rasterize_line(p_iter.info.start.a, p_iter.info.start.b, r, [this, &p_iter, &result, &r](Vector2i coord) -> bool {
				(*(result->data))[r.idx(coord)].add_portal(p_iter.info);
				return false;
			});
		}

		if (is_changed) *is_changed = true;
	}
	else {
		if (is_changed) *is_changed = false;
	}

	return result->data;
}

MovableGridRef ObstacleGridProvider::get_movable_field(SimulationCore& sc, Raster r, bool* out_has_moved)
{
	StoredGrid<MoveInfo>* result = nullptr;
	auto key = Key_passable{ r };

	auto find_result = m_store_pass.find(key);
	if (find_result != m_store_pass.end()) {
		result = &find_result->second;
	}
	else {
		// if not, add it
		auto emplace_result = m_store_pass.emplace(key, StoredGrid<MoveInfo>());
		auto inserted_element_iterator = emplace_result.first;
		result = &inserted_element_iterator->second;
	}

	if (result->last_update < m_env.last_obstacle_changed_step())
	{
		if (out_has_moved) *out_has_moved = true;
		result->last_update = sc.step_id();

		auto* pGrid = result->arr; // new std::vector<std::array<bool, 4>>();
		pGrid->clear();
		pGrid->resize(r.cell_count());

		// all directions to the outside are impassable:

		int xMax = r.resolution(0) - 1;
		int yMax = r.resolution(1) - 1;

		// all crossings that go over an obstacle are impassable:

		std::vector<int> obstacle_ids_t;
		std::vector<int> obstacle_ids_r;
		std::vector<int> obstacle_ids_b;
		std::vector<int> obstacle_ids_l;

		auto& obst_list = sc.environment().obstacles();
		sc.collision_resolver().neighbourFinder().require_obstacle_register(sc);

		FOR_EACH_CELL_IDX(r, x, y, idx)
		{
			auto& cell_ref = (*pGrid)[idx];
			int top_cell_idx = r.idx(x, y + 1);
			int right_cell_idx = r.idx(x + 1, y);

			bool left = (x != 0);
			bool right = (x != xMax);
			bool bottom = (y != 0);
			bool top = (y != yMax);

			auto cell_center = r.cell_center(x, y);
			auto cell_center_l = r.cell_center(x - 1, y);
			auto cell_center_r = r.cell_center(x + 1, y);
			auto cell_center_b = r.cell_center(x, y - 1);
			auto cell_center_t = r.cell_center(x, y + 1);

			float dx = cell_center_r(0) - cell_center(0);
			float dy = cell_center_t(1) - cell_center(1);

			// only to top and right + mirrors, unless in left or bottom row

			// TODO: the "nearby" grid must be at least the size of the resolution of the raster used here
			// to use this method the finder grid must be at least the resolution of the raster
			auto& nf = sc.collision_resolver().neighbourFinder();

			//if (nf.find_nearby_obstacles_range() >= r.influence_range() * 2.1)
			//{
			//	sc.collision_resolver().neighbourFinder().find_nearby_obstacles_single_floor(cell_center, obstacle_ids);
			//}
			//else
			{
				nf.trace_nearby_obstacles({
					{ cell_center, cell_center_l },
					}, obstacle_ids_l);
				nf.trace_nearby_obstacles({
					{ cell_center, cell_center_r },
					}, obstacle_ids_r);
				nf.trace_nearby_obstacles({
					{ cell_center, cell_center_b },
					}, obstacle_ids_b);
				nf.trace_nearby_obstacles({
					{ cell_center, cell_center_t }
					}, obstacle_ids_t);
			}

			for (auto& obst_id : obstacle_ids_t) {
				auto& obst = obst_list[obst_id];
				if (obst.agent_collider().contains(cell_center)) {
					cell_ref.inside_obj = true;
				}
			}

			// disable moving outside the grid:
			if (!left) cell_ref.disable_normal_move(Dir4::W);
			if (!right) cell_ref.disable_normal_move(Dir4::E);
			if (!bottom) cell_ref.disable_normal_move(Dir4::S);
			if (!top) cell_ref.disable_normal_move(Dir4::N);

			Obstacle::HitInfo info;

			if (right) {
				Obstacle::hit_single_collider(sc, cell_center, cell_center_r, obstacle_ids_r, info, false);
				cell_ref.build(sc, info, r, cell_center, cell_center_r, Dir4::E);
			}
			if (left) {
				Obstacle::hit_single_collider(sc, cell_center, cell_center_l, obstacle_ids_l, info, false);
				cell_ref.build(sc, info, r, cell_center, cell_center_l, Dir4::W);
			}
			if (top) {
				Obstacle::hit_single_collider(sc, cell_center, cell_center_t, obstacle_ids_t, info, false);
				cell_ref.build(sc, info, r, cell_center, cell_center_t, Dir4::N);
			}
			if (bottom) {
				Obstacle::hit_single_collider(sc, cell_center, cell_center_b, obstacle_ids_b, info, false);
				cell_ref.build(sc, info, r, cell_center, cell_center_b, Dir4::S);
			}

			/*for (auto it = obstacle_ids.begin(); it < obstacle_ids.end(); it++)
			{
			if (right && m_env.obstacles()[*it].intersects_outer(cell_center, cell_center_r))
			{
			cell_ref.disable_normal_move(Dir4::E);
			if (right) (*pGrid)[right_cell_idx].disable_normal_move(Dir4::W);
			}
			if (top && m_env.obstacles()[*it].intersects_outer(cell_center, cell_center_t))
			{
			cell_ref.disable_normal_move(Dir4::N);
			if (top) (*pGrid)[top_cell_idx].disable_normal_move(Dir4::S);
			}

			// the portal ones are a bit trickier?
			}*/

		}
		// CHECK VALIDITY:

		/*FOR_EACH_CELL_IDX(r, i, j, idx)
		{
		for (int d = 0; d < Dir4::DIR_COUNT; d++)
		{
		int i2 = i + Dir4::dir_x[d];
		int j2 = j + Dir4::dir_y[d];

		if (r.is_coord_valid({ i2, j2 }))
		{
		int id2 = r.idx(i2, j2);
		auto& cell_ref_mine = (*pGrid)[idx];
		auto& cell_ref_other = (*pGrid)[id2];
		Q_ASSERT(cell_ref_mine[d] == cell_ref_other[Dir4::opposed(d)]);
		}
		}
		}*/
	}
	else {
		if (out_has_moved) *out_has_moved = false;
	}

	return MovableGridRef(result->arr);
}

void ObstacleGridProvider::reset()
{
	for (auto& pair : m_store_bool) {
		pair.second.last_update = -1;
	}
	for (auto& pair : m_store_list) {
		pair.second.last_update = -1;
	}
	for (auto& pair : m_store_count) {
		pair.second.last_update = -1;
	}
	for (auto& pair : m_store_pass) {
		pair.second.last_update = -1;
	}
}

void ObstacleGridProvider::MoveInfo::build(SimulationCore& sc, Obstacle::HitInfo& info, Raster& r, Vector2f center, Vector2f target, Dir4::DIR dir)
{
	if (info.has_hit)
	{
		if (info.is_portal)
		{
			// check if we move into the portal, then its a portal move
			// otherwise we need it as a normal move to get through the portal
			// (and it doesnt block the normal move)
			if (orientation_safe(center, info.p1, info.p2) != 1)
			{
				// get the correct cell on the other side:
				Vector2f projection = info.move_through_portal(center, target);// +(target - center) / 2.0f;
				Vector2i coords = r.coords(projection);
				portal_target_idx = r.idx(coords);

				// it might not be the correct cell, if the center lies on the wrong side of the portal
				if (orientation_safe(r.cell_center(coords), info.portal_p1, info.portal_p2) != 2) {
					projection = info.move_through_portal(center, target) + (target - center) / 2.0f;
					portal_target_idx = r.idx(projection);
				}

				// must now be able to move from the projection 
				// to the cell at portal_target_idx without obstacle intersection

				NeighbourFinder::FindResult4Cell fr4c;				
				Vector2f portal_end_center = r.cell_center(r.coords(portal_target_idx));
				sc.collision_resolver().neighbourFinder().find_nearby_obstacles_single_floor(portal_end_center, fr4c);
				
				LOOP_4CELL(fr4c, id) {
					auto& obst = sc.environment().obstacles()[id];
					Obstacle::HitInfo info;
					obst.hit_single_collider(sc, projection, portal_end_center, &id, 1, info, false);
					if (info.has_hit && !info.is_portal) {
						disable_normal_move(dir);
						return;
					}
				}

				portal_move = dir;
				portal_A_p1 = info.p1;
				portal_A_p2 = info.p2;
				portal_B_p1 = info.portal_p1;
				portal_B_p2 = info.portal_p2;

				disable_normal_move(dir);
			}
			// not else disable_normal_move(dir);
		}
		else {
			// it is an obstacle
			disable_normal_move(dir);
		}
	}
	// else : normal move is enabled by default
}


bool PortalCel::try_portal_project(SimulationCore& sc, Vector2f point, ProjectInfo& out)
{
	for (int i = 0; i < PORTAL_MAX; i++) {
		if (portals_exist[i]) {
			if (orientation_safe(point, portals[i].start.a, portals[i].start.b) != 1) {
				auto obst_id = portals[i].obst_id;
				if (portals[i].up) out = { portals[i], sc.environment().obstacles()[obst_id].project_through_portal_up(point) };
				else out = { portals[i], sc.environment().obstacles()[obst_id].project_through_portal_down(point) };
				return true;
			}
		}
	}
	return false;
}

void PortalCel::add_portal(PortalIterator::PortalDescriptor& info)
{
	for (int i = 0; i < PORTAL_MAX; i++) {
		if (!portals_exist[i]) {
			portals[i] = info;
			portals_exist[i] = true;
			return;
		}
	}
	Q_ASSERT(false); // there should only be PORTAL_MAX portals close by
}
