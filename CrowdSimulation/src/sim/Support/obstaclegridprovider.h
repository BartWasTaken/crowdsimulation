#pragma once
#include "src/sim/simdeclr.h"
#include "src/sim/obstacle.h"
#include "src/tools/include.h"
#include <vector>
#include <array>
#include <unordered_map>

struct MovableGridRef;

struct PortalCel {
	constexpr static int PORTAL_MAX = 2;
	PortalIterator::PortalDescriptor portals[PORTAL_MAX];
	bool portals_exist[PORTAL_MAX];

	struct ProjectInfo {
		PortalIterator::PortalDescriptor portal;
		Vector2f projected_pos;

		bool projected_space_correct_orientation(Vector2f point) {
			return (orientation_safe(point, portal.end.a, portal.end.b) != 1);
		}

		bool can_connect_through_portal(Vector2f projected_space_point, Vector2f goal_point) {
			if (orientation_safe(goal_point, portal.end.a, portal.end.b) == 1) return false;
			else {
				// should intersect the portal line
				return line_segment_intersection(projected_space_point, goal_point, portal.end.a, portal.end.b);
			}
		}
	};

	bool try_portal_project(SimulationCore& sc, Vector2f point, ProjectInfo& out);
	void add_portal(PortalIterator::PortalDescriptor& info);
	void reset_portal_info() {
		for (int i = 0; i < PORTAL_MAX; i++) {
			portals_exist[i] = false;
		}
	}
	bool is_near_portal() { return portals_exist[0]; } //  for (int i = 0; i < PORTAL_MAX; i++) if (portals_exist[i]) return true; }
};

class ObstacleGridProvider
{
public:
	ObstacleGridProvider(IEnvironment&);

	// pointer only valid for the current step
	std::vector<std::vector<int>>*		get_obstacle_field_list(Raster, bool full_cover, int step_id, bool* is_changed = nullptr);
	std::vector<bool>*					get_obstacle_field_bool(Raster, bool full_cover, int step_id, bool* is_changed = nullptr);
	std::vector<int>*					get_obstacle_field_count(Raster, bool full_cover, int step_id, bool* is_changed = nullptr);
	std::unordered_map<int, PortalCel>*	get_portal_field(Raster, int step_id, bool* is_changed = nullptr);


	struct MoveInfo {
		bool inside_obj = false;
		bool normal_move[4] = { true, true, true, true }; // true if can move
		Dir4::DIR portal_move = Dir4::NONE;
		int portal_target_idx;

		// these describe the local portal edge A and target portal edge B
		Vector2f portal_A_p1;
		Vector2f portal_A_p2;
		Vector2f portal_B_p1;
		Vector2f portal_B_p2;

		void build(SimulationCore& nf, Obstacle::HitInfo& info, Raster& r, Vector2f center, Vector2f target, Dir4::DIR dir);

		inline void				disable_normal_move(Dir4::DIR d) { normal_move[(int)d] = false; }

		inline bool				can_normal_move(int dir) { return normal_move[dir]; }

		inline bool				can_portal_move(int dir) { return portal_move == dir; }
		inline bool				can_portal_move() { return portal_move != Dir4::NONE; }
		inline bool				can_portal_move(Vector2f pos) {
			return portal_move && (orientation(pos, portal_A_p1, portal_A_p2) != 1);
		}
		inline Dir4::DIR		portal_move_dir() { return portal_move; }
		inline int				portal_move_dest_idx() { return portal_target_idx; }
	};

	MovableGridRef						get_movable_field(SimulationCore&, Raster, bool* out_has_moved = nullptr);

	void reset();

private:

	template<typename T, typename RG, typename Store>
	std::vector<T>*					get_obstacle_field(Store&, Raster, RG&, bool full_cover, int step_id, bool* is_changed);

	struct Key_coverage {
		bool				full_cover;
		Raster				raster;
		bool operator==(const Key_coverage& other) const { return full_cover == other.full_cover && raster == other.raster; }
	};
	struct KeyHash_coverage
	{
		std::size_t operator()(const Key_coverage& k) const
		{
			return k.raster.cell_count() + (int)k.full_cover;
		}
	};

	struct Key_passable {
		Raster				raster;
		bool operator==(const Key_passable& other) const { return raster == other.raster; }
	};
	struct KeyHash_passable
	{
		std::size_t operator()(const Key_passable& k) const
		{
			return k.raster.cell_count();
		}
	};

	struct Key_portal {
		Raster				raster;
		bool operator==(const Key_portal& other) const { return raster == other.raster; }
	};
	struct KeyHash_portal
	{
		std::size_t operator()(const Key_portal& k) const
		{
			return k.raster.cell_count();
		}
	};


	template<typename T>
	struct StoredGrid {
		StoredGrid() {
			arr = new std::vector<T>();
		}
		std::vector<T>* arr;
		int64_t last_update = -1;
	};
	
	template<typename T>
	struct StoredData  {
		StoredData() {
			data = new T();
		}
		T* data = nullptr;
		int64_t last_update = -1;
	};

	template<typename RG, typename source_items>
	void build_grid(RG&, Raster, bool full_cover, source_items);

	IEnvironment&																				m_env;
	std::unordered_map<Key_coverage, StoredGrid<std::vector<int>>, KeyHash_coverage>			m_store_list;
	std::unordered_map<Key_coverage, StoredGrid<bool>, KeyHash_coverage>						m_store_bool;
	std::unordered_map<Key_coverage, StoredGrid<int>, KeyHash_coverage>							m_store_count;

	std::unordered_map<Key_passable, StoredGrid<MoveInfo>, KeyHash_passable>					m_store_pass;

	std::unordered_map<Key_portal, StoredData<std::unordered_map<int, PortalCel>>, KeyHash_portal>	m_store_portal;
};

template<typename RG, typename source_items>
void ObstacleGridProvider::build_grid(RG& rg, Raster r, bool full_cover, source_items items)
{
	rg.reset(r.cell_count());

	//auto& obstacle_list = m_env.obstacles();
	int obstacle_count = items.size();

	for (int i = 0; i < obstacle_count; i++) {
		items[i].agent_collider_bloat().write_to_grid(rg, r, full_cover, i);
	}
}

struct MovableGridRef
{
public:
	inline MovableGridRef() {}
	inline MovableGridRef(std::vector<ObstacleGridProvider::MoveInfo>* data)
		: m_pData(data) {}

	inline ObstacleGridProvider::MoveInfo& operator[] (int idx) {
		return (*m_pData)[idx];
	}

private:
	std::vector<ObstacleGridProvider::MoveInfo>* m_pData;
};