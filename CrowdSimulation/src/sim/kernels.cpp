#include "kernels.h"

Poly6::Poly6(float h)
{
	h2 = h * h;
	c_base =
		4.0f / (M_PI * ipow(h, 8));
		//315.0f / (64 * M_PI * ipow(h, 9));
		//1 / ipow(h, 8);
	c_grad =
		-24.0f / (M_PI * ipow(h, 8)); // from Particle-Based Fluid Simulation for Interactive Applications
		//-945.0f / (32 * M_PI * ipow(h, 9)); // from https://nccastaff.bournemouth.ac.uk/jmacey/MastersProjects/MSc15/06Burak/BurakErtekinMScThesis.pdf
		//-6 / ipow(h, 8);
}

float Poly6::get(Vector2f r)
{ // (4 / (pi * h^8)) * (h^2 - r^2)^3
	float r2 = r.squaredNorm();
	if (r2 > h2) return 0.0f;
	return c_base * ipow(h2 - r2, 3);
}

Vector2f Poly6::grad(Vector2f r)
{ // r * (-24) / (pi * h^8) * (h^2 -r^2)^2
	auto r2 = r.squaredNorm();
	if (r2 > h2) return Vector2f::Zero();
	float diff = h2 - r2;
	return r * (c_grad * diff * diff);
}

Vector2f gradSpiky(const Vector2f &r, float h)
{
	float l = r.norm();

	if (l > h) {
		return Vector2f{ 0,0 };
	}

	return -30.0f / (M_PI * ipow(h, 5)) * (h - l) * (h - l) * (r / l);
}