#ifndef SIMULATIONSTATS_H
#define SIMULATIONSTATS_H

#include <chrono>
#include <deque>
#include <QtGlobal>
#include <QString>
#include <stack>
#include "src/tools/timer.h"
#include "src/tools/INonCopyable.hpp"

namespace StatVars {
enum {

	T_SEC,
	AGENT_COUNT,
	DT_MS,
	MDE_ITER_COUNT,
	SPH_ITER_COUNT,
	COLLISION_LEFT,
	/////
	NR_VARS
};
static const QString var_names[NR_VARS] = { 
	"Time (sec)",
		"Agents",
	"Timestep (ms)", 
	"MDE Iterations", 
	"SPH Iteratinos",
	"Collisions left"
};
static const int var_decimals[NR_VARS] = { 2,0,2,0,0,0 };
static const bool do_avg[NR_VARS] = { 0,0,1,1,1,1 };
}

class SimulationStats
{
public:
	SimulationStats();

	struct FrameData
	{
		std::chrono::time_point<Timer::my_clock>	timestamp;

		float& var(int id) {
			Q_ASSERT(id < StatVars::NR_VARS);
			return variables[id];
		}

		//float												dt = 0;
		//float												mde_iter_count = 0;
		//float												collisions_left = 0;

		float age_seconds(std::chrono::time_point<Timer::my_clock> now)
		{
			return std::chrono::duration_cast<std::chrono::milliseconds>(now - timestamp).count() * 0.001f;
		}

		class NamedTimer : public Timer {
		public:
			template<typename T>
			inline NamedTimer(T var) : Timer(var) {};
			QString name;
		};

		class TimerAcces {
		public:
			TimerAcces(std::vector<NamedTimer>* ptr, int id) {
				m_pTimers = ptr;
				timer_idx = id;
			}
			void start();
			void stop();
		private:
			int timer_idx;
			std::vector<NamedTimer>* m_pTimers;
		};

		TimerAcces push_timer(QString name, bool auto_start);

		// returns time in milliseconds
		float pop_timer();

		std::vector<NamedTimer>& timers() {
			return m_timers;
		}

	private:
		float variables[StatVars::NR_VARS] = {0}; // set all to zero

		std::vector<NamedTimer> m_work_timers;

		std::vector<NamedTimer> m_timers;
	};

	FrameData avg();

	FrameData& last() {
		Q_ASSERT(m_memory.size() > 0);
		return m_memory.back();
	}

	FrameData& current() {
		return m_working_data;
	}

	void push(std::chrono::time_point<Timer::my_clock> time_stamp);

private:
	float								m_trail_time = 1.5f;
	FrameData							m_working_data;
	std::deque<FrameData>				m_memory;
};

#endif // SIMULATIONSTATS_H