#pragma once

#include "Support/neighbourfinder.h"

#include <QObject>


class CollisionResolver : public QObject
{
	Q_OBJECT
public:
	CollisionResolver(OpenGLView& f);
	~CollisionResolver();

	void resolve_collisions(SimulationCore& sc, VecX<float>& m_displacement, int iter = 0);
	//float try_move(SimulationCore& sc, int agent_id, Vector2f direction);

	void prune(SimulationCore& sc, float offset = 0);

	bool settings_changed(SimulationCore&, SimulationSettings_ptr old_settings);
	void reset(SimulationCore&);

	void make_ui(SimulationCore & sc);

	void render(RenderContext rc);

	NeighbourFinder& neighbourFinder();

	inline void act_vis_grid() {
		m_nf.toggle_render();
	}

private:
	NeighbourFinder m_nf;
};