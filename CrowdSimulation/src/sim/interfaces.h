#ifndef INTERFACES_H
#define INTERFACES_H

#include "tools/shared_eigen_incl.h"
#include "simdeclr.h"
#include "simulationsettings.h"
#include "vis/openglincl.h"
#include <QSharedPointer>

class EnginePart
{
public:
	virtual bool settings_changed(SimulationCore&, SimulationSettings_ptr old_settings) { return false; };
	virtual void reset(SimulationCore&) {};
	virtual void make_ui(SimulationCore&) {};
};

class IGoalPicker :
	public virtual EnginePart
{
public:
	IGoalPicker(VecX<int>& goals_reference) : m_goals_ref(goals_reference) {}

	virtual void choose_goals(SimulationCore&) = 0;
	virtual void update_goals(SimulationCore&) {}

	virtual QString goal_picker_name() = 0;

	virtual void make_ui(SimulationCore&) {};
	inline int goal(int id) { return m_goals_ref(id); }
	inline void set_goal(int id, int goal) { m_goals_ref(id) = goal; }

private:

	VecX<int>&			m_goals_ref;
};

using IGoalPicker_ptr = QSharedPointer<IGoalPicker>;

class IGlobalPanner :
	public virtual EnginePart
{
public:
	virtual void set_pref_velocity(SimulationCore& sc, VecX<float>& pref_velocities, float dt) = 0;
	virtual void render_gp(RenderContext) {};
	virtual void update(SimulationCore&) {};
};
using IGlobalPanner_ptr = QSharedPointer<IGlobalPanner>;

class IAgentMover :
	public virtual EnginePart
{
public:
	virtual void step(SimulationCore& sc, float dt) = 0;
	virtual void render_am(RenderContext) {}
};
using IAgentMover_ptr = QSharedPointer<IAgentMover>;

class GoalColorizer : public IColorizer {
public:
	void set_goalpicker(IGoalPicker_ptr ptr) {
		m_pGoalPicker = ptr;
	}
	inline virtual Color color(int agent_id) override {
		int class_id = m_pGoalPicker.data() ? m_pGoalPicker->goal(agent_id) : 0;
		return colors[qMin(class_id, (int)(colors.size() - 1))];
	}
private:
	const static std::vector<Color> colors;
	IGoalPicker_ptr m_pGoalPicker;
};


#endif // INTERFACES_H
