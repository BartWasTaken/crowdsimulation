#include "IEnvironment.h"
#include "mainwindow.h"
#include "simulationcore.h"
#include "sim/obstacle.h"
#include "src/external/earcut.hpp"
#include "Support/neighbourfinder.h"
#include "collisionresolver.h"
#include <limits>
#include <QDebug>
#include <QAction>

void IEnvironment::update(SimulationCore& sc, float dt)
{
	m_smoke.step(sc, dt);
	set_heights(sc);

	for (auto& e : m_obst_events) {
		bool changed = e.execute(sc, m_obstacles);
		if (changed) {
			m_obstacles_changed_frame = sc.step_id();
			m_renew_vis_shapes = true;
		}
	}


	/* test code for moving obstacles */ 
	
	/*{
		m_obstacles[0].move({ dt, 0 });
		m_renew_vis_shapes = true;
		m_obstacles_changed_frame = sc.step_id();
	}*/
	
}

void IEnvironment::set_settings(QJsonObject& json)
{
	m_settings = json;
}

QJsonObject& IEnvironment::get_settings()
{
	return m_settings;
}

void IEnvironment::enable(SimulationCore& sc)
{
	// set goalpicker:
	if (m_pref_goalpicker.length() > 0 && (m_force_prefs || !sc.has_goalpicker()))
	{
		auto gp = sc.find_goalpicker(m_pref_goalpicker);
		if (gp != nullptr) sc.set_goalpicker(gp);
	}

	// set engine:
	if (m_pref_engine.length() > 0 && (m_force_prefs || !sc.has_engine()))
	{
		auto engine = sc.find_engine(m_pref_engine);
		if (engine.is_complete()) sc.set_engine(engine);
	}
}

void IEnvironment::add_obstacle(const Obstacle& obstacle)
{
	m_obstacles.push_back(obstacle);
	m_renew_vis_shapes = true;
}

void IEnvironment::add_goal(const IGoal_ptr& goal)
{
	m_goals.push_back(goal);
	m_renew_vis_shapes = true;
}

void IEnvironment::add_staircase(StairCaseArea& staircase)
{
	m_staircases.push_back(staircase);
	m_renew_vis_shapes = true;
	//staircase.find_pairs(m_staircases.size() -1, m_staircases);
}

void IEnvironment::add_elevation(ElevationArea& area)
{
	m_elevations.push_back(area);
}

void IEnvironment::add_spawn_area(SpawnArea& area)
{
	m_spawn_areas.push_back(area);
}

void IEnvironment::add_obstacle_event(NewObstacleEvent& e)
{
	m_obst_events.push_back(e);
}

void IEnvironment::distribute_agents(SimulationCore& sc)
{
	if (m_distr_func == nullptr)
	{
		if (m_spawn_areas.size() > 0) {
			spawn_from_areas(sc);
		}
		else {
			int agent_count = sc.nr_active_agents();

			Vector2f min = bbox().min();
			Vector2f range = bbox().sizes();

			NeighbourFinder& nf = sc.collision_resolver().neighbourFinder();
			nf.require_obstacle_register(sc);
			auto& obstacles = sc.environment().obstacles();

			for (int id = 0, guard = 0; id < agent_count && guard < 100 * agent_count; guard++)
			{
				Vector2f rand = { rand_float(), rand_float() };
				sc.position(id) = min + rand.cwiseProduct(range);
				NeighbourFinder::FindResult4Cell fr4c;
				nf.find_nearby_obstacles_single_floor(sc.position(id), fr4c);

				bool fail = false;

				LOOP_4CELL(fr4c, obst_id) {
					if (obstacles[obst_id].agent_collider().contains(sc.position(id))) {
						fail = true;
						goto END_4CELL;
					}
				} END_4CELL:

				if (!fail) id++;
			}
		}
	}
	else {
		m_distr_func(sc, std::bind(&IEnvironment::rand_float, this));
	}

	set_heights(sc);
}

void IEnvironment::reset(SimulationCore& sc)
{
	m_obstacle_field_provider.reset();

	// a decrementing loop as we pop from the back during the loop
	for (int i = (int) m_obstacles.size() - 1; i >= 0; i--)
	{
		auto& obst = m_obstacles[i];

		if (obst.is_staircase() || !obst.is_persistent()) {
			m_obstacles[i] = m_obstacles.back();
			m_obstacles.pop_back();
		}
		else {
			obst.rebuild(sc.settings().agent_rad(), sc.settings().agent_rad());
		}
	}

	int obstacle_start = (int)m_obstacles.size();
	for (const auto& staircase : m_staircases) {
		staircase.make_obstacles(m_obstacles, sc.settings().agent_rad());
	}

	for (int i = (int) m_obstacles.size() - 2; i >= obstacle_start; i--) {
		
		// bind staircases is a symmetric function, we use the symmetry by calling on from i with everying after i as input
		m_obstacles[i].bind_staircases(m_obstacles.data() + i + 1, (int) m_obstacles.size() - i - 1);
	}

	m_obstacles_changed_frame = sc.step_id();

	for (auto& stair_case : m_staircases)
	{
		stair_case.bind_elevation(nullptr);
	}
	for (const auto& elevation : m_elevations)
	{
		for (auto& stair_case : m_staircases)
		{
			bool contained = true;

			for (const auto& p : stair_case.vertices()) {
				if (!elevation.polygon.contains(p)) {
					contained = false;
					break;
				}
			}

			if (contained) stair_case.bind_elevation(&elevation);
		}
	}
	for (auto& stair_case : m_staircases)
	{
		stair_case.set_upper_height(m_staircases);
	}

	for (auto& e : m_obst_events) {
		e.reset(m_obstacles);
	}

	reset_smoke(sc.settings());

	m_renew_vis_shapes = true;
}

ObstacleGridProvider& IEnvironment::obstacleFields()
{
	return m_obstacle_field_provider;
}

int64_t IEnvironment::last_obstacle_changed_step()
{
	return m_obstacles_changed_frame;
}

void IEnvironment::set_distributor(Distributor f)
{
	m_distr_func = f;
}

void IEnvironment::set_heights(SimulationCore& sc)
{
	// make faster via and neighbourfinder grid matching

	for (int i = 0, n = sc.nr_active_agents(); i < n; i++)
	{
		auto pos_2d = sc.position(i);

		Vector3f pos_3d = { pos_2d(0), 0, pos_2d(1) };
		ElevationArea const* matched = nullptr;

		for (const auto& elevation : m_elevations) {
			if (elevation.polygon.contains(sc.position(i))) {
				matched = &elevation;
				break;
			}
		}

		if (matched) {
			pos_3d(1) = matched->height;
			auto displacement = matched->translation;
			pos_3d(0) += displacement(0);
			pos_3d(2) += displacement(1);
		}

		// change coordinate system
		pos_3d(2) = -pos_3d(2);

		// maybe override if on a staircase
		for (const auto& stair_case : m_staircases) {
			if (matched == stair_case.get_bound_elevation()) {
				if (stair_case.base_polygon().contains(sc.position(i)) && stair_case.has_upwards()) {
					pos_3d(1) = stair_case.get_height_at(sc.position(i));
					break;
				}
			}
		}

		sc.pos_3d(i) = pos_3d;
	}

}

std::vector<IGoal_ptr>& IEnvironment::goals()
{
	return m_goals;
}

std::vector<Obstacle>& IEnvironment::obstacles()
{
	return m_obstacles;
}

std::vector<StairCaseArea>& IEnvironment::staircases()
{
	return m_staircases;
}

std::vector<ElevationArea>& IEnvironment::elevation_areas()
{
	return m_elevations;
}

AABB2 IEnvironment::bbox()
{
	return m_bbox;
}

Vector3f IEnvironment::center3d()
{
	if (m_elevations.size() > 0)
	{
		float total = 0;
		int count = 0;
		bool found_zero = false;
		Vector2f total_translation = { 0,0 };

		for (auto& a : elevation_areas()) {
			count++;
			total += a.height;
			total_translation += a.polygon.bbox().center() + a.translation;
		}

		Vector2f pos = (1.0f / count) * total_translation;

		if (!found_zero) count++;
		float height = total / count;

		return to_3d(pos, height);
	}
	else {
		return to_3d(m_bbox.center(), 0);
	}
}

QString IEnvironment::name()
{
	return m_name;
}

void IEnvironment::render_env(RenderContext rc)
{
	if (m_renew_vis_shapes)
	{
		m_renew_vis_shapes = false;

		for (auto& s : m_vis_shapes) s.GLDestroy();
		m_vis_shapes.clear();

		// first the bounds
		if (!are_equal_fv(m_bbox.sizes(), Vector2f::Zero())) { // uninitialzed compare to zero works
			m_vis_shapes.emplace_back(rc.f, ColorF{ 1.0f, 1.0f, 1.0f, 0.15f });
			m_vis_shapes.back().make_shape(bbox_to_polygon(m_bbox));
		}

		// then the elevations
		for (auto& s : m_elevations) {
			m_vis_shapes.emplace_back(rc.f, ColorF{ 0.05f, 0.05f, 0.05f, 0.2f });
			m_vis_shapes.back().make_shape(s.polygon.vertices());
		}

		for (const auto& shape : m_spawn_areas) {
			m_vis_shapes.emplace_back(rc.f, ColorF{ 0.8f, 0.8f, 0, 0.3f });
			m_vis_shapes.back().make_shape(shape.polygon.vertices());
		}

		for (const auto& goal_ref : m_goals)
		{
			if (ObstacleGoal* goal = dynamic_cast<ObstacleGoal*>(goal_ref.data())) {
				m_vis_shapes.emplace_back(rc.f, ColorF{ 0.1f,1,0.1f,0.8f });
				m_vis_shapes.back().make_shape(goal->base().vertices());
			}
		}

		for (const auto& obst : m_obstacles)
		{
			if (!obst.is_staircase()) {
				m_vis_shapes.emplace_back(rc.f, ColorF{ 1,0.2f,0.0f,0.3f });
				m_vis_shapes.back().make_shape(obst.base().vertices());
			}

			m_vis_shapes.emplace_back(rc.f, ColorF{ 1,0.25f,0.0f,0.3f });
			m_vis_shapes.back().make_shape(obst.agent_collider().vertices());

			m_vis_shapes.emplace_back(rc.f, ColorF{ 1,0.0f,0.25f,0.3f });
			m_vis_shapes.back().make_shape(obst.agent_collider_bloat().vertices());
		}


		//for (const auto& staircase : m_staircases)
		//{
		//	m_vis_shapes.emplace_back(rc.f, ColorF{ 0.5f,0.5f,1,0.3f });
		//	m_vis_shapes.back().make_shape(staircase.vertices());
		//}



	}

	rc.f.glEnable(GL_BLEND);
	rc.f.glxBlendFuncBasic();
	for (auto& s : m_vis_shapes) s.render(rc);
	rc.f.glDisable(GL_BLEND);

	m_smoke.render(rc);
}

SmokeField& IEnvironment::smoke()
{
	return m_smoke;
}

void IEnvironment::set_preferences(QString goal_pref, QString engine_pref, bool force_prefs)
{
	m_pref_goalpicker = goal_pref;
	m_pref_engine = engine_pref;
}

void IEnvironment::set_bounds(AABB2 box)
{
	m_bbox = box;
	m_renew_vis_shapes = true;
}

std::vector<Obstacle> IEnvironment::get_non_staircase_obstacles()
{
	std::vector<Obstacle> result;
	for (const auto& obst : m_obstacles) {
		if (!obst.is_staircase()) {
			result.push_back(obst);
		}
	}
	return result;
}

std::vector<IEnvironment::NewObstacleEvent>& IEnvironment::get_obstacle_events()
{
	return m_obst_events;
}

void IEnvironment::reset_smoke(SimulationSettings& settings)
{
	auto dim = SimulationCore::compute_grid_dimensions(settings.agent_rad(), m_bbox);
	m_smoke.setup(m_bbox, dim);
}

void IEnvironment::spawn_from_areas(SimulationCore& sc)
{
	reset_random();
	
	// triangulate the areas:
	// The number type to use for tessellation
	struct TriangleArea {
		TriangleArea(Vector2f p1, Vector2f p2, Vector2f p3)
			: a(p1), b(p2), c(p3) {
			area = triangle_area(a, b, c);
		}
		Vector2f a, b, c;
		float area;
	};
	std::vector<TriangleArea> triangles;

	for (const auto& area : m_spawn_areas) {
		using Coord = float;

		// The index type. Defaults to uint32_t, but you can also pass uint16_t if you know that your
		// data won't have more than 65536 vertices.
		using N = GLuint;

		// Create array
		using Point = std::array<Coord, 2>;
		std::vector<std::vector<Point>> polygon;

		// Fill polygon structure with actual data. Any winding order works.
		// The first polyline defines the main polygon.
		polygon.push_back({});
		for (const auto& v : area.polygon.vertices()) {
			polygon[0].push_back({ v(0), v(1) });
		}

		// Run tessellation
		// Output triangles are clockwise.
		auto indices = mapbox::earcut<N>(polygon);
		for (int i = (int) indices.size() - 3; i >= 0; i -= 3) {
			triangles.push_back(TriangleArea(
				area.polygon.vertices()[indices[i]],
				area.polygon.vertices()[indices[i + 1]],
				area.polygon.vertices()[indices[i + 2]]
			));
		}
	}

	{
		int agent_count = sc.nr_active_agents();
		int placed_count = 0;
		int guard_id = 0;
		int guard_max = 2 + agent_count * 10;
		int tri_guard_max = 2 + guard_max / triangles.size();
		NeighbourFinder& nf = sc.collision_resolver().neighbourFinder();
		nf.require_obstacle_register(sc);
		auto& obstacles = sc.environment().obstacles();

		float total_weight = 0;
		for (auto& tri : triangles) total_weight += tri.area;

		while (placed_count < agent_count) {
			// pick a triangle
			float selector = rand_float() * total_weight;
			float cumu_weight = 0;
			TriangleArea* selected;
			for (auto& tri : triangles) {
				cumu_weight += tri.area;
				if (selector <= cumu_weight) {
					selected = &tri;
					break;
				}
			}
			int tri_guard_id = 0;
			while (guard_id++ < guard_max && tri_guard_id++ < tri_guard_max)
			{
				float r1 = rand_float(), r2 = rand_float();
				float sqrt_r1 = qSqrt(r1);

				// select a point on the triangle randomly
				Vector2f p = (1 - sqrt_r1) * selected->a
					+ (sqrt_r1 * (1 - r2)) * selected->b
					+ (r2 * sqrt_r1) * selected->c;

				// make sure it doesnt intersect an obstacle
				NeighbourFinder::FindResult4Cell fr4c;
				nf.find_nearby_obstacles_single_floor(p, fr4c);
				bool fail = false;
				LOOP_4CELL(fr4c, obst_id) {
					if (obstacles[obst_id].agent_collider_bloat().contains(p)) {
						fail = true;
						goto END_4CELL;
					}
				} END_4CELL:

				if (!fail) {
					sc.position(placed_count++) = p;
					break;
				}
			}
		}
	}

}


IEnvironment::NewObstacleEvent::NewObstacleEvent(QString n, bool is_manual, float time_sec, Polygon2D shape)
	: ObstacleEvent(is_manual, time_sec, n), m_polygon(shape)
{

}

bool IEnvironment::NewObstacleEvent::execute(SimulationCore& sc, std::vector<Obstacle>& m_obstacles)
{
	if (m_is_manual) {
		if (m_manual_activated != m_active) {
			if (m_active) {
				this->reset(m_obstacles);
			}
			else {
				m_obstacles.emplace_back(sc.settings().agent_rad(), m_polygon.vertices(), false);
				m_idref = m_obstacles.back().id;
				m_active = true;
			}
			return true;
		}
	}
	else {
		if (!m_active && sc.simtime_sec() > m_event_time_sec) {
			m_obstacles.emplace_back(sc.settings().agent_rad(), m_polygon.vertices(), false);
			m_idref = m_obstacles.back().id;
			m_active = true;
			return true;
		}
	}
	return false;
}

void IEnvironment::NewObstacleEvent::reset(std::vector<Obstacle>& olist)
{
	if (m_active) {
		for (std::vector<Obstacle>::iterator iter = olist.begin(); iter < olist.end(); ++iter)
		{
			if (m_idref == (*iter).id)
			{
				olist.erase(iter);
				break;
			}
		}
		m_idref.reset();
		m_active = false;
	}
}
