#ifndef ENVIRONMENT2D_H
#define ENVIRONMENT2D_H

#include "IEnvironment.h"
#include "obstacle.h"
#include <vector>
/*

class Environment2D : public IEnvironment
{
public:
	Environment2D(AABB2 field);

	virtual AABB2 main_region() override;

	virtual void hard_reset(SimulationCore&) override;

	virtual void settings_changed(SimulationCore&) override;

	virtual const std::vector<IGoal_ptr>& get_goals(SimulationCore& sc) override;

	virtual void assign_goals_ids(SimulationCore&, VecX<int>& ids) override;

	virtual void distribute_agents(SimulationCore& sc) override;

	virtual void set_pref_velocity(SimulationCore& sc, VecX<float>& positions, VecX<float>& velocities) override;

	virtual void colorize(SimulationCore& sc) override;

	virtual NeighbourFinder& get_neighbourfinder() override;

	virtual float pref_speed() override;

	virtual QString name() override;

	virtual void resolve_collisions(SimulationCore& sc, int iter = 0, bool inter_agent = true, bool obstacles = true) override;

private:

	inline float rand_float() { return m_dis(m_rgen); }

	NeighbourFinder			m_nbfinder;
	AABB2					m_field;
	std::vector<Obstacle>	m_obstacles;
	std::vector<IGoal_ptr>	m_goals;

	std::mt19937						m_rgen;
	std::uniform_real_distribution<>	m_dis{ 0.0f, 1.0f };

};
*/
#endif // ENVIRONMENT2D_H