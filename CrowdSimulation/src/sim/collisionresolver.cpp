#include "collisionresolver.h"
#include "mainwindow.h"

#include <QAction>

CollisionResolver::CollisionResolver(OpenGLView& f)
	: m_nf(f)
{

}

CollisionResolver::~CollisionResolver()
{

}

void CollisionResolver::resolve_collisions(SimulationCore& sc, VecX<float>& m_displacement, int max_iter)
{
	m_nf.enforce_min_dist(
		sc, sc.positions(), m_displacement,
		max_iter, true
	);
}

void CollisionResolver::prune(SimulationCore& sc, float offset)
{
	// if outside map bbox, disable?
	// TODO: replace by polygon?
	auto bbox = sc.environment().bbox();
	auto r = sc.settings().agent_rad();
	float d = r + offset;
	bbox = { bbox.min() - Vector2f{ d,d }, bbox.max() + Vector2f{ d,d } };
	for (int i = 0, n = sc.nr_active_agents(); i < n; i++)
	{
		Vector2f pos = sc.position(i);
		if (!bbox.contains(pos))
		{
			sc.disable_agent(i);
		}
	}
}

bool CollisionResolver::settings_changed(SimulationCore& sc, SimulationSettings_ptr old_settings)
{
	reset(sc);
	return true;
}

void CollisionResolver::reset(SimulationCore& sc)
{
	m_nf.reset(sc.environment().bbox());
}

void CollisionResolver::make_ui(SimulationCore & sc)
{
	QMenu& menu = sc.get_vismenu();
	{
		auto act = new QAction("Show register grid", &sc.get_window());
		act->setCheckable(true);
		act->setChecked(m_nf.render_enabled());
		menu.addAction(act);
		QObject::connect(act, &QAction::triggered, this, &CollisionResolver::act_vis_grid);
	}
}

void CollisionResolver::render(RenderContext rc)
{
	m_nf.render(rc);
}

NeighbourFinder& CollisionResolver::neighbourFinder()
{
	return m_nf;
}
