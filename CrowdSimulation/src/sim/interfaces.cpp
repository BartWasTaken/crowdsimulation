#include "interfaces.h"
#include "simulationcore.h"

const std::vector<Color> GoalColorizer::colors = {
		{ 150,80,0,255 },
		{ 0,150,80,255 },
		{ 80,0,150,255 },

		{ 255,0,0,255 },
		{ 0,255,0,255 },
		{ 0,0,255,255 },

		{ 255,255,255,255 }
};
