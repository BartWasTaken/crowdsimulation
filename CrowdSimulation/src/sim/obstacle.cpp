#include "obstacle.h"
#include "simulationcore.h"
#include "tools/geomfunc.h"
#include "files/VertexDataInterlaced.h"

Obstacle::Obstacle(float inflate_rad, const std::vector<Vector2f>& v, bool persistent)
{
	m_persistent = persistent;
	m_initial.setup(v);
	m_base.setup(v);
	rebuild(inflate_rad, inflate_rad);
}

void Obstacle::set_staircase(int up_id, int down_id, int up_vid, int down_vid)
{
	type = ObstacleFlags::STAIRCASE;
	this->up_id = up_id;
	this->down_id = down_id;
	this->up_vid = up_vid;
	this->down_vid = down_vid;
	Q_ASSERT((up_id >= 0) == (up_vid >= 0));
	Q_ASSERT((down_id >= 0) == (down_vid >= 0));
}

Vector2f Obstacle::closestPointOnEdge_base(const Vector2f& in_point) const
{
	float min_dist_sq = std::numeric_limits<float>::infinity();
	Vector2f best_dir;


	for (int i = 0; i < m_base.m_vcount; i++)
	{
		if (i == down_vid || i == up_vid) continue;

		Vector2f dir = line_to_point(
			m_base.m_vertices[(i + 1) % m_base.m_vcount] - m_base.m_vertices[i],
			in_point - m_base.m_vertices[i]
		);

		float dist_sq = dir.squaredNorm();
		if (dist_sq < min_dist_sq) {
			min_dist_sq = dist_sq;
			best_dir = dir;
		}
	}

	return in_point - best_dir;
}

void Obstacle::bind_staircases(Obstacle* arr, int len)
{
	for (int i = 0; i < len; i++) {
		Obstacle& obst = arr[i];
		if (down_id >= 0 && down_id == obst.up_id) {
			obst.get_up_line(endpoint_down);
			get_down_line(obst.endpoint_up);

			Q_ASSERT(difference_less_than(endpoint_down.line(), -obst.endpoint_up.line(), 0.01f));
		}
		if (up_id >= 0 && up_id == obst.down_id) {
			obst.get_down_line(endpoint_up);
			get_up_line(obst.endpoint_down);

			Q_ASSERT(difference_less_than(endpoint_up.line(), -obst.endpoint_down.line(), 0.01f));
		}
	}
}

void Obstacle::rebuild(float inflate_rad, float agent_rad)
{
	m_base = m_initial;
	inflate(m_base.vertices(), inflate_rad, agent_rad);
}

void Obstacle::move(Vector2f translation)
{
	m_base.move(translation);
	m_agent_collider.move(translation);
	m_agent_collider_bloat.move(translation);
}

/*bool Obstacle::contains_outer(const Vector2f& point) const
{
	if (!m_bbox.contains(point)) return false;

	int nIntersections = 0;

	Vector2f prev = m_collide_vertices.back();

	for (int i = 0; i < m_vcount; i++)
	{
		Vector2f curr = m_collide_vertices[i];
		if (intersection(point, m_inf, prev, curr)) {
			nIntersections++;
		}
		prev = curr;
	}

	return nIntersections % 2 == 1;
}*/

const Polygon2D& Obstacle::agent_collider() const
{
	return m_agent_collider;
}

const Polygon2D& Obstacle::base() const
{
	return m_base;
}

const Polygon2D& Obstacle::agent_collider_bloat() const
{
	return m_agent_collider_bloat;
}

void Obstacle::hit_single_collider(SimulationCore& sc,
	Vector2f p1, Vector2f p2,
	int* obstacle_ids, int obstacle_id_count,
	HitInfo& hit_info, bool ignore_outwards)
{
	auto& obstacles = sc.environment().obstacles();

	// loop over all the lines of all objects
	// find closest intersection with ray
	// return intersection
	// return false if no intersection

	hit_info.has_hit = false;
	hit_info.is_portal = false;
	hit_info.any_staircase = false;
	hit_info.collision_prog = 1;

	Vector2f dir = p2 - p1;

	if (obstacle_id_count > 0) 
		for (int* id_it = obstacle_ids, *it_end = (obstacle_ids + obstacle_id_count); id_it < it_end; id_it++)
	{
		auto& obst = obstacles[*id_it];

		int nvert = (int) obst.agent_collider().vertices().size();
		const Vector2f* vert = obst.agent_collider().vertices().data();
		int i1, i2;
		for (i2 = 0, i1 = nvert - 1; i2 < nvert; i1 = i2++)
		{
			float prog;
			if (line_segment_intersection2(p1, p2, vert[i1], vert[i2], &prog))
			{
				if (obst.is_staircase()) hit_info.any_staircase = true;

				bool outward_move = orientation_safe(p1, vert[i1], vert[i2]) == 1;

				// check for colinearity
				if (!(outward_move && ignore_outwards) && prog < hit_info.collision_prog)
				{
					if (obst.is_staircase() && !outward_move) {
						if (obst.up_vid == i1) {
							hit_info.is_portal = true;
							// retrevie portal endpoint
							hit_info.portal_p1 = obst.endpoint_up.a;
							hit_info.portal_p2 = obst.endpoint_up.b;
						}
						else if (obst.down_vid == i1) {
							hit_info.is_portal = true;
							// retrevie portal endpoint
							hit_info.portal_p1 = obst.endpoint_down.a;
							hit_info.portal_p2 = obst.endpoint_down.b;
						}
						else hit_info.is_portal = false;
					}
					else hit_info.is_portal = false;

					hit_info.collision_prog = prog;
					hit_info.has_hit = true;
					hit_info.obstacle_id = *id_it;
					hit_info.p1 = vert[i1];
					hit_info.p2 = vert[i2];
				}
			}
		}
	}

	if (hit_info.has_hit) {
		Q_ASSERT(is_valid_vec2(hit_info.p1) && is_valid_vec2(hit_info.p2));
	}
	else {
		// maybe inside obstacle?
	}
}

Vector2f Obstacle::project_through_portal_up(Vector2f p)
{
	Q_ASSERT(are_equal_fv(m_base.vertices()[(up_vid + 1) % m_base.vertices().size()] - m_base.vertices()[up_vid], -endpoint_up.line()));
	return p - m_base.vertices()[up_vid] + endpoint_up.b;
}

Vector2f Obstacle::project_through_portal_down(Vector2f p)
{
	//if (are_equal_fv(m_base.vertices()[(down_vid + 1) % m_base.vertices().size()] - m_base.vertices()[down_vid], -endpoint_down.line())) {
	//	DebugBreak();
	//}
	Q_ASSERT(are_equal_fv(m_base.vertices()[(down_vid + 1) % m_base.vertices().size()] - m_base.vertices()[down_vid], -endpoint_down.line()));
	return p - m_base.vertices()[down_vid] + endpoint_down.b;
}

void Obstacle::inflate(const std::vector<Vector2f>& vertices, float inflation, float agent_rad)
{
	// make this value too large and agents cant get through portals on with very low time steps
	const float bloat = 0.001f * agent_rad;

	int vcount = (int) vertices.size();
	m_agent_collider.edit_vertices().resize(vcount);
	m_agent_collider_bloat.edit_vertices().resize(vcount);

	// a beter method would add an extra point when inner angles are below 90 degrees.
	// or even add rounded corners...

	for (int i = 0; i < vcount; i++)
	{
		int ia = ((i + vcount - 1) % vcount);
		int ib = i;
		int ic = (i + 1) % vcount;
		Vector2f A = vertices[ia];
		Vector2f B = vertices[ib];
		Vector2f C = vertices[ic];

		Vector2f ABn = rotate_90_deg_ccw(B - A).normalized();
		Vector2f BCn = rotate_90_deg_ccw(C - B).normalized();

		//bool portal_a = up_vid == ia || down_vid == ia;
		//bool portal_b = up_vid == ib || down_vid == ib;

		auto& collider_vertice = m_agent_collider.edit_vertices()[i];
		auto& bloat_vertice = m_agent_collider_bloat.edit_vertices()[i];

		Vector2f offset = corner_offset(ABn, BCn);

		collider_vertice = B + inflation * offset;
		
		//if (!is_staircase() || !(portal_a || portal_b)) {
			bloat_vertice = collider_vertice + offset * bloat;
		//}
		//else if (portal_a) bloat_vertice = collider_vertice + BCn * inflation;
		//else bloat_vertice = collider_vertice + ABn * inflation;		
	}

	m_agent_collider.finish();
	m_agent_collider_bloat.finish();
}

StairCaseArea::StairCaseArea(std::vector<Vector2f>& vertices, float thickness, int up_id, int down_id, int up_vid, int down_vid, int opening_vid)
{
	m_polygon.setup(vertices);
	up_portal_id = up_id;
	down_portal_id = down_id;
	up_portal_vid = up_vid;
	down_portal_vid = down_vid;

	Q_ASSERT((up_portal_id >= 0) == (up_portal_vid >= 0));
	Q_ASSERT((down_portal_id >= 0) == (down_portal_vid >= 0));

	this->opening_vid = opening_vid;
	this->thickness = thickness;
}

struct CornerPoints
{
	void push(Vector2f v) {
		Q_ASSERT(count + 1 <= 3);
		points[count++] = v;
	}

	void reverse() {

		if (count > 1) {
			int swap_id = count - 1;
			Vector2f swap = points[0];
			points[0] = points[swap_id];
			points[swap_id] = swap;
		}

		//int max = count - 1;
		//for (int i = 0; i < count/2; i++) {
		//	Vector2f swap = points[max-i];
		//	points[max-i] = points[i];
		//	points[i] = swap;
		//}
	}

	Vector2f points[3];
	int count = 0;
};

// the bevel if the next line doesnt use bevel, square corner
// normals must point in the bevel direction
inline void corner_bevel_end(float inflate, Vector2f a, Vector2f b, Vector2f c,
	Vector2f ab_n, Vector2f bc_n, CornerPoints& output)
{
	Vector2f intersect_point;
	Vector2f p0 = a + ab_n * inflate; // line origin (functions as infinity)

	Vector2f ab_dir = (b - a).normalized();

	Vector2f p1 = b + (ab_dir + ab_n) * inflate;
	if (intersection(b, c, p0, p1, &intersect_point)) {
		output.push(intersect_point);
		return;
	}
	output.push(p1);

	Vector2f p2 = b + (ab_dir - ab_n) * inflate;
	if (intersection(b, c, p1, p2, &intersect_point)) {
		output.push(intersect_point);
		return;
	}
	output.push(p2);

	Vector2f p3 = a - ab_n * inflate;
	if (intersection(b, c, p2, p3, &intersect_point)) {
		output.push(intersect_point);
		return;
	}
	else {
		DebugBreak();
		Q_ASSERT(false);
	}
}

void StairCaseArea::make_obstacles(std::vector<Obstacle>& out_obstacles, float agent_rad, float inflate_rad_override) const
{
	const auto& vertices = m_polygon.vertices();
	int v_count = (int) vertices.size();

	float inflate_rad;
	if (inflate_rad_override < 0) inflate_rad = thickness;
	else inflate_rad = inflate_rad_override;
	
	// make obstacle
	// for each edge, define if the inside/outside is an obstacle

	// [0]:inside, [1]:outside
	enum EdgeType {
		Obstacle, Open, Portal
	};
	std::vector<std::array<int, 2>> edge_definition;//TODO the bool should be an int, as we need a small stroke for an up+down edge
	edge_definition.resize(v_count, { Obstacle, Obstacle });

	if (opening_vid >= 0) edge_definition[opening_vid] = { Open, Open };
	if (up_portal_vid >= 0) edge_definition[up_portal_vid][0] = Portal;
	if (down_portal_vid >= 0) edge_definition[down_portal_vid][1] = Portal;

	bool has_no_inside = true;
	for (auto arr : edge_definition) {
		if (arr[0] == Open && arr[1] == Open) {
			has_no_inside = false;
		}
	}

	std::vector<std::vector<Vector2f>> outside_corners(v_count);
	std::vector<std::vector<Vector2f>> inside_corners(v_count);

	for (int i = 0, prev_i = v_count - 1; i < v_count; prev_i = i++)
	{
		auto& outer_corner = outside_corners[i];
		auto& inner_corner = inside_corners[i];

		// Vertices:
		Vector2f a, b, c;
		a = vertices[prev_i];
		b = vertices[i];
		c = vertices[(i + 1) % v_count];

		// Edges
		Vector2f ab, bc;
		ab = b - a;
		bc = c - b;

		// Directions
		Vector2f ab_dir, bc_dir, ab_n, bc_n;
		ab_dir = ab.normalized();
		bc_dir = bc.normalized();
		ab_n = rotate_90_deg_ccw(ab_dir);
		bc_n = rotate_90_deg_ccw(bc_dir);

		// Parameters
		bool has_inside_ab = edge_definition[prev_i][0] == Obstacle;
		bool has_outside_ab = edge_definition[prev_i][1] == Obstacle;
		bool double_portal_ab = edge_definition[prev_i][0] == Portal && edge_definition[prev_i][1] == Portal;

		bool has_inside_bc = edge_definition[i][0] == Obstacle;
		bool has_outside_bc = edge_definition[i][1] == Obstacle;
		bool double_portal_bc = edge_definition[i][0] == Portal && edge_definition[i][1] == Portal;


		// Define corner points:
		CornerPoints outer_points;
		CornerPoints inner_points;

		float thickness_double_portal = 0.1f;

		if (has_outside_ab && has_outside_bc) {
			outer_points.push(b + inflate_rad * corner_offset(ab_n, bc_n));
		}
		else if (has_outside_ab) { // no outside bc
			if (double_portal_bc) {
				// if the bc is a double portal, add a slight margin
				auto margin = bc_n * inflate_rad * thickness_double_portal;
				corner_bevel_end(inflate_rad, a, b + margin, c + margin, ab_n, bc_n, outer_points);
			}
			else {
				corner_bevel_end(inflate_rad, a, b, c, ab_n, bc_n, outer_points);
			}
		}
		else if (has_outside_bc) { // no outside ab
			if (double_portal_ab) {
				auto margin = ab_n * inflate_rad * thickness_double_portal;
				corner_bevel_end(inflate_rad, c, b + margin, a + margin, bc_n, ab_n, outer_points);
			}
			else {
				corner_bevel_end(inflate_rad, c, b, a, bc_n, ab_n, outer_points);
			}
			outer_points.reverse();
		}
		else if (double_portal_ab) {
			Q_ASSERT("not implemented to have an opening next to the portal (a rotating stairs)");
		}
		else if (double_portal_bc) {
			Q_ASSERT("not implemented to have an opening next to the portal (a rotating stairs)");
		}
		else {
			outer_points.push(b);
		}

		if (has_inside_ab && has_inside_bc) { // no outside
			inner_points.push(b + inflate_rad * corner_offset(-ab_n, -bc_n));
		}
		else if (has_inside_ab) { // no inside bc
			if (double_portal_bc) {
				// if the bc is a double portal, add a slight margin
				auto margin = bc_n * inflate_rad * thickness_double_portal;
				corner_bevel_end(inflate_rad, a, b - margin, c - margin, -ab_n, -bc_n, inner_points);
			}
			else {
				corner_bevel_end(inflate_rad, a, b, c, -ab_n, -bc_n, inner_points);
			}
		}
		else if (has_inside_bc) {
			if (double_portal_ab) {
				auto margin = ab_n * inflate_rad * thickness_double_portal;
				corner_bevel_end(inflate_rad, c, b - margin, a - margin, -bc_n, -ab_n, inner_points);
			}
			else {
				corner_bevel_end(inflate_rad, c, b, a, -bc_n, -ab_n, inner_points);
			}
			inner_points.reverse();
		}
		else if (double_portal_ab) {
			Q_ASSERT("not implemented to have an opening next to the portal (a rotating stairs)");
		}
		else if (double_portal_bc) {
			Q_ASSERT("not implemented to have an opening next to the portal (a rotating stairs)");
		}
		else {
			inner_points.push(b);
		}

		if (inner_points.count == 1 && outer_points.count == 1 && inner_points.points[0] == outer_points.points[0]) {
			// empty corner...?
			Q_ASSERT(false);
		}
		else {
			outer_corner.reserve(outer_points.count);
			inner_corner.reserve(inner_points.count);

			for (int i = 0; i < outer_points.count; i++) outer_corner.push_back(outer_points.points[i]);
			for (int i = 0; i < inner_points.count; i++) inner_corner.push_back(inner_points.points[i]);
		}
	}

	if (has_no_inside) {
		std::vector<Vector2f> polygon;

		int obstacle_up_vid = -1;
		int obstacle_down_vid = -1;

		// connect outer corners only
		for (int i = 0; i < v_count; i++) {
			auto& c = outside_corners[i];
			polygon.insert(polygon.end(), c.begin(), c.end());
			if (i == down_portal_vid) {
				obstacle_down_vid = (int)polygon.size() - 1;
			}
		}

		out_obstacles.push_back({ 0, polygon });
		out_obstacles.back().rebuild(0, agent_rad);
		out_obstacles.back().set_staircase(
			up_portal_id, down_portal_id,
			obstacle_up_vid, obstacle_down_vid);
	}
	else {
		// find an empty line
		int open_line_idx = -1;
		for (int prog_i = 0; prog_i < v_count; prog_i++) {
			if (edge_definition[prog_i][0] == Open && edge_definition[prog_i][1] == Open) {
				open_line_idx = prog_i;
				break;
			}
		}
		Q_ASSERT(open_line_idx >= 0);

		int start_i = (open_line_idx + 1) % v_count;
		int prog_i = (start_i + 1) % v_count;
		int guard = start_i;

		do {

			// if the next line is open, the segment is done
			if (edge_definition[prog_i][0] == Open && edge_definition[prog_i][1] == Open)
			{
				// we found a polygon, build it:
				std::vector<Vector2f> polygon;

				// define how large it is:
				int p_edge_count = (prog_i - start_i + v_count) % v_count;

				int obstacle_up_vid = -1;
				int obstacle_down_vid = -1;

				// iterate outside:
				for (int i = 0; i <= p_edge_count; i++)
				{
					int v_id = (start_i + i) % v_count;
					auto& corner = outside_corners[v_id];

					//int old_size = polygon.size();
					polygon.insert(polygon.end(), corner.begin(), corner.end());
					//polygon.erase(std::unique(polygon.begin() + old_size, polygon.end()), polygon.end());

					if (v_id == down_portal_vid) {
						obstacle_down_vid = (int)polygon.size() - 1;
					}
				}
				// iterate reversed inside
				for (int i = p_edge_count; i >= 0; i--)
				{
					int v_id = (start_i + i) % v_count;
					auto& corner = inside_corners[v_id];
					if (v_id == up_portal_vid) {
						obstacle_up_vid = (int)polygon.size() - 1;
					}

					//int old_size = polygon.size();
					polygon.insert(polygon.end(), corner.rbegin() + (polygon.back() == corner.back() ? 1 : 0), corner.rend());
					//polygon.erase(std::unique(polygon.begin() + old_size, polygon.end()), polygon.end());
				}

				for (int i = 0; i < polygon.size()-1; i++) {
					Q_ASSERT(polygon[i] != polygon[i + 1]);
				}

				out_obstacles.push_back({ 0, polygon });
				out_obstacles.back().set_staircase(
					up_portal_id, down_portal_id,
					obstacle_up_vid, obstacle_down_vid);
				out_obstacles.back().rebuild(0, agent_rad);

				start_i = (prog_i + 1) % v_count;
				prog_i = (start_i + 1) % v_count;

				if (start_i == guard) break;
			}
			else {
				prog_i = (prog_i + 1) % v_count;
			}
		} while (true);
	}
}

float StairCaseArea::get_height_at(Vector2f point) const
{
	const auto& vertices = base_polygon().vertices();
	Q_ASSERT(up_portal_id >= 0);
	Q_ASSERT(opening_vid >= 0);
	// must not be consecutive or equal edges (angle stairs not supported)
	Q_ASSERT(qAbs(up_portal_vid - opening_vid) > 1 && (vertices.size() - up_portal_vid + opening_vid) % vertices.size() > 1);
	// distance from open line to portal line

	// assume straight stairs for simplicity
	auto end1 = vertices[up_portal_vid];
	auto start2 = vertices[(opening_vid + 1) % vertices.size()];
	float prog = closest_point_on_line_prog(end1, start2, point);
	return base_height() * (prog) + m_next_floor_height * (1-prog);
}

void StairCaseArea::set_upper_height(std::vector<StairCaseArea>& staircases)
{
	for (const auto& stairs : staircases) {
		if (up_portal_id == stairs.down_portal_id) {
			m_next_floor_height = stairs.base_height();
		}
	}
}

PortalIterator::PortalIterator(std::vector<Obstacle>* ptr)
	: m_pObstacles(ptr)
{
	check_obstacle();
}

bool PortalIterator::next()
{
	if (!(up || down)) return false;

	int obst_count = (int)m_pObstacles->size();
	int cur_id = obst_count - reverse_id - 1;
	info.obst_id = cur_id;

	Q_ASSERT(cur_id < m_pObstacles->size());

	if (up) {
		(*m_pObstacles)[cur_id].get_up_line(info.start);
		(*m_pObstacles)[cur_id].get_up_line_end(info.end);
		info.up = true;
		up = false;
	}
	else if (down) {
		(*m_pObstacles)[cur_id].get_down_line(info.start);
		(*m_pObstacles)[cur_id].get_down_line_end(info.end);
		info.up = false;
		down = false;
	}

	if (!(up || down)) {
		reverse_id++;
		check_obstacle();
	}
	
	return true;
}

bool PortalIterator::check_obstacle()
{
	int obst_count = (int)m_pObstacles->size();
	int cur_id = obst_count - reverse_id - 1;
	if (cur_id < 0) return false;

	if ((*m_pObstacles)[cur_id].is_staircase()) {
		up = (*m_pObstacles)[cur_id].stairs_can_up();
		down = (*m_pObstacles)[cur_id].stairs_can_down();
		return true;
	}
	else {
		up = false;
		down = false;
		return false;
	}
}

ObstacleEvent::ObstacleEvent(bool is_manual, float time, QString name)
{
	m_name = name;
	m_is_manual = is_manual;
	m_event_time_sec = time;
}

void ObstacleEvent::activate_manual(bool b)
{
	m_manual_activated = b;
}

bool ObstacleEvent::is_manually_activated()
{
	return m_manual_activated;
}

bool ObstacleEvent::is_activated()
{
	return (m_is_manual && m_manual_activated) || m_active;
}
