#include "goals.h"
#include "simulationcore.h"
#include <QtMath>

std::unordered_map<int, Vector2i> IGoal::get_indices(AABB2 region, Vector2i dim)
{
	std::unordered_map<int, Vector2i> result;
	this->fill_indices_inner(result, region, dim);
	for (auto& goal : sub_goals) goal->fill_indices_inner(result, region, dim);
	return result;
}

Vector2i IGoal::idx(Vector2f pos, AABB2 region, Vector2i dim) const
{
	Vector2f posn{
		(pos(0) - region.min()(0)) / region.sizes()(0),
		(pos(1) - region.min()(1)) / region.sizes()(1)
	};
	int x = qBound(0, (int)(posn(0) * dim(0)), dim(0) - 1);
	int y = qBound(0, (int)(posn(1) * dim(1)), dim(1) - 1);
	return { x, y };
}

int IGoal::flat_idx(Vector2i id, Vector2i dim) const
{
	return id(1) * dim(0) + id(0);
}

SquareGoal::SquareGoal(AABB2 bbox)
{
	m_square = bbox;
}

Vector2f SquareGoal::closest_point(Vector2f pos)
{
	if (m_square.contains(pos)) {
		return pos;
	}

	Vector2f corners[] = {
		m_square.corner(AABB2::BottomLeft),
		m_square.corner(AABB2::TopLeft),
		m_square.corner(AABB2::TopRight),
		m_square.corner(AABB2::BottomRight)
	};

	Vector2f nearest_point;
	float dist_sq = std::numeric_limits<float>::max();

	for (int i = 0; i < 4; i++) {
		auto cpoint = closest_point_on_line(corners[i], corners[(i+1)%4], pos);
		auto d_sq = (pos - cpoint).squaredNorm();
		if (d_sq < dist_sq) {
			dist_sq = d_sq;
			nearest_point = cpoint;
		}
	}

	return nearest_point;	
}

void SquareGoal::fill_indices_inner(std::unordered_map<int, Vector2i>& result, AABB2 region, Vector2i dim) const
{
	// left bottom:
	auto lb = idx(m_square.min(), region, dim);
	// right top:
	auto rt = idx(m_square.max(), region, dim);

	for (int y = lb(1); y <= rt(1); y++) {
		for (int x = lb(0); x <= rt(0); x++) {
			result[flat_idx({ x,y }, dim)] = { x,y };
		}
	}
}

ObstacleGoal::ObstacleGoal(std::vector<Vector2f>& vertices)
	: Obstacle(0, vertices)
{

}

Vector2f ObstacleGoal::closest_point(Vector2f pos)
{
	if (Obstacle::base().contains(pos)) {
		return pos;
	}
	else {
		Vector2f surface, normal;
		Obstacle::base().project(pos, surface, normal);
		return surface;
	}
}

void ObstacleGoal::fill_indices_inner(std::unordered_map<int, Vector2i>& result, AABB2 region, Vector2i dim) const
{
	Raster raster{ region, dim };

	auto min = region.min();
	auto max = region.max();
	auto range = region.sizes();

	for (int y = 0; y < dim(1); y++)
	{
		float py = (y + 0.5f) / dim(1);
		for (int x = 0; x < dim(0); x++)
		{
			float px = (x + 0.5f) / dim(0);
			
			//Vector2f pos = min + Vector2f{ px, py }.cwiseProduct(range);
			//Obstacle::contains(pos);

			if (Obstacle::base().intersects(raster.cell_bbox(x, y)))
			{
				result[flat_idx({ x, y }, dim)] = { x, y };
			}
		}
	}

	if (result.size() == 0) {
		// do a line trace on all edges through the grid
	}
}
