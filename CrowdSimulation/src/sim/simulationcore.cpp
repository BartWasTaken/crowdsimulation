#include "simulationcore.h"
#include "tools/include.h"
#include "Engines/testsimulation.h"
#include "Engines/sphengine.h"
#include "collisionresolver.h"
#include "mainwindow.h"
#include "IEnvironment.h"

#include "ui/statsview.h"
#include "ui/eventview.h"

#include <QAction>
#include <QSharedPointer>
#include <QDebug>
#include <QObject>
#include <QDialog>

SimulationCore::SimulationCore(OpenGLView& func, MainWindow& mw, SimulationSettings_ptr& pSettings)
	: f(func), m_window(mw), m_pCollision_resolver{ new CollisionResolver(func) }, m_renderer3d(func, *this), m_eventview(mw.getEventForm())
{
	m_pStatsView = mw.getStatsView();

	m_leading_color = { 200, 0, 0, 180 };

	refresh_ui(true);
	func.opengl_master().get_inputmanager()->AddInputListener(this);
	m_pCircleDrawer = new CircleDrawer2D(f);
	//m_pLineDrawer = new LineDrawer2D(f);
	if (m_pLineDrawer) m_draw_lines = false;
	else m_draw_lines = false;

	m_pSettings = pSettings;
	m_pSettings->add_listener(this);

	m_simulation_thread = std::thread(&SimulationCore::run, this);

	m_pSettings = SimulationSettings_ptr::create();
	m_pSettings->add_listener(this);

	m_renderer3d.enable(false);
}

SimulationCore::~SimulationCore()
{
	m_flag_alive = false;
	m_simulation_thread.join();

	SafeDeletePtr(m_pCircleDrawer);
}

void SimulationCore::set_settings(QJsonObject& new_settings, bool delay_reset)
{
	auto task = [this, new_settings, delay_reset]()
	{
		SimulationSettings_ptr old_settings = m_pSettings->copy();
		m_pSettings->from_json(new_settings);
		handle_settings_change(old_settings, m_pSettings, delay_reset);
	};

	auto id = std::this_thread::get_id();
	if (m_simulation_thread_id == id) task();
	else m_sim_tasks.add(task);
}

void SimulationCore::render(RenderContext rc)
{
	QMutexLocker locker(&m_rendermutex);

	if (m_render_enabled) {

		if (m_renderer3d.enabled()) {
			rc.f.opengl_master().enable2DCam(false);

			m_renderer3d.render(rc);
		}
		else {
			rc.f.opengl_master().enable2DCam(true);

			m_pCollision_resolver->render(rc);

			if (m_pEnvironment) m_pEnvironment->render_env(rc);

			m_engine.render(rc);

			bool do_agents = m_pCircleDrawer && m_draw_agents;
			bool do_lines = m_pLineDrawer && m_draw_lines;

			if (do_agents || do_lines) {
				rc.f.glEnable(GL_BLEND);
				rc.f.glxBlendFuncBasic();

				if (do_agents) m_pCircleDrawer->render(rc);
				if (do_lines) m_pLineDrawer->render(rc);

				rc.f.glDisable(GL_BLEND);
			}
		}
	}
}

void SimulationCore::update()
{
	/*
	auto t2 = my_clock::now();
	double dt_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t0).count();
	auto dt_sec = dt_ns / 1e9;

	if (auto window = MainWindow::gInst())
		window->setWindowTitle(
			QString("No. particles: ").append(as_readable(settings().agent_count()))
			.append(" | Simulation dt: ").append(QString::number(m_time_analyser.avg_dt()))
			.append(" | Max dt: ").append(QString::number(m_time_analyser.dt_max))
			.append(" | Sim sec: ").append(QString::number(dt_sec))
		);*/

	m_main_tasks.execute();
	m_pStatsView->update_ui();
	m_eventview.update_ui();
}

bool SimulationCore::has_goalpicker()
{
	return m_pGoalPicker != nullptr;
}

bool SimulationCore::has_engine()
{
	return m_engine.is_complete();
}

void SimulationCore::sim_step()
{
	// time:
	float real_dt_sec;
	if (m_post_pause_timestep) {
		m_post_pause_timestep = false;

		m_time_analyser.reset();
		//m_total_timer.start();

		std::vector<Timer*> t_list{ /*&m_total_timer,*/ &m_step_timer };
		Timer::start_together(t_list);

		real_dt_sec = 0.01f;
		//m_time_analyser.step(sim_dt_sec);
	}
	else {
		Timer::StartInfo restart_info;
		m_step_timer.restart(&restart_info); // returns how much was elapsed at the moment of restart

		m_time_analyser.step(restart_info.previous_elapsed_sec());
		real_dt_sec = m_time_analyser.avg_dt();

		//m_sim_time_sec = m_total_timer.elapsed_sec();
		m_sim_time_sec += real_dt_sec;
	}


	float dt_limit = m_paused ? 0.01f : 0.15f;

	float final_dt = qMin(real_dt_sec, dt_limit);

	if (m_stable_record) final_dt = 0.1f;

	m_sim_time_sec += final_dt;

	frame_data().var(StatVars::T_SEC) = m_sim_time_sec;
	frame_data().var(StatVars::DT_MS) = real_dt_sec * 1000.0f;
	frame_data().var(StatVars::AGENT_COUNT) = m_active_agent_count;

	m_pEnvironment->update(*this, final_dt);
	m_engine.step(*this, final_dt);
	//int abp = nr_active_agents();
	m_pCollision_resolver->prune(*this);
	//qInfo() << "agents berfore/after prune" << abp << "/" << nr_active_agents();

	update_agents_vis();

	m_step_count++;
	if (m_stable_record) {
		f.opengl_master().snap_screenshot();
		f.opengl_master().wait_till_screenshot_done();
	}

	// the timer is already restarted for the next frame, and its start is exactly the end of the last frame
	m_frame_stats.push(m_step_timer.get_start_time());
	m_pStatsView->update(m_frame_stats);
}

KeyEventReturnValue SimulationCore::KeyClick(Qt::Key key)
{
	if (key == Qt::Key::Key_P) {
		if (!m_paused) {
			//m_total_timer.stop();
			m_paused = true;
		}
		else {
			m_post_pause_timestep = true;
			m_paused_step = 0;
			m_paused = false;
		}
	}
	if (key == Qt::Key::Key_S) {
		m_debug_step_event.Signal_One();
		m_paused_step = qMax(m_step_count, m_paused_step) + 1;
	}
	if (key == Qt::Key::Key_D) {
		m_debug_step_event.Signal_One();
		m_paused_step = qMax(m_step_count, m_paused_step) + 1;
	}
	if (key == Qt::Key::Key_F6) {
		m_stable_record = !m_stable_record;
		qInfo() << "Start stable record";
	}
	return {};
}

void SimulationCore::set_engine(EngineSetup new_engine)
{
	auto id = std::this_thread::get_id();

	if (id == m_simulation_thread_id)
	{
		bool do_add_engine = false;

		if (new_engine == m_engine || !new_engine.is_complete())
		{
			m_engine = { nullptr, nullptr, "None" };
		}
		else {
			if (!m_engine.is_complete()) {
				m_step_count = 0;
				m_paused_step = 0;
				m_time_analyser.reset();
			}

			m_engine = new_engine;

			if (m_pEnvironment) m_engine.hard_reset(*this);

			do_add_engine = true;
		}

		if (do_add_engine) m_main_tasks.add(std::bind(&SimulationCore::add_engine, this, new_engine));
		m_main_tasks.add(std::bind(&SimulationCore::refresh_ui, this, false));
	}
	else {
		m_sim_tasks.add(
			std::bind(&SimulationCore::set_engine, this, new_engine)
		);
	}
}

void SimulationCore::add_engine(EngineSetup new_engine)
{
	for (auto& setup : m_engine_list) {
		if (setup == new_engine) return;
	}

	m_engine_list.push_back(new_engine);
	int i = (int)m_engine_list.size() - 1;

	auto act = new QAction(new_engine.name, &m_window);
	m_simmenu->addAction(act);

	QObject::connect(act, &QAction::triggered, this, [this, i] { act_set_engine(i); });
	m_sim_actionmap[new_engine.name] = act;
}

void SimulationCore::set_goalpicker(IGoalPicker_ptr pGoalPicker)
{
	auto id = std::this_thread::get_id();

	if (id == m_simulation_thread_id)
	{
		bool do_add_gp = false;

		if (pGoalPicker == m_pGoalPicker || pGoalPicker == nullptr)
		{
			m_pGoalPicker = nullptr;
		}
		else {
			m_pGoalPicker = pGoalPicker;
			m_pColorizer.set_goalpicker(m_pGoalPicker);

			if (m_pEnvironment) {
				pGoalPicker->choose_goals(*this);
				colorize_agents();
			}
			do_add_gp = true;
		}

		if (do_add_gp) m_main_tasks.add(std::bind(&SimulationCore::add_goalpicker, this, pGoalPicker));
		m_main_tasks.add(std::bind(&SimulationCore::refresh_ui, this, false));
	}
	else {
		m_sim_tasks.add(
			std::bind(&SimulationCore::set_goalpicker, this, pGoalPicker)
		);
	}
}

void SimulationCore::add_goalpicker(IGoalPicker_ptr pGoalPicker)
{
	Q_ASSERT(pGoalPicker != nullptr);

	for (auto ptr : m_goal_picker_list) {
		if (ptr == pGoalPicker) return;
	}

	m_goal_picker_list.push_back(pGoalPicker);
	int i = (int)m_goal_picker_list.size() - 1;

	auto act = new QAction(pGoalPicker->goal_picker_name(), &m_window);
	m_goalmenu->addAction(act);

	QObject::connect(act, &QAction::triggered, this, [this, i] { act_set_goalpicker(i); });
	m_goalpicker_actionmap[pGoalPicker] = act;
}

void SimulationCore::set_environment(IEnvironment_ptr pEnv, bool delay_reset)
{
	auto id = std::this_thread::get_id();

	if (id == m_simulation_thread_id)
	{
		{
			// wait for the render thread to finish the current frame
			QMutexLocker locker(&m_rendermutex);
			m_render_enabled = false;
		}

		bool do_add_env = false;

		if (pEnv == m_pEnvironment || pEnv == nullptr) {
			m_pEnvironment = nullptr;
		}
		else {
			m_pEnvironment = pEnv;
			f.opengl_master().focusView2D(pEnv->bbox());

			m_renderer3d.set_center(pEnv->center3d());// to_3d(pEnv->bbox().center(), pEnv->average_elevation()));

			pEnv->enable(*this);

			m_step_count = 0;
			m_paused_step = 0;
			m_time_analyser.reset();

			do_add_env = true;
		}

		set_settings(pEnv->get_settings(), true);

		if (do_add_env) m_main_tasks.add(std::bind(&SimulationCore::add_environment, this, pEnv));
		else m_main_tasks.add(std::bind(&SimulationCore::refresh_ui, this, true));

		if (!delay_reset) reset();

		m_render_enabled = true;
	}
	else {
		m_sim_tasks.add(
			std::bind(&SimulationCore::set_environment, this, pEnv, false)
		);
	}
}

void SimulationCore::add_environment(IEnvironment_ptr pEnv)
{
	Q_ASSERT(pEnv != nullptr);

	for (auto ptr : m_environment_list) {
		if (ptr == pEnv) return;
	}

	m_environment_list.push_back(pEnv);
	int i = (int)m_environment_list.size() - 1;

	auto act = new QAction(pEnv->name(), &m_window);
	m_envmenu->addAction(act);

	QObject::connect(act, &QAction::triggered, this, [this, i] { act_set_env(i); });
	m_env_actionmap[pEnv] = act;

	refresh_ui(false);
}

void SimulationCore::colorize_agents()
{
	circleDrawer().colorize(m_pColorizer);
}

EngineSetup SimulationCore::find_engine(QString name)
{
	for (auto& eng : m_engine_list) {
		if (eng.name.compare(name) == 0) {
			return eng;
		}
	}
	return {};
}

IGoalPicker_ptr SimulationCore::find_goalpicker(QString name)
{
	for (auto& gp : m_goal_picker_list) {
		if (gp->goal_picker_name().compare(name) == 0) {
			return gp;
		}
	}
	return nullptr;
}

SimulationStats::FrameData& SimulationCore::frame_data()
{
	return m_frame_stats.current();
}

Vector2f SimulationCore::try_move(int agent_id, Vector2f displacement)
{
	//Q_ASSERT(displacement.norm() < settings().agent_rad() * 100);
	//  TODO:
	//	This will assume a position of the agent not inside an obstacle
	//	and move the agent as far into the direction of displacement without
	//	causing it to collide with it
	//	TODO:
	//	Allow small deviations in direction
	//	OR:
	//	Somehow detect an impossible movement from pre-post

	static thread_local std::vector<int> obst_id_list;

	constexpr float bloat_pre = 0.001f;
	constexpr float bloat_post = 0.005f;

	Vector2f target_pos = position(agent_id) + displacement * (1 + bloat_post);
	Vector2f trace_start = position(agent_id) - displacement * bloat_pre;
	Vector2f true_diff = target_pos - trace_start;

	// these two traces could be made into a single trace
	collision_resolver().neighbourFinder().trace_nearby_obstacles({ { trace_start, target_pos } }, obst_id_list);

	Obstacle::HitInfo info;
	Obstacle::hit_single_collider(*this, trace_start, target_pos, obst_id_list, info, true);

	// reinterpret progress in displacement space
	info.collision_prog = qBound(0.0f, info.collision_prog * (1 + (bloat_pre + bloat_post)) - bloat_pre, 0.999f);

	if (!info.has_hit) {
		position(agent_id) += displacement;
		return { 0, 0 };
	}
	else if (info.is_portal && info.collision_prog < 1 - Geometry::Epsilon) {
		Vector2f teleportation;
		position(agent_id) = info.move_through_portal(position(agent_id), position(agent_id) + displacement, teleportation);
		return teleportation;
	}
	else {
		position(agent_id) = trace_start + displacement * info.collision_prog;

		if (info.collision_prog < 0.8f)
		{
			Vector2f move_left = (1 - info.collision_prog) * displacement;
			Vector2f hit_edge_dirn = (info.p2 - info.p1).normalized();
			float dot_align = move_left.dot(hit_edge_dirn);
			Vector2f next_move_attempt = dot_align * hit_edge_dirn;

			if (next_move_attempt.squaredNorm() > ipow(0.0001f, 2)) { // TODO not hardcoded but in terms of agent size
				return try_move(agent_id, next_move_attempt - displacement * 0.01f);
			}
		}

		return { 0, 0 };
	}
}

void SimulationCore::reset()
{
	Q_ASSERT(std::this_thread::get_id() == m_simulation_thread_id);
	m_step_count = 0;
	//m_total_timer.stop(true);
	m_post_pause_timestep = true;
	m_sim_time_sec = 0;
	m_paused_step = 0;

	position_buffer = {}; // zero size
	if (m_pLineDrawer) m_pLineDrawer->clear();

	if (m_pEnvironment) {

		m_pEnvironment->reset(*this);
		m_pCollision_resolver->reset(*this);

		setup_agents();

		m_agent_positions = m_agentpositions_initial;

		m_time_analyser.reset();

		if (m_pGoalPicker) {
			m_pGoalPicker->reset(*this);
			m_pGoalPicker->choose_goals(*this);
			colorize_agents();
		}

		if (m_engine.is_complete()) {
			m_engine.hard_reset(*this);
		}

		m_renderer3d.reset();

		m_pStatsView->renew();
	}


}

void SimulationCore::refresh_ui(bool true_reset)
{
	auto id = std::this_thread::get_id();

	Q_ASSERT(id != m_simulation_thread_id);

	// top level menu : environment
	if (m_envmenu == nullptr) {
		m_envmenu = m_window.add_menu("Environment");
	}
	m_envmenu->clear();

	// add environments
	for (auto entry : m_env_actionmap) {
		m_envmenu->addAction(entry.second);
		entry.second->setCheckable(true);
		entry.second->setChecked(entry.first == m_pEnvironment);
	}

	// top level menu : goal picker
	if (m_goalmenu == nullptr) {
		m_goalmenu = m_window.add_menu("Goal picker");
	}
	m_goalmenu->clear();

	for (auto entry : m_goalpicker_actionmap) {
		m_goalmenu->addAction(entry.second);
		entry.second->setCheckable(true);
		entry.second->setChecked(entry.first == m_pGoalPicker);
	}

	// top level menu : simulation
	if (m_simmenu == nullptr) {
		m_simmenu = m_window.add_menu("Simulation");
	}
	m_simmenu->clear();

	// simulation : reset
	if (m_act_reset == nullptr) {
		m_act_reset = new QAction("Reset", &m_window);
		QObject::connect(m_act_reset, &QAction::triggered, this, &SimulationCore::act_reset);
	}
	m_simmenu->addAction(m_act_reset);

	// simulation : settings
	if (m_act_settings == nullptr) {
		m_act_settings = new QAction("Settings", &m_window);
		QObject::connect(m_act_settings, &QAction::triggered, this, &SimulationCore::act_settings);
	}
	m_simmenu->addAction(m_act_settings);

	for (auto entry : m_sim_actionmap)
	{
		m_simmenu->addAction(entry.second);
		entry.second->setCheckable(true);
		entry.second->setChecked(entry.first == m_engine.name);
	}

	// top level menu : visualize
	if (m_vismenu == nullptr) {
		m_vismenu = m_window.add_menu("Visualize");
	}
	m_vismenu->clear();

	// visualize : switch renderer 
	if (m_switch_renderer == nullptr) {
		m_switch_renderer = new QAction("Switch renderer", &m_window);
		QObject::connect(m_switch_renderer, &QAction::triggered, this, &SimulationCore::act_switch_renderer);
	}
	m_vismenu->addAction(m_switch_renderer);

	// visualize : show particles
	if (m_show_particles == nullptr) {
		m_show_particles = new QAction("Show particles", &m_window);
		m_show_particles->setCheckable(true);
		QObject::connect(m_show_particles, &QAction::triggered, this, &SimulationCore::act_show_part);
	}
	m_show_particles->setChecked(m_draw_agents);
	m_vismenu->addAction(m_show_particles);

	if (m_show_smoke == nullptr) {
		m_show_smoke = new QAction("Show smoke", &m_window);
		m_show_smoke->setCheckable(true);
		QObject::connect(m_show_smoke, &QAction::triggered, this, &SimulationCore::act_show_smoke);
	}
	m_show_smoke->setChecked(true);
	m_vismenu->addAction(m_show_smoke);

	if (m_pLineDrawer) {
		if (m_show_lines == nullptr) {
			m_show_lines = new QAction("Show lines", &m_window);
			m_show_lines->setCheckable(true);
			QObject::connect(m_show_lines, &QAction::triggered, this, &SimulationCore::act_show_lines);
		}
		m_show_lines->setChecked(m_draw_lines);
		m_vismenu->addAction(m_show_lines);
	}

	if (m_engine.is_complete()) m_engine.make_ui(*this);

	if (m_pCollision_resolver) {
		m_pCollision_resolver->make_ui(*this);
	}

	if (m_pEnvironment) {
		m_eventview.reset_ui(*this, m_pEnvironment->get_obstacle_events(), !true_reset);
	}

	//m_pEnvironment->make_ui();
}

void SimulationCore::update_agents_vis()
{
	if (m_renderer3d.enabled()) {
		m_renderer3d.update(m_pColorizer);
	}
	else {

		for (int i = 0, n = nr_active_agents(); i < n; i++) {
			m_pCircleDrawer->move(i, position(i));
		}

		m_pCircleDrawer->update();

		update_line_draw();
	}
}

void SimulationCore::update_line_draw()
{
	if (!m_pLineDrawer) return;



	/*m_pLineDrawer->clear();
	auto no_agents = nr_active_agents();

	SPHEngine* sph = dynamic_cast<SPHEngine*>(&(this->agent_mover()));
	if (sph) {
		auto& forces = sph->forces();
		for (int i = 0; i < no_agents; i++) {
			auto v = sph->debug_agent_nearest_obst(i);
			if (v(0) != std::numeric_limits<float>::infinity()) {
				m_pLineDrawer->add(position(i), v, {0,255,0,255});
			}
			m_pLineDrawer->add(position(i), position(i) + VEC2_ACCES(forces, i) * 0.1f, { 255,0,0,255 });

		}
	}

	//auto no_agents = nr_active_agents();
	auto& nf = collision_resolver().neighbourFinder();
	nf.update_agent_query_grid(*this, settings().compute_sph_kernel_rad()); //settings().compute_sph_kernel_rad() // nf.pref_infl_rad_agents_collide(*this)
	NeighbourFinder::FindResultMultiCell frmc;

	for (int i = 0; i < no_agents; i++) {
		nf.find_nearby_agents_virtual(*this, position(i), frmc);
		for (auto& va : frmc.virtual_agents) {
			Q_ASSERT(va.agent_id >= 0 && va.agent_id < no_agents);
			if (i <= va.agent_id) continue;

			Vector2f p1 = position(i);
			Vector2f p2 = va.virtual_location;
			Vector2f norm = (p2 - p1).normalized();
			Vector2f offset = 0.1f * m_pSettings->agent_rad() * rotate_90_deg_cw(norm);
			Color c;
			//if (norm(1) >= 0) {
				c = { 0, 255, 0, 100 };
			//}
			//else {
				//c = { 0, 0, 255, 100 };
			//}
			m_pLineDrawer->add(position(i) + offset, va.virtual_location + offset, c);
		}
	}*/


	// tracing:

	if (position_buffer.size() > 0)
	{
		float time = 10;
		float low = 0.2f;
		float high = 0.3f;

		float delta = 1 / (high - low);
		float progress = qMin(m_sim_time_sec / time, 1.0f);
		unsigned char opacity = (uchar)(255 * clamp(low + (high - low) * progress, low, high));

		auto no_agents = nr_active_agents();
		float max_len = settings().agent_rad() * 8;

		for (int i = 0; i < no_agents; i++)
		{
			Color c = circleDrawer().get_color(i);
			c.a = opacity;
			float len = (position(i) - VEC2_ACCES(position_buffer, i)).norm();
			if (len <= max_len) m_pLineDrawer->add(position(i), VEC2_ACCES(position_buffer, i), c);
		}
	}
	position_buffer = positions();


	// forces:

	m_pLineDrawer->update();
}

template<typename T>
void value_swap(T& a, T& b) {
	T temp = a;
	a = b;
	b = temp;
}

void SimulationCore::disable_agent(int n)
{
	position(n) = position(m_active_agent_count - 1);
	velocity(n) = velocity(m_active_agent_count - 1);
	m_pGoalPicker->set_goal(n, m_pGoalPicker->goal(m_active_agent_count - 1));

	m_active_agent_count -= 1;
	m_pCircleDrawer->pop();
}

Vector2i SimulationCore::compute_grid_dimensions(float agent_rad, AABB2 region)
{
	Vector2f dimf = (0.5f * region.sizes() / (agent_rad * 1.0f)) + Vector2f{ 0.5f, 0.5f };
	dimf(0) = qMax(1.0f, dimf(0));
	dimf(1) = qMax(1.0f, dimf(1));
	return dimf.cast<int>();
}

void SimulationCore::act_reset()
{
	m_sim_tasks.add(
		std::bind(&SimulationCore::reset, this)
	);
}

void SimulationCore::act_settings()
{
	m_pSettings->open_dialog();
}

void SimulationCore::act_show_part()
{
	m_sim_tasks.add([this]() {
		m_draw_agents = !m_draw_agents;
	});
}

void SimulationCore::act_show_smoke()
{
	m_sim_tasks.add([this]() {
		m_pEnvironment->smoke().toggle_render();
	});
}

void SimulationCore::act_show_lines()
{
	m_sim_tasks.add([this]() {
		m_draw_lines = !m_draw_lines;
	});
}

void SimulationCore::act_switch_renderer()
{
	m_renderer3d.enable(!m_renderer3d.enabled());
	update_agents_vis();
}

void SimulationCore::act_set_engine(int i)
{
	set_engine(m_engine_list[i]);
}

void SimulationCore::act_set_env(int i)
{
	set_environment(m_environment_list[i]);
}

void SimulationCore::act_set_goalpicker(int i)
{
	set_goalpicker(m_goal_picker_list[i]);
}

bool SimulationCore::object_changed(SimulationSettings* pObj)
{
	if (pObj == m_pSettings.data())
	{
		m_sim_tasks.add([this]() {

			auto pOldSettings = m_pSettings->apply_changes();
			handle_settings_change(pOldSettings, m_pSettings);
		});
		return true;
	}
	return false;
}

void SimulationCore::debug_wait_step()
{
	m_debug_step_event.Wait(true);
}

void SimulationCore::setup_agents()
{
	m_active_agent_count = settings().agent_count();

	require_vector_size(m_agent_positions);
	require_vector_size(m_agent_velocities);
	require_vector_size(m_agent_pos_3d, 3);

	// ask the environment to position agents:
	m_pEnvironment->distribute_agents(*this);

	/*{ // temp test:
		position(0) = Vector2f{ 4.0,3.0 };
		position(1) = Vector2f{ 3.0,3.0 };
	}*/


#ifdef _DEBUG
	int collision = 5;
#else
	int collision = 50;
#endif
	VecX<float> dummy;
	require_vector_size(dummy);
	m_pCollision_resolver->resolve_collisions(*this, dummy, collision);

	// backup agent locations, reuse on simulation reset
	m_agentpositions_initial = m_agent_positions;

	setup_agents_draw();
}

void SimulationCore::setup_agents_draw()
{
	if (m_renderer3d.enabled()) {
		m_renderer3d.update(m_pColorizer);
	}

	// this is needed even if 3d rendering is currently enabled
	if (m_pCircleDrawer) {
		m_pCircleDrawer->clear();
		//float size = 1.1f / qSqrt(ac);
		float rad = settings().agent_rad();

		for (int i = 0; i < nr_active_agents(); i++) {
			Vector2f pos = position(i);
			m_pCircleDrawer->add(m_leading_color, { pos(0), pos(1) }, rad);
		}

		m_pCircleDrawer->update();

		update_line_draw();
	}
}

void SimulationCore::handle_settings_change(SimulationSettings_ptr old_settings, SimulationSettings_ptr new_settings, bool delay_reset)
{
	// check those settings whose change requires a reset
	// also adept the state to the new settings where dynamically possible

	{
		// wait for the render thread to finish the current frame
		QMutexLocker locker(&m_rendermutex);
		m_render_enabled = false;
	}

	if (m_pEnvironment) {
		auto json = new_settings->to_json();
		m_pEnvironment->set_settings(json);
	}
	if (!delay_reset) {
		bool needs_reset = old_settings->agent_count() != new_settings->agent_count();
		if (needs_reset) reset();
		else {

			// everything depends on the environment
			// unless environment is set, do nothing

			if (m_pEnvironment) {
				m_pEnvironment->reset(*this);

				m_pCollision_resolver->settings_changed(*this, old_settings);

				if (m_pGoalPicker) {
					bool goalpicker_has_changed = m_pGoalPicker->settings_changed(*this, old_settings);
					if (goalpicker_has_changed) {
						m_pGoalPicker->choose_goals(*this);
					}
					colorize_agents();
				}

				setup_agents_draw();

				if (m_engine.is_complete()) m_engine.settings_changed(*this, old_settings);
			}
		}
	}

	m_render_enabled = true;
}

void SimulationCore::run()
{
	m_simulation_thread_id = std::this_thread::get_id();

	while (m_flag_alive) {
		m_sim_tasks.execute();
		if (m_pEnvironment && m_paused) {
#ifdef _DEBUG
			m_pCollision_resolver->neighbourFinder().debug_neighbours(*this);
#endif
		}
		if (m_engine.is_complete() && m_pEnvironment && m_pGoalPicker
			&& (!m_paused || m_paused_step > m_step_count)) sim_step();
		else std::this_thread::sleep_for(std::chrono::milliseconds(100));
}
}

void SimulationCore::TimeAnalyse::reset()
{
	temp_total[0] = temp_total[1] = 0;
	steps[0] = steps[1] = 0;
	dt_max = 0;
}

void SimulationCore::TimeAnalyse::step(double dt)
{
	if (steps[current_bufferid] == stepRefresh) {
		current_bufferid = 1 - current_bufferid;
		steps[current_bufferid] = 0;
		temp_total[current_bufferid] = 0;
	}
	steps[current_bufferid]++;
	temp_total[current_bufferid] += dt;
	if (dt > dt_max) dt_max = dt;
}

double SimulationCore::TimeAnalyse::avg_dt()
{
	int cache_bufferid = 1 - current_bufferid;
	int s_total = steps[cache_bufferid] + steps[current_bufferid];
	if (s_total > 0) {
		double t_total = temp_total[cache_bufferid] + temp_total[current_bufferid];
		return t_total / s_total;
	}
	else return 0;
}

void EngineSetup::render(RenderContext rc)
{
	if (pGlobalPlanner) pGlobalPlanner->render_gp(rc);
	if (pAgentMover) pAgentMover->render_am(rc);
}

void EngineSetup::step(SimulationCore& sc, float dt)
{
	sc.goal_picker().update_goals(sc);
	sc.collision_resolver().neighbourFinder().require_obstacle_register(sc);
	pGlobalPlanner->update(sc); // this update does not do global planning, the agent mover has control over that
	pAgentMover->step(sc, dt);
}

bool EngineSetup::is_complete()
{
	return pGlobalPlanner != nullptr && pAgentMover != nullptr;
}

void EngineSetup::hard_reset(SimulationCore& sc)
{
	pGlobalPlanner->reset(sc);
	pAgentMover->reset(sc);
}

void EngineSetup::settings_changed(SimulationCore& sc, SimulationSettings_ptr old_settings)
{
	//pGoalPicker->settings_changed(sc, old_settings);
	pGlobalPlanner->settings_changed(sc, old_settings);
	pAgentMover->settings_changed(sc, old_settings);
}

void EngineSetup::make_ui(SimulationCore& sc)
{
	//pGoalPicker->make_ui(sc);
	pGlobalPlanner->make_ui(sc);
	pAgentMover->make_ui(sc);
}

bool EngineSetup::operator==(const EngineSetup& other) const
{
	return (name.compare(other.name) == 0);
	//&& (pGoalPicker == other.pGoalPicker)
	//&& (pAgentMover == other.pAgentMover)
	//&& (pAgentMover == other.pAgentMover);
}
