#pragma once

#include <QSharedPointer>

class SimulationCore;

class SimulationSettings;
using SimulationSettings_ptr = QSharedPointer<SimulationSettings>;

class IEnvironment;
using IEnvironment_ptr = QSharedPointer<IEnvironment>;

class SquareEnvironment;
class ContinuumEngine;
class SPHEngine;
class TestSimulation;
class CollisionResolver;
