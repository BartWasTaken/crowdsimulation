#ifndef SIMULATIONSETTINGS_H
#define SIMULATIONSETTINGS_H

#include <QJsonObject>
#include <QMenu>
#include <QDialog>
#include <QTableView>
#include <QSharedPointer>
#include <QStandardItem>
#include <QStandardItemModel>
#include <vector>
#include <utility>

#include "simdeclr.h"
#include "tools/ChangedListener.h"
#include "tools/shared_eigen_incl.h"

#define ADD_VAR(type, name)\
public:\
	inline void name(type v) {m_##name = v;}\
	inline type name() const {return m_##name;}\
private:\
	type m_##name;

class SimulationSettings
	: public QObject, public ChangedShouter<SimulationSettings>
{
	Q_OBJECT
public:
	SimulationSettings();
	SimulationSettings(QJsonObject);
	~SimulationSettings();

	QJsonObject to_json() const;
	void from_json(const QJsonObject&);

	void open_dialog();

	// returns old version
	SimulationSettings_ptr apply_changes();
	SimulationSettings_ptr copy();
;
	void accept();
	void reject();

	template<typename T>
	void add_variable(QString name, T var) {
		m_json_extra[name] = var;
		m_refill_dialog = true;
	}
	bool exists_var(QString name) {
		return m_json_extra.contains(name);
		//auto res = m_json_extra.find(name);
		//if (res->isNull()) return false;
		//return true;
	}
	float retreive_float(QString name, bool* succes = nullptr) {
		auto jsonobj = m_json_extra[name];
		if (succes) *succes = jsonobj.isDouble();
		return (float)jsonobj.toDouble(0);
	}
	int retreive_int(QString name, bool* succes = nullptr) {
		auto jsonobj = m_json_extra[name];
		if (succes) *succes = jsonobj.isDouble();
		return (int)(jsonobj.toDouble(0) + 0.5);
	}

	float compute_sph_kernel_rad();
	bool is_valid();

	ADD_VAR(qint32, agent_count);
	ADD_VAR(float, agent_rad);
	ADD_VAR(float, agent_speed);

	ADD_VAR(qint32, mde_max_iter);

	ADD_VAR(float, sph_kernel_rad);
	ADD_VAR(float, sph_max_dens_coef);
	ADD_VAR(float, sph_stiffness);
	ADD_VAR(qint32, sph_max_iter_dens);

	ADD_VAR(float, cnt_discomfort_project_dt);
	ADD_VAR(float, cnt_weight_discomfort_smoke);
	ADD_VAR(float, cnt_weight_discomfort_agent);
	ADD_VAR(float, cnt_weight_discomfort_obst);
	ADD_VAR(float, cnt_weight_dist);
	ADD_VAR(float, cnt_weight_time);

private:

	QJsonObject m_json_extra;
	QJsonObject m_dialog_json;

	QDialog* m_pDialog = nullptr;
	std::vector<std::pair<QStandardItem*, QStandardItem*>> m_dialog_items;
	QStandardItemModel* m_dialog_items_ui;
	QTableView*	m_ui_table;

	bool m_refill_dialog = true;
};


#undef ADD_VAR

#endif // SIMULATIONSETTINGS_H
