#pragma once

#include "Support/neighbourfinder.h"
#include "vis/openglincl.h"
#include "vis/GridDrawer2D.h"
#include "tools/include.h"
#include "kernels.h"
#include "sim/goals.h"
#include "tools/parrallelforpool.h"
#include "tools/changedlistener.h"
#include "support/obstaclegridprovider.h"
#include <vector>
#include <array>
#include <QObject>
#include <map>

class SimulationCore;

class ContinuumField
	: public QObject, public ChangedShouter<ContinuumField>
{
	Q_OBJECT
public:
	ContinuumField(OpenGLView&);
	void render(RenderContext rc);

	void setup(SimulationCore&, AABB2 region, std::vector<IGoal_ptr> goals);

	void update(SimulationCore&, float dt);
	void step_end(SimulationCore&);

	void make_ui(SimulationCore& sc);
	void act_vis_grid();
	void act_vis_arrow();
	void act_vis_dir();

	Vector2f retreive_velocity(Vector2f pos, int group);
	Vector2f retreive_avg_velocity(Vector2f);
	float retreive_potential(Vector2f pos, int group);

	int goal_count();

	//void bleed_local_velocity(float range, int max_steps);

private:

	//Vector2f evaluate_flow(Vector2f dirn);

	static inline bool valid_x(int x, const Vector2i& dim) {
		return x >= 0 && x < dim(0);
	}
	static inline bool valid_y(int y, const Vector2i& dim) {
		return y >= 0 && y < dim(1);
	}
	inline bool valid_x(int x) {
		return x >= 0 && x < m_raster.resolution(0);
	}
	inline bool valid_y(int y) {
		return y >= 0 && y < m_raster.resolution(1);
	}

	//inline bool is_obstacle(int x, int y) { return is_obstacle(m_raster.idx(x, y)); }
	//bool is_obstacle(int idx);

	void update_reachability(SimulationCore&);

	void reset_fields(bool keep_potential);
	void make_dens_vel_fields(int agent_count, VecX<float>& positions, VecX<float>& velocities);
	void make_flow_field(float max_velocity);
	void handle_smoke(SimulationCore&);
	void project_agent_discomfort(int agent_count, float dt, VecX<float>& positions, VecX<float>& velocities);
	void project_obstacle_discomfort(SimulationCore&);
	void make_cost_field();

	float max_potential(int group_id);

	void compute_grad_vel(SimulationCore& sc, int group_id);

	void cost_field_test_1(int agent_count, VecX<float>& positions, VecX<float>& velocities, float pref_speed);

	struct SCell // shared cell
	{
		float density;
		Vector2f velocity;
		Vector2f velocity_bleedbuffer;
		float flow[4];
		float cost[4];
		float portal_flow;
		float portal_cost;

		float discomfort;

		// only a single possible portal allowed per cell
		int portal_cell_neighbour;
		int direction;

		void reset(bool keep_environment) 
		{
			velocity.setZero();
			density = 0;
			discomfort = 0;
			for (int i = 0; i < 4; i++) {
				flow[i] = 0;
				cost[i] = 0;
			}
			if (!keep_environment) portal_cell_neighbour = -1;
		}
	};
	struct GCell // group cell
	{
		enum STATE {
			KNOWN, UNKNOWN, CANDIDATE
		} state = UNKNOWN;

		float potential = std::numeric_limits<float>::infinity();
		bool unreachable;
		//Vector2f grad{ 0,0 };
		Vector2f pref_vel{ 0,0 };

		void reset(bool keep_potential = false) {
			if (!keep_potential) potential = std::numeric_limits<float>::infinity();
			state = UNKNOWN;
			unreachable = false;
			//grad.setZero();
			//pref_vel.setZero(); will be overwriten
		}
		void set_goal(Vector2f inner_dir) {
			state = KNOWN;
			potential = 0;
			pref_vel = inner_dir;
		}
	};
	struct Group {
		std::vector<GCell> field;
		std::vector<Vector2i> goal;
		/*inline void init(std::vector<Vector2i>& goal, int cell_count) {
			this->goal = goal;
			field.clear();
			field.resize(cell_count);
		}*/
	};

	struct Compare_potential {
		Compare_potential(std::vector<GCell>& field)
			: m_field(field)
		{}
		bool operator()(const int& a_id, const int& b_id) const {
			float pa = m_field[a_id].potential;
			float pb = m_field[b_id].potential;
			if (pa != pb) return pa < pb;
			else return a_id < b_id;
		}
		std::vector<GCell>& m_field;
	};

	void visualize_update(int group_id);

	inline float density_at(Vector2f pos) {
		return density_at_01(m_raster.to_unit_space(pos));
	}
	float density_at_01(Vector2f posn);
	inline float& density_at(int i, int j) { return density_at(m_raster.idx(i, j)); }
	inline float& density_at(int id) { return m_grid[id].density; }

	inline Vector2f avg_velocity_at(Vector2f pos) {
		return avg_velocity_at_01(m_raster.to_unit_space(pos));
	}
	Vector2f avg_velocity_at_01(Vector2f posn);
	inline Vector2f& avg_velocity_at(int i, int j) { return avg_velocity_at(m_raster.idx(i, j)); }
	inline Vector2f& avg_velocity_at(int id) { return m_grid[id].velocity; }

	inline float& flow_at(int i, int j, Dir4::DIR d) { return flow_at(m_raster.idx(i, j), d); }
	inline float& flow_at(int id, Dir4::DIR d) { return m_grid[id].flow[(int)d]; }

	inline float& cost_at(int i, int j, Dir4::DIR d) { return cost_at(m_raster.idx(i, j), d); }
	inline float& cost_at(int id, Dir4::DIR d) { return m_grid[id].cost[(int)d]; }
	inline float& portal_cost_at(int id) { return m_grid[id].portal_cost; }

	inline float& discomfort_at(int i, int j) { return discomfort_at(m_raster.idx(i, j)); }
	inline float& discomfort_at(int id) { return m_grid[id].discomfort; }

	inline float potential_at(int g, int id) {
		return m_groups[g].field[id].potential;
	}
	inline float potential_at(int g, int i, int j) {
		return potential_at(g, m_raster.idx(i, j));
	}

	inline Vector2f& pref_vel_at(int g, int id) {
		return m_groups[g].field[id].pref_vel;
	}
	inline Vector2f& pref_vel_at(int g, int i, int j) {
		return pref_vel_at(g, m_raster.idx(i, j));
	}

	void compute_cost(int i, int j, Dir4::DIR d);
	void compute_portal_cost(int from_idx, int portal_idx, Dir4::DIR portal_dir);

	void write_agent_discomfort(Vector2f posn, float magnitude);

	// evaluate speed in one of N,E,S,W
	float eval_flow(Vector2f future_pos_01, float max_speed, float min_dens, float max_dens, int dimension, float sign);

	inline float eval_flow_N(Vector2f pos_01, float r_01, float max_speed, float min_dens, float max_dens) {
		return eval_flow(pos_01 + Vector2f{ 0, r_01 }, max_speed, min_dens, max_dens, 1, 1);
	}
	inline float eval_flow_E(Vector2f pos_01, float r_01, float max_speed, float min_dens, float max_dens) {
		return eval_flow(pos_01 + Vector2f{ r_01, 0 }, max_speed, min_dens, max_dens, 0, 1);
	}
	inline float eval_flow_S(Vector2f pos_01, float r_01, float max_speed, float min_dens, float max_dens) {
		return eval_flow(pos_01 + Vector2f{ 0, -r_01 }, max_speed, min_dens, max_dens, 1, -1);
	}
	inline float eval_flow_W(Vector2f pos_01, float r_01, float max_speed, float min_dens, float max_dens) {
		return eval_flow(pos_01 + Vector2f{ -r_01, 0 }, max_speed, min_dens, max_dens, 0, -1);
	}

	void write_agent_to_field(Vector2f pos_01, Vector2f vel);

	float solve_equasion(double a, double b, double c, double d);

	float evaluate_potential(int g, int i, int j);
	float evaluate_potential_trough_portal(int g, int i, int j, int portal_origin_dir, int origin_idx);
	float evaluate_potential(float P[4], float C[4]);

	void write_grad_vel(int g_i, int i, int j, std::vector<Vector2i>& edge);

	void consider_next_candidate(ContinuumField::Group& g, int g_i, int nidx, int nx, int ny,
		std::map<int, Vector2i, Compare_potential>& candidates, bool origin_unreachable);

	Raster								m_raster;
	std::vector<SCell>					m_grid;
	std::vector<Group>					m_groups;
	//std::vector<bool>*					m_pObstacle_grid;
	MovableGridRef						m_movegrid;
	std::vector<float>					m_obstacle_discomfort_cache;

	bool								m_field_initialized = false;
	float								m_project_velocity_cell_dist;

	// variable cache:
	float								m_dist_weight;
	float								m_time_weight;
	float								m_agent_discomfort_weight;
	float								m_obst_discomfort_weight;
	float								m_very_large;

#pragma region graphics
	GridDrawer2D*						m_pGridDrawer = nullptr;
	bool								m_draw_grid;
	bool								m_draw_arrow;
	bool								m_draw_dir;
	bool								m_step_end_was_called = true;
#pragma endregion

#pragma region multithreaded groups
	struct TaskData {
		int g_i;
		float dt;

		// for thread 1:
		bool do_debug_step;
		int debug_step_count;
	};
	ParrallelForPool<TaskData>			m_par_for_pool;
#pragma endregion

	bool								m_enable_debug_vars;
	QString								m_debug_step_varname;
	QString								m_visualize_group_id_varname;
	int									m_visualize_group_id;
};
