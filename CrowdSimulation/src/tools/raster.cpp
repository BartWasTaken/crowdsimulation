#include "raster.h"
#include <QDebug>
#include <QtGlobal>
#include "tools/include.h"

Raster::Raster(Vector2f min, Vector2f max, Vector2i dim)
{
	set_region(min, max);
	set_dim(dim);
}

Raster::Raster(AABB2 bbox, Vector2i dim)
{
	set_region(bbox);
	set_dim(dim);
}

Raster::Raster()
{

}

void Raster::set_dim(Vector2i dim)
{
	m_dim = dim;
}

void Raster::set_region(AABB2 bbox)
{
	this->m_min = bbox.min();
	this->m_max = bbox.max();
	this->m_range = bbox.sizes();
}

void Raster::set_region(Vector2f min, Vector2f max)
{
	set_region({ min, max });
}

int Raster::idx(int x, int y) const
{
	return y * m_dim(0) + x;
}

int Raster::idx(Vector2i index) const
{
	return index(1) * m_dim(0) + index(0);
}

int Raster::idx(Vector2f point) const
{
	return idx(coords(point));
}

int Raster::safe_idx(Vector2f point)
{
	return idx(safe_coords(point));
}

int Raster::idx_01(Vector2f unit_space_point) const
{
	return idx(coords_01(unit_space_point));
}

Vector2i Raster::coords(Vector2f point) const
{
	return coords_f(point).cast<int>();
}

Vector2i Raster::coords(int index) const
{
	int x = index % resolution()(0);
	int y = (index - x) / resolution()(0);
	return { x, y };
}

Vector2i Raster::coords_01(Vector2f unit_space_point) const
{
	return { unit_space_point(0) * m_dim(0), unit_space_point(1) * m_dim(1) };
}

Vector2f Raster::cell_center(int x, int y) const
{
	return m_min + Vector2f{
		m_range(0) * (x + 0.5f) / m_dim(0),
		m_range(1) * (y + 0.5f) / m_dim(1)
	};
}

Vector2f Raster::cell_center(Vector2i c) const
{
	return cell_center(c(0), c(1));
}

bool Raster::contains(Vector2f point) const
{
	return point(0) >= m_min(0)
		&& point(0) < m_max(0)
		&& point(1) >= m_min(1)
		&& point(1) < m_max(1);
}

bool Raster::is_coord_valid(Vector2i c) const
{
	return is_coord_valid(c(0), c(1));
}

bool Raster::is_coord_valid(int x, int y) const
{
	return x >= 0 && x < m_dim(0) && y >= 0 && y < m_dim(1);
}

Vector2f Raster::clamp(Vector2f p) const
{
	for (int i = 0; i < 2; i++) {
		if (p(i) < m_min(i)) {
			p(i) = m_min(i);
		}
		else if (p(i) >= m_max(i)) {
			p(i) = m_max(i) - Geometry::Epsilon;
		}
	}
	Q_ASSERT(contains(p));
	return p;
}

Vector2f Raster::clamp_inner(Vector2f p) const
{
	auto cs = cell_size();
	for (int i = 0; i < 2; i++) {
		if (p(i) < m_min(i) + cs(i)) {
			p(i) = m_min(i) + cs(i);
		}
		else if (p(i) >= m_max(i) - cs(i)) {
			p(i) = m_max(i) - cs(i) - Geometry::Epsilon;
		}
	}
	return p;
}

Vector2i Raster::snap(Vector2i v) const
{
	for (int i = 0; i < 2; i++) {
		if (v(i) < 0) v(i) = 0;
		else if (v(i) >= m_dim(i)) v(i) = m_dim(i) - 1;
	}
	return v;
}

Vector2i Raster::snap_inner(Vector2i v) const
{
	for (int i = 0; i < 2; i++) {
		if (v(i) < 1) v(i) = 1;
		else if (v(i) >= m_dim(i) - 1) v(i) = m_dim(i) - 2;
	}
	return v;
}

Vector2i Raster::safe_coords(Vector2f p) const
{
	return snap(coords(p));
}

Vector2i Raster::safe_inner_coordinates(Vector2f p) const
{
	return snap_inner(coords(p));
}

Vector2i Raster::safe_coords_01(Vector2f normalized_point) const
{
	return snap(coords_01(normalized_point));
}

Vector2f Raster::bottom_left() const
{
	return m_min;
}

Vector2f Raster::bottom_right() const
{
	return { m_max(0), m_min(1) };
}

Vector2f Raster::top_left() const
{
	return { m_min(0), m_max(1) };
}

Vector2f Raster::top_right() const
{
	return m_max;
}

Vector2f Raster::operator[](int i) const
{
	//			y		x
	// 0	=	bottom	left
	// 1	=	top		left
	// 2	=	top		right
	// 3	=	bottom	right

	/*switch (i) {
		case 0: return bottom_left();
		case 1: return top_left();
		case 2: return top_right();
		case 3: return bottom_right();
		default: Q_ASSERT(false);
	}*/

	float x, y;

	if (i >= 2) // 2 or 3
	{
		x = m_max(0);
		if (i == 3) y = m_min(1);
		else y = m_max(1);
	}
	else { // 0 or 1
		x = m_min(0);
		if (i == 0) y = m_min(1);
		else y = m_max(1);
	}

	return { x, y };
}

Vector2f Raster::cell_corner(Vector2i coord, Vector2f dir) const
{
	float x_plus = dir(0) > 0 ? 1 : 0;
	float y_plus = dir(1) > 0 ? 1 : 0;
	return m_min + Vector2f{
		m_range(0) * (coord(0) + x_plus) / m_dim(0),
		m_range(1) * (coord(1) + y_plus) / m_dim(1)
	};
}

Vector2f Raster::cell_corner_bl(Vector2i coords) const
{
	return m_min + Vector2f{
		m_range(0) * (coords(0)) / m_dim(0),
		m_range(1) * (coords(1)) / m_dim(1)
	};
}

Vector2f Raster::cell_size() const
{
	return { m_range(0) / m_dim(0), m_range(1) / m_dim(1) };
}

Vector2i Raster::resolution() const
{
	return m_dim;
}

int Raster::resolution(int i) const
{
	return m_dim(i);
}

Vector2f Raster::range() const
{
	return m_range;
}

Vector2f Raster::min() const
{
	return m_min;
}

Vector2f Raster::max() const
{
	return m_max;
}

AABB2 Raster::bbox() const
{
	return { m_min, m_max };
}

AABB2 Raster::cell_bbox(int i, int j) const
{
	Vector2f bl = m_min + m_range.cwiseProduct(Vector2f{ i / (float)m_dim(0), j / (float)m_dim(1) });
	Vector2f tr = bl + cell_size();
	return { bl, tr };
}

int Raster::cell_count() const
{
	return m_dim(0) * m_dim(1);
}

Vector2f Raster::to_unit_space(Vector2f pos) const
{
	return {
		(pos(0) - m_min(0)) / m_range(0),
		(pos(1) - m_min(1)) / m_range(1),
	};
}

Raster::Block Raster::find_block(Vector2f pos, bool valid_only) const
{
	Block block;
	pos = to_unit_space(pos);

	float x = m_dim(0) * pos(0) - 0.5f;
	float y = m_dim(1) * pos(1) - 0.5f;

	block.l = qBound(-1, (int)x, m_dim(0) - 1);
	block.b = qBound(-1, (int)y, m_dim(1) - 1);
	block.r = block.l + 1;
	block.t = block.b + 1;

	if (valid_only) block.clamp_to_valid(m_dim);

	return block;
}

Raster::Block Raster::find_block(AABB2 region, bool valid_only) const
{
	Vector2i bl = safe_coords(region.min());
	Vector2i tr = snap((coords_f(region.max()) + Vector2f{ 0.99999999f,0.99999999f }).cast<int>());

	Block block{ bl(0), tr(0), bl(1), tr(1) };
	if (valid_only) block.clamp_to_valid(m_dim);
	return block;
}

Raster Raster::make_raster(Block block) const
{
	Raster raster;

	// set dimention
	raster.set_dim(block.dim());

	auto min = cell_corner_bl({ block.l, block.b });
	auto max = cell_corner_bl({ block.r + 1, block.t + 1 });
	raster.set_region(min, max);

	// compute bounding box
	//float minx = m_min(0) + m_range(0) * block.l / m_dim(0);
	//float maxx = m_min(0) + m_range(0) * (block.r+1) / m_dim(0);

	//float miny = m_min(1) + m_range(1) * block.b / m_dim(1);
	//float maxy = m_min(1) + m_range(1) * (block.t+1) / m_dim(1);

	// set bounding box
	//raster.set_region({ Vector2f{minx, miny}, Vector2f{maxx, maxy} });

	return raster;
}

Raster Raster::make_raster_plus_1_boundary() const
{
	auto cs = cell_size();
	return Raster({ m_min - cs,  m_max + cs }, m_dim + Vector2i{ 2, 2 });
}

Raster Raster::make_centers_raster() const
{
	auto hcs = 0.5f * cell_size();
	return Raster({ m_min - hcs,  m_max + hcs }, m_dim - Vector2i{ 1, 1 });
}

Raster::Segment Raster::horizontal_line(int vertical_index) const
{
	float y_coord = m_min(1) + m_range(1) * vertical_index / m_dim(1);
	Segment s;
	s.a = { m_min(0), y_coord };
	s.b = { m_max(0), y_coord };
	return s;
}

Raster::Segment Raster::vertical_line(int horizontal_index) const
{
	float x_coord = m_min(0) + m_range(0) * horizontal_index / m_dim(0);
	Segment s;
	s.a = { x_coord, m_min(1) };
	s.b = { x_coord, m_max(1) };
	return s;
}

bool Raster::operator==(const Raster& other) const
{
	return are_equal_iv(this->m_dim, other.m_dim)
		&& are_equal_fv(this->m_min, other.m_min)
		&& are_equal_fv(this->m_max, other.m_max);
}

Vector2f Raster::coords_f(Vector2f point) const
{
	float x = m_dim(0) * (point(0) - m_min(0)) / m_range(0);
	float y = m_dim(1) * (point(1) - m_min(1)) / m_range(1);
	return { x, y };
}

bool Raster::check_square_cell() const
{
	auto cs = cell_size();
	if (qAbs(1 - cs(0) / cs(1)) > 0.1f) return false;
	return true;
}

float Raster::influence_range() const
{
	auto hcs = cell_size() * 0.5f;
	return qMin(hcs(0), hcs(1));
}
