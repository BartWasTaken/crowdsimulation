#include "resizedlistener.h"

ResizedListener::ResizedListener()
{

}

void ISurface::add_resized_listener(ResizedListener* ptr)
{
	m_resized_listeners.push_back(ptr);
}

void ISurface::shout_resize(Tools::Surface2D<int> size)
{
	for (auto ptr : m_resized_listeners) {
		ptr->object_resized(this, size);
	}
}
