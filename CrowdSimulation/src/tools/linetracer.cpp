#include "linetracer.h"
#include "geomfunc.h"

LineTracer::LineTracer()
{

}

void LineTracer::rasterize_line(Vector2f p0, Vector2f p1, const Raster& grid, IRegisterGrid& output, int line_id)
{
	auto f = [&output, line_id](Vector2i c) -> bool {
		output.write(c(0), c(1), line_id);
		return false;
	};

	LineTracer::rasterize_line(p0, p1, grid, f);
}

void LineTracer::project_line_to_grid(Raster grid, Vector2f p0, Vector2f p1, Vector2f& out_start_pos, Vector2i& out_start_coords)
{
	if (grid.is_coord_valid(out_start_coords) == false)
	{
		float min_dist_sq = std::numeric_limits<float>::infinity();

		for (int i = 0, j = 3; i < 4; j = i++)
		{
			Vector2f pos;
			if (intersection(p0, p1, grid[j], grid[i], &pos))
			{
				auto dist_sq = (p0 - pos).squaredNorm();
				if (dist_sq < min_dist_sq) {
					out_start_pos = pos;
					out_start_coords = grid.safe_coords(pos);
				}
			}
		}

		if (grid.is_coord_valid(out_start_coords) == false)
		{
			out_start_coords = grid.safe_coords(out_start_pos);
		}
	}

}

void LineTracer::rasterize_delta(Vector2f a, Vector2f b, const Raster& grid, Vector2f target, Vector2f& out_delta, Vector2f& out_max)
{
	auto dir = b - a;
	auto cs = grid.cell_size();

	{
		float abs_dx = qAbs(b(0) - a(0));
		float abs_dy = qAbs(b(1) - a(1));

		float slope = abs_dx / abs_dy;

		if (slope > 1) {
			out_delta(0) = slope;
			out_delta(1) = 1;
		}
		else {
			out_delta(0) = 1;
			out_delta(1) = 1 / slope;
		}

		out_delta = out_delta.cwiseProduct(cs);
	}

	{
		if (qAbs(dir(1)) < Geometry::Epsilon) {
			out_max(0) = std::numeric_limits<float>::max();
			out_max(1) = 0;
		}
		else if (qAbs(dir(0)) < Geometry::Epsilon) {
			out_max(0) = 0;
			out_max(1) = std::numeric_limits<float>::max();
		}
		else {
			auto cc = grid.cell_size();

			float px = qAbs(target(0) - a(0)) / cs(0);
			float py = qAbs(target(1) - a(1)) / cs(1);

			out_max(0) = py * out_delta(0);
			out_max(1) = px * out_delta(1);
		}
	}
}


Vector2i LineTracer::rasterize_line(Vector2f p0, Vector2f p1, const Raster& grid, std::function<bool(Vector2i)> func)
{
	auto cs = grid.cell_size();
	Vector2f start_pos = p0;
	Vector2i start_coords = grid.coords(p0);
	Vector2i end_coords = grid.safe_coords(p1);

	project_line_to_grid(grid, p0, p1, start_pos, start_coords);

	Q_ASSERT(grid.is_coord_valid(start_coords));

	Vector2f dir = p1 - start_pos;
	float stepX = (dir(0) >= 0) ? 1 : -1;
	float stepY = (dir(1) >= 0) ? 1 : -1;

	Vector2f target = grid.cell_corner(start_coords, dir);

	Vector2f tDelta, tMax;
	rasterize_delta(start_pos, p1, grid, target, tDelta, tMax);

	Vector2i P = start_coords;
	Vector2i dim = grid.resolution();

	int max_step_x = qAbs(end_coords(0) - start_coords(0));
	int max_step_y = qAbs(end_coords(1) - start_coords(1));
	int nstepx = 0;
	int nstepy = 0;

	Vector2i Pprev;

#ifdef DEBUG
	Pprev = { std::numeric_limits<float>::nan(), std::numeric_limits<float>::nan() };
#endif

	while (true)
	{
		Q_ASSERT(grid.is_coord_valid(P));

		if (func(P)) break;
		//output.write(P(0), P(1), line_id);

		if (tMax(1) > tMax(0)) {
			if (++nstepy > max_step_y) break;
			tMax(0) = tMax(0) + tDelta(0);
			P(1) += stepY;
		}
		else {
			if (++nstepx > max_step_x) break;
			tMax(1) = tMax(1) + tDelta(1);
			P(0) += stepX;
		}

#ifdef DEBUG
		assert( ! ( (Pprev(0) == P(0) && Pprev(1) == P(1)) ) );
#endif

		Pprev = P;
	}

	return P;
}