#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <vector>

class Timer
{
public:
	using my_clock = std::chrono::high_resolution_clock;

	struct StartInfo {
		std::chrono::time_point<my_clock>					start_stamp;
		std::chrono::nanoseconds							previous_elapsed;

		float previous_elapsed_sec() { return previous_elapsed.count() / 1e9f; }
		float previous_elapsed_ms() { return previous_elapsed.count() / 1e6f; }
	};

	Timer(bool auto_start = false);
	Timer(std::chrono::nanoseconds initial_elapsed);

	void start(std::chrono::time_point<my_clock> start_time = my_clock::now());
	void stop(bool clear_progress = false);

	// returns previous elapsed seconds
	void restart(StartInfo* info = nullptr);

	bool is_running();

	// will use the input time, but only if the timer is active
	std::chrono::nanoseconds elapsed();
	std::chrono::nanoseconds elapsed(std::chrono::time_point<my_clock> override_now_time);

	float elapsed_sec();
	float elapsed_ms();

	std::chrono::time_point<my_clock> get_start_time();

	static void start_together(std::vector<Timer*>&);

	std::chrono::time_point<my_clock> get_end_time();

private:

	bool m_active = false;
	std::chrono::time_point<my_clock> m_tstart;
	std::chrono::time_point<my_clock> m_tstop;
	std::chrono::nanoseconds m_cumulative_previous { 0 };


};

#endif // TIMER_H