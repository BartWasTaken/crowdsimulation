#include "InputListener.h"
#include "InputManager.h"

InputListener::~InputListener()
{

}

MouseEventReturnValue InputListener::MousePress(MouseEventInfo)
{
	return MouseEventReturnValue();
}

MouseEventReturnValue InputListener::MouseMove(MouseEventInfo)
{
	return MouseEventReturnValue();
}

MouseEventReturnValue InputListener::MouseRelease(MouseEventInfo)
{
	return MouseEventReturnValue();
}

MouseEventReturnValue InputListener::MouseScroll(MouseEventInfo)
{
	return MouseEventReturnValue();
}

void InputListener::StartDrag(int x, int y)
{
	dragStart = Tools::Point2D<int>{ x, y };
	drag_enabled = true;
}

void InputListener::EndDrag()
{
	drag_enabled = false;
}

Tools::Direction2D<int> InputListener::GetDragIncr(int x, int y)
{
	Tools::Direction2D<int> prog = GetDragTotal(x, y);
	StartDrag(x, y);
	return prog;
}

Tools::Direction2D<int> InputListener::GetDragTotal(int x, int y)
{
	return { x - dragStart.x, y - dragStart.y };
}

Tools::Point2D<int> InputListener::GetDragStart()
{
	return dragStart;
}

bool InputListener::HasDrag()
{
	return drag_enabled;
}

InputManager* InputListener::GetAnyManager()
{
	return AnyLinked();
}

