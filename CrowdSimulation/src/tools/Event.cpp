#include "Event.h"

namespace Tools {

Event::Event(int n)
{
	this->n = n;
	gFlags = new bool[n];
	Reset_All();
}

void Event::Signal_One(int id)
{
	std::lock_guard<std::mutex> lk(gMutex);
	gFlags[id] = true;
	gCondition.notify_one();
}

void Event::Signal_All()
{
	std::unique_lock<std::mutex> lk(gMutex);
	for(int i = 0; i < n; i++) gFlags[i] = true;
	gCondition.notify_all();
}

void Event::Wakeup_All()
{
	gCondition.notify_all();
}

void Event::Wait(bool reset, int id)
{
	std::unique_lock<std::mutex> lk(gMutex);
	if (gFlags[id] == false) {	
		gCondition.wait(lk, [this, id] { return gFlags[id]; });
	}
	if (reset) gFlags[id] = false;
}

void Event::Wait_All(bool reset)
{
	if (!all_flagged()) {
		std::unique_lock<std::mutex> lk(gMutex);
		gCondition.wait(lk, [this] { 
			return all_flagged();
		});
	}
	if (reset) for (int i = 0; i < n; i++) gFlags[i] = false;
}

bool Event::isFlagged(int id)
{
	return gFlags[id];
}

void Event::Reset(int id)
{
	std::unique_lock<std::mutex> lk(gMutex);
	gFlags[id] = false;
}

void Event::Reset_All()
{
	std::unique_lock<std::mutex> lk(gMutex);
	for (int i = 0; i < n; i++) gFlags[i] = false;
}

}