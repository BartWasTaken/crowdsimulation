#ifndef CHANGEDLISTENER_H
#define CHANGEDLISTENER_H

#include <vector>

template<typename T>
class ChangedListener
{
public:
	// return 'false' to remove registration
	virtual bool object_changed(T* pObj) = 0;
};

template<typename T>
class ChangedShouter
{
public:
	void add_listener(ChangedListener<T>* ptr) {
		for (auto* p : m_listeners) {
			if (p == ptr) return;
		}
		m_listeners.push_back(ptr);
	}
	void remove_listener(ChangedListener<T>* ptr) {
		// reverse loop as the back can be removed each iteration
		for (int i = m_listeners.size() - 1; i >= 0; i--) {
			if (m_listeners[i] == ptr) {
				m_listeners[i] = m_listeners.back();
				m_listeners.resize(m_listeners.size() - 1);
				return;
			}
		}
	}
protected:
	void shout_change(T* ptr) {
		for (int i = 0, n = (int) m_listeners.size(); i < n; i++) {
			auto res = m_listeners[i]->object_changed(ptr);
			if (res == false) { // swap remove element
				m_listeners[i] = m_listeners.back();
				m_listeners.resize(m_listeners.size() - 1);
			}
		}
	}
	void copy_clients(const ChangedShouter& other) {
		m_listeners = other.m_listeners;
	}
private:
	std::vector<ChangedListener<T>*> m_listeners;
};

#endif // CHANGEDLISTENER_H