#include "InputManager.h"
#include "InputListener.h"
#include <QtDebug>

void InputManager::SetFocusMsg(bool b)
{
	gHasFocus = b;
	if (!gHasFocus) FocusLost();
	else FocusGained();
}

bool InputManager::IsKeyDown(Qt::Key key, AState aState /*= AState()*/)
{
	return IsKeyDownI(key) && HasState(aState);
}

bool InputManager::IsKeyPressed(Qt::Key key, AState aState /*= AState()*/)
{
	return IsKeyPressedI(key) && HasState(aState);
}

bool InputManager::IsKeyReleased(Qt::Key key, AState aState/*= AState()*/)
{
	return IsKeyReleasedI(key) && HasState(aState);
}

void InputManager::MousePressMsg(MouseEventInfo info)
{
	qInfo() << "MousePressMsg" << "LB: " << info.is_lb() << "RB: " << info.is_rb();


	InputControl::ControlType control_type;
	if (info.is_rb()) control_type = InputControl::ControlType::RB;
	else if (info.is_lb()) control_type = InputControl::ControlType::LB;
	else {
		return;
	}

	shout(control_type, &InputListener::MousePress, info);

	/*
	InputListener* original_owner = m_control_owners[control_type];

	for (auto it = ListenerList().begin(); it != ListenerList().end(); ++it)
	{
		InputListener* obj = (*it);
		
		if (should_do_call(obj, original_owner))
		{
			info.control_owner = m_control_owners[control_type];
			auto result = obj->MousePress(info);
			handle_control(obj, control_type, result.status_request);
		}
	}
	*/
}

void InputManager::MouseMoveMsg(MouseEventInfo info)
{
	InputControl::ControlType control_type = InputControl::ControlType::MOVE;

	shout(control_type, &InputListener::MouseMove, info);

	/*
	InputListener* original_owner = m_control_owners[control_type];

	//std::cout << "Move" << " x:" << x << " y:" << y << "\n";
	for (auto it = ListenerList().begin(); it != ListenerList().end(); ++it)
	{
		InputListener* obj = (*it);
		
		if (should_do_call(obj, original_owner))
		{
			info.control_owner = m_control_owners[control_type];
			auto result = obj->MouseMove(info);
			handle_control(obj, control_type, result.status_request);
		}
	}
	*/
}

void InputManager::MouseReleaseMsg(MouseEventInfo info)
{
	qInfo() << "MouseReleaseMsg" << "LB: " << info.is_lb() << "RB: " << info.is_rb();
	//std::cout << "Release" << " x:" << x << " y:" << y << "\n";

	if (info.is_rb()) {
		shout(InputControl::ControlType::RB, &InputListener::MouseRelease, info);
	}
	if (info.is_lb()) {
		shout(InputControl::ControlType::LB, &InputListener::MouseRelease, info);
	}


	

	/*

	InputListener* original_owner = m_control_owners[control_type];

	for (auto it = ListenerList().begin(); it != ListenerList().end(); ++it)
	{
		InputListener* obj = (*it);
		
		if (should_do_call(obj, original_owner))
		{
			info.control_owner = m_control_owners[control_type];
			auto result = obj->MouseRelease(info);
			handle_control(obj, control_type, result.status_request);
		}
	}
	*/
}

void InputManager::MouseScrollMsg(MouseEventInfo info)
{
	InputControl::ControlType control_type = InputControl::ControlType::SCROLL;
	InputListener*& original_owner = m_control_owners[control_type];

	//std::cout << "Scroll" << " x:" << x << " y:" << y << " d:" << d << "\n";
	for (auto& obj : GetLinkIterator())
	{
		if (should_do_call(obj, original_owner))
		{
			info.control_owner = m_control_owners[control_type];
			auto result = obj->MouseScroll(info);
			handle_control(obj, result.status_request);
		}
	}
}

bool InputManager::try_take_control(InputListener* ptr, InputControl::ControlType ct)
{
	if (m_control_owners[ct] == nullptr) {
		m_control_owners[ct] = ptr;
		return true;
	}
	return false;
}

const std::vector<InputListener*>& InputManager::ListenerList()
{
	return ManyToManyLink::GetLinked();
}

bool InputManager::HasState(const AState& aState)
{
	if (HasState(Qt::Key::Key_Control, /*KeyCodes::ControlRight,*/ aState.Control) == false) {
		return false;
	}
	if (HasState(Qt::Key::Key_Shift, /*KeyCodes::ShiftRight,*/ aState.Shift) == false) {
		return false;
	}
	if (HasState(Qt::Key::Key_Alt, aState.Alt) == false) {
		return false;
	}

	return true;
}

bool InputManager::HasState(Qt::Key key_code, AStateType state)
{
	if (state == AStateType::DONT_CARE)
	{
		return true;
	}
	else if (state == AStateType::UP) {
		if (IsKeyDownI(key_code)) return false;
	}
	else if (state == AStateType::DOWN) {
		if (!IsKeyDownI(key_code)) return false;
	}
	return true;
}

bool InputManager::HasState(Qt::Key key_code_l, Qt::Key key_code_r, AStateTypeLR state)
{
	if (state == AStateTypeLR::DONT_CARE_LR)
	{
		return true;
	}
	else if (state == AStateTypeLR::EITHER_DOWN) {
		if (!(IsKeyDownI(key_code_l) || IsKeyDownI(key_code_r))) return false;// IsKeyDownI(key_code_l) == false && IsKeyDownI(key_code_r) == false) return false;
	}
	else if (state == AStateTypeLR::BOTH_DOWN) {
		if (!(IsKeyDownI(key_code_l) && IsKeyDownI(key_code_r))) return false;
	}
	else if (state == AStateTypeLR::BOTH_UP) {
		if (IsKeyDownI(key_code_l) || IsKeyDownI(key_code_r)) return false;
	}
	else if (state == AStateTypeLR::LEFT_DOWN_ONLY) {
		if (!IsKeyDownI(key_code_l) || IsKeyDownI(key_code_r)) return false;
	}
	else if (state == AStateTypeLR::RIGHT_DOWN_ONLY) {
		if (IsKeyDownI(key_code_l) || !IsKeyDownI(key_code_r)) return false;
	}
	else if (state == AStateTypeLR::RIGHT_DOWN) {
		if (!IsKeyDownI(key_code_r)) return false;
	}
	else if (state == AStateTypeLR::LEFT_DOWN) {
		if (!IsKeyDownI(key_code_l)) return false;
	}

	return true;
}

void InputManager::clear_control()
{
	/*MouseEventInfo info;
	info.button_flags = MouseEventInfo::LB | MouseEventInfo::RB;
	info.x = 0;
	info.y = 0;
	MouseReleaseMsg(info);

	ReleaseAllKeys();*/

	//for (int i = 0; i < InputControl::CONTROL_TYPE_COUNT; i++) {
	//	m_control_owners[i] = nullptr;
	//}
}

void InputManager::handle_control(InputListener* ptr, InputControl::Request& request)
{
	using namespace InputControl;
	for (int i = 0; i < CONTROL_TYPE_COUNT; i++) {
		switch (request.status[i]) {
			case Request::TRY_TAKE:
			case Request::TRY_TAKE_KEEP:
				if (m_control_owners[i] == nullptr) {
					m_control_owners[i] = ptr;
					qDebug() << "Take control, type:" << i;
				}
				break;
			case Request::TRY_RELEASE:
				if (m_control_owners[i] == ptr) {
					m_control_owners[i] = nullptr;
					qDebug() << "Release control, type:" << i;
				}
				break;
		}
	}
}

bool InputManager::should_do_call(InputListener* ptr, InputListener*& original_owner)
{
	if (original_owner == nullptr) {
		return ptr->is_input_enabled();
	}
	else if (original_owner == ptr)
	{
		bool enabled = ptr->is_input_enabled();
		if (enabled) return true;
		else original_owner = nullptr;
	}
	else return false;
	// TODO: allow objects to request a call even if they have no control, by using flags per registered object
}

InputManager::InputManager()
{
}

InputManager::~InputManager()
{

}

bool InputManager::HasDefaultState()
{
	return HasState(AState::Default);
}

void InputManager::AddInputListener(InputListener* ptr)
{
	Link(this, ptr);
}

void InputManager::RemoveInputListener(InputListener* ptr)
{
	UnLink(this, ptr);
	for (int i = 0; i < InputControl::CONTROL_TYPE_COUNT; i++) {
		if (this->m_control_owners[i] == ptr) m_control_owners[i] = nullptr;
	}
}

bool InputManager::HasFocus()
{
	return gHasFocus;
}

const InputManager::AState InputManager::AState::Default;

EventBasedInputManager::EventBasedInputManager()
{

}

bool EventBasedInputManager::IsKeyDownI(Qt::Key key)
{
	return m_keyboard_state[key] & KFlag::DOWN;
}

bool EventBasedInputManager::IsKeyPressedI(Qt::Key key)
{
	return m_keyboard_state[key] & KFlag::PRESSED;
}

bool EventBasedInputManager::IsKeyReleasedI(Qt::Key key)
{
	return m_keyboard_state[key] & KFlag::RELEASED;
}

void EventBasedInputManager::PrepForNextFrame()
{
	for (auto& pair : m_keyboard_state) {
		auto& kflag = pair.second;
		kflag = (KFlag)(kflag & ~KFlag::PRESSED);
		kflag = (KFlag)(kflag & ~KFlag::RELEASED);
	}
}

void EventBasedInputManager::KeyDownMsg(Qt::Key key)
{
	if ((m_keyboard_state[key] & KFlag::DOWN) == false) { // if it was not down
		m_keyboard_state[key] |= KFlag::PRESSED; // it is now pressed
		m_keyboard_state[key] |= KFlag::DOWN; // it is now down


		InputControl::ControlType control_type = InputControl::ControlType::KEYS;


		shout(control_type, &InputListener::KeyClick, key);

		/*
		InputListener* original_owner = m_control_owners[control_type];
		for (auto it = ListenerList().begin(); it != ListenerList().end(); ++it)
		{
			InputListener* obj = (*it);

			if (should_do_call(obj, original_owner))
			{
				//info.control_owner = m_control_owner;
				auto result = obj->KeyClick(key);
				handle_control(obj, control_type, result.status_request);
			}
		}
		*/
	}


}

void EventBasedInputManager::KeyUpMsg(Qt::Key key)
{
	m_keyboard_state[key] |= KFlag::RELEASED;
	m_keyboard_state[key] &= ~KFlag::DOWN;
}

void EventBasedInputManager::FocusGained()
{
	SetFocusMsg(true);
}

void EventBasedInputManager::FocusLost()
{
	SetFocusMsg(false);
}

void EventBasedInputManager::ReleaseAllKeys()
{
	for (auto& pair : m_keyboard_state) {
		if (pair.second != 0) KeyUpMsg(pair.first);
	}
}

void EventBasedInputManager::RenewKeyBoardState()
{
	//for (int i = 0; i < 256; i++) {
		// qt doesnt allow this...
		//if (GetKeyState(i) & 0x8000) keyFlags[i] = KFlag::DOWN;
		//else 
	//	m_keyboard_state[i] = KFlag::NONE;
	//}
	m_keyboard_state.clear();
}
