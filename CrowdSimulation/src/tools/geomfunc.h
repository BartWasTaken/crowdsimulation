#pragma once

#include "raster.h"
#include "shared_eigen_incl.h"
#include "qpolygon.h"
#include <qmath.h>

namespace Geometry {
constexpr const float Epsilon = 0.001f;
}

template<typename T> 
T clamp(T a, T low, T high) {
	return a < low ? low : (a > high ? high : a);
}

inline bool is_valid_float(float value) {
	if (value != value) {
		return false; // nan
	}
	else if (value > std::numeric_limits<qreal>::max()) {
		return false; // +inf;
	}
	else if (value < -std::numeric_limits<qreal>::max()) {
		return false; // -inf;
	}
	else return true;
}

inline bool is_valid_vec2(Vector2f v) {
	return is_valid_float(v(0)) && is_valid_float(v(1));
}

inline float slope(Vector2f v) {
	return v(1) / v(0);
}

inline bool are_colinear(Vector2f a, Vector2f b) {

	float sa = slope(a);
	float sb = slope(b);

	float sa_inf = !is_valid_float(sa);
	float sb_inf = !is_valid_float(sb);
	if (sa_inf || sb_inf) {
		return sa_inf && sb_inf;
	}
	else return qAbs(slope(a) - slope(b)) < Geometry::Epsilon;
}

inline bool are_equal_iv(const Vector2i& a, const Vector2i& b) {
	return a(0) == b(0) && a(1) == b(1);
}

inline bool are_equal(float a, float b) {
	return qAbs(b - a) < Geometry::Epsilon;
}

inline bool are_equal_fv(const Vector2f& a, const Vector2f& b) {
	return are_equal(a(0), b(0)) && are_equal(a(1), b(1));
}

inline bool difference_less_than(const float& a, const float& b, const float max_rel_diff) {
	if (qAbs(a) < Geometry::Epsilon || qAbs(b) < Geometry::Epsilon) {
		return true;
	}
	float rel = a / b;
	if (rel < 1) rel = 1 / rel;
	return (rel - 1) < max_rel_diff;
}

inline bool difference_less_than(const Vector2f& a, const Vector2f& b, const float max_rel_diff) {
	return difference_less_than(a(0), b(0), max_rel_diff) && difference_less_than(a(1), b(1), max_rel_diff);
}

// rotate v such that it becomes relative to b what is was relative to a
inline Vector2f rotated(Vector2f v, Vector2f a, Vector2f b) {
	float angle = atan2(b(1), b(0)) - atan2(a(1), a(0));
	//if (angle < 0) angle += 2 * M_PI;
	
	float cos = std::cos(angle);
	float sin = std::sin(angle);

	float px = v(0) * cos - v(1) * sin;
	float py = v(0) * sin + v(1) * cos;

	return { px, py };
}

inline Vector4f rotation_matrix_align(Vector2f from, Vector2f to) {
	float angle = atan2(to(1), to(0)) - atan2(from(1), from(0));
	float cos = std::cos(angle);
	float sin = std::sin(angle);
	return { cos, -sin, sin, cos };
}

inline float Angle2D(Vector2f from, Vector2f to) {
	return atan2(to(1), to(0)) - atan2(from(1), from(0));;
}

// To find orientation of ordered triplet (p1, p2, p3).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
template<typename P>
inline int orientation(P p1, P p2, P p3)
{
	float val = (p2[1] - p1[1]) * (p3[0] - p2[0]) -
		(p2[0] - p1[0]) * (p3[1] - p2[1]);

	if (qAbs(val) < Geometry::Epsilon) return 0;  // colinear

	return (val > 0) ? 1 : 2; // clock or counterclock wise
}

template<typename P>
inline int orientation_safe(P p1, P p2, P p3)
{
	if (are_colinear(p2 - p1, p3 - p1)) return 0;

	float val = (p2[1] - p1[1]) * (p3[0] - p2[0]) - (p2[0] - p1[0]) * (p3[1] - p2[1]);

	if (val == 0) return 0;  // colinear

	return (val > 0) ? 1 : 2; // clock or counterclock wise
}

template<typename P>
inline float squared_dist(P a, P b) {
	float x = a[0] - b[0];
	float y = a[1] - b[1];
	return x * x + y * y;
}

inline std::vector<Vector2f> bbox_to_polygon(AABB2 bbox) 
{
	std::vector<Vector2f> result = {
		bbox.corner(AABB2::BottomLeft),
		bbox.corner(AABB2::TopLeft),
		bbox.corner(AABB2::TopRight),
		bbox.corner(AABB2::BottomRight)
	};
	return result;
}

inline Vector3f to_3d(Vector2f v, float height) {
	return {v(0), height, -v(1)};
}


// input normals in clockwise order of the polygon
inline Vector2f corner_offset(Vector2f n1, Vector2f n2) 
{
	float dot = n1.dot(n2);
	float corner_mp;

	if (dot >= 0) corner_mp = 1 + (M_SQRT2 - 1) * (1 - dot);
	else if (dot < 0) corner_mp = 1 + (M_SQRT2 - 1) / (1 + dot);

	return corner_mp * (n1 + n2).normalized();
}

inline AABB2 make_bbox(Vector2f a, Vector2f b)
{
	Vector2f minmax[2];

	for (int i = 0; i < 2; i++) {
		if (a(i) < b(i)) minmax[i] = { a(i), b(i) };
		else minmax[i] = { b(i), a(i) };
	}

	return AABB2{ Vector2f{ minmax[0](0), minmax[1](0) }, Vector2f{ minmax[0](1), minmax[1](1) } };
}

inline bool is_valid_vec(Vector2f v) {
	for (int i = 0; i < 2; i++) {
		if (!is_valid_float(v(i))) return false;
	}
	return true;
}


inline Vector2f rotate_90_deg_ccw(const Vector2f& v) {
	return { -v(1), v(0) };
}

inline Vector2f rotate_90_deg_cw(const Vector2f& v) {
	return { v(1), -v(0) };
}

inline float closest_point_on_line_prog(Vector2f la, Vector2f lb, Vector2f p) {
	Vector2f AB = lb - la;
	Vector2f AP = p - la;
	float lengthSqrAB = AB.dot(AB);
	float t = AP.dot(AB) / lengthSqrAB;

	if (t > 1) t = 1;
	else if (t < 0) t = 0;

	return t;
}

// la : line start
// lb : line end
// p : point
inline Vector2f closest_point_on_line(Vector2f la, Vector2f lb, Vector2f p)
{
	Vector2f AB = lb - la;
	Vector2f AP = p - la;
	float lengthSqrAB = AB.dot(AB);
	float t = AP.dot(AB) / lengthSqrAB;

	if (t > 1) t = 1;
	else if (t < 0) t = 0;

	return la + t * AB;
}



// la : line start
// AB : end - start
// AP : point - start
inline Vector2f closest_point_on_line_2(const Vector2f& la, const Vector2f& AB, const Vector2f& AP)
{
	float lengthSqrAB = AB.dot(AB);
	float t = AP.dot(AB) / lengthSqrAB;
	if (t > 1) t = 1;
	else if (t < 0) t = 0;
	return la + t * AB;
}

inline Vector2f line_to_point(const Vector2f& AB, const Vector2f& AP)
{
	//auto AB = B - A;
	//auto AP = P - A;
	float lengthSqrAB = AB.dot(AB);
	float t = AB.dot(AP) / lengthSqrAB;

	if (t < 0) t = 0;
	else if (t > 1) t = 1;

	// return A + t * AB; <- this would be the point on the line
	return AP - t * AB;
}

inline float Det(float a, float b, float c, float d)
{
	return a * d - b * c;
}

inline Vector2f closestpointonline(float lx1, float ly1,
	float lx2, float ly2, float x0, float y0)
{
	float A1 = ly2 - ly1;
	float B1 = lx1 - lx2;
	float C1 = (ly2 - ly1)*lx1 + (lx1 - lx2)*ly1;
	float C2 = -B1 * x0 + A1 * y0;
	float det = A1 * A1 - -B1 * B1;
	float cx = 0;
	float cy = 0;

	if (qAbs(det) > Geometry::Epsilon) {
		cx = (float)((A1*C1 - B1 * C2) / det);
		cy = (float)((A1*C2 - -B1 * C1) / det);
	}
	else {
		cx = x0;
		cy = y0;
	}
	return Vector2f{ cx, cy };
}

/*
inline float collision(Vector2f c1p, Vector2f c2p, Vector2f c1v, Vector2f c2v, float rad, Vector2f& coll_point_1)
{
	c1v -= c2v;

	Vector2f closepoint = closest_point_on_line(c1p, c1p + c1v, c2p);
	float dist_sqr = (closepoint - c2p).squaredNorm();
	float min_dist = 2 * rad;
	float mind_dist_sqr = min_dist * min_dist;
	if (dist_sqr < mind_dist_sqr) {
		float speed = c1v.norm();
		float dist_coll_cp1 = std::sqrt(mind_dist_sqr - dist_sqr);
		coll_point_1 = c1p - dist_coll_cp1 * c1v / speed;
		float t = (coll_point_1 - c1p).norm() / speed;
		return t;
	}
	return -1;


// 	float closestdistsq = d.squaredNorm();
// 	float combo_rad = rad * 2.0f;
// 	float mindistsq = combo_rad * combo_rad;
// 	if (closestdistsq <= mindistsq) {
// 		float backdist = std::sqrt(mindistsq - closestdistsq);
// 		float movementvectorlength = c1v.norm();// std::sqrt(std::pow(circle1.vx, 2) + Math.pow(circle1.vy, 2));
// 		float c_x = d(0) - backdist * (c1v(0) / movementvectorlength);
// 		float c_y = d(1) - backdist * (c1v(1) / movementvectorlength);
// 		float t = (c_x - c1p(0)) / c1v(0);
// 		return t;
// 	}
// 	else {
// 		// no collision has occurred
// 		return -1;
// 	}
}*/


inline bool intersection(
	Vector2f start1, Vector2f end1,
	Vector2f start2, Vector2f end2,
	Vector2f* pOut_point = nullptr)
{
	float ax = end1[0] - start1[0];     // direction of line a
	float ay = end1[1] - start1[1];     // ax and ay as above

	float bx = start2[0] - end2[0];     // direction of line b, reversed
	float by = start2[1] - end2[1];     // really -by and -by as above

	float dx = start2[0] - start1[0];   // right-hand side
	float dy = start2[1] - start1[1];

	float det = ax * by - ay * bx;

	if (det == 0) return false;

	float r = (dx * by - dy * bx) / det;
	float s = (ax * dy - ay * dx) / det;

	if (pOut_point) {
		*pOut_point = start1 + r * Vector2f{ ax, ay };
	}
	
	return !(r <= 0 || r >= 1 || s <= 0 || s >= 1);
}

inline bool line_segment_intersection(Vector2f p0, Vector2f p1, Vector2f p2, Vector2f p3, Vector2f* pIntersection = nullptr)
{
	float s1_x, s1_y, s2_x, s2_y;

	s1_x = p1(0) - p0(0);     
	s1_y = p1(1) - p0(1);

	s2_x = p3(0) - p2(0);     
	s2_y = p3(1) - p2(1);

	float s, t;
	float det = (-s2_x * s1_y + s1_x * s2_y);
	s = (-s1_y * (p0(0) - p2(0)) + s1_x * (p0(1) - p2(1))) / det;
	t = (s2_x * (p0(1) - p2(1)) - s2_y * (p0(0) - p2(0))) / det;

	if (pIntersection) {
		*pIntersection = p0 + t * Vector2f{ s1_x, s1_y };
	}

	return (s >= 0 && s <= 1 && t >= 0 && t <= 1);
}

inline bool line_segment_intersection2(Vector2f p0, Vector2f p1, Vector2f p2, Vector2f p3, float* pIntersection_progress)
{
	float s1_x, s1_y, s2_x, s2_y;
	
	s1_x = p1(0) - p0(0);     
	s1_y = p1(1) - p0(1);
	
	s2_x = p3(0) - p2(0);     
	s2_y = p3(1) - p2(1);

	float s, t;
	s = (-s1_y * (p0(0) - p2(0)) + s1_x * (p0(1) - p2(1))) / (-s2_x * s1_y + s1_x * s2_y);
	t = (s2_x * (p0(1) - p2(1)) - s2_y * (p0(0) - p2(0))) / (-s2_x * s1_y + s1_x * s2_y);

	if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
	{
		// Collision detected
		if (pIntersection_progress != nullptr) {
			*pIntersection_progress = t;
		}
		return true;
	}

	return false; // No collision
}

inline AABB2 compute_aabb(const QPolygonF& vertices) {
	if (vertices.size() == 0) {
		return { Vector2f{ 0,0 }, Vector2f{ 0,0 } };
	}

	QPointF min = vertices[0];
	QPointF max = vertices[0];

	for (int i = 1, n = vertices.size(); i < n; i++) {
		if (vertices[i].x() < min.x()) min = { vertices[i].x(), min.y() };
		if (vertices[i].x() > max.x()) min = { vertices[i].x(), max.y() };
		if (vertices[i].y() < min.y()) max = { min.x(), vertices[i].y() };
		if (vertices[i].y() > max.y()) max = { max.x(), vertices[i].y() };
	}

	return { Vector2f{min.x(), min.y() }, Vector2f{ max.x(), max.y() } };
}

inline AABB2 compute_aabb(const std::vector<Vector2f>& vertices)
{
	if (vertices.size() == 0) {
		return { Vector2f{ 0,0 }, Vector2f{ 0,0 } };
	}

	Vector2f min = vertices[0];
	Vector2f max = vertices[0];

	for (int i = 1, n = (int) vertices.size(); i < n; i++) {
		for (int j = 0; j < 2; j++) {
			if (vertices[i](j) < min(j)) min(j) = vertices[i](j);
			if (vertices[i](j) > max(j)) max(j) = vertices[i](j);
		}
	}

	return { min, max };
}

inline float triangle_area(Vector2f p1, Vector2f p2, Vector2f p3) {
	float a = (p1-p2).norm(), b = (p2-p3).norm(), c = (p3-p1).norm();
	auto s = (a + b + c) / 2;
	return std::sqrt(s*(s - a)*(s - b)*(s - c));
}