#include "griddecorators.h"

RegisterGrid_list::RegisterGrid_list(Vector2i dim) : m_dim(dim)
{

}

RegisterGrid_list::RegisterGrid_list(std::vector<std::vector<int>>* grid, Vector2i dim)
	: m_dim(dim), m_grid(grid)
{

}

void RegisterGrid_list::write(int x, int y, int id)
{
	(*m_grid)[y * m_dim(0) + x].push_back(id);
}

void RegisterGrid_list::reset(int required_size)
{
	int old_size = m_grid->size();

	if (m_grid->size() < required_size) {
		m_grid->resize(required_size);
	}

	int clear_range = (required_size < old_size) ? required_size : old_size;
	for (int i = 0; i < clear_range; i++) {
		(*m_grid)[i].clear();
	}
}

void RegisterGrid_list::set_grid(std::vector<std::vector<int>>* g)
{
	m_grid = g;
}

RegisterGrid_count::RegisterGrid_count(std::vector<int>* grid, Vector2i dim)
	: m_grid(grid), m_dim(dim)
{

}

RegisterGrid_count::RegisterGrid_count(Vector2i dim)
	: m_dim(dim)
{

}

void RegisterGrid_count::set_grid(std::vector<int>* ptr)
{
	m_grid = ptr;
}

void RegisterGrid_count::reset(int required_size)
{
	m_grid->clear();
	m_grid->resize(required_size, 0);
}

void RegisterGrid_count::write(int x, int y, int id)
{
	(*m_grid)[y * m_dim(0) + x]++;
}
