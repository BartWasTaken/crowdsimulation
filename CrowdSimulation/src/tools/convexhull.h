#ifndef CONVEXHULL_H
#define CONVEXHULL_H
#include "shared_eigen_incl.h"
#include "polygon2D.h"
#include "qpolygon.h"
#include <stack>
#include "geomfunc.h"

class ConvexHull
{
public:
	ConvexHull();

	template<typename P>
	static std::vector<P> compute(std::vector<P>& vertices);

private:
	template<typename P>
	static inline P nextToTop(std::stack<P>& S)
	{
		auto temp = S.top();
		S.pop();
		auto res = S.top();
		S.push(temp);
		return res;
	}
};

template<typename P>
std::vector<P> ConvexHull::compute(std::vector<P>& vertices)
{
	// Graham scan

	// STEP 1: remove duplicates from input
	// put lowest point at the first spot, with left priority on equal height
	struct SortVertices {
		bool operator()(const P& a, const P& b) const {
			if (a[1] < b[1]) return true;
			else if (a[1] == b[1] && a[0] < b[0]) return true;
			else return false;
		}
	} v_sorter;
	std::sort(vertices.begin(), vertices.end(), v_sorter);
	vertices.erase(std::unique(vertices.begin(), vertices.end()), vertices.end());

	if (vertices.size() < 3) return {};


	// STEP 2:
	// sort by angle
	struct Compare_angle {
		Compare_angle(P base_point) : base(base_point) {}

		bool operator()(const P& a, const P& b) const
		{
			if (a == b) return false;

			// Find orientation
			int o = orientation_safe(base, a, b);
			if (o == 0) return (squared_dist(base, a) < squared_dist(base, b)) ? true : false;

			if (o == 1) Q_ASSERT(orientation_safe(base, b, a) == 2);
			if (o == 2) Q_ASSERT(orientation_safe(base, b, a) == 1);

			return (o == 2) ? true : false;
		}
		P base;
	} comparer(vertices[0]);

	std::sort(vertices.begin() + 1, vertices.end(), comparer);

	// STEP 3:
	int m = 1; // Initialize size of modified array
	int n = (int) vertices.size();
	for (int i = 1; i < n; i++)
	{
		// Keep removing i while angle of i and i+1 is same
		// with respect to p0
		while (i < n - 1 && orientation(vertices[0], vertices[i], vertices[i + 1]) == 0)
			i++;

		vertices[m] = vertices[i];
		m++;  // Update size of modified array
	}

	if (m < 3) return {};

	// STEP 4:
	std::stack<P> stack;
	stack.push(vertices[0]);
	stack.push(vertices[1]);
	stack.push(vertices[2]);


	// Process remaining n-3 points
	for (int i = 3; i < m; i++)
	{
		// Keep removing top while the angle formed by
		// points next-to-top, top, and points[i] makes
		// a non-left turn
		while (orientation(nextToTop(stack), stack.top(), vertices[i]) != 2)
			stack.pop();
		stack.push(vertices[i]);
	}

	// STEP 5:
	// produce output

	std::vector<P> out;

	while (!stack.empty())
	{
		out.push_back(stack.top());
		stack.pop();
	}

	return out;
}

#endif // CONVEXHULL_H