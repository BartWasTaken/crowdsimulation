#include "timer.h"
#include <QtGlobal>

Timer::Timer(bool auto_start)
{
	m_tstart = m_tstop = my_clock::now();
	if (auto_start) m_active = true;
}

Timer::Timer(std::chrono::nanoseconds initial_elapsed)
{
	m_cumulative_previous = initial_elapsed;
	m_tstart = m_tstop = my_clock::now();
}

void Timer::start(std::chrono::time_point<my_clock> start_time /*= my_clock::now()*/)
{
	m_tstart = start_time;
	m_active = true;
}

void Timer::stop(bool clear_progress)
{
	if (clear_progress) {
		m_cumulative_previous = { 0 };
		m_active = false;
	}
	else {
		if (m_active) {
			m_tstop = my_clock::now();
			m_cumulative_previous += m_tstop - m_tstart;
			m_active = false;
		}
	}
}

void Timer::restart(StartInfo* info /*= nullptr*/)
{
	auto now = my_clock::now();

	if (m_active) m_cumulative_previous += now - m_tstart;

	if (info) *info = { now, m_cumulative_previous };

	m_cumulative_previous = { 0 };
	m_tstart = now;
	m_active = true;
}

bool Timer::is_running()
{
	return m_active;
}

float Timer::elapsed_sec()
{
	return elapsed().count() / 1e9f;
}

float Timer::elapsed_ms()
{
	return elapsed().count() / 1e6f;
}

void Timer::start_together(std::vector<Timer*>& timers)
{
	auto time = my_clock::now();
	for (auto* pTime : timers) {
		pTime->start(time);
	}
}

std::chrono::time_point<Timer::my_clock> Timer::get_end_time()
{
	Q_ASSERT(m_active == false);
	return m_tstop;
}

std::chrono::time_point<Timer::my_clock> Timer::get_start_time()
{
	return m_tstart;
}

std::chrono::nanoseconds Timer::elapsed(std::chrono::time_point<my_clock> now)
{
	return m_cumulative_previous + (m_active ? (now - m_tstart) : std::chrono::nanoseconds{ 0 });
}

std::chrono::nanoseconds Timer::elapsed()
{
	return m_cumulative_previous + (m_active ? (my_clock::now() - m_tstart) : std::chrono::nanoseconds{ 0 });
}
