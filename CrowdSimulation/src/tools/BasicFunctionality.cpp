#include "BasicFunctionality.h"
namespace Tools {

Tools::FilePathDescr SplitFullPath(std::string full_path, bool extra_extentions_with_name /*= true*/)
{
	FilePathDescr result;
	std::string rest_path = full_path;

	// fist split on last slash
	std::size_t last_slash = full_path.find_last_of('/');
	if (last_slash == std::string::npos) result.path = "";
	else {
		result.path = rest_path.substr(0, last_slash + 1);
		rest_path = rest_path.substr(last_slash + 1, std::string::npos);
	}

	// get file name
	std::size_t target_point = extra_extentions_with_name ? rest_path.find_last_of(".") : rest_path.find_first_of(".");
	if (target_point == std::string::npos) result.name = "";
	else {
		result.name = rest_path.substr(0, target_point + 1);
		rest_path = rest_path.substr(target_point + 1, std::string::npos);
	}
	
	// rest is extention
	result.ext = rest_path;

	return result;
}

std::string FilePathDescr::GetFull()
{
	return path + name + ext;
}

}