#ifndef INCLUDE_H
#define INCLUDE_H

#include <QString>
#include <QtGlobal>
#include <QtMath>
#include <math.h>
#include <QMouseEvent>
#include <QWheelEvent>

#include "macros.h"
#include "DataTypes.hpp"
#include "timer.h"
#include "shared_eigen_incl.h"
#include "geomfunc.h"
#include "linetracer.h"
#include "raster.h"
#include "tools/INonCopyable.hpp"
#include "tools/InputListener.h"
#include "resizedlistener.h"
#include "polygon2d.h"
#include "SmartPtrClass.h"
#include "BasicFunctionality.h"
#include <vector>

constexpr const float c_sqrt3f = 1.73205080756f;
constexpr const float c_sqrt2f = 1.41421356237f;


inline MouseEventInfo to_release_mouse_event(QMouseEvent* e)
{
	MouseEventInfo info;
	info.x = e->x();
	info.y = e->y();

	info.button_flags = 0;
	Qt::MouseButton button = e->button();
	Qt::MouseButtons buttons = e->buttons();

	// getting "buttons" excludes the button at release, since it returns current state

	if ((button == Qt::MouseButton::LeftButton) != (buttons & Qt::MouseButton::LeftButton)) {
		info.button_flags |= MouseEventInfo::Buttons::LB;
	}
	if ((button == Qt::MouseButton::RightButton) != (buttons & Qt::MouseButton::RightButton)) {
		info.button_flags |= MouseEventInfo::Buttons::RB;
	}

	return info;
}

inline MouseEventInfo to_press_mouse_event(QMouseEvent* e)
{
	MouseEventInfo info;
	info.x = e->x();
	info.y = e->y();

	info.button_flags = 0;
	Qt::MouseButtons buttons = e->buttons();

	if (buttons & Qt::MouseButton::LeftButton) {
		info.button_flags |= MouseEventInfo::Buttons::LB;
	}
	if (buttons & Qt::MouseButton::RightButton) {
		info.button_flags |= MouseEventInfo::Buttons::RB;
	}

	return info;
}

inline MouseEventInfo to_move_mouse_event(QMouseEvent* e)
{
	MouseEventInfo info;
	info.x = e->x();
	info.y = e->y();
	return info;
}

inline MouseEventInfo to_scroll_mouse_event(QWheelEvent* e)
{
	MouseEventInfo info;
	info.x = e->x();
	info.y = e->y();
	info.delta = e->delta();
	return info;
}

namespace Dir4 {

enum DIR : int {
	NONE = -1,
	N = 0,
	E = 1,
	S = 2,
	W = 3,
	DIR_COUNT = 4
};

inline int opposed(int d) {
	return (d + 2) % DIR_COUNT;
}

constexpr const char* name[4] = {
	"North", "East", "South", "West"
};

constexpr const static int dir_x[4] = { 0,1,0,-1 };
constexpr const static int dir_y[4] = { 1,0,-1,0 };


}

inline void clamp_in_place(float min, float& value, float max) {
	if (value < min) value = min;
	else if (value > max) value = max;
} 

template<typename T>
inline const constexpr T ipow(T base, int exp)
{
	T result = 1;
	while (exp)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		base *= base;
	}

	return result;
}

inline QString as_readable(int x) {
	if (x < 1e3) return QString::number(x);
	if (x < 1e6) return QString::number(x / 1e3).append("k");
	if (x < 1e9) return QString::number(x / 1e6).append("m");

	int n = 1e3;
	while (x / n >= 100) {
		n *= 10;
	}
	return QString::number(x / n).append("e").append(QString::number(log10(n)));
}

template<typename T>
inline void SafeDeletePtr(T*& ptr) {
	if (ptr) {
		delete ptr;
		ptr = nullptr;
	}
}

template<typename T>
inline void SafeDeleteArr(T*& ptr) {
	if (ptr) {
		delete[] ptr;
		ptr = nullptr;
	}
}

/*
template<typename T>
class ReuseArray : public std::vector<T> {
public:
	inline void require_free_space(int x) {}
};
*/


template<typename T>
class ReuseArray
{
public:
	inline int size() const {
		return m_size;
	}
	inline void clear() {
		m_size = 0;
	}
	inline void require_free_space(int amount) {
		Q_ASSERT(amount >= 0 && amount < 2e16);
		if (m_reserved - m_size < amount) {
			auto* new_data = new T[m_size + amount];
			if (m_data) {
				std::copy(m_data, m_data + m_size, new_data);
				delete[] m_data;
			}
			m_data = new_data;
			m_reserved = m_size + amount;
		}
	}
	inline void push_back(T val) {
		Q_ASSERT(m_size < m_reserved);
		m_data[m_size++] = val;
	};
	inline T* last() { return m_data + (m_size - 1); }
	inline T* first() { return m_data; }
	inline T* begin() { return m_data; }
	inline T* end() { return m_data + m_size; }
private:
	int m_size = 0;
	int m_reserved = 0;
	T* m_data = nullptr;
};

struct SurroundingCells {
public:
	inline void make(Vector2f position, Raster raster) {
		make(raster.to_unit_space(position), raster.resolution());
	}
	inline void make(Vector2f posn, Vector2i dim) {
		float gx = posn(0) * dim(0);
		float gy = posn(1) * dim(1);
		int x_i = (int)(gx - 0.4999999f);
		int y_i = (int)(gy - 0.4999999f);

		bool x_high = valid_x(x_i + 1, dim);
		bool x_low = valid_x(x_i, dim);
		bool y_high = valid_y(y_i + 1, dim);
		bool y_low = valid_y(y_i, dim);

		if (x_low) x1 = x_i;
		else x1 = x_i + 1;
		if (x_high) x2 = x_i + 1;
		else x2 = x_i;

		if (y_low) y1 = y_i;
		else y1 = y_i + 1;
		if (y_high) y2 = y_i + 1;
		else y2 = y_i;
	}
	int x1, x2, y1, y2;
private:
	static inline bool valid_x(int x, const Vector2i& dim) {
		return x >= 0 && x < dim(0);
	}
	static inline bool valid_y(int y, const Vector2i& dim) {
		return y >= 0 && y < dim(1);
	}
};

struct BilinearInfo {
public:
	// make from world space position and a raster
	// positions must lie inside the raster (use raster.clamp if unsure)
	inline void make(Vector2f position, Raster raster) {
		make(raster.to_unit_space(position), raster.resolution());
	}

	// make from 0-1 range position within dimention
	inline void make(Vector2f posn, Vector2i dim) {
		float gx = posn(0) * dim(0);
		float gy = posn(1) * dim(1);

		// lower left index:
		x_i = (int)(gx - 0.4999999f);
		y_i = (int)(gy - 0.4999999f);

		x_high = valid_x(x_i + 1, dim);
		x_low = valid_x(x_i, dim);
		y_high = valid_y(y_i + 1, dim);
		y_low = valid_y(y_i, dim);

		dx = gx - (x_i + 0.5f);
		dy = gy - (y_i + 0.5f);
	}

	// x_i, y_i are the index of the lower left squre
	int x_i, y_i;
	// the low and high variables give wether that corner is valid or not
	bool x_high, x_low, y_high, y_low;

	// dx, dy are the progressions towards the right top between 0 and 1.
	float dx, dy;

private:

	static inline bool valid_x(int x, const Vector2i& dim) {
		return x >= 0 && x < dim(0);
	}
	static inline bool valid_y(int y, const Vector2i& dim) {
		return y >= 0 && y < dim(1);
	}
};


#endif // INCLUDE_H
