#pragma once
#include <memory>

namespace Tools {
template <class A>
class SmartPtrClass
{
public:
	template <typename... T>
	static ::std::shared_ptr<A> Create(T &&...args) {
		return ::std::make_shared<A>(EmptyKeyStruct { 0 },
			::std::forward<T>(args)...);
	}
};

}