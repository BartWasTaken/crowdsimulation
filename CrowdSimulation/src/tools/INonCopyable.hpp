#pragma once

namespace Tools {

class INonCopyable {
private:
	INonCopyable(const INonCopyable&) = delete;
	INonCopyable& operator=(INonCopyable const&) = delete;
public:
 	INonCopyable() {}
	// Allow moving by adding the following to your class:
	// Movement constructors:
	// T(T&& other) {};
	// T& operator=(T&&) { return *this; }
};

}