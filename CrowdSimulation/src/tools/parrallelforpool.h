#ifndef PARRALLELFORPOOL_H
#define PARRALLELFORPOOL_H
#include <functional>
#include <vector>
#include <thread>
#include "Event.h"
#include <QDebug>

template<typename TaskDescr>
class ParrallelForPool
{
public:
	~ParrallelForPool();
	using FuncType = std::function<void(int, TaskDescr)>;

	void setup(int no_threads, FuncType body);
	bool is_setup() { return m_is_setup; }

	inline void set_task(int thread_id, TaskDescr descr);
	inline void run_loop();

	void reset();

private:

	void destroy();

	class WorkerThread {
	public:
		~WorkerThread() {}

		void setup(int id, Tools::Event* start, Tools::Event* finish, FuncType f) 
		{
			m_tid = id;
			m_startevent = start;
			m_finishevent = finish;
			m_f = f;
		}

		void run_async() {
			m_alive = true;
			m_t = std::thread(&WorkerThread::run, this);
		}
		void flag_dead() {
			m_alive = false;
		}

		void run() {
			while (m_alive) {
				m_startevent->Wait(true, m_tid); // , t.id
				if (m_alive) {
					m_f(m_tid, get_descr());
					m_job_done = true;
				}
				m_finishevent->Signal_One(m_tid);
			}
		}

		void join() {
			m_t.join();
		}

		TaskDescr get_descr() {
			TaskDescr res;
			volatile char* src_data = reinterpret_cast<volatile char*>(&m_descr);
			char* snk_data = reinterpret_cast<char*>(&res);

			for (int i = 0; i < sizeof(TaskDescr); i++) {
				snk_data[i] = src_data[i];
			}

			return res;
		}

		FuncType m_f;
		int m_tid;
		bool m_alive = false;
		std::thread m_t;
		volatile TaskDescr m_descr;
		Tools::Event* m_startevent = nullptr;
		Tools::Event* m_finishevent = nullptr;
		volatile bool m_job_done = false;
	};

	const int task_bytesize = sizeof(TaskDescr);
	int m_no_threads;
	bool m_is_setup = false;

	Tools::Event* m_startevent = nullptr;
	Tools::Event* m_finishevent = nullptr;
	WorkerThread* workers = nullptr;
};

template<typename TaskDescr>
void ParrallelForPool<TaskDescr>::destroy()
{
	if (m_startevent && m_finishevent && workers)
	{
		for (int i = 0; i < m_no_threads; i++) {
			workers[i].flag_dead();
		}
		m_startevent->Signal_All();
		m_finishevent->Wait_All(false);
		for (int i = 0; i < m_no_threads; i++) {
			workers[i].join();
		}
	}

	SafeDeleteArr(workers);
	SafeDeletePtr(m_startevent);
	SafeDeletePtr(m_finishevent);
}

template<typename TaskDescr>
void ParrallelForPool<TaskDescr>::reset()
{
	destroy();
	m_is_setup = false;
}

template<typename TaskDescr>
void ParrallelForPool<TaskDescr>::run_loop()
{
	m_startevent->Signal_All();
	m_finishevent->Wait_All(true);

	for (int i = 0; i < m_no_threads; i++) {
		if (!workers[i].m_job_done) {
			qWarning() << "ParrallelForPool : thread has not done its work!";
		}
		workers[i].m_job_done = false;
	}
}

template<typename TaskDescr>
void ParrallelForPool<TaskDescr>::set_task(int thread_id, TaskDescr descr)
{
	char* src_data = reinterpret_cast<char*>(&descr);
	volatile char* snk_data = reinterpret_cast<volatile char*>(&(workers[thread_id].m_descr));

	for (int i = 0; i < task_bytesize; i++) {
		snk_data[i] = src_data[i];
	}
	//workers[thread_id].m_descr = descr;
}

template<typename TaskDescr>
void ParrallelForPool<TaskDescr>::setup(int no_threads, FuncType body)
{
	m_is_setup = true;
	m_no_threads = no_threads;

	m_startevent = new Tools::Event(m_no_threads);
	m_finishevent = new Tools::Event(m_no_threads);
	workers = new WorkerThread[m_no_threads];

	for (int i = 0; i < no_threads; i++) {
		workers[i].setup(i, m_startevent, m_finishevent, body);
		workers[i].run_async();
	}
}

template<typename TaskDescr>
ParrallelForPool<TaskDescr>::~ParrallelForPool()
{
	destroy();
}


#endif // PARRALLELFORPOOL_H