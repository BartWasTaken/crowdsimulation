#pragma once
#include "manytomanylink.h"
#include "DataTypes.hpp"
#include "InputManager.h"

class InputManager;
class InputListener;


// only has a single master!
class InputListener 
	: public ManyToManyLink<InputListener, InputManager>
{
public:
	virtual ~InputListener();
	inline void disable_input() { m_enabled = false; }
	inline void enable_input(bool s = true) { m_enabled = s; }
	inline bool is_input_enabled() { return m_enabled; }

private:
	Tools::Point2D<int> dragStart;
	bool drag_enabled = false;

protected:
	friend class InputManager;
	friend class EventBasedInputManager;

	virtual MouseEventReturnValue MousePress(MouseEventInfo);
	virtual MouseEventReturnValue MouseMove(MouseEventInfo);
	virtual MouseEventReturnValue MouseRelease(MouseEventInfo);
	virtual MouseEventReturnValue MouseScroll(MouseEventInfo);

	// key click responds to normal key presses
	// meaning a direct message when pressed, 
	// and if it holds after a short delay it will start spamming the key message
	// for direct key flow use the " iskeypressed " way
	virtual KeyEventReturnValue KeyClick(Qt::Key) { return {}; }

	void StartDrag(int x, int y);
	void EndDrag();

	// gives incremental changes
	Tools::Direction2D<int> GetDragIncr(int x, int y);
	// gives the total relative change since the last call to either StartDrag or GetDragIncr
	Tools::Direction2D<int> GetDragTotal(int x, int y);

	Tools::Point2D<int> GetDragStart();

	bool HasDrag();

	InputManager* GetAnyManager();

private:
	bool m_enabled = true;
};