#pragma once

#include <vector>
#include "shared_eigen_incl.h"

class IRegisterGrid
{
public:
	virtual void write(int x, int y, int id) = 0;
	virtual void reset(int required_size) = 0;
};

class RegisterGrid_list : public IRegisterGrid
{
public:
	RegisterGrid_list(Vector2i dim);
	RegisterGrid_list(std::vector<std::vector<int>>*, Vector2i dim);
	virtual void write(int x, int y, int id);
	virtual void reset(int required_size) override;
	void set_grid(std::vector<std::vector<int>>*);
private:
	std::vector<std::vector<int>>* m_grid;
	Vector2i m_dim;
};

template<typename T>
class RegisterGrid_value : public IRegisterGrid
{
public:
	RegisterGrid_value(std::vector<T>* grid, Vector2i dim, T write_value);
	RegisterGrid_value(Vector2i dim, T write_value);
	void set_grid(std::vector<T>*);
	virtual void reset(int required_size) override;
	virtual void write(int x, int y, int id) override;
private:
	bool				m_write_value;
	std::vector<T>*		m_grid;
	Vector2i			m_dim;
};

class RegisterGrid_count : public IRegisterGrid
{
public:
	RegisterGrid_count(std::vector<int>* grid, Vector2i dim);
	RegisterGrid_count(Vector2i dim);
	void set_grid(std::vector<int>*);
	virtual void reset(int required_size) override;
	virtual void write(int x, int y, int id) override;
private:
	std::vector<int>*	m_grid;
	Vector2i			m_dim;
};

template<typename T>
RegisterGrid_value<T>::RegisterGrid_value(std::vector<T>* grid, Vector2i dim, T write_value)
	: m_grid(grid), m_dim(dim), m_write_value(write_value)
{

}

template<typename T>
RegisterGrid_value<T>::RegisterGrid_value(Vector2i dim, T write_value)
	: m_dim(dim), m_write_value(write_value)
{

}

template<typename T>
void RegisterGrid_value<T>::reset(int required_size)
{
	m_grid->clear();
	m_grid->resize(required_size);
}

template<typename T>
void RegisterGrid_value<T>::set_grid(std::vector<T>* g)
{
	m_grid = g;
}

template<typename T>
void RegisterGrid_value<T>::write(int x, int y, int id)
{
	(*m_grid)[y * m_dim(0) + x] = m_write_value;
}


