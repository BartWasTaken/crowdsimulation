#ifndef POLYGON2D_H
#define POLYGON2D_H
#include <vector>
#include "shared_eigen_incl.h"
#include "raster.h"
#include "griddecorators.h"

class Polygon2D {
public:
	inline Polygon2D() {}
	Polygon2D(const std::vector<Vector2f>& v);
	void setup(const std::vector<Vector2f>& v);
	const std::vector<Vector2f>& vertices() const;

	bool intersects(const Vector2f& a, const Vector2f& b) const;
	bool intersects(AABB2) const;
	bool contains(const Vector2f& point) const;
	bool contains(const Polygon2D&) const;
	void move(Vector2f);

	void project(const Vector2f& point, Vector2f& surface, Vector2f& normal) const;
	float distanceTo(const Vector2f& point) const;

	void write_to_grid(IRegisterGrid& reg, Raster global_raster, bool full_cover, int id) const;

	std::vector<Vector2f>& edit_vertices();
	void finish();

	QPolygonF toQPolygon() const;
	
	template<typename P>
	std::vector<P> convertTo() const {
		std::vector<P> out;
		for (auto v : m_vertices) {
			out.push_back({v(0), v(1)});
		}
		return out;
	}

	inline AABB2 bbox() { return m_bbox; }

protected:
	friend class Obstacle;
	std::vector<Vector2f>	m_vertices;
	AABB2					m_bbox;
	int						m_vcount;
	Vector2f				m_inf;

private:
	bool contains(const Vector2f& point, const Vector2f* vert, std::size_t nvert) const;
	mutable std::vector<bool>	m_local_cache;
};


#endif // POLYGON2D_H