#pragma once

#include <cstdint>

using byte = unsigned char;
using uint = unsigned int;

namespace Tools {

	template<class T, typename X> 
	struct TwoValue {
		T x, y;
		inline TwoValue() {
			// default constructor
		}
		inline TwoValue(int x, int y) {
			this->x = x;
			this->y = y;
		}
		inline bool operator==(const TwoValue<T, X>& other) const
		{
			return x == other.x && y == other.y;
		}
		inline bool operator!=(const TwoValue<T, X>& other) const
		{
			return !(*this == other);
		}
		inline void operator*=(T s)
		{
			x *= s;
			y *= s;
		}
		inline bool IsZero() const 
		{
			return x == 0 && y == 0;
		}
	};

	template<class T>
	using Direction2D = TwoValue<T, struct UNIQUE>;

	template<class T>
	using Point2D = TwoValue <T, struct UNIQUE>;

	template<class T>
	struct Surface2D {
		T width, height;
		inline Surface2D() {
			// default constructor
		}
		inline Surface2D(T width, T height) {
			this->width = width;
			this->height = height;
		}
		inline Surface2D(const Surface2D<T>& other) {
			this->width = other.width;
			this->height = other.height;
		}
		inline bool operator==(const Surface2D& other) const
		{
			return width == other.width && height == other.height;
		}
		inline bool operator!=(Surface2D& other) const
		{
			return !(*this == other);
		}
		inline bool IsZero() const  
		{
			return width == 0 && height == 0;
		}
		inline T Area() const  
		{
			return width * height;
		}
	};

	template<typename T, typename M>
	inline Surface2D<T> operator*(const Surface2D<T>& s, M v)
	{
		return { (T)(s.width * v), (T)(s.height * v) };
	}
	template<typename T, typename M>
	inline Surface2D<T> operator*(M v, const Surface2D<T>& s)
	{
		return { (T)(s.width * v), (T)(s.height * v) };
	}

	template<class T>
	struct Region2D {
		Point2D<T> origin;
		Surface2D<T> size;

		inline bool operator==(const Region2D<T>& other) const
		{
			return origin == other.origin && size == other.size;
		}
		inline bool operator!=(const Region2D<T>& other) const
		{
			return !(*this == other);
		}
		inline void Scale(T sx, T sy)
		{
			max.x *= sx;
			max.y *= sy;
		}
	};

	template<class T>
	struct Rect2D {
		Point2D<T> min;
		Point2D<T> max;
		Rect2D(Point2D<T> min, Point2D<T> max)
		{
			this->min = min; 
			this->max = max;
		}
		Rect2D(T minx, T miny, T maxx, T maxy)
		{
			this->min.x = minx;
			this->min.y = miny;
			this->max.x = maxx;
			this->max.y = maxy;
		}
		Rect2D() // empty 
		{ }

		inline void operator*=(T s)
		{
			min *= s;
			max *= s;
		}
		const inline bool operator==(const Point2D<T>& other)
		{
			return x == other.x && y == other.y;
		}

		inline void Scale(T sx, T sy)
		{
			max.x = min.x + (max.x - min.x) * sx;
			max.y = min.y + (max.y - min.y) * sy;
		}

	};


}