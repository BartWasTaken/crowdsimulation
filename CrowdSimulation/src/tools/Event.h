#pragma once
#include <mutex>
#include <QDebug>

namespace Tools {

class Event
{
public:
	Event(int n = 1);

	void Signal_One(int from_id = 0);
	void Signal_All();
	void Wakeup_All();
	void Wait(bool reset = false, int id = 0);
	void Wait_All(bool reset);
	bool isFlagged(int id = 0);

	void Reset(int id = 0);
	void Reset_All();

	inline bool all_flagged() {
		for (int i = 0; i < n; i++) if (!gFlags[i]) return false;
		return true;
	}

private:
	std::condition_variable gCondition;
	std::mutex				gMutex;
	int						n;
	volatile bool*			gFlags = nullptr;
};


class WorkerSync
{
public:
	WorkerSync(int n)
		: e_master(1), e_many(n-1)
	{
		this->n = n;
	}

	void reset() {
		e_many.Reset_All();
		e_master.Reset_All();
	}

	void wait(int id)
	{
		/*
		e_many.Signal_One(id);
		e_many.Wakeup_All();
		e_many.Wait_All(false);*/
		
		if (id == 0) {
			e_many.Wait_All(false);
			e_master.Signal_All();
		}
		else {
			e_many.Signal_One(id - 1);
			e_master.Wait(false);
		}
	}

private:

	int n = 0;
	Event e_master;
	Event e_many;
};
}