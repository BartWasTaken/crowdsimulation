#pragma once

#define EIGEN_FAST_MATH 1

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Sparse>

//using namespace Eigen;

#define VEC3_ACCES(vec, i) \
	(vec.block<3, 1>(i*3, 0))

#define VEC2_ACCES(vec, i) \
	(vec.block<2, 1>(i*2, 0))


using AABB2 = Eigen::AlignedBox2f;
using AABB3 = Eigen::AlignedBox3f;

using Eigen::Vector2f;
using Eigen::Vector3f;
using Eigen::Vector4f;
using Eigen::VectorXf;
using Eigen::Matrix3f;
using Eigen::Matrix4f;
using Eigen::MatrixXf;
using Eigen::Affine3f;
using Eigen::Translation3f;
using Eigen::Quaternionf;

using Eigen::Vector2d;
using Eigen::Vector3d;
using Eigen::Vector4d;
using Eigen::VectorXd;
using Eigen::Matrix3d;
using Eigen::Matrix4d;
using Eigen::MatrixXd;
using Eigen::Affine3d;
using Eigen::Translation3d;
using Eigen::Quaterniond;

using Eigen::Matrix;
using Eigen::SparseMatrix;
using Eigen::DiagonalMatrix;
using Eigen::AngleAxis;

typedef Matrix<uint32_t, 2, 1> Vector2u;
typedef Matrix<uint32_t, 3, 1> Vector3u;

typedef Matrix<int32_t, 2, 1> Vector2i;
typedef Matrix<int32_t, 3, 1> Vector3i;
typedef Matrix<uint8_t, 4, 1> Vector4uchar;
typedef Matrix<uint8_t, 3, 1> Vector3uchar;

template<class T>
using VecX = Eigen::Matrix<T, Eigen::Dynamic, 1>;

template<class T>
using MatX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

namespace Eigen {

template<unsigned int N, class T> using Vector = Eigen::Matrix<T, N, 1>;

}

template<typename T>
using Vec3Acces = Eigen::Block<Matrix<T, -1, 1, 0, -1, 1>, 3, 1, false>;
template<typename T>
using Vec2Acces = Eigen::Block<Matrix<T, -1, 1, 0, -1, 1>, 2, 1, false>;

