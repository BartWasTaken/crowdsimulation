#include "scenebuilder.h"
#include "src/sim/obstacle.h"
#include "src/programmanager.h"
#include "src/files/scenefilereader.h"

SceneBuilder::SceneBuilder(ProgramManager& prgm, OpenGLMaster& opengl) :
	m_opengl(opengl),
	m_shape_drawer_cache(opengl.getFunc(), ColorF{ 1,0,1,0.7f }),
	m_shape_drawer_cache2(opengl.getFunc(), ColorF{ 1,.2f,1,0.9f }),
	m_pgrm(prgm)
{
	opengl.get_inputmanager()->AddInputListener(this);

	m_shape_drawer_cache2.render_lines();
	m_pSettings = SimulationSettings_ptr::create();
	m_pSettings->add_listener(this);

	key_binds[Qt::Key::Key_Q] = [this]() -> KeyEventReturnValue {
		if (is_building) {
			m_opengl.stop_render_obj(this);
			is_building = false;
			return { InputControl::Request::ReleaseAll() };
		}
		else {
			start_building();
			m_pEnvironment = IEnvironment_ptr{ new IEnvironment(m_opengl.getFunc(), AABB2(Vector2f{ 0,0 },Vector2f{ 0,0 }), "manual environment") };
		}
		return {};
	};
	key_binds[Qt::Key::Key_O] = [this]() -> KeyEventReturnValue {
		if (is_building) m_state = State::Build_Obstacle;
		return {};
	};
	key_binds[Qt::Key::Key_G] = [this]() -> KeyEventReturnValue {
		if (is_building) m_state = State::Build_Goal;
		return {};
	};
	key_binds[Qt::Key::Key_S] = [this]() -> KeyEventReturnValue {
		if (is_building) m_state = State::Build_Staircase;
		return {};
	};
	key_binds[Qt::Key::Key_B] = [this]() -> KeyEventReturnValue {
		if (is_building) m_state = State::Build_Bounds;
		return {};
	};
	auto accept_func = [this]() -> KeyEventReturnValue {
		if (is_building) {
			if (m_polygon_cache.size() > 2) {
				switch (m_state) {
					case State::Build_Obstacle:
					{
						Obstacle obstacle(0.1f, m_polygon_cache);
						m_pEnvironment->add_obstacle(obstacle);

						m_scene_cache.obstacles.push_back(m_polygon_cache);

					} break;
					case State::Build_Goal:
					{
						IGoal_ptr pGoal = IGoal_ptr{ new ObstacleGoal(m_polygon_cache) };
						m_pEnvironment->add_goal(pGoal);

						m_scene_cache.goals.push_back(m_polygon_cache);
					} break;
					case State::Build_Staircase:
					{
						StairCaseArea staircase(m_polygon_cache, 0.1f, -1, -1, -1, -1, -1);
						m_scene_cache.staircases.push_back(staircase);
						m_pEnvironment->add_staircase(staircase);
					} break;
					case State::Build_Bounds: {
						m_scene_cache.bounds = compute_aabb(m_polygon_cache);
						m_pEnvironment->set_bounds(m_scene_cache.bounds);
						break;
					}
				}
				clear_polygon_cache();
			}
			else if (m_polygon_cache.size() == 0) {
				auto mysettingsjson = m_pSettings->to_json();
				m_pEnvironment->set_settings(mysettingsjson);
				m_pgrm.get_sim_core()->add_environment(m_pEnvironment);
				SceneFileReader::Write(m_pEnvironment);
			}
		}
		return {};
	};
	key_binds[Qt::Key::Key_Return] = accept_func;
	key_binds[Qt::Key::Key_Enter] = accept_func;
	key_binds[Qt::Key::Key_S] = [this]() -> KeyEventReturnValue {
		if (is_building) m_pSettings->open_dialog();
		return {};
	};
}

void SceneBuilder::start_building()
{
	if (!is_building) {
		//is_building = InputListener::GetAnyManager()->try_take_control(this, );
		is_building = true;
		if (is_building) {
			m_opengl.set_render_obj(this);
		}
	}
}

void SceneBuilder::render(RenderContext rc)
{
	rc.f.glLineWidth(12);
	rc.f.glPointSize(12);
	rc.f.glEnable(GL_BLEND);
	rc.f.glxBlendFuncBasic();
	rc.f.opengl_master().enable2DCam(true);

	m_shape_drawer_cache.render(rc);
	m_shape_drawer_cache2.render(rc);

	m_pEnvironment->render_env(rc);

	for (auto& shape : m_shape_drawers_scene) {
		shape.render(rc);
	}

	rc.f.glDisable(GL_BLEND);
}

bool SceneBuilder::object_changed(SimulationSettings* pObj)
{
	if (pObj == m_pSettings.data()) {
		m_pSettings->apply_changes();
		return true;
	}
	return false;
}

KeyEventReturnValue SceneBuilder::KeyClick(Qt::Key key)
{
	auto result = key_binds.find(key);
	if (result != key_binds.end())
	{
		// call function from the map
		return result->second();
	}
	else return {};
}

MouseEventReturnValue SceneBuilder::MouseMove(MouseEventInfo info)
{
	bool dir = snap_dir();
	bool pos = snap_pos();

	if (dir || pos) {
		if (m_polygon_cache_moving == false) {
			m_polygon_cache.push_back({ 0,0 });
			m_polygon_cache_moving = true;
		}
		Vector2f v = project_next_point(m_opengl.camera2D().pixel_to_world(info.x, info.y));
		m_polygon_cache.back() = v;

		update_polycache_render();
	}

	return {};
}

MouseEventReturnValue SceneBuilder::MousePress(MouseEventInfo)
{
	return {};
}

MouseEventReturnValue SceneBuilder::MouseRelease(MouseEventInfo info)
{
	if (is_building) {
		if (info.is_rb())
		{
			clear_polygon_cache();
		}
		if (info.is_lb())
		{
			Vector2f world_space_point = project_next_point(m_opengl.camera2D().pixel_to_world(info.x, info.y));

			if (m_polygon_cache_moving) {
				m_polygon_cache.back() = world_space_point;
				m_polygon_cache_moving = false;
			}
			else m_polygon_cache.push_back(world_space_point);

			update_polycache_render();
		}
	}
	return {};
}

MouseEventReturnValue SceneBuilder::MouseScroll(MouseEventInfo)
{
	return {};
}

void SceneBuilder::update_polycache_render()
{
	m_shape_drawer_cache.make_shape(m_polygon_cache);
	m_shape_drawer_cache2.make_shape(m_polygon_cache);
}

void SceneBuilder::clear_polygon_cache()
{
	m_polygon_cache_moving = false;
	m_polygon_cache.clear();

	update_polycache_render();
}

Vector2f SceneBuilder::project_next_point(Vector2f v)
{
	// m_polygon_cache

	if (snap_dir()) {
		// if left shift is held down, we only allow rotations of 45 degrees

		int prev_idx = (int) m_polygon_cache.size() - (m_polygon_cache_moving ? 2 : 1);

		if (prev_idx >= 0) {
			Vector2f prev; prev = m_polygon_cache[prev_idx];

			Vector2f dir = v - prev;
			float len = dir.norm();
			dir /= len;

			int compass = (((int)round(atan2(dir(1), dir(0)) / (2 * M_PI / 8))) + 8) % 8;
			Vector2f adjust_dir = angle_vector[compass];
			v = prev + len * adjust_dir;
		}
	}
	if (snap_pos()) {
		int x = (v(0) >= 0) ? (int)(v(0) + 0.5f) : (int)(v(0) - 0.5f);
		int y = (v(1) >= 0) ? (int)(v(1) + 0.5f) : (int)(v(1) - 0.5f);
		v = { x, y };
	}
	qDebug() << "new vertice:" << v(0) << v(1);

	return v;
}
