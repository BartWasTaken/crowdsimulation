#ifndef SCENEBUILDER_H
#define SCENEBUILDER_H

#include "src/vis/openglmaster.h"
#include "tools/InputManager.h"
#include "tools/InputListener.h"
#include "tools/changedlistener.h"
#include "src/sim/simulationsettings.h"
#include "src/vis/irenderable.h"
#include "src/vis/shapedrawer2d.h"
#include "src/sim/IEnvironment.h"
#include <unordered_map>
#include <QJsonObject>

class ProgramManager;

class SceneBuilder 
	: public InputListener, public IRenderable, public ChangedListener<SimulationSettings>
{
public:
	SceneBuilder(ProgramManager&, OpenGLMaster&);

	void start_building();

	virtual void render(RenderContext) override;

	virtual bool object_changed(SimulationSettings* pObj) override;

protected:
	virtual KeyEventReturnValue KeyClick(Qt::Key key) override;

	virtual MouseEventReturnValue MouseMove(MouseEventInfo) override;

	virtual MouseEventReturnValue MousePress(MouseEventInfo) override;

	virtual MouseEventReturnValue MouseRelease(MouseEventInfo) override;

	virtual MouseEventReturnValue MouseScroll(MouseEventInfo) override;

private:

	typedef void (SceneBuilder::*KeyFunc)();

	void update_polycache_render();
	void clear_polygon_cache();

	Vector2f project_next_point(Vector2f v);

	enum class State {
		None,
		Build_Obstacle,
		Build_Staircase,
		Build_Bounds,
		Build_Goal
	} m_state = State::None;

	std::unordered_map<Qt::Key, std::function<KeyEventReturnValue()>> key_binds;

	struct SceneData {
		std::string name;
		bool require_outline;
		std::string pref_goalpicker;
		std::string pref_engine;
		bool force_pref;
		//
		QJsonObject settings;
		//
		AABB2 bounds;
		//
		std::vector<std::vector<Vector2f>> obstacles;
		std::vector<std::vector<Vector2f>> goals;
		std::vector<StairCaseArea> staircases;
	} m_scene_cache;

	IEnvironment_ptr		m_pEnvironment = nullptr;
	SimulationSettings_ptr	m_pSettings = nullptr;
	
	bool					is_building = false;
	OpenGLMaster&			m_opengl;
	ProgramManager&			m_pgrm;

	std::vector<Vector2f>	m_polygon_cache;
	bool					m_polygon_cache_moving = false;

	Vector2f angle_vector[8] = { // all 8 wind directions from east, counter clockwise
		Vector2f{ 1,0 },
		Vector2f{ 1,1 }.normalized(),
		Vector2f{ 0,1 },
		Vector2f{ -1,1 }.normalized(),
		Vector2f{ -1,0 },
		Vector2f{ -1,-1 }.normalized(),
		Vector2f{ 0,-1 },
		Vector2f{ 1,-1 }.normalized()
	};

	// visuals
	ShapeDrawer2D				m_shape_drawer_cache;
	ShapeDrawer2D				m_shape_drawer_cache2;
	std::vector<ShapeDrawer2D>	m_shape_drawers_scene;

	inline bool snap_dir() { return InputListener::GetAnyManager()->HasState(InputManager::AState::LeftShift()); }
	inline bool snap_pos() { return InputListener::GetAnyManager()->HasState(InputManager::AState::LeftCtrl()); }
};

#endif // SCENEBUILDER_H