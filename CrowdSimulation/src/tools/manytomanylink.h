#ifndef MANYTOMANYLINK_H
#define MANYTOMANYLINK_H

#include <vector>

template<typename T>
class LinkIterator {
public:
	LinkIterator(typename std::vector<T*>::iterator begin, typename std::vector<T*>::iterator end) : m_begin(begin), m_end(end) {}
	typename std::vector<T*>::iterator begin() { return m_begin; }
	typename std::vector<T*>::iterator end() { return m_end; }
private:
	typename std::vector<T*>::iterator m_begin;
	typename std::vector<T*>::iterator m_end;
};

template<typename ME, typename T>
class ManyToManyLink {
protected:

	virtual ~ManyToManyLink() {
		// this side is destroyed, unlink with all linked
		for (auto* ptr : linked) {
			ptr->Remove(this);
		}
	}

	inline void Link(ME* a, T* b) {
		a->Add(b);
		b->Add(a);
	}

	inline void Link(T* b, ME* a) {
		a->Add(b);
		b->Add(a);
	}

	inline void UnLink(ME* a, T* b) {
		a->Remove(b);
		b->Remove(a);
	}

	inline void UnLink(T* b, ME* a) {
		a->Remove(b);
		b->Remove(a);
	}

	const std::vector<T*>& GetLinked() {
		return linked;
	}
	
	inline LinkIterator<T> GetLinkIterator() {
		return LinkIterator<T>{ linked.begin(), linked.end() };
	}
	
	inline T* AnyLinked() {
		if (linked.size() > 0) return *(linked.begin());
		else return nullptr;
	}

private:

	friend class ManyToManyLink<T, ME>;

	void Add(void* ptr) {
		if (std::find(linked.begin(), linked.end(), ptr) == linked.end()) {
			linked.push_back((T*)ptr);
		}
	}
	void Remove(void* ptr) {
		for (auto iter = linked.begin(); iter < linked.end(); ++iter)
		{
			if (*iter == ptr)
			{
				linked.erase(iter);
				break;
			}
		}
	}

	std::vector<T*> linked; // todo: use set
};


#endif // MANYTOMANYLINK_H
