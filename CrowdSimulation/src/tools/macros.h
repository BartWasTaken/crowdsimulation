#pragma once
#include <type_traits>


#define FOR_EACH_CELL(r, i, j) \
for(int j = 0; j < r.resolution(1); j++) \
for(int i = 0; i < r.resolution(0); i++)

#define FOR_EACH_CELL_IDX(r, i, j, idx) \
for(int j = 0, idx = 0, J_GUARD = r.resolution(1), I_GUARD = r.resolution(0); j < J_GUARD; j++) \
for(int i = 0; i < I_GUARD; i++, idx++)

#define FOR_EACH_INNER_CELL_IDX(r, i, j, idx) \
for(int j = 1, idx = 1 + r.resolution(0), J_GUARD = r.resolution(1) - 1, I_GUARD = r.resolution(0) - 1; j < J_GUARD; j++, idx += 2) \
for(int i = 1; i < I_GUARD; i++, idx++)

#define LOOP_4CELL(fr4c, pid) \
	static_assert(std::is_same<decltype(fr4c), ::NeighbourFinder::FindResult4Cell>::value, "first parameter must be ::NeighbourFinder::FindResult4Cell"); \
	for (int QUADRANT = 0, Q_IDX = fr4c.infl_count[QUADRANT] - 1; QUADRANT < 4; QUADRANT += 1, Q_IDX = fr4c.infl_count[QUADRANT] - 1) \
		if (Q_IDX >= 0) for (int pid = fr4c.infl_ids[QUADRANT][Q_IDX]; /* none */ ; \
			 --Q_IDX, pid = (Q_IDX >= 0) ? fr4c.infl_ids[QUADRANT][Q_IDX] : -1) if (pid == -1) break; else

