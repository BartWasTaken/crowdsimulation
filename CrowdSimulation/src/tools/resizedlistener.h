#ifndef RESIZEDLISTENER_H
#define RESIZEDLISTENER_H
#include "DataTypes.hpp"
#include <vector>

class ISurface;

class ResizedListener
{
public:
	ResizedListener();

	virtual void object_resized(ISurface* obj, Tools::Surface2D<int> new_size) = 0;
};

class ISurface {
public:
	void add_resized_listener(ResizedListener* ptr);
	virtual Tools::Surface2D<int> get_size() = 0;

	inline float ratio() {
		auto s = get_size();
		return s.width / (float) s.height;
	}

protected:
	void shout_resize(Tools::Surface2D<int> size);
private:
	std::vector<ResizedListener*> m_resized_listeners;
};

#endif // RESIZEDLISTENER_H
