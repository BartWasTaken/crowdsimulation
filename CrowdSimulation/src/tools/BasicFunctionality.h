#pragma once
#include <stdint.h>
#include <type_traits>
#include <bitset>
#include <vector>
#include <string>
#define NOMINMAX
#include <windows.h>
#include <chrono>
#include <thread>
#include <algorithm>

namespace Tools {

// allowes elements to be altered, but not the vector itself
template <typename T>
class ElementEditor {
public:
	ElementEditor(std::vector<T>& vector)
		: imVector(vector)
	{
	}
	T& operator[] (uint32_t i) {
		return imVector[i];
	}
	size_t Size() {
		return imVector.size();
	}
private:
	std::vector<T>& imVector;
};

inline void Sleep(int ms) {
	std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

inline void SetHighPriority() {
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
}

template<class T>
T Clamp(T v, T a, T b) {
	return v < a ? a : (v > b ? b : v);
}

template<class T>
T Max(T a, T b) {
	return a > b ? a : b;
}

template<class T>
T Min(T a, T b) {
	return a < b ? a : b;
}

template <typename T, unsigned S>
inline constexpr unsigned ArraySize(const T(&v)[S]) { return S; }

template <typename T>
inline uint32_t BitSetCount(T flags)
{
	return std::bitset<sizeof(sizeof(T))>(flags).count();
}

/* usage: CompilePower<base, exp>::value */
template <unsigned long int B, unsigned long int N>
struct CompilePower
{
	enum : unsigned int { value = B * CompilePower<B, N - 1UL>::value };
};

template <unsigned int B>
struct CompilePower<B, 0>
{
	enum : unsigned long int { value = 1UL };
};

// IntPower is equivalent as CompilePower
constexpr long long IntPower(long long B, unsigned long long N) {
	return N == 0 ? 1 : (B * IntPower(B, N - 1));
}

template <typename VectorType>
inline void SwapElements(std::vector<VectorType>& vector, int id0, int id1)
{
	VectorType tmp = vector[id0];
	vector[id0] = vector[id1];
	vector[id1] = tmp;
}

template <typename VectorType>
inline void RemoveAll(std::vector<VectorType>& vector, const VectorType& val)
{
	vector.erase(std::remove(vector.begin(), vector.end(), val), vector.end());
}

// removes the first item that is equal according to the "==" sign.
// returns if an item was found that matched the value
template <typename VectorType>
inline bool RemoveOne(std::vector<VectorType>& vector, const VectorType& val)
{
	for (std::vector<VectorType>::iterator iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == val)
		{
			vector.erase(iter);
			return true;
		}
	}
	return false;
}

// removes the first item that is equal according to the "==" sign.
// returns if an item was found that matched the value
template <typename VectorType>
inline bool RemoveOneReverse(std::vector<VectorType>& vector, const VectorType& val)
{
	for (std::vector<VectorType>::reverse_iterator iter = vector.rbegin(); iter < vector.rend(); ++iter)
	{
		if (*iter == val)
		{
			vector.erase(std::next(iter).base());
			return true;
		}
	}
	return false;
}

// removes the first item that is equal according to the "==" sign.
// returns if an item was found that matched the value
template <typename VectorType>
inline bool RemoveOneVal(std::vector<VectorType>& vector, const VectorType val)
{
	for (std::vector<VectorType>::iterator iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == val)
		{
			vector.erase(iter);
			return true;
		}
	}
	return false;
}

// removes the first item that is equal according to the "==" sign.
// returns if an item was found that matched the value
template <typename VectorType>
inline bool RemoveOnePtr(std::vector<VectorType>& vector, const void* ptr)
{
	for (std::vector<VectorType>::iterator iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == ptr)
		{
			vector.erase(iter);
			return true;
		}
	}
	return false;
}


// removes the first item that is equal according to the "==" sign.
// returns if an item was found that matched the value
template <typename VectorType>
inline bool RemoveOneValReverse(std::vector<VectorType>& vector, const VectorType val)
{
	for (std::vector<VectorType>::reverse_iterator iter = vector.rbegin(); iter < vector.rend(); ++iter)
	{
		if (*iter == val)
		{
			vector.erase(std::next(iter).base());
			return true;
		}
	}
	return false;
}

// remove item at index
template <typename VectorType>
inline void RemoveAt(std::vector<VectorType>& vector, unsigned int id)
{
	vector.erase(vector.begin() + id);
}

// remove item from index a upto excluding index b
template <typename VectorType>
inline bool RemoveRange(std::vector<VectorType>& vector, unsigned int id1, unsigned int id2)
{
	vector.erase(vector.begin() + id1, vector.begin() + id2);
}

template<typename T, typename I>
inline std::vector<T> RemoveAtIndices(const std::vector<T>& data, std::vector<I>& indicesToDelete)
{
	if (indicesToDelete.empty())
		return data;

	std::vector<T> ret;
	ret.reserve(data.size() - indicesToDelete.size());

	std::sort(indicesToDelete.begin(), indicesToDelete.end());

	// new we can assume there is at least 1 element to delete. copy blocks at a time.
	std::vector<T>::const_iterator itBlockBegin = data.begin();
	for (std::vector<I>::const_iterator it = indicesToDelete.begin(); it != indicesToDelete.end(); ++it)
	{
		std::vector<T>::const_iterator itBlockEnd = data.begin() + *it;
		if (itBlockBegin != itBlockEnd)
		{
			std::copy(itBlockBegin, itBlockEnd, std::back_inserter(ret));
		}
		itBlockBegin = itBlockEnd + 1;
	}

	// copy last block.
	if (itBlockBegin != data.end())
	{
		std::copy(itBlockBegin, data.end(), std::back_inserter(ret));
	}

	return ret;
}

// makes sure the item was not allready in the set
template <typename VectorType>
inline bool AddUnique(std::vector<VectorType>& vector, VectorType& val)
{
	if (std::find(vector.begin(), vector.end(), val) == vector.end()) {
		vector.push_back(val);
		return true;
	}
	return false;
}

// makes sure the item was not allready in the set
template <typename VectorType>
inline bool AddUniquePtr(std::vector<VectorType>& vector, void* ptr)
{
	if (std::find(vector.begin(), vector.end(), ptr) == vector.end()) {
		vector.push_back((VectorType)ptr);
		return true;
	}
	return false;
}

// makes sure the value was not allready in the set
template <typename VectorType>
inline bool AddUniqueVal(std::vector<VectorType>& vector, VectorType val)
{
	if (std::find(vector.begin(), vector.end(), val) == vector.end()) {
		vector.push_back(val);
		return true;
	}
	return false;
}

// if the "Contains" function does not give an error, you dont need "ContainsVal" (ContainsVal doenst pass by reference)
template <typename VectorType>
inline bool Contains(const std::vector<VectorType>& vector, const VectorType& val)
{
	for (std::vector<VectorType>::iterator iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == val) return true;
	}
	return false;
}

template <typename VectorType>
inline bool ContainsVal(const std::vector<VectorType>& vector, const VectorType val)
{
	for (std::vector<VectorType>::const_iterator iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == val) return true;
	}
	return false;
}

// find the first occurance of the element (forward iteration)
template <typename VectorType>
inline VectorType GetElement(const std::vector<VectorType>& vector, VectorType& val)
{
	for (std::vector<VectorType>::iterator iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == val) return *iter;
	}
	return {0};
}

// find the first occurance of the element (forward iteration)
template <typename VectorType>
inline VectorType* GetElementPtr(std::vector<VectorType>& vector, VectorType& val)
{
	for (auto iter = vector.begin(); iter < vector.end(); ++iter)
	{
		if (*iter == val) {
			return &(*iter);
		}
	}
	return nullptr;
}

// find occurance of the element
template <typename VectorType>
inline int32_t GetElementId(const std::vector<VectorType>& vector, VectorType& val)
{
	for (size_t i = 0, n = vector.size(); i < n 0; i++) {
		if (vector[i] == val) return i;
	}
	return -1;
}

// returns index if it was unique, -1 if it was not unique
template <typename VectorType>
inline int32_t UniqueIndex(const std::vector<VectorType>& vector, VectorType& val) {
	int32_t ptr = -1;
	for (size_t i = vector.size() - 1; i >= 0; i--) {
		if (vector[i] == val) {
			if (ptr == -1) ptr = i;
			else return -1;
		}
	}
	return ptr;
}

// returns index if it was unique, -1 if it was not unique, copying the input variable
template <typename VectorType>
inline int32_t UniqueIndexVal(const std::vector<VectorType>& vector, VectorType val) {
	int32_t ptr = -1;
	for (size_t i = 0, n = vector.size(); i < n 0; i++) {
		if (vector[i] == val) {
			if (ptr == -1) ptr = i;
			else return -1;
		}
	}
	return ptr;
}

template<typename T>
bool dereference_less(T const *l, T const *r)
{
	return *l < *r;
}

template<typename T>
bool less(const T l, const T r)
{
	return l < r;
}

template <typename VectorType>
inline bool AllDifferent(const std::vector<VectorType>& vector)
{
	std::vector<const VectorType*> vp;
	vp.reserve(vector.size());
	for (size_t i = vector.size() - 1; i >= 0; i--) vp.push_back(&vector[i]);
	sort(vp.begin(), vp.end(), std::ptr_fun(&dereference_less<VectorType>)); // O(N log N)
	return adjacent_find(vp.begin(), vp.end(),
		std::not2(std::ptr_fun(&dereference_less<VectorType>))) // "opposite functor"
		== vp.end(); // if no adjacent pair (vp_n,vp_n+1) has *vp_n < *vp_n+1
}

template <class VectorType>
bool AllDifferentSort(std::vector<VectorType> &vector) {
	std::sort(vector.begin(), vector.end()); // O(N log N)
	return std::adjacent_find(vector.begin(), vector.end()) == vector.end();
}

template <typename VectorType>
inline bool AllDifferentPtr(const std::vector<VectorType>& vector)
{
	std::vector<const VectorType> vp;
	vp.reserve(vector.size());
	for (size_t i = vector.size() - 1; i >= 0; i--) vp.push_back(vector[i]);
	sort(vp.begin(), vp.end(), std::ptr_fun(&less<VectorType>)); // O(N log N)
	return adjacent_find(vp.begin(), vp.end(),
		std::not2(std::ptr_fun(&less<VectorType>))) // "opposite functor"
		== vp.end(); // if no adjacent pair (vp_n,vp_n+1) has *vp_n < *vp_n+1
}

template <class VectorType>
bool AllDifferentPtrSort(std::vector<VectorType> &vector) {
	std::sort(vector.begin(), vector.end(), std::ptr_fun(&less<VectorType>)); // O(N log N)
	return std::adjacent_find(vector.begin(), vector.end()) == vector.end();
}

template <typename VectorType>
inline void Append(std::vector<VectorType>& v1, std::vector<VectorType>& v2)
{
	v1.insert(v1.end(), v2.begin(), v2.end());
}

template <typename VectorType>
inline void Fill(std::vector<VectorType>& vector, VectorType val)
{
	std::fill(vector.begin(), vector.end(), val);
}

template<typename VectorType>
void Print(std::vector<VectorType>& vector)
{
	std::cout << "[";
	for (auto t : vector)
	{
		std::cout << t << ", ";
	}
	std::cout << "]" << std::endl;
}

template<typename C>
void Print(C collection) {
	std::cout << "[";
	for (auto t : collection)
	{
		std::cout << t << ", ";
	}
	std::cout << "]" << std::endl;
}

template <typename T, unsigned S>
void Print(const T(&v)[S])
{
	std::cout << "[";
	for (T t : v)
	{
		std::cout << t << ", ";
	}
	std::cout << "]" << std::endl;
}

inline std::vector<std::string> SplitString(std::string& input, std::string delimiter)
{
	std::vector<std::string> result;
	size_t pos = 0;
	size_t front = 0;
	while ((pos = input.find_first_of(delimiter, front)) != std::string::npos)
	{
		result.push_back(input.substr(front, pos - front));
		front = pos + 1;
	}
	result.push_back(input.substr(front));
	return result;
}

inline std::vector<std::string> GetExtentions(std::string& filename)
{
	std::vector<std::string> result = SplitString(filename, ".");
	result.erase(result.begin());
	return result;
}

inline std::string GetExtention(std::string& filename, bool includePoint = false)
{
	size_t pos = filename.find_last_of(".");
	if (pos == std::string::npos) return "";
	return filename.substr(pos + (includePoint ? 0 : 1));
}

inline std::string RemoveExtention(std::string filename, bool excludeExtraExtentions = false)
{
	size_t pos = excludeExtraExtentions ? filename.find_first_of(".") : filename.find_last_of(".");
	if (pos == std::string::npos) return filename;
	return filename.substr(0, pos);
}

struct FilePathDescr {
	std::string path; // includes last slash
	std::string name;
	std::string ext; // includes first point
	std::string GetFull();
};
FilePathDescr SplitFullPath(std::string full_path, bool extra_extentions_with_name = true);

inline size_t findLastOf(std::string string, char value)
{
	for (size_t i = string.length() - 1; i >= 0; i--) {
		if (string[i] == value) return i;
	}
	return -1;
}

inline std::wstring StringToWide(const std::string& str)
{
	// Convert an ASCII string to a Unicode String
	std::wstring wstrTo;
	wchar_t *wszTo = new wchar_t[str.length() + 1];
	wszTo[str.size()] = L'\0';
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, wszTo, (int)str.length());
	wstrTo = wszTo;
	delete[] wszTo;
	return wstrTo;
}

inline std::string WideToString(const std::wstring& wstr) {
	// Convert a Unicode string to an ASCII string
	std::string strTo;
	char *szTo = new char[wstr.length() + 1];
	szTo[wstr.size()] = '\0';
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, szTo, (int)wstr.length(), NULL, NULL);
	strTo = szTo;
	delete[] szTo;
	return strTo;
}

inline std::string ToLower(const std::string input) {
	std::string result = input;
	std::transform(result.begin(), result.end(), result.begin(), ::tolower);
	return result;
}

inline bool StringEqualsLowerCase(const std::string& a, const std::string& b)
{
	auto sz = a.size();

	if (b.size() != sz) {
		return false;
	}

	for (unsigned int i = 0; i < sz; ++i)
	{
		if (tolower(a[i]) != tolower(b[i])) {
			return false;
		}
	}

	return true;
}

}