#pragma once 
#include "shared_eigen_incl.h"
#include "raster.h"
#include "griddecorators.h"

class LineTracer
{
public:
	LineTracer();

	static void rasterize_line(Vector2f p0, Vector2f p1, const Raster& grid, IRegisterGrid& output, int line_id);


	//template<typename F> // of type bool(vector2i)
	static Vector2i rasterize_line(Vector2f p0, Vector2f p1, const Raster& grid, std::function<bool(Vector2i)> func);

private:
	static void project_line_to_grid(Raster grid, Vector2f p0, Vector2f p1, Vector2f& out_start_pos, Vector2i& out_start_coords);
	static void rasterize_delta(Vector2f a, Vector2f b, const Raster& grid, Vector2f target, Vector2f& out_delta, Vector2f& out_max);
};
