#ifndef RASTER_H
#define RASTER_H

#include "shared_eigen_incl.h"
#include <QDebug>

struct Raster
{
public:
	Raster();
	Raster(AABB2 bbox, Vector2i dim);
	Raster(Vector2f min, Vector2f max, Vector2i dim);

	struct Block
	{
		// a block is a square in a raster
		// it is inclusive right and top
		// a bock might contain -1 and 'size' as values
		int l, r, b, t;
		inline void clamp_to_valid(Vector2i dim) 
		{
			Q_ASSERT(l < dim(0) && b < dim(1) && t >= 0 && r >= 0);
			if (l < 0) l = 0;
			if (b < 0) b = 0;
			if (t >= dim(1)) t = dim(1) - 1;
			if (r >= dim(0)) r = dim(0) - 1;
		}
		inline int cell_count() {
			auto d = dim();
			return d(0) * d(1);
		}
		inline Vector2i dim() {
			return { r - l + 1, t - b + 1 };
		}

		inline void fill_idx_list(int* idx_list, Raster raster) {
			idx_list[0] = (l >= 0 && b >= 0) ? raster.idx(l, b) : -1;
			idx_list[1] = (l >= 0 && t < raster.resolution(1)) ? raster.idx(l, t) : -1;
			idx_list[2] = (r < raster.resolution(0) && b >= 0) ? raster.idx(r, b) : -1;
			idx_list[3] = (r < raster.resolution(0) && t < raster.resolution(1)) ? raster.idx(r, t) : -1;
		}
	};

	void set_dim(Vector2i);
	void set_region(AABB2);
	void set_region(Vector2f min, Vector2f max);

	// indexes for linear data of the 2d grid
	int			idx(int x, int y) const;
	int			idx(Vector2i index) const;
	int			idx(Vector2f point) const;
	int			safe_idx(Vector2f point);
	int			idx_01(Vector2f unit_space_point) const;

	Vector2i	coords(int index) const;
	// returns the cell indices, might give coords outside the grid if "contains(point) == false"
	Vector2i	coords(Vector2f point) const;
	Vector2i	coords_01(Vector2f unit_space_point) const;
	// give valid grid coordinates
	Vector2i	safe_coords(Vector2f point) const;
	Vector2i	safe_inner_coordinates(Vector2f point) const;
	Vector2i	safe_coords_01(Vector2f unit_space_point) const;

	// returns the closest point inside the raster
	Vector2f	clamp(Vector2f) const;
	Vector2f	clamp_inner(Vector2f) const;
	// returns the closest cell inside the raster (to avoid out of bound coordinates)
	Vector2i	snap(Vector2i) const;
	Vector2i	snap_inner(Vector2i) const;

	// returns the real location of cells based on their center
	Vector2f	cell_center(Vector2i c) const;
	Vector2f	cell_center(int x, int y)const;

	// includes min, excludes max
	bool		contains(Vector2f point) const;

	// checks if 'c' is within the grid coordinates
	bool		is_coord_valid(Vector2i c) const;
	bool		is_coord_valid(int x, int y) const;

	Vector2f	bottom_left() const;
	Vector2f	bottom_right() const;
	Vector2f	top_left() const;
	Vector2f	top_right() const;

	Vector2f	operator[](int i) const;

	Vector2f	cell_corner(Vector2i coords, Vector2f dir) const;
	Vector2f	cell_corner_bl(Vector2i coords) const;
	Vector2f	cell_size() const;

	Vector2i	resolution() const;
	int			resolution(int i) const;
	Vector2f	range() const;
	Vector2f	min() const;
	Vector2f	max() const;
	AABB2		bbox() const;
	AABB2		cell_bbox(int i, int j) const;
	int			cell_count() const;

	Vector2f	to_unit_space(Vector2f pos) const;

	Block		find_block(Vector2f pos, bool valid_only = false) const;

	Block		find_block(AABB2 region, bool valid_only = false) const;
	Raster		make_raster(Block) const;

	Raster		make_raster_plus_1_boundary() const;
	Raster		make_centers_raster() const;

	struct Segment {
		Vector2f a, b;
	};
	Segment		horizontal_line(int vertical_index) const;
	Segment		vertical_line(int horizontal_index) const;

	bool operator==(const Raster& other) const;

	bool		check_square_cell() const;
	float		influence_range() const;

private:
	Vector2f	coords_f(Vector2f point) const;

	Vector2f	m_min;
	Vector2f	m_max;
	Vector2f	m_range;
	Vector2i	m_dim;
};

#endif // RASTER_H