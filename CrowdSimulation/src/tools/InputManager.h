#pragma once
#include <vector>
#include <Qt>
#include "manytomanylink.h"
#include <unordered_map>


/*
	There is a problem wit hthe right alt
	it creates wrong behaviour because it sends 17+18
	if control is pressed and you release alt+gr, control seems to get released?!
*/

class InputListener;

namespace InputControl
{
enum ControlType { LB = 0, RB = 1, SCROLL = 2, MOVE = 3, KEYS = 4, CONTROL_TYPE_COUNT = 5 };

struct Request {
	enum Value { NONE = 0, TRY_TAKE, TRY_TAKE_KEEP, TRY_RELEASE };
	Value status[CONTROL_TYPE_COUNT] = { NONE };
	constexpr const static Request ReleaseAll() {
		Request result{};
		for (int i = 0; i < CONTROL_TYPE_COUNT; i++) result.status[i] = Request::TRY_RELEASE;
		return result;
	};
	constexpr const static Request Take_Keep_All() {
		Request result{};
		for (int i = 0; i < CONTROL_TYPE_COUNT; i++) result.status[i] = Request::TRY_TAKE_KEEP;
		return result;
	};
	constexpr const static Request Take_Keep_Mouse() {
		Request result{};
		for (int i = 0; i < CONTROL_TYPE_COUNT; i++) result.status[i] = Request::NONE;
		result.status[LB] = Request::TRY_TAKE_KEEP;
		result.status[RB] = Request::TRY_TAKE_KEEP;
		return result;
	}
};



static Request TakeControl(bool lb, bool rb, bool scroll, bool keys) {
	Request result{ };
	if (lb) result.status[LB] = Request::TRY_TAKE_KEEP;
	if (rb) result.status[LB] = Request::TRY_TAKE_KEEP;
	if (scroll) result.status[SCROLL] = Request::TRY_TAKE_KEEP;
	if (keys) result.status[KEYS] = Request::TRY_TAKE_KEEP;
	return result;
}

};

struct MouseEventInfo {
	enum Buttons : int {
		LB = 0x001,
		RB = 0x010
	};
	using ButtonFlag = int;

	inline bool is_acces_blocked(InputListener* ptr) {
		return control_owner != nullptr && control_owner != ptr;
	}

	inline bool is_lb() {
		return button_flags & LB;
	}

	inline bool is_rb() {
		return button_flags & RB;
	}

	inline bool empty() {
		return !(is_lb() || is_rb());
	}

	int x = 0;
	int y = 0;
	int delta = 0;
	ButtonFlag button_flags = 0;
	InputListener* control_owner = nullptr;
};

struct MouseEventReturnValue
{
	InputControl::Request status_request; // default none
};

struct KeyEventReturnValue
{
	InputControl::Request status_request; // default none
};

class InputManager
	: public ManyToManyLink<InputManager, InputListener>
{
public:
	InputManager();
	virtual ~InputManager();

	enum class AStateType {
		DONT_CARE, DOWN, UP
	};

	enum class AStateTypeLR {
		DONT_CARE_LR, EITHER_DOWN, BOTH_DOWN, BOTH_UP, LEFT_DOWN_ONLY, RIGHT_DOWN_ONLY,
		LEFT_DOWN, RIGHT_DOWN
	};

	struct AState { // alternatives key states
		const static AState Default;
		AStateType Control = AStateType::UP; // we assume neither control should be pressed
		AStateType Shift = AStateType::UP; // we assume neither shift should be pressed
		AStateType Alt = AStateType::UP; // we assume alt should not be pressed

		// common ones:
		static AState LeftShift() {
			return { AStateType::DONT_CARE, AStateType::DOWN, AStateType::DONT_CARE };
		}
		static AState LeftShiftOnly() {
			return { AStateType::UP, AStateType::DOWN, AStateType::UP };
		}
		static AState LeftCtrl() {
			return { AStateType::DOWN, AStateType::DONT_CARE, AStateType::DONT_CARE };
		}
		static AState LeftCtrlOnly() {
			return { AStateType::DOWN, AStateType::UP, AStateType::UP };
		}
	};

	enum KFlag { // : unsigned char
		NONE = 0,
		PRESSED = 1,
		RELEASED = 2,
		DOWN = 4
	};

	void AddInputListener(InputListener*);
	void RemoveInputListener(InputListener*);

	bool HasState(const AState&);
	bool HasDefaultState();

	// IsKeyDown returns if at this moment, the key is beeing pressed
	// this returns false while the window does not have focus
	bool IsKeyDown(Qt::Key, AState = AState());

	// return if the key was pressed at some moment during the last frame
	// used for calls from the thread doing the main loop (reading messages from win32)
	// when u dont want to miss key presses
	bool IsKeyPressed(Qt::Key, AState = AState());

	// return if the key was released at some moment during the last frame
	// used for calls from the thread doing the main loop (reading messages from win32)
	// when u dont want to miss key presses
	bool IsKeyReleased(Qt::Key, AState = AState());

	// " I " is for ignorant towards other keys
	virtual bool IsKeyDownI(Qt::Key) = 0;
	virtual bool IsKeyPressedI(Qt::Key) = 0;
	virtual bool IsKeyReleasedI(Qt::Key) = 0;

	virtual void ReleaseAllKeys() = 0;

	virtual void PrepForNextFrame() = 0;

	bool HasFocus();

	// events:
	void SetFocusMsg(bool);
	virtual void MousePressMsg(MouseEventInfo);
	virtual void MouseMoveMsg(MouseEventInfo);
	virtual void MouseReleaseMsg(MouseEventInfo);
	virtual void MouseScrollMsg(MouseEventInfo);
	virtual void KeyDownMsg(Qt::Key key) = 0;
	virtual void KeyUpMsg(Qt::Key key) = 0;

	bool try_take_control(InputListener*, InputControl::ControlType);


protected:
	const std::vector<InputListener*>& ListenerList();

	bool gHasFocus = true;

	bool HasState(Qt::Key key_code, AStateType);
	bool HasState(Qt::Key key_code_l, Qt::Key key_code_r, AStateTypeLR);

	// calls to the the children
	virtual void FocusGained() = 0;
	virtual void FocusLost() = 0;

	InputListener* m_control_owners[InputControl::CONTROL_TYPE_COUNT] = { nullptr };

	void clear_control();

	void handle_control(InputListener*, InputControl::Request&);
	bool should_do_call(InputListener*, InputListener*& original_owner);

	template<typename FUNC, typename ARG> void shout(InputControl::ControlType ct, FUNC f, ARG arg)
	{
		InputListener*& original_owner = m_control_owners[ct];

		for (auto& obj : GetLinkIterator())
		{
			if (should_do_call(obj, original_owner))
			{
				//info.control_owner = m_control_owner;
				//auto result = obj->KeyClick(key);
				auto result = std::invoke(f, *obj, arg);
				handle_control(obj, result.status_request);
			}
		}
	}

};

inline InputManager::KFlag operator ~(InputManager::KFlag a)
{
	return (InputManager::KFlag) ~(int)a;
}

inline InputManager::KFlag& operator |=(InputManager::KFlag& a, InputManager::KFlag b)
{
	return a = (InputManager::KFlag) (a | b);
}

inline InputManager::KFlag& operator &=(InputManager::KFlag& a, InputManager::KFlag b)
{
	return a = (InputManager::KFlag) (a & b);
}

inline InputManager::KFlag operator |(InputManager::KFlag a, InputManager::KFlag b)
{
	return (InputManager::KFlag) (a | b);
	//static_cast<InputManager::KFlag>(static_cast<int>(a) | static_cast<int>(b));
}

class EventBasedInputManager
	: public InputManager
{
public:
	EventBasedInputManager();

	virtual bool IsKeyDownI(Qt::Key key) override;
	virtual bool IsKeyPressedI(Qt::Key key) override;
	virtual bool IsKeyReleasedI(Qt::Key key) override;
	virtual void PrepForNextFrame() override;

	virtual void KeyDownMsg(Qt::Key key) override;
	virtual void KeyUpMsg(Qt::Key key) override;

	virtual void FocusGained() override;
	virtual void FocusLost() override;

	virtual void ReleaseAllKeys() override;

private:

	void RenewKeyBoardState();

	std::unordered_map<Qt::Key, KFlag> m_keyboard_state;
};
