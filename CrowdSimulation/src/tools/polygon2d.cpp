#include "polygon2d.h"
#include "geomfunc.h"
#include "linetracer.h"
#include "qpolygon.h"

Polygon2D::Polygon2D(const std::vector<Vector2f>& v)
{
	setup(v);
}

void Polygon2D::setup(const std::vector<Vector2f>& v)
{
	m_vertices = v;
	finish();
}

const std::vector<Vector2f>& Polygon2D::vertices() const
{
	return m_vertices;
}

bool Polygon2D::intersects(const Vector2f& a, const Vector2f& b) const
{
	// check if the bounding boxes of both objects intersect -> means no intersect
	if (!m_bbox.intersects(make_bbox(a, b))) return false;

	auto nvert = m_vertices.size();
	const Vector2f* vert = m_vertices.data();

	// check if either point is in the polygon -> means intersect
	if (contains(a, vert, nvert) || contains(b, vert, nvert)) return true;

	// check for each line in the polygon if it intersects the input line
	int i, j, c = 0;
	for (i = 0, j = nvert - 1; i < nvert; j = i++)
	{
		if (intersection(vert[i], vert[j], a, b)) {
			return true;
		}
	}

	return false;
}

bool Polygon2D::intersects(AABB2 aabb) const
{
	if (!m_bbox.intersects(aabb)) {
		return false;
	}

	// check for one of the points of the input aabb to be inside the polygon

	if (this->contains(aabb.corner(AABB2::BottomLeft))
		|| this->contains(aabb.corner(AABB2::TopLeft))
		|| this->contains(aabb.corner(AABB2::TopRight))
		|| this->contains(aabb.corner(AABB2::BottomRight)))
	{
		return true;
	}

	auto nvert = m_vertices.size();
	const Vector2f* vert = m_vertices.data();

	// and check for one of the polylines to intersect any of the input lines

	int i, j, c = 0;
	for (i = 0, j = nvert - 1; i < nvert; j = i++)
	{
		if (intersection(vert[i], vert[j], aabb.corner(AABB2::BottomLeft), aabb.corner(AABB2::TopLeft))
			|| intersection(vert[i], vert[j], aabb.corner(AABB2::TopLeft), aabb.corner(AABB2::TopRight))
			|| intersection(vert[i], vert[j], aabb.corner(AABB2::TopRight), aabb.corner(AABB2::BottomRight))
			|| intersection(vert[i], vert[j], aabb.corner(AABB2::BottomRight), aabb.corner(AABB2::TopLeft)))
		{
			return true;
		}
	}

	// then check if polygon inside bbox
	return false;
}

bool Polygon2D::contains(const Vector2f& point, const Vector2f* vert, std::size_t nvert) const
{
	// https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html

	int i, j, c = 0;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((vert[i](1) >= point(1)) != (vert[j](1) >= point(1))) &&
			(point(0) <= (vert[j](0) - vert[i](0)) * (point(1) - vert[i](1)) / (vert[j](1) - vert[i](1)) + vert[i](0)))
			c = !c;
	}

	return c;
}

bool Polygon2D::contains(const Vector2f& point) const
{
	if (!m_bbox.contains(point)) return false;

	std::size_t nvert = m_vertices.size();
	const Vector2f* vert = m_vertices.data();

	return contains(point, vert, nvert);
}

bool Polygon2D::contains(const Polygon2D& other) const
{
	if (m_bbox.intersects(other.m_bbox)) {
		for (const auto other_point : other.m_vertices) {
			if (!this->contains(other_point)) return false;
		}
		return true;
	}
	else Q_ASSERT(!m_bbox.contains(other.m_bbox)); // intersects should include containment

	return false;
}

void Polygon2D::move(Vector2f translation)
{
	for (auto& v : m_vertices) v += translation;
}

void Polygon2D::project(const Vector2f& point, Vector2f& surface, Vector2f& normal) const
{
	float min_dist_sq = std::numeric_limits<float>::infinity();
	Vector2f min_dist_line_dir;

	for (int i = 0; i < m_vcount; i++)
	{
		Vector2f a = m_vertices[i];
		Vector2f b = m_vertices[(i + 1) % m_vcount];

		Vector2f sp = closest_point_on_line(a, b, point);
		float dist_sq = (sp - point).squaredNorm();
		if (dist_sq < min_dist_sq) {
			min_dist_sq = dist_sq;
			min_dist_line_dir = (b - a);
			surface = sp;
		}
	}

	normal = rotate_90_deg_ccw(min_dist_line_dir).normalized();
}

float Polygon2D::distanceTo(const Vector2f& point) const
{
	float min_dist_sq = std::numeric_limits<float>::infinity();

	if (contains(point)) return 0;

	for (int i = 0; i < m_vcount; i++)
	{
		Vector2f dir = line_to_point(
			m_vertices[(i + 1) % m_vcount] - m_vertices[i],
			point - m_vertices[i]
		);

		float dist_sq = dir.squaredNorm();
		if (dist_sq < min_dist_sq) min_dist_sq = dist_sq;
	}

	// check if it should be negative or positive (inside or outside the polygon)
	// get the normal of the it was closest too
	// check if dot product is positive or negative

	return std::sqrt(min_dist_sq);
}
void Polygon2D::write_to_grid(IRegisterGrid& reg, Raster global_raster, bool full_cover, int id) const
{
	auto local_block = global_raster.find_block(m_bbox, true);
	auto local_raster = global_raster.make_raster(local_block);

	// require enough space in the raster array
	if (m_local_cache.size() < local_raster.cell_count()) {
		m_local_cache.resize(local_raster.cell_count());
	}

	// for each cell center inside the obstacle raster, check if it's in the obstacle
	int inner_register_count = 0;


	// loop over all cell centers and check if inside
	// but stay within the bounding box

	// then trace the polygon lines and write them to the grid

	// use a boolean cache to avoid set comparison


	// for all obstacle register types for now, this register method is used first:
	for (int y = local_block.b; y <= local_block.t; y++)
	{
		for (int x = local_block.l; x <= local_block.r; x++)
		{
			if (this->contains(global_raster.cell_center(x, y)))
			{
				int local_x = x - local_block.l;
				int local_y = y - local_block.b;
				m_local_cache[local_raster.idx(local_x, local_y)] = true;
				inner_register_count++;
			}
		}
	}

	// now we will write the outline of the obstacles
	// this is all the cells that are partially covered by the obstacle
	// if we want them included "outer" is true and the cells are set to "true"
	// otherwise these cells are set to false (in case 'inner' considered them inside)

	RegisterGrid_value<bool> local_reg{ &m_local_cache, local_raster.resolution(), full_cover };

	for (int i = 0, j = m_vcount - 1; i < m_vcount; j = i++)
	{
		Vector2f a = m_vertices[i];
		Vector2f b = m_vertices[j];
		LineTracer::rasterize_line(a, b, local_raster, local_reg, id);
	}

	// push to grid:
	for (int y = 0; y < local_raster.resolution(1); y++)
	{
		for (int x = 0; x < local_raster.resolution(0); x++)
		{
			if (m_local_cache[local_raster.idx(x, y)])
			{
				int real_x = x + local_block.l;
				int real_y = y + local_block.b;
				reg.write(real_x, real_y, id);
				m_local_cache[local_raster.idx(x, y)] = false;
			}
		}
	}
}


std::vector<Vector2f>& Polygon2D::edit_vertices()
{
	return m_vertices;
}

void Polygon2D::finish()
{
	m_vcount = m_vertices.size();
	m_bbox = compute_aabb(m_vertices);
	m_inf = m_bbox.max() + m_bbox.sizes() * 0.1f;
}

QPolygonF Polygon2D::toQPolygon() const
{
	QPolygonF out_polygon;
	for (auto v : m_vertices) {
		out_polygon.append(QPointF(v(0), v(1)));
	}
	if (m_vertices.size() > 0)
	out_polygon.append(QPointF(m_vertices[0](0), m_vertices[0](1)));
	return out_polygon;
}
