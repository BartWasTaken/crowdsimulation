#include "geomfunc.h"


/*

auto cs = grid.cell_size();
Vector2f start_pos = p0;
Vector2i start_coords = grid.coords(p0);
Vector2i end_coords = grid.safe_coords(p1);

if (grid.is_coord_valid(start_coords) == false)
{
float min_dist_sq = std::numeric_limits<float>::infinity();

for (int i = 0, j = 3; i < 4; j = i++)
{
Vector2f pos;
if (intersection(p0, p1, grid[j], grid[i], &pos))
{
auto dist_sq = (p0 - pos).squaredNorm();
if (dist_sq < min_dist_sq) {
start_pos = pos;
start_coords = grid.safe_coords(pos);
}
}
}

if (grid.is_coord_valid(start_coords) == false)
{
start_coords = grid.safe_coords(start_pos);
//rasterize_line(p0, p1, grid, output, line_id);
}
}


Q_ASSERT(grid.is_coord_valid(start_coords));

Vector2f dir = p1 - start_pos;
float stepX = (dir(0) >= 0) ? 1 : -1;
float stepY = (dir(1) >= 0) ? 1 : -1;

// compute distance to first vertical and first horizontal crossing
// first find line direction's corner
Vector2f target = grid.cell_corner(start_coords, dir);
// then determine how much we have to go vertically to reach the x-boundary

Vector2f tMax;

constexpr float delta = 0.0000001f;

if (qAbs(dir(1)) < delta) {
tMax(0) = std::numeric_limits<float>::max();
tMax(1) = 0;
}
else if (qAbs(dir(0)) < delta) {
tMax(0) = 0;
tMax(1) = std::numeric_limits<float>::max();
}
else {
auto cc = grid.cell_size();

// store x-distance to hit a horizontal line edge of current cell

auto v_prog_to_top = (target(1) - start_pos(1)) / dir(1);
tMax(0) = qAbs(dir(0) * v_prog_to_top);

auto x_prog_to_side = (target(0) - start_pos(0)) / dir(0);
tMax(1) = qAbs(dir(1) * x_prog_to_side);
}

Vector2f tDelta = get_delta(p0, p1, grid);

Vector2i P = start_coords;
Vector2i dim = grid.resolution();

int max_step_x = qAbs(end_coords(0) - start_coords(0));
int max_step_y = qAbs(end_coords(1) - start_coords(1));
int nstepx = 0;
int nstepy = 0;

while (true)
{
output.write(P(0), P(1), line_id);
if (tMax(1) > tMax(0)) {
tMax(0) = tMax(0) + tDelta(0);
P(1) += stepY;
if (++nstepy > max_step_y) break;
}
else {
tMax(1) = tMax(1) + tDelta(1);
P(0) += stepX;
if (++nstepx > max_step_x) break;
}
Q_ASSERT(grid.is_coord_valid(P));
}
*/
