#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "programmanager.h"

#include <QFileInfo>
#include <QFileDialog>
#include <QTimer>
#include <QSurfaceFormat>
#include <QKeyEvent>
#include <QTreeView>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	m_ui(new Ui::MainWindow)
{
	setAttribute(Qt::WA_DeleteOnClose);

	m_ui->setupUi(this);
	my_setupui();

	// do "step()" on each cycle of the mainloop
	auto timer = new QTimer(parent);
	connect(timer, &QTimer::timeout, this, [this] { step(); });
	timer->start();

	s_gInst = this;

}

MainWindow& MainWindow::Create(QWidget *parent /*= 0*/)
{
	return *(new MainWindow(parent));
}

MainWindow::~MainWindow()
{
	SafeDeletePtr(m_pm);
	SafeDeletePtr(m_ui);
}

OpenGLView& MainWindow::opengl()
{
	return m_ui->openGLWidget->getFunc();
}

void MainWindow::simulationFromFile()
{
	QString filename = QFileDialog::getOpenFileName(this, "Select a simulation file", m_last_sim_load_filename, "JSON files (*.json);;All files (*)");
	m_last_sim_load_filename = filename;

	// optionally show the user the file config, and allow changes:
	//if (QFileInfo::exists(filename) && scDialog.exec() == QDialog::Accepted) {

   //}

	if (QFileInfo::exists(filename)) {
		m_pm->simulationFromFile(filename);
	}
}

void MainWindow::step()
{
	// Main loop logic

	if (m_ui) {

		if (m_pm == nullptr && m_ui->openGLWidget->is_initialized()) {
			m_pm = new ProgramManager(*m_ui->openGLWidget, *this);
			m_pm->simulationDefault();
			m_ui->openGLWidget->set_render_obj(m_pm);
		}

		if (m_ui->openGLWidget) {
			m_ui->openGLWidget->update(); // triggers a render
			if (m_pm) m_pm->step();
			m_ui->openGLWidget->step(); // updates input
		}

	}

}

QMenu* MainWindow::add_menu(QString name)
{
	return menuBar()->addMenu(name);
}


StatsView* MainWindow::getStatsView()
{
	return m_ui->tableView;
}

QFormLayout* MainWindow::getEventForm()
{
	return m_ui->event_form_2;
}

void MainWindow::closeEvent(QCloseEvent*)
{
	m_closed = true;
}

void MainWindow::mousePressEvent(QMouseEvent *e)
{
	input.MousePressMsg(to_press_mouse_event(e));
}

void MainWindow::mouseReleaseEvent(QMouseEvent *e)
{
	input.MouseReleaseMsg(to_release_mouse_event(e));
}

void MainWindow::mouseMoveEvent(QMouseEvent *e)
{
	input.MouseMoveMsg(to_move_mouse_event(e));
}

void MainWindow::wheelEvent(QWheelEvent *e)
{
	input.MouseScrollMsg(to_scroll_mouse_event(e));
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
	input.KeyDownMsg((Qt::Key) e->key());
}

void MainWindow::keyReleaseEvent(QKeyEvent* e)
{
	input.KeyUpMsg((Qt::Key) e->key());
}

void MainWindow::focusInEvent(QFocusEvent* e)
{
	input.FocusGained();
}

void MainWindow::focusOutEvent(QFocusEvent*)
{
	input.FocusLost();
}

void MainWindow::my_setupui()
{
	// connect buttons to actions
	// connect(m_ui->actionColor, &QAction::triggered, m_ui->openGLWidget, &MyOpenGLWidget::color_button_click);
	connect(m_ui->actionLoad_Simulation, &QAction::triggered, this, &MainWindow::simulationFromFile);
}

MainWindow* MainWindow::s_gInst = nullptr;
