#include "polygon3d.h"
#include "vis/openglincl.h"
#include "vis/shaderloader.h"

Polygon3D::Polygon3D(OpenGLView& opengl) 
	: m_indice_buffer(opengl), f(opengl)
{

}

void Polygon3D::build(std::vector<Vector3f>& vertices, std::vector<GLuint>& indices)
{
	Q_ASSERT(f.glGetError() == 0);

	f.glGenVertexArrays(1, &m_vao);
	f.glBindVertexArray(m_vao);

	m_icount = indices.size();
	
	if (vertices.data() > 0) { // vertice buffer
		int vertice_bytes = vertices.size() * sizeof(Vector3f);
		m_vbuffers.push_back(f);
		m_vbuffers.back().Write(vertice_bytes, 0, vertices.data(), GL_STATIC_DRAW);
		f.glEnableVertexAttribArray(0);
		f.glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}

	if (indices.data() >0) { // indice buffer
		int indice_bytes = indices.size() * sizeof(GLuint);
		m_indice_buffer.Write(indice_bytes, 0, indices.data(), GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER);
	}

	//f.glDisableVertexAttribArray(0);
	f.glBindVertexArray(0);

	Q_ASSERT(f.glGetError() == 0);
}

void Polygon3D::build(CGMain::IVertexData* vertex_data)
{
	std::vector<CGMain::VertexBufferDescr> buffer_descriptions = vertex_data->GetLayout();

	f.glGenVertexArrays(1, &m_vao);
	f.glBindVertexArray(m_vao);

	//SETUP INDICES
	if (vertex_data->is_indexed()) {
		m_indice_buffer.Write(sizeof(uint32_t) * vertex_data->iCount(), 0, vertex_data->indices(), GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER);
	}

	//SETUP VERTICES
	for (int i = 0; i < buffer_descriptions.size(); i++) {
		m_vbuffers.push_back(f);
		m_vbuffers.back().Write(buffer_descriptions[i].byte_size, 0, buffer_descriptions[i].data, GL_STATIC_DRAW);
	}
}

void Polygon3D::render(RenderContext3D rc)
{
	Q_ASSERT(f.glGetError() == 0);
	if (m_indice_buffer.is_written()) {
		rc.f.glBindVertexArray(m_vao);
		prep_shader(rc);
		rc.f.glDrawElements(GL_TRIANGLES, m_icount, GL_UNSIGNED_INT, (void*)0);
		//rc.f.glBindVertexArray(0);
		rc.f.glBindVertexArray(0);
	}
	Q_ASSERT(f.glGetError() == 0);
}

void Polygon3D::destroy()
{
	m_indice_buffer.Destroy();

	for (auto& buffer : m_vbuffers) buffer.Destroy();	
	m_vbuffers.clear();

	f.glDeleteVertexArrays(1, &m_vao);
	m_vao = 0;
}

void Polygon3D::prep_shader(RenderContext3D rc)
{
	Q_ASSERT(f.glGetError() == 0);
	auto vp = rc.pViewpoint->getVPMatrix();
	Matrix4f mvp = vp * this->GetModelMatrix();

	constexpr const int mpv_loc = 0;
	f.glUseProgram(rc.f.shader_loader().poly3d_shader());
	Q_ASSERT(f.glGetError() == 0);
	f.glUniformMatrix4fv(mpv_loc, 1, FALSE, mvp.data());
	Q_ASSERT(f.glGetError() == 0);
}
