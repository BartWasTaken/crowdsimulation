#include "instanceddrawer3d.h"
#include <QDebug>
#include "vis/openglmaster.h"

InstancedDrawer3D::InstancedDrawer3D(OpenGLView& func)
	: IInstancedDrawerBase(func), m_f(func), m_matrix_buffer(func), m_vertex_buffer(func), 
	m_normal_buffer(func), m_indice_buffer(func), m_color_buffer(func)
{
	m_shader_id = provide_shader(func.shader_loader());
}

void InstancedDrawer3D::render(RenderContext3D rc)
{
	upload_data(rc);
	draw(rc);
}

void InstancedDrawer3D::set_shape(std::vector<Vector3f>& v, std::vector<Vector3f>& n, std::vector<GLuint>& i)
{
	Q_ASSERT(m_vertices.size() == 0, "changing of shape not supported");
	m_vertices = v;
	m_indices = i;
	m_normals = n;
	set_changed();
}

void InstancedDrawer3D::set_shape(MeshGen::IndexedMesh<Vector3f>& mesh)
{
	Q_ASSERT(m_vertices.size() == 0, "changing of shape not supported");
	m_vertices.assign(mesh.vertices, mesh.vertices + mesh.vCount);
	m_indices.assign(mesh.indices, mesh.indices + mesh.iCount);
	set_changed();
}

void InstancedDrawer3D::set_shape(CGMain::IVertexData* vdata)
{
	int vcount = vdata->vCount();

	// write vertices
	{
		m_vertices.resize(vcount);
		for (int i = 0; i < vcount; i++) m_vertices[i] = vdata->pos(i);
	}

	// write normals
	if (vdata->has_norm()) {
		m_normals.resize(vcount);
		for (int i = 0; i < vcount; i++) m_normals[i] = vdata->norm(i);
	}

	// write indices
	m_indices.assign(vdata->indices(), vdata->indices() + vdata->iCount());
}

void InstancedDrawer3D::add(Vector3f position, Vector3f rotation, Vector3f scale, Color color)
{
	QMutexLocker locker(&m_mutex);
	m_orientations.push_back({});
	m_orientations.back().SetRotation(rotation);
	m_orientations.back().SetTranslation(position);
	m_orientations.back().SetScale(scale);
	m_colors.push_back(color.as_vector_rgba());
	set_changed(true);
}

void InstancedDrawer3D::set_amount(int n)
{
	m_orientations.resize(n);
	m_colors.resize(n);
}

void InstancedDrawer3D::move_to(int id, Vector3f position)
{
	Q_ASSERT(id < m_orientations.size());
	m_orientations[id].SetTranslation(position);
	set_changed();
}

void InstancedDrawer3D::change(int i, Vector3f position, Vector3f rotation, Vector3f scale, Color color)
{
	Q_ASSERT(i < m_orientations.size());
	m_orientations[i].SetTranslation(position);
	m_orientations[i].SetRotation(rotation);
	m_orientations[i].SetScale(scale);
	m_colors[i] = color.as_vector_rgba();
}

void InstancedDrawer3D::pop()
{

}

int InstancedDrawer3D::provide_shader(ShaderLoader& sl)
{
	SHADER_FILE_DESCR shader;
	shader.frag = "instanced_3d_moving";
	shader.vert = "instanced_3d_moving";
	return sl.LoadShader(shader);
}

void InstancedDrawer3D::prep_shader(RenderContext3D rc)
{
	Q_ASSERT(f.glGetError() == 0);
	constexpr const int mpv_loc = 0;
	f.glUseProgram(m_shader_id);
	Matrix4f vp = rc.pViewpoint->getVPMatrix();
	f.glUniformMatrix4fv(mpv_loc, 1, FALSE, vp.data());
	Q_ASSERT(f.glGetError() == 0);
}

void InstancedDrawer3D::draw(RenderContext3D rc)
{
	Q_ASSERT(f.opengl_master().verify_opengl_thread_id());
	Q_ASSERT(f.glGetError() == 0);

	if (m_buffered_shape_count == 0 || m_buffered_indice_count == 0) return;

	if (m_vao == 0) {

		f.glGenVertexArrays(1, &m_vao);
		f.glBindVertexArray(m_vao);

		m_indice_buffer.Bind();

		int LAYOUT;

		LAYOUT = 0;
		{ // colors
		  // 2nd attribute buffer : particles' colors
			f.glEnableVertexAttribArray(LAYOUT);
			m_color_buffer.Bind();
			f.glVertexAttribPointer(
				LAYOUT, // attribute. No particular reason for 1, but must match the layout in the shader.
				4, // element count : r + g + b + a => 4
				GL_UNSIGNED_BYTE, // type
				GL_TRUE, // normalized? YES (because its color!), this means that the unsigned char[4] will be accessible with a vec4 (floats) in the shader
				0, // stride
				(void*)0 // array buffer offset
			);
			f.glVertexAttribDivisor(LAYOUT, 1); // color : one per instance -> 1 
		}

		LAYOUT = 1;
		{ // vertices
			f.glEnableVertexAttribArray(LAYOUT);
			m_vertex_buffer.Bind();
			f.glVertexAttribPointer(
				LAYOUT, // attribute. No particular reason for 0, but must match the layout in the shader.
				3, // nr of components
				GL_FLOAT, // type
				GL_FALSE, // normalized?
				0, // stride
				(void*)0 // array buffer offset
			);
			f.glVertexAttribDivisor(LAYOUT, 0); // vertices : always reuse the same vertices -> 0
		}

		LAYOUT = 2;
		{ // normal
			f.glEnableVertexAttribArray(LAYOUT);
			m_normal_buffer.Bind();
			f.glVertexAttribPointer(
				LAYOUT, // attribute. No particular reason for 0, but must match the layout in the shader.
				3, // nr of components
				GL_FLOAT, // type
				GL_FALSE, // normalized?
				0, // stride
				(void*)0 // array buffer offset
			);
			f.glVertexAttribDivisor(LAYOUT, 0); // normals : always reuse the same vertices -> 0
		}

		Q_ASSERT(f.glGetError() == 0);

		LAYOUT = 3;
		{ // orientation
			f.glEnableVertexAttribArray(LAYOUT);
			f.glEnableVertexAttribArray(LAYOUT + 1);
			f.glEnableVertexAttribArray(LAYOUT + 2);
			f.glEnableVertexAttribArray(LAYOUT + 3);
			m_matrix_buffer.Bind();
			f.glVertexAttribPointer(LAYOUT, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(Vector4f), (void*)0);
			f.glVertexAttribPointer(LAYOUT + 1, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(Vector4f), (void*)(sizeof(Vector4f)));
			f.glVertexAttribPointer(LAYOUT + 2, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(Vector4f), (void*)(2 * sizeof(Vector4f)));
			f.glVertexAttribPointer(LAYOUT + 3, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(Vector4f), (void*)(3 * sizeof(Vector4f)));
			f.glVertexAttribDivisor(LAYOUT, 1); // orientation matrix : one per instance -> 1
			f.glVertexAttribDivisor(LAYOUT + 1, 1); //	,,
			f.glVertexAttribDivisor(LAYOUT + 2, 1); //	,,
			f.glVertexAttribDivisor(LAYOUT + 3, 1); //	,,
		}

		Q_ASSERT(f.glGetError() == 0);
	}
	else {
		f.glBindVertexArray(m_vao);
		// draw
	}

	prep_shader(rc);

	f.glDrawElementsInstanced(GL_TRIANGLES, m_buffered_indice_count, GL_UNSIGNED_INT, (void*)0, m_buffered_shape_count);

	f.glBindVertexArray(0);
}

void InstancedDrawer3D::upload_data(RenderContext3D rc)
{
	Q_ASSERT(f.glGetError() == 0);

	QMutexLocker locker(&m_mutex);

	// update matrices
	{
		if (m_matrices.size() != m_orientations.size()) {
			m_matrices.resize(m_orientations.size());
		}
		for (int i = m_matrices.size() - 1; i >= 0; i--) {
			m_matrices[i] = m_orientations[i].GetModelMatrix();
		}
	}

	if (m_require_update)
	{
		m_require_update = false;

		Q_ASSERT(m_orientations.size() == m_colors.size());

		if (m_orientations.size() > 0)
		{
			m_color_buffer.Write(m_colors, GL_STREAM_DRAW);
			m_matrix_buffer.Write(m_matrices, GL_STREAM_DRAW);

			if (m_vertex_buffer.ID() == 0) {
				m_vertex_buffer.Write(m_vertices, GL_STATIC_DRAW);
				m_normal_buffer.Write(m_normals, GL_STATIC_DRAW);
				m_indice_buffer.Write(m_indices, GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER);
			}
		}

		m_buffered_shape_count = m_orientations.size();
		m_buffered_indice_count = m_indices.size();
	}


	Q_ASSERT(f.glGetError() == 0);
}
