#ifndef VISDECLR_H
#define VISDECLR_H

class IRenderable;
class OpenGLMaster;
class ShaderLoader;
class CircleDrawer2D;
class GridDrawer2D;

#endif // VISDECLR_H
