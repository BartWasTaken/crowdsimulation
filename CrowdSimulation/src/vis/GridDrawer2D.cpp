#include "GridDrawer2D.h"
#include <QDebug>
#include <cmath>

GridDrawer2D::GridDrawer2D(OpenGLView &func)
	: m_grid_drawer(func), m_dir_drawer(func), m_arrow_drawer(func), m_func(func)
{
	m_dir_colors.push_back(ColorF{ 1.0f,	0.1f,	0.1f,	1.0f });
	m_dir_colors.push_back(ColorF{ 1.0f,	.77f,	0.1f,	1.0f });
	m_dir_colors.push_back(ColorF{ 0.1f,	1.0f,	.55f,	1.0f });
	m_dir_colors.push_back(ColorF{ .33f,	0.1f,	1.0f,	1.0f });
}

GridDrawer2D::~GridDrawer2D()
{

}

void GridDrawer2D::make_grid(Vector2f orig, Vector2f size, Vector2i count, bool include_block, bool include_dir, bool include_arrow, float split)
{
	m_has_block = include_block;
	m_has_dir = include_dir;
	m_has_arrow = include_arrow;

	auto cell_count = count(0) * count(1);
	m_cell_scale = { size(0) / (float)count(0), size(1) / (float)count(1) };
	std::vector<Vector2f> vertices;

	if (include_block) {
		QMutexLocker locker(m_grid_drawer.mutex_ptr());

		write_shape_square(vertices, split);
		if (!m_grid_drawer.has_vertices()) m_grid_drawer.init(vertices);

		// clears
		m_grid_drawer.orientations().clear();
		m_grid_drawer.colors().clear();
		m_grid_drawer.rotations().clear();

		// allocate:
		m_grid_drawer.orientations().reserve(cell_count);
		m_grid_drawer.colors().resize(cell_count);
		m_grid_drawer.rotations().resize(cell_count);

		// add orientations:
		// devide size by count element-wise

		for (int y = 0; y < count(1); y++) {
			float y_loc = orig(1) + (y * size(1)) / (float)(count(1));
			for (int x = 0; x < count(0); x++) {
				float x_loc = orig(0) + (x * size(0)) / (float)(count(0));
				m_grid_drawer.orientations().push_back({ x_loc, y_loc, m_cell_scale(0), m_cell_scale(1) });
			}
		}

		// fill colors:
		std::fill(m_grid_drawer.colors().begin(), m_grid_drawer.colors().end(), Color{ 255,0,0,128 });
		// fill rotations
		std::fill(m_grid_drawer.rotations().begin(), m_grid_drawer.rotations().end(), Vector4f{ 1,0,0,1 });

		m_dim = count;
		m_grid_drawer.set_changed(true);

	}
	if (include_dir) {
		QMutexLocker locker(m_dir_drawer.mutex_ptr());

		write_shape_dir(vertices);
		if (!m_dir_drawer.has_vertices()) m_dir_drawer.init(vertices);

		// clear
		m_dir_drawer.orientations().clear();
		m_dir_drawer.colors().clear();
		m_dir_drawer.rotations().clear();

		// allocate:
		m_dir_drawer.orientations().reserve(cell_count * 4);
		m_dir_drawer.colors().reserve(cell_count * 4);
		m_dir_drawer.rotations().reserve(cell_count * 4);

		//
		for (int y = 0; y < count(1); y++) {
			float y_loc = orig(1) + (y * size(1)) / (float)(count(1));
			for (int x = 0; x < count(0); x++) {
				float x_loc = orig(0) + (x * size(0)) / (float)(count(0));
				for (int i = 0; i < 4; i++) {
					m_dir_drawer.orientations().push_back({
						x_loc + m_cell_scale(0) / 2, y_loc + m_cell_scale(1) / 2,
						m_cell_scale(0), m_cell_scale(1)
					});
				}
				// ordered North East South West like Dir4::DIR
				m_dir_drawer.colors().push_back(m_dir_colors[0].c());
				m_dir_drawer.colors().push_back(m_dir_colors[1].c());
				m_dir_drawer.colors().push_back(m_dir_colors[2].c());
				m_dir_drawer.colors().push_back(m_dir_colors[3].c());
				m_dir_drawer.rotations().push_back({ 1,0,0,1 });
				m_dir_drawer.rotations().push_back({ 0,1,-1,0 });
				m_dir_drawer.rotations().push_back({ -1,0,0,-1 });
				m_dir_drawer.rotations().push_back({ 0,-1,1,0 });
			}
		}

	}
	if (include_arrow) {
		QMutexLocker locker(m_arrow_drawer.mutex_ptr());

		write_arrow_shape(vertices);
		if (!m_arrow_drawer.has_vertices()) m_arrow_drawer.init(vertices);

		// clear:
		m_arrow_drawer.orientations().clear();
		m_arrow_drawer.colors().clear();
		m_arrow_drawer.rotations().clear();

		// allocate:
		m_arrow_drawer.orientations().reserve(cell_count);
		m_arrow_drawer.colors().resize(cell_count);
		m_arrow_drawer.rotations().resize(cell_count);

		// write
		for (int y = 0; y < count(1); y++) {
			float y_loc = orig(1) + (y * size(1)) / (float)(count(1));
			for (int x = 0; x < count(0); x++) {
				float x_loc = orig(0) + (x * size(0)) / (float)(count(0));
				m_arrow_drawer.orientations().push_back({
					x_loc + m_cell_scale(0) / 2, y_loc + m_cell_scale(1) / 2,
					m_cell_scale(0), m_cell_scale(1)
				});
			}
		}

		// fill colors:
		std::fill(m_arrow_drawer.colors().begin(), m_arrow_drawer.colors().end(), Color{ 255,255,255,200 });
		// fill rotations
		std::fill(m_arrow_drawer.rotations().begin(), m_arrow_drawer.rotations().end(), Vector4f{ 1,0,0,1 });
	}
}

void GridDrawer2D::set_block_color(int x, int y, Color c)
{
	Q_ASSERT(x < m_dim(0) && y < m_dim(1));
	if (m_has_block) m_grid_drawer.colors()[y * m_dim(0) + x] = c;
}

void GridDrawer2D::set_dir_alpha(int x, int y, Dir4::DIR dir, float alpha)
{
	Q_ASSERT(x < m_dim(0) && y < m_dim(1));
	if (m_has_dir) m_dir_drawer.colors()[(y * m_dim(0) + x) * 4 + dir].a = qMin(255, (int)(alpha * 255));
}

void GridDrawer2D::set_dir_color(int x, int y, Dir4::DIR dir, ColorF c)
{
	Q_ASSERT(x < m_dim(0) && y < m_dim(1));
	if (m_has_dir) m_dir_drawer.colors()[(y * m_dim(0) + x) * 4 + dir] = c.c();
}

void GridDrawer2D::set_arrow_dir(int x, int y, Vector2f dir_n, float mag)
{
	if (m_has_arrow) {
		float a = std::atan2(dir_n(1), dir_n(0)) - M_PI / 2;
		float sin = qSin(a);
		float cos = qCos(a);
		Vector4f r = { cos, -sin, sin, cos };
		int id = y * m_dim(0) + x;
		m_arrow_drawer.rotations()[id] = r;
		m_arrow_drawer.orientations()[id](3) = mag * m_cell_scale(1);
		m_arrow_drawer.colors()[id] = get_dir_color(dir_n);
	}
}

void GridDrawer2D::update(bool block /*= true*/, bool dir /*= true*/, bool arrow /*= true*/)
{
	if (m_has_block && block) m_grid_drawer.set_changed();
	if (m_has_dir && dir) m_dir_drawer.set_changed();
	if (m_has_arrow && arrow) m_arrow_drawer.set_changed();
}

void GridDrawer2D::render(RenderContext rc, bool block, bool dir, bool arrow)
{
	if (m_has_block && block) m_grid_drawer.render(rc);
	if (m_has_dir && dir) m_dir_drawer.render(rc);
	if (m_has_arrow && arrow) m_arrow_drawer.render(rc);
}

OpenGLView& GridDrawer2D::get_func()
{
	return m_func;
}

Vector2f GridDrawer2D::position()
{
	return m_grid_drawer.orientation().head<2>();
}

Vector2f GridDrawer2D::scale()
{
	return Vector2f(m_grid_drawer.orientation()(3), 1);
}

void GridDrawer2D::write_shape_dir(std::vector<Vector2f>& out_data)
{
	/*out_data = {
		{ -0.25f, 0.25f },
		{ 0.25f, 0.25f },
		{ 0.0f, 0.5 }
	};*/

	float head_len = 0.333f;
	float head_width = 0.333f;
	out_data = {
		{ 0.0f, 0.0f },
		{ head_width / 2, 0.5f - head_len },
		{ 0, 0.5 },
		{ -head_width / 2, 0.5f - head_len },
	};
}

void GridDrawer2D::write_shape_square(std::vector<Eigen::Vector2f> &out_data, float delta)
{
	out_data = {
		{ delta, delta },
		{ 1 - delta, delta },
		{ 1 - delta, 1 - delta },
		{ delta, 1 - delta }
	};
}

void GridDrawer2D::write_arrow_shape(std::vector<Vector2f>& out_data)
{
	float width = 0.1f;
	float head_len = 0.4f;
	float head_width = 0.25f;
	out_data = {
		{ -width / 2, 0.5f - head_len },
		{ -width / 2, 0.0f },
		{ width / 2, 0.0f },
		{ width / 2, 0.5f - head_len },
		{ head_width / 2, 0.5f - head_len },
		{ 0, 0.5 },
		{ -head_width / 2, 0.5f - head_len},
	};
}

Color GridDrawer2D::get_dir_color(Vector2f vn)
{
	float N = qMax(0.0f, vn(1));
	float E = qMax(0.0f, vn(0));
	float S = qMax(0.0f, -vn(1));
	float W = qMax(0.0f, -vn(0));
	float total = N + E + S + W;

	ColorF result {0,0,0,0};
	result = result.add(m_dir_colors[0].mult(N / total));
	result = result.add(m_dir_colors[1].mult(E / total));
	result = result.add(m_dir_colors[2].mult(S / total));
	result = result.add(m_dir_colors[3].mult(W / total));
	return result.c();
}

