#ifndef MESHGEN_H
#define MESHGEN_H

#include <QtGlobal>
#include <QtMath>

#include <vector>
#include "tools/shared_eigen_incl.h"

namespace MeshGen
{
	template<typename VType>
	struct IndexedMesh {
		VType*		vertices;
		quint32*	indices;

		int			iCount;
		int			vCount;
	};

	template<class VType>
	IndexedMesh<VType> GenSphereIndexed(unsigned int subX)
	{
		IndexedMesh<VType> result;

		quint32 hSegments = subX;
		quint32 vSegments = (int)(hSegments / 2.0f + 0.5f);

		quint32 vCount = (2 + (vSegments - 1) * hSegments);
		quint32 iCount = (2 * hSegments + (vSegments - 2) * hSegments * 2) * 3;

		result.indices = GenSphereIndices(iCount, vCount, hSegments, vSegments);
		result.vertices = GenSphereVertices<VType>(iCount, vCount, hSegments, vSegments);
		result.iCount = iCount;
		result.vCount = vCount;
		return result;
	}

	template<class VType>
	VType* GenSphereVertices(const quint32 iCount, const quint32 vCount, const quint32 hSeg, const quint32 vSeg, VType* in_storage = nullptr);

	extern template Vector3f* GenSphereVertices(
		const quint32 iCount, const quint32 vCount, const quint32 hSeg, const quint32 vSeg, Vector3f* in_storage);

	quint32* GenSphereIndices(const quint32 iCount, const quint32 vCount, const quint32 hSeg, const quint32 vSeg, quint32* in_storage = nullptr);
};

#endif // MESHGEN_H