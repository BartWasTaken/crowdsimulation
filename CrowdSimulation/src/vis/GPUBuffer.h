#pragma once
#include "openglincl.h"
#include <vector>

class GPUBuffer 
{
public:

	GPUBuffer(OpenGLView& func);


	template<typename T>
	void Write(std::vector<T> v, GLenum usage, GLenum target = GL_ARRAY_BUFFER /*GL_ELEMENT_ARRAY_BUFFER*/) {
		int bytes = sizeof(T) * v.size();
		Write(bytes, 0, v.data(), usage, target);
	}

	void		Write(int bytes, int offset, void* data, GLenum usage, GLenum target = GL_ARRAY_BUFFER /*GL_ELEMENT_ARRAY_BUFFER for indices*/);
	void		Bind();
	GLuint		ID() const;
	bool		is_written();
	void		Destroy();

private:
	void		RequireCapacity(int bytesize, GLenum usage, GLenum target);
	void		Build(int bytesize, GLenum usage, GLenum target);
	static bool is_valid_usage(GLenum usage);
	static bool is_valid_target(GLenum target);

	OpenGLView& f;
	int		m_capacity = 0;
	GLuint	m_id = 0;
	GLenum	m_usage = 0;
	GLenum	m_target = 0;
};
