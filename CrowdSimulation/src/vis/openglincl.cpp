#include "openglincl.h"
#include "vis/openglmaster.h"
#include "tools/geomfunc.h"

void MyOpenGLFunc::render_text(float x, float y, Color c, const QString &str, const QFont & font /*= QFont()*/)
{
	//m_pMaster->render_text(x, y, c, str, font);
}

std::vector<std::array<float, 3>> ColorF::color_5 = {
		{ 0,0,1 },
		{ 0,1,1 },
		{ 0,1,0 },
		{ 1,1,0 },
		{ 1,0,0 }
};
std::vector<std::array<float, 3>> ColorF::color_bw = {
		{ 0,0,0 },
		{ 1,1,1 }
};

ColorF ColorF::heatmap(float value, std::vector<std::array<float, 3>>& colors, float multi /*= 1*/)
{
	assert(value == value); // check for NaN
	value = clamp(value, 0.0f, 1.0f);

	int NUM_COLORS = colors.size();

	int range = (int)(multi * NUM_COLORS + 0.5f) - 1;

	float p = value * range; // p might be slightly above range
	float a = p - (int)p;

	int low = (int)(p) % NUM_COLORS; // (int) p is at most range
	int high = qMin(low + 1, range) % NUM_COLORS; // high is at most range

	float c[3]; // output color
	for (int i = 0; i < 3; i++) {
		float c1 = colors[low][i];
		float c2 = colors[high][i];
		c[i] = c1 + a * (c2 - c1);
	}

	return ColorF{ c[0], c[1], c[2], 1 };
}
