#include "IViewPoint.h"


namespace GLBase {
namespace Environment {
namespace Cameras {

IViewPoint::~IViewPoint()
{

}

Matrix4f IViewPoint::MakePerspective(float fovY, float aspect, float zNear, float zFar)
{
	Matrix4f result;
	SetPerspective(fovY, aspect, zNear, zFar, result);
	return result;
}

Matrix4f IViewPoint::MakeLookAt(const Vector3f& position, const Vector3f& target, const Vector3f& up)
{
	Matrix4f result;
	SetLookAt(position, target, up, result);
	return result;
}

void IViewPoint::SetPerspective(float fovY, float aspect, float zNear, float zFar, Matrix4f& out)
{
	float theta = fovY*0.5f;
	float range = zFar - zNear;
	float invtan = 1.0f / tan(theta);

	// out = Matrix4f::Zero(); assume all others are zero
	out(0, 0) = invtan / aspect;
	out(1, 1) = invtan;
	out(2, 2) = -(zNear + zFar) / range;
	out(3, 2) = -1;
	out(2, 3) = -2 * zNear * zFar / range;
	out(3, 3) = 0;
}

void IViewPoint::SetLookAt(const Vector3f& position, const Vector3f& target, const Vector3f& up, Matrix4f& out)
{
	Matrix3f R;
	R.col(2) = (position - target).normalized();
	R.col(0) = up.cross(R.col(2)).normalized();
	R.col(1) = R.col(2).cross(R.col(0));
	out = Matrix4f::Zero();
	out.topLeftCorner<3, 3>() = R.transpose();
	out.topRightCorner<3, 1>() = -R.transpose() * position;
	out(3, 3) = 1.0f;
}

Vector3f IViewPoint::FindScreenRay(float x, float y, IViewPoint& viewPoint)
{
	float proj_x = (2 * x - 1) * tanf(viewPoint.getFOV() / 2.0f) * viewPoint.getRatio();
	float proj_y = (1 - 2 * y) * tanf(viewPoint.getFOV() / 2.0f);
	
	Vector3f camSpaceRay = Vector3f(proj_x, proj_y, -1);
	camSpaceRay.normalize();
	Vector3f worldSpaceRay = (viewPoint.getViewMatrixOrigin().inverse() * Vector4f(camSpaceRay(0), camSpaceRay(1), camSpaceRay(2), 1)).head<3>();
	worldSpaceRay.normalize();

	return worldSpaceRay;
}


Vector3f IViewPoint::FindScreenRay(float x, float y)
{
	return FindScreenRay(x, y, *this);
}

}
}
}