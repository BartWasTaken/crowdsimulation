
#include "StandardViewPoint.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

StandardViewPoint::StandardViewPoint() :
	FOV(1.15f), zNear(0.01f), zFar(6000.0f), ratio(1), eye(0,0,1), target(0,0,0), up(0,1,0)
{

}

void StandardViewPoint::Update()
{
	CacheFrame();
}

Vector3f StandardViewPoint::getEye()
{
	return eye;
}

Vector3f StandardViewPoint::getTarget()
{
	return target;
}

Vector3f StandardViewPoint::getUp()
{
	return up;
}

float StandardViewPoint::getFOV()
{
	return FOV;
}

float StandardViewPoint::getZNear()
{
	return zNear;
}

float StandardViewPoint::getZFar()
{
	return zFar;
}

float StandardViewPoint::getRatio()
{
	return ratio;
}

void StandardViewPoint::setFov(float f)
{
	FOV = f;
}

void StandardViewPoint::setzFar(float f)
{
	zFar = f;
}

void StandardViewPoint::setzNear(float f)
{
	zNear = f;
}

void StandardViewPoint::setRatio(float r)
{
	ratio = r;
}

Matrix4f StandardViewPoint::getVPMatrix()
{
	return vpMatrix;
}

Matrix4f StandardViewPoint::getVPMatrixOrigin()
{
	return projMatrix * CalcViewMatrixOrigin();
}

Matrix4f StandardViewPoint::getPPMatrix(Tools::Surface2D<int> surface)
{
	return CalcPixelProjMatrix(surface);
}

Matrix4f StandardViewPoint::getProjMatrix()
{
	return projMatrix;
}

Matrix4f StandardViewPoint::getViewMatrix()
{
	return viewMatrix;
}

Matrix4f StandardViewPoint::getViewMatrixOrigin()
{
	return CalcViewMatrixOrigin();
}

Matrix4f StandardViewPoint::getViewMatrixOriginInvZ()
{
	return MakeLookAt(Vector3f(0, 0, 0), eye - target, up);
}

void StandardViewPoint::setUP(Vector3f v)
{
	up = v;
}

void StandardViewPoint::setTarget(Vector3f v)
{
	target = v;
}

void StandardViewPoint::setEye(Vector3f v)
{
	eye = v;
}

Matrix4f StandardViewPoint::CalcViewMatrixOrigin()
{
	return MakeLookAt(Vector3f(0, 0, 0), target - eye, up);
}

Matrix4f StandardViewPoint::CalcPixelProjMatrix(Tools::Surface2D<int> surface)
{
	Matrix4f P = projMatrix;
	float sx = surface.width / 2.0f;
	float sy = surface.height / 2.0f;

	Matrix4f result;
	result << sx, 0, 0, sx + 0,
		0, -sy, 0, sy + 0,
		0, 0, 1, 0,
		0, 0, 0, 1; // maybe transpose for eigen? untested, probably ok

	return result * P;
}

Vector3f StandardViewPoint::getTargetDir()
{
	return targetDirFrame;
}

Vector3f StandardViewPoint::getRealUpFrame()
{
	return realUpVecFrame;
}

Vector3f StandardViewPoint::getSideDirRightHand()
{
	return rightHandSideDir;
}

void StandardViewPoint::CacheFrame()
{
	SetLookAt(eye, target, up, viewMatrix);
	SetPerspective(FOV, ratio, zNear, zFar, projMatrix);
	vpMatrix = projMatrix * viewMatrix;

	targetDirFrame = (target - eye).normalized();
	rightHandSideDir = targetDirFrame.cross(up).normalized();
	realUpVecFrame = rightHandSideDir.cross(targetDirFrame).normalized();
}

void StandardViewPoint::UpdateProj()
{
	SetPerspective(FOV, ratio, zNear, zFar, projMatrix);
}

}
}
}