#pragma once
#include "Tools/include.h"
#include "IViewPoint.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

class StandardViewPoint :
	public IViewPoint
{
public:
	StandardViewPoint();

	virtual void Update();

	Vector3f getEye() final override;
	Vector3f getTarget() final override;
	Vector3f getUp() final override;

	float getFOV() final override;
	float getZNear() final override;
	float getZFar() final override;
	float getRatio() final override;
	void setFov(float) final override;
	void setzFar(float) final override;
	void setzNear(float)final override;
	void setRatio(float) final override;

	Matrix4f getVPMatrix() final override;
	Matrix4f getVPMatrixOrigin() final override;
	Matrix4f getPPMatrix(Tools::Surface2D<int>) final override;
	Matrix4f getProjMatrix() final override;
	Matrix4f getViewMatrix() final override;
	Matrix4f getViewMatrixOrigin() final override;
	Matrix4f getViewMatrixOriginInvZ() final override;

	// Specific for this type of viewpoint

	void setUP(Vector3f v);
	void setTarget(Vector3f v);
	void setEye(Vector3f v);

	Matrix4f CalcViewMatrixOrigin();
	Matrix4f CalcPixelProjMatrix(Tools::Surface2D<int>);


	virtual Vector3f getTargetDir() override;


	virtual Vector3f getRealUpFrame() override;


	virtual Vector3f getSideDirRightHand() override;

private:
	void CacheFrame();
	void UpdateProj();

	// state description:

	float FOV;
	float zNear;
	float zFar;
	float ratio;

	Vector3f eye;
	Vector3f target;
	Vector3f up;

	// cached on a per-frame-basis
	Matrix4f viewMatrix = Matrix4f::Zero();
	Matrix4f projMatrix = Matrix4f::Zero();
	Matrix4f vpMatrix = Matrix4f::Zero();
	Vector3f targetDirFrame;
	Vector3f realUpVecFrame;
	Vector3f rightHandSideDir;
};

}
}
}