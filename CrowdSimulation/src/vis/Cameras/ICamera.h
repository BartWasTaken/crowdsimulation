#pragma once
#include "IViewPoint.h"
#include <vector>

namespace GLBase {
namespace Environment {
namespace Cameras {

/*
	The ICamera can provide mutliple views, to support 3D-stereo and 360-composing
*/
class ICamera 
{
public:
	virtual std::vector<IViewPoint*>& GetViewPoints() = 0;
	virtual void Update(float dt) = 0;
};

}
}
}