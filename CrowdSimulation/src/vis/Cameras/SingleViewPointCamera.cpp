
#include "SingleViewPointCamera.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

void SingleViewPointCamera::Update(float dt)
{
	UpdateDerived(dt);
	StandardViewPoint::Update();
}

void SingleViewPointCamera::Update()
{
	StandardViewPoint::Update();
}

std::vector<GLBase::Environment::Cameras::IViewPoint*>& SingleViewPointCamera::GetViewPoints()
{
	return gViewpoints;
}

SingleViewPointCamera::~SingleViewPointCamera()
{

}

}
}
}