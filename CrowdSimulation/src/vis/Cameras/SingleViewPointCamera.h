#pragma once

#include "Tools/include.h"
#include "ICamera.h"
#include "StandardViewPoint.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

class SingleViewPointCamera :
	public ICamera,
	public StandardViewPoint
{
public:
	virtual SingleViewPointCamera::~SingleViewPointCamera();

	virtual void Update(float dt) final;

	virtual std::vector<IViewPoint*>& GetViewPoints() final override;

private:
	std::vector<GLBase::Environment::Cameras::IViewPoint*> gViewpoints { this };

	virtual void UpdateDerived(float dt) = 0;

	void Update(); // hide function call to viewpoint update
};

class CamUser
{
public:
	ICamera* camera;

	virtual void UpdateForFrame() {}

	void ChangeCam(ICamera* newC) { camera = newC; }
};

}
}
}