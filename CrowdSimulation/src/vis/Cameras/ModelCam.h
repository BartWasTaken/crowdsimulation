#pragma once
#include "tools/include.h"
#include "SingleViewPointCamera.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

class ModelCam :
	public SingleViewPointCamera,
	public InputListener,
	public ResizedListener
{
public:
	ModelCam(InputManager*, ISurface*);
	~ModelCam();

	virtual MouseEventReturnValue MousePress(MouseEventInfo) override;
	virtual MouseEventReturnValue MouseMove(MouseEventInfo) override;
	virtual MouseEventReturnValue MouseRelease(MouseEventInfo) override;
	virtual MouseEventReturnValue MouseScroll(MouseEventInfo) override;

	virtual void UpdateDerived(float) override;
	virtual void object_resized(ISurface* pCaller, Tools::Surface2D<int> newSize) override;
	
	void set_center(Vector3f);

private:
	const float angleVMin;
	const float angleVmax;
	float angleH;
	float angleV;
	float radius;

	float key_rotational_speed;
	float mouse_rotational_speed;

	Vector3f targetToEye;
	Vector3f targetDir2d;
	Vector3f axisRight;

	Vector3f scene_center;

	void UpdateEye();
	void UpdateEyeFull();
};

}
}
}