#pragma once
#include "tools/include.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

class IViewPoint {
public:
	virtual ~IViewPoint();

	// orientation
	virtual Vector3f getEye() = 0; // location of the eye
	virtual Vector3f getTarget() = 0; // location of target (could just be a direction)
	virtual Vector3f getTargetDir() = 0; // normalized target
	virtual Vector3f getUp() = 0; // usually just (0,1,0), independant of target
	virtual Vector3f getRealUpFrame() = 0; // real up vector, changes when u change vertical look direction
	virtual Vector3f getSideDirRightHand() = 0; // normalized right hand side

	// settings
	virtual float getFOV() = 0;
	virtual float getZNear() = 0;
	virtual float getZFar() = 0;
	virtual float getRatio() = 0;
	virtual void setFov(float) = 0;
	virtual void setzFar(float) = 0;
	virtual void setzNear(float) = 0;
	virtual void setRatio(float) = 0;

	// matrices
	virtual Matrix4f getVPMatrix() = 0;
	virtual Matrix4f getVPMatrixOrigin() = 0;
	virtual Matrix4f getPPMatrix(Tools::Surface2D<int>) = 0;
	virtual Matrix4f getProjMatrix() = 0;
	virtual Matrix4f getViewMatrix() = 0;
	virtual Matrix4f getViewMatrixOrigin() = 0;
	virtual Matrix4f getViewMatrixOriginInvZ() = 0;

	// matrix generation
	static Matrix4f MakePerspective(float fovY, float aspect, float zNear, float zFar);
	static Matrix4f MakeLookAt(const Vector3f& position, const Vector3f& target, const Vector3f& up);
	static void SetPerspective(float fovY, float aspect, float zNear, float zFar, Matrix4f& out);
	static void SetLookAt(const Vector3f& position, const Vector3f& target, const Vector3f& up, Matrix4f& out);
	
	// maps a screen position to a view ray
	// use for cpu based rayctracing
	// x and y between 0 and 1
	// top left 00, bottom right 11
	static Vector3f FindScreenRay(float x, float y, IViewPoint& camState);
	Vector3f FindScreenRay(float x, float y);
};

}
}
}