#include "ModelCam.h"

namespace GLBase {
namespace Environment {
namespace Cameras {

void ModelCam::UpdateEye()
{
	StandardViewPoint::setEye(getTarget() + targetToEye);
}

void ModelCam::UpdateEyeFull()
{
	// assumes
	//	Y
	// 	|
	// 	|___X
	//  /
	// Z
	targetToEye = Vector3f(
		radius * sin(angleV) * cos(angleH),
		radius * cos(angleV),
		radius * sin(angleV) * sin(angleH)
	);
	targetDir2d = Vector3f(-targetToEye(0), 0, -targetToEye(2)).normalized();
	axisRight = targetDir2d.cross(getUp()).normalized();
	UpdateEye();
}

ModelCam::ModelCam(InputManager* IM, ISurface* glw)
	: SingleViewPointCamera(),
	angleH(3.14f * 2.60f), angleV(1.35f), radius(9),
	key_rotational_speed(1.5f), mouse_rotational_speed(1 / 80.0f),
	angleVMin(0.001f), angleVmax((float)M_PI - 0.10f), scene_center{ 0,0,0 }
{
	UpdateEyeFull();
	IM->AddInputListener(this);
	glw->add_resized_listener(this);
	object_resized(glw, glw->get_size());
}


ModelCam::~ModelCam()
{
	
}

MouseEventReturnValue ModelCam::MousePress(MouseEventInfo e)
{
	if (InputListener::GetAnyManager()->HasDefaultState()) {
		InputListener::StartDrag(e.x, e.y);
		return { InputControl::Request::Take_Keep_All() };
	}
	else return {};
}

MouseEventReturnValue ModelCam::MouseMove(MouseEventInfo e)
{
	if (InputListener::HasDrag()) {
		auto dragProg = InputListener::GetDragIncr(e.x, e.y);

		angleV = clamp(angleV - dragProg.y * mouse_rotational_speed, angleVMin, angleVmax);
		angleH = angleH + dragProg.x * mouse_rotational_speed;

		UpdateEyeFull();
	}
	return {};
}

MouseEventReturnValue ModelCam::MouseRelease(MouseEventInfo e)
{
	InputListener::EndDrag();
	return { InputControl::Request::ReleaseAll() };
}

MouseEventReturnValue ModelCam::MouseScroll(MouseEventInfo e)
{
	qInfo() << "CAM SCROLL" << e.delta;
	InputManager::AState s = InputManager::AState::Default;
	s.Control = InputManager::AStateType::DOWN;

	if (InputListener::GetAnyManager()->HasState(s)) // IsKeyDown(System::InputManager::KeyCodes::Ctrl)
	{
		setFov(getFOV() - 0.0003f * e.delta);
	}
	else {
		float zoom = 1.0f - clamp(e.delta / 2000.0f, -0.3f, 0.3f);
		//std::cout << "delta: " << delta << "zoom: " << zoom << "\n";
		radius = clamp(radius * zoom, 0.05f, 2000.0f);
		UpdateEyeFull();
	}
	//std::cout << "zfar : " << zFar << "\n";

	return {};
}

void ModelCam::UpdateDerived(float dt)
{
	bool sNull = InputListener::GetAnyManager()->HasDefaultState();

	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_W))
	{
		setTarget(getTarget() + 2.0f * targetDir2d * dt * sqrt(radius));
		UpdateEye();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_S))
	{
		setTarget(getTarget() - 2.0f * targetDir2d * dt * sqrt(radius));
		UpdateEye();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_A))
	{
		setTarget(getTarget() - axisRight * dt * sqrt(radius));
		UpdateEye();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_D))
	{
		setTarget(getTarget() + axisRight * dt * sqrt(radius));
		UpdateEye();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_Q))
	{
		angleH += key_rotational_speed * dt;
		UpdateEyeFull();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_E))
	{
		angleH -= key_rotational_speed * dt;
		UpdateEyeFull();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_Z))
	{
		setTarget(getTarget() - getUp() * dt * sqrt(radius));
		UpdateEye();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_X))
	{
		setTarget(getTarget() + getUp() * dt * sqrt(radius));
		UpdateEye();
	}
	if (InputListener::GetAnyManager()->IsKeyDown(Qt::Key::Key_C))
	{
		setTarget(scene_center);
		UpdateEye();
	}

}

void ModelCam::object_resized(ISurface* pCaller, Tools::Surface2D<int> newSize)
{
	setRatio((float)newSize.width / newSize.height);
}

void ModelCam::set_center(Vector3f c)
{
	scene_center = c;
}

}
}
}



