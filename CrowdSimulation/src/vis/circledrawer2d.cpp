#include "circledrawer2d.h"
#include <QtMath>
#include <random>
#include "vis/openglincl.h"

CircleDrawer2D::CircleDrawer2D(OpenGLView &f)
	: m_drawer(f)
{
	std::vector<Vector2f> vertices;
	write_shape(vertices);
	m_drawer.init(vertices);
}

void CircleDrawer2D::add(Color c, Eigen::Vector2f loc, float scale)
{
	QMutexLocker locker(m_drawer.mutex_ptr());
	m_drawer.orientations().push_back(Vector4f{ loc(0), loc(1), scale, scale });
	m_drawer.colors().push_back(c);
	m_drawer.rotations().push_back(Vector4f{ 1, 0, 0, 1 });
	m_drawer.set_changed(true);
}

void CircleDrawer2D::move(int i, Vector2f loc)
{
	m_drawer.orientations()[i].head<2>() = loc;
}

void CircleDrawer2D::pop()
{
	QMutexLocker locker(m_drawer.mutex_ptr());
	int new_size = count() - 1;
	m_drawer.orientations().resize(new_size);
	m_drawer.colors().resize(new_size);
	m_drawer.rotations().resize(new_size);
	m_drawer.set_changed(true);
}

void CircleDrawer2D::change_color(int i, Color c, bool change_alpha)
{
	Q_ASSERT(i < m_drawer.colors().size());
	if (change_alpha) m_drawer.colors()[i] = c;
	else m_drawer.colors()[i].set_rgb(c);
}

void CircleDrawer2D::update()
{
	m_drawer.set_changed();
}

int CircleDrawer2D::count()
{
	return m_drawer.orientations().size();
}

void CircleDrawer2D::clear()
{
	m_drawer.orientations().clear();
	m_drawer.colors().clear();
	m_drawer.rotations().clear();
}

void CircleDrawer2D::render(RenderContext rc)
{
	m_drawer.render(rc);
}

Color CircleDrawer2D::get_color(int i)
{
	return m_drawer.colors()[i];
}

void CircleDrawer2D::colorize(IColorizer& c)
{
	int n = m_drawer.colors().size();
	for (int i = 0; i < n; i++) {
		m_drawer.colors()[i] = c.color(i);
	}
	m_drawer.set_changed();
}

void CircleDrawer2D::write_shape(std::vector<Eigen::Vector2f> &out_data)
{
	int circle_point_count = 9; // best performance tradeoff somewhere around 3,4,5. Higher number gives more vertices but less fragments.
	out_data.resize(circle_point_count);
	for (int i = 0; i < circle_point_count; i++) {
		float p = i / (float)circle_point_count;
		float x = cosf(p * 2 * M_PI);
		float y = sinf(p * 2 * M_PI);
		out_data[i] = { x, y };
	}
	// Make the circle an outer circle on radius 1. (eg, a n-gon with no lines inside radius 1)
	// take two neighbouring vertices
	// go halfway
	// compute length
	// devide all points by this length to get an " outer " circle
	float l = ((out_data[0] + out_data[1]) / 2).norm();
	for (int i = 0; i < circle_point_count; i++) {
		out_data[i] /= l;
	}
}

LineDrawer2D::LineDrawer2D(OpenGLView& func) : m_drawer(func, GL_LINES) // GL_LINES
{
	std::vector<Vector2f> vertices;
	write_shape(vertices);
	m_drawer.init(vertices);
}

void LineDrawer2D::clear()
{
	QMutexLocker locker(m_drawer.mutex_ptr());
	m_drawer.colors().clear();
	m_drawer.rotations().clear();
	m_drawer.orientations().clear();
}

void LineDrawer2D::add(Vector2f a, Vector2f b, Color c)
{
	QMutexLocker locker(m_drawer.mutex_ptr());
	Vector2f diff = b - a;
	float len = diff.norm();
	if (len > 0) {
		m_drawer.colors().push_back(c);
		m_drawer.orientations().push_back(Vector4f{ a(0),a(1),len,len });
		m_drawer.rotations().push_back(rotation_matrix_align({ 0,1 }, diff / len));
	}
}

void LineDrawer2D::update()
{
	m_drawer.set_changed();
}

void LineDrawer2D::render(RenderContext rc)
{
	m_drawer.func().glLineWidth(2);
	m_drawer.render(rc);
}

void LineDrawer2D::write_shape(std::vector<Eigen::Vector2f>& out_data)
{
	out_data.clear();
	out_data = {
		Vector2f{ 0, 0 }, Vector2f{ 0, 1 },
	};
}
