#ifndef OPENGLINCL_H
#define OPENGLINCL_H

#include <QOpenGLFunctions_4_3_Core>
//#include <QOpenGLFunctions_2_1>
#include <QFont>
#include "visdeclr.h"
#include "tools/include.h"
#include "vis/Cameras/IViewPoint.h"
#include <vector>
#include <array>
#include <assert.h>
#include <cmath>

struct ColorF;
struct Color {
	unsigned char r = 0, g = 0, b = 0, a = 255;
	inline ColorF cf();
	inline void set_rgb(Color c) {
		r = c.r;
		g = c.g;
		b = c.b;
	}
	inline Vector4uchar as_vector_rgba() {
		return { r,g,b,a };
	}
	inline Vector3uchar as_vector_rgb() {
		return { r,g,b };
	}
};

struct ColorF {
private:
	inline static uchar to_255(float f) {
		return (uchar)qMin((int)(f * 255 + 0.5f), 255);
	}
public:
	float r = 0, g = 0, b = 0, a = 0;

	inline Color c() { return Color{ to_255(r), to_255(g), to_255(b), to_255(a) }; }
	inline Vector4f as_vector_rgba() { return { r,g,b,a }; }
	inline Vector3f as_vector_rgb() { return { r,g,b }; }
	inline ColorF add(ColorF c) {
		return ColorF{ r + c.r, g + c.g, b + c.b, a + c.a };
	}
	inline ColorF mult(float s) {
		return ColorF{ r*s,g*s,b*s,a*s };
	}

	static std::vector<std::array<float, 3>> color_5;
	static std::vector<std::array<float, 3>> color_bw;

	static ColorF heatmap(float value, std::vector<std::array<float, 3>>& colors, float multi = 1);
};

ColorF Color::cf()
{
	return ColorF{ r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f };
}

class MyOpenGLFunc
	: public QOpenGLFunctions_4_3_Core, Tools::INonCopyable
{
public:
	inline MyOpenGLFunc() {}

	inline ShaderLoader& shader_loader() { return *m_pSL; }
	inline OpenGLMaster& opengl_master() { return *m_pMaster; }

	inline void set_shader_loader(ShaderLoader* pSL) { m_pSL = pSL; }
	inline void set_opengl_master(OpenGLMaster* pMaster) { m_pMaster = pMaster; }

	inline void glxBlendFuncBasic() { glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); }
	inline void glxBlendFuncAdd() { glBlendFunc(GL_SRC_ALPHA, GL_ONE); }
	inline void glxBlendFuncMultiply() { glBlendFunc(GL_DST_COLOR, GL_SRC_COLOR); };

	void render_text(float x, float y, Color c, const QString &str, const QFont & font = QFont("Courier New"));

private:
	ShaderLoader* m_pSL = nullptr;
	OpenGLMaster* m_pMaster = nullptr;
};

using OpenGLView = MyOpenGLFunc;

struct Orient2D {
	Vector2f pos{ 0,0 };
	float scale = 1;
	float ratio = 1;
	Vector4f to_4_orientation() {
		return { pos(0), pos(1), scale, ratio };
	}
};

struct RenderContext {
	OpenGLView& f;
	Orient2D orient_2d;
	float dt;
};

struct RenderContext3D {
	OpenGLView& f;
	GLBase::Environment::Cameras::IViewPoint* pViewpoint;
	float dt;
};

class IColorizer {
public:
	virtual Color color(int i) = 0;
};


#endif // OPENGLINCL_H
