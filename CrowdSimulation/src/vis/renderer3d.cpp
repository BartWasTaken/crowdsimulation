#include "renderer3d.h"
#include "qpolygon.h"
#include "src/sim/obstacle.h"
#include "src/sim/simulationcore.h"
#include "src/sim/IEnvironment.h"

#include "src/vis/openglmaster.h"
#include "src/tools/convexhull.h"
#include <bitset>
#include <map>
#include "src/external/earcut.hpp"
#include <clipper.hpp>
#include "meshgen.h"
#include <QTemporaryDir>
#include "files/ObjLoader.h"

Renderer3D::Renderer3D(OpenGLView& opengl, SimulationCore& sim)
	: f(opengl), sc(sim)
{
	m_pCamera = new GLBase::Environment::Cameras::ModelCam(opengl.opengl_master().get_inputmanager(), &opengl.opengl_master());
	m_pCamera->Update(0);
	enable(false);

	{
		QTemporaryDir temp_dir;
		QString human1_path = "/models/human1.obj";
		QString human1_qpath = ":" + human1_path;
		QString human1_target_path = temp_dir.path() + human1_path;
		std::string std_path = human1_target_path.toUtf8().constData();
		
		QDir().mkpath(QString::fromStdString(Tools::SplitFullPath(std_path, true).path));
		QFile::copy(human1_qpath, human1_target_path);

		CGMain::IO::LoadDescription descr;
		descr.full_path = std_path;
		descr.hint_print_info = CGMain::IO::LoadDescription::PrintInfo::Detail;
		descr.norm_load = CGMain::IO::NormalLoadFlagBits::SMOOTH;
		std::vector<CGMain::IVertexData*> load_data = CGMain::IO::LoaderTypes::ObjLoader::LoadObject(descr);
		
		for (auto* ptr : load_data) {
			m_instanced_drawers.push_back(new InstancedDrawer3D{f});
			m_instanced_drawers.back()->set_shape(ptr);
		}
		if (load_data.size() > 0) {
			AABB3 bbox = load_data[0]->GetAABB();
			for (int i = (int) load_data.size() - 1; i > 0; i--) {
				bbox.extend(load_data[i]->GetAABB());
			}
			float object_rad_horizontal = qMax(bbox.sizes()(0), bbox.sizes()(2));
			m_object_scale_modifier = 1.5f / object_rad_horizontal; // specific to the object we chose
		}
	}

	//auto data = MeshGen::GenSphereIndexed<Vector3f>(8);
	//m_instanced_drawer.set_shape(data);
}

void Renderer3D::reset()
{
	m_rebuild = true;
}

void Renderer3D::update(IColorizer& c)
{
	update_agents(c);
}

void Renderer3D::render(RenderContext rc)
{
	Q_ASSERT(f.glGetError() == 0);

	m_pCamera->Update(rc.dt);

	if (m_rebuild) {
		m_rebuild = false;
		build_scene();
	}

	f.glEnable(GL_CULL_FACE);
	f.glEnable(GL_DEPTH_TEST);

	RenderContext3D rc3d{ rc.f, m_pCamera->GetViewPoints()[0], rc.dt };

	// agents:
	for (auto& drawer : m_instanced_drawers) {
		drawer->render(rc3d);
	}

	f.glEnable(GL_BLEND);
	f.glDisable(GL_CULL_FACE);
	f.glxBlendFuncBasic();
	f.glDepthMask(GL_FALSE);

	// environment:
	for (auto& poly : m_polygons) {
		poly.render(rc3d);
	}

	f.glDepthMask(GL_TRUE);
	f.glDisable(GL_BLEND);
	f.glDisable(GL_DEPTH_TEST);
	//f.glEnable(GL_CULL_FACE);

	Q_ASSERT(f.glGetError() == 0);
}

void Renderer3D::enable(bool state)
{
	m_pCamera->enable_input(state);
	m_enabled = state;
}

void Renderer3D::set_center(Vector3f c)
{
	m_pCamera->set_center(c);
}

class HoledPolygon {
public:
	void set_outline(std::vector<Vector2f> poly) {
		if (data.size() == 0) data.push_back(poly);
		else data[0] = poly;
	}
	void add_hole(std::vector<Vector2f> poly) {
		if (data.size() == 0) data.push_back({});
		data.push_back(poly);
	}
private:
	std::vector<std::vector<Vector2f>> data;
};

struct PolygonData {
	std::vector<Vector2f> vertices;
	std::vector<GLuint> indices;
};

class PolyPart {
public:
	PolyPart(std::vector<Vector2f>& data) { seperate_holes(data); }
	std::vector<Vector2f> outline;
	std::vector<PolyPart> children;

	void make_polygons(std::vector<PolygonData>& result, bool is_solid = true)
	{
		if (is_solid) {
			std::vector<std::vector<Vector2f>> earcut_input;
			std::vector<Vector2f> vertices = outline;

			earcut_input.push_back(outline);

			for (auto& child : children) {
				earcut_input.push_back(child.outline);
				vertices.insert(vertices.end(), child.outline.begin(), child.outline.end());
				child.make_polygons(result, false);
			}

			std::vector<GLuint> indices = mapbox::earcut<GLuint>(earcut_input);

			result.push_back({ vertices, indices });
		}
		else {
			for (auto& child : children) {
				child.make_polygons(result, true);
			}
		}
	}

private:

	void seperate_holes(std::vector<Vector2f>& data)
	{
		// for each edge, find if there is a mirrored version, meaning there is a hole
		// all points between those edges is a different part
		// do this recursively?

		{
		BEGIN_LOOP:
			int n = (int) data.size();

			for (int i = 0; i < n; i++)
			{
				for (int j = i + 1; j < n; j++)
				{
					if (data[i] == data[j])
					{
						std::vector<Vector2f> inner, outer;

						if (i == 0) {
							int k;
							for (k = i; k < j; k++) outer.push_back(data[k]);
							for (k = j + 1; k < n && data[k] != data[i]; k++) inner.push_back(data[k]);
							for (; k < n; k++) outer.push_back(data[k]);
						}
						else {
							int k;
							for (k = 0; k <= i; k++) outer.push_back(data[k]);
							for (k = i + 1; k < j; k++) { inner.push_back(data[k]); Q_ASSERT(data[k] != data[i]); }
							for (k = j + 1; k < n; k++) outer.push_back(data[k]);
						}

						children.push_back({ inner });
						data = outer;

						goto BEGIN_LOOP;
					}
				}
			}


		}

		outline = data;
	}
};

void Renderer3D::build_scene()
{
	int poly_id = 0;

	// for each elevation, find the corresponding obstacles

	auto& ordered_elevations = sc.environment().elevation_areas();
	struct MinComparer {
		bool operator()(const ElevationArea& a, const ElevationArea& b) const {
			return a.height < b.height;
		}
	} min_cmp;
	std::sort(ordered_elevations.begin(), ordered_elevations.end(), min_cmp);

	// first entry is for elevation 0 which doesnt have a specified area
	std::vector<std::vector<Obstacle*>> obstacles_per_level;
	obstacles_per_level.resize(ordered_elevations.size() + 1);


	for (auto& obstacle : sc.environment().obstacles())
	{
		int i = 1; // start at 1, since 0 is for ground 0, which does not have a specific elevation
		int found_id = 0;
		for (auto& elevation : ordered_elevations) {
			if (elevation.polygon.contains(obstacle.base())) {
				found_id = i;
				break;
			}
			i++;
		}
		obstacles_per_level[found_id].push_back(&obstacle);
	}

	// we have obstacles grouped per elevation now
	// make outline from obstacles
	// make floor inside the outline
	// substract obstacles and stairs from the outline // QPolygon::subtracted

	int level_count = (int) obstacles_per_level.size();

	for (int i = 0; i < level_count; i++) {
		float cur_height = (i == 0) ? 0 : ordered_elevations[i - 1].height;
		Vector2f translation = (i == 0) ? Vector2f{ 0, 0 } : ordered_elevations[i - 1].translation;

		auto& obstacles = obstacles_per_level[i];

		std::vector<Vector2f> points;
		for (auto& o : obstacles) {
			for (auto v : o->base().vertices()) {
				points.push_back(Vector2f{ v(0), v(1) });
			}
		}

		//auto aabb = sc.environment().bbox();
		//points.push_back(aabb.corner(AABB2::BottomLeft));
		//points.push_back(aabb.corner(AABB2::TopLeft));
		//points.push_back(aabb.corner(AABB2::TopRight));
		//points.push_back(aabb.corner(AABB2::BottomRight));

		auto hull_points = ConvexHull::compute(points);
		auto hull_polygon = QPolygonF();

		for (auto p : hull_points) hull_polygon.append({ p[0], p[1] });

		//m_polygons.back().build(..., x);

		//earcut_input.push_back({}); // first one is to be the floor

		// now substract all the obstacles and staircases from the hull

		QPolygonF floor = hull_polygon;

		for (auto& o : obstacles) {
			QPolygonF hole = o->base().toQPolygon();
			floor = floor.subtracted(hole);
		}
		for (auto& stairs : sc.environment().staircases()) {
			if (stairs.base_height() == cur_height) {
				floor = floor.subtracted(stairs.base_polygon().toQPolygon());
			}
		}

		if (floor.size() > 0) {

			// remove repeats vertice at end
			if (floor.first() == floor.last()) floor.removeLast();

			// created triangulation

			// convert to Vector2f
			std::vector<Vector2f> floor_v2;
			floor_v2.reserve(floor.size());
			for (auto v : floor) floor_v2.push_back({ (float)v.x(), (float)v.y() });

			std::vector<PolygonData> polygons_data;
			PolyPart root(floor_v2);
			root.make_polygons(polygons_data);

			// make 3d polygons
			for (auto& data : polygons_data) {
				std::vector<Vector3f> vertices3d;
				for (auto v : data.vertices) {
					// apply translation to have floors above eachother
					v += translation;
					vertices3d.push_back(to_3d(v, cur_height)); // { v(0), cur_height, -v(1) });
				}
				if (m_polygons.size() <= poly_id) m_polygons.push_back({ f });

				m_polygons[poly_id++].build(vertices3d, data.indices);
			}
		}
	}

	// add staircases
	for (auto& stairs : sc.environment().staircases())
	{
		if (stairs.has_upwards()) {
			auto vertices2d = stairs.base_polygon().vertices();
			std::vector<std::vector<Vector2f>> shapes{ vertices2d };
			// do triangulation
			std::vector<GLuint> indices = mapbox::earcut<GLuint>(shapes);

			// offset based on elevation area

			// only if it doesnt match any of the levels, do we consider the default height 0 level
			Vector2f translation;
			for (int i = level_count - 1; i >= 0; i--) {
				float cur_height = (i == 0) ? 0 : ordered_elevations[i - 1].height;
				if (cur_height == stairs.base_height()) {
					translation = (i == 0) ? Vector2f{0, 0} : ordered_elevations[i - 1].translation;
					break;
				}
			}

			// make 3d
			std::vector<Vector3f> vertices;
			for (auto v2d : vertices2d) {
				// first evaluate height in un-translated space
				auto h = stairs.get_height_at(v2d);
				// then translate the vector
				v2d += translation;
				vertices.emplace_back(v2d(0), h, -v2d(1));
			}

			// add polygon to visualization
			if (m_polygons.size() <= poly_id) m_polygons.push_back({ f });
			m_polygons[poly_id++].build(vertices, indices);
		}
	}


	// clean up: remove [length - highest used index] items from the back
	for (int i = (int) m_polygons.size() - 1; i >= poly_id; i--) {
		m_polygons[i].destroy();
	}
	for (int i = (int) m_polygons.size() - 1 - poly_id; i >= 0; i--) {
		m_polygons.pop_back();
	}
}

void Renderer3D::update_agents(IColorizer& c)
{
	int n = sc.nr_active_agents();
	for(auto& d : m_instanced_drawers) d->set_amount(n);

	float rad = sc.settings().agent_rad();

	for (int i = 0; i < n; i++) {
		Vector3f pos = sc.pos_3d(i);
		
		Vector3f rotation = { 0, Angle2D(Vector2f{ 0,-1 }, sc.velocity(i).normalized()), 0 }; // { 0, 0, 0, };
		for (auto& d : m_instanced_drawers) {
			d->change(i, pos, rotation, m_object_scale_modifier * Vector3f{ rad, rad, rad }, c.color(i));
		}
	}

	for(auto& d : m_instanced_drawers) d->set_changed();
}

/*std::vector<std::vector<Point>> earcut_input;

// first polygon is the main polygon
earcut_input.push_back(hull);

// ferther polygons are the holes
for (auto& o : obstacles) {
auto x = o->inner().convertTo<Point>();
earcut_input.push_back(x);
}
for (auto& stairs : sc.environment().staircases()) {
if (stairs.base_height() == cur_height) {
earcut_input.push_back(stairs.polygon().convertTo<Point>());
}
}

auto x = mapbox::earcut<GLuint>(earcut_input);
optimise(earcut_input, x);

// make 3d polygon


std::vector<Vector3f> points3d;
for (auto v : earcut_input[0]) {
points3d.push_back({v(0), cur_height, v(1)});
}

m_polygons.push_back({ f });
m_polygons.back().build(points3d, x);
*/
