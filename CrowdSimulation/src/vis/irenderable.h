#ifndef IRENDERABLE_H
#define IRENDERABLE_H

#include "openglincl.h"

class IRenderable
{
public:
	virtual void render(RenderContext) = 0;
};

#endif // RENDERABLE_H
