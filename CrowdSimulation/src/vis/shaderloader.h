#ifndef SHADERLOADER_H
#define SHADERLOADER_H

#include <string>
#include "src/tools/shared_eigen_incl.h"
#include "openglincl.h"

struct SHADER_FILE_DESCR {
	std::string vert;
	std::string frag;
	std::string tess;
	/*inline bool operator==(const SHADER_FILE_DESCR& other) const
	{
		return Tools::StringEqualsLowerCase(vert, other.vert)
			&& Tools::StringEqualsLowerCase(frag, other.frag)
			&& Tools::StringEqualsLowerCase(tess, other.tess);
	}*/
};

class ShaderLoader
{
public:
	ShaderLoader(OpenGLView& func, QString base_location = ":/shaders/");
	~ShaderLoader();

	GLuint	LoadShader(SHADER_FILE_DESCR&);
	GLuint	LoadShader(const char * vertex_file_path, const char * fragment_file_path, const char * tesselation_file_path);

	void	render_tex0();
	void	draw_quad();

	GLuint	shape2d_shader();
	GLuint	poly3d_shader();

private:

	void		make_quad();
	QByteArray	read_file(QString filename);
	void		compile(GLuint ID, QByteArray code, QString path, QString type);

	QString								m_path;

	Vector4f							gMapOrient;
	OpenGLView&							f;

	GLuint m_quad_vao = 0;
	GLuint m_quad_vb = 0;

	GLuint m_copy_shader = 0;
	GLuint m_shape2d_shader = 0;
	GLuint m_poly3d_shader = 0;
};

#endif // SHADERLOADER_H
