#ifndef IINSTANCEDDRAWER2D_H
#define IINSTANCEDDRAWER2D_H


#include "openglincl.h"
#include "tools/include.h"
#include "tools/shared_eigen_incl.h"
#include "tools/resizedlistener.h"
#include "shaderloader.h"
#include "Orientable.h"
#include "GPUBuffer.h"
#include <QMutex>

class IInstancedDrawerBase : Tools::INonCopyable
{
public:

	inline void	set_changed(bool avoid_lock = false)
	{
		if (avoid_lock) m_require_update = true;
		else {
			QMutexLocker locker(&m_mutex);
			m_require_update = true;
		}
	}

	inline QMutex*			mutex_ptr() { return &m_mutex; }
	//inline void				lock() { m_mutex.lock(); }
	//inline void				unlock() { m_mutex.unlock(); }

	inline OpenGLView &		func() { return f;}

protected:

	IInstancedDrawerBase(OpenGLView& func);
	
	OpenGLView&				f;
	GLuint					m_shader_id = 0;
	GLuint					m_vao = 0;

	QMutex					m_mutex;
	bool					m_require_update = true;
};


class IInstancedDrawer2D : public IInstancedDrawerBase
{
public:

	IInstancedDrawer2D(OpenGLView& func, GLenum draw_type = GL_TRIANGLE_FAN);

	void			init(
		std::vector<Vector2f>& vertex_data, 
		std::function<void(int)> extra_shader_update_func = nullptr
	);

	void			render(RenderContext);
	inline bool		has_vertices() { return m_vertex_data.size() > 0; }
	void			destroy();

	inline std::vector<Color>&		colors() { return m_colors; }
	inline std::vector<Vector4f>&	orientations() { return m_orientations; }
	inline std::vector<Vector4f>&	rotations() { return m_rotations; }
	inline Vector4f&				orientation() { return m_orientation; }

protected:

	void						upload_data();

	Vector4f					m_orientation {0,0,1,1};
	std::function<void(int)>	m_prep_shader_func = nullptr;

	// children can freely change the instance data:
	// - use set_changed() after modifying to see the changes
	// - requirement: m_colors.size() == m_orientations.size()
	std::vector<Color>			m_colors;
	std::vector<Vector4f>		m_orientations;  // x, y, scx, scy
	std::vector<Vector4f>		m_rotations;
	std::vector<Vector2f>		m_vertex_data;
	GLenum						m_draw_type;

private:

	void					draw(RenderContext rc);
	void					prep_shader(RenderContext rc);
	int						provide_shader(ShaderLoader& sl);

	// todo : specify update per data buffer


	GPUBuffer				m_vertex_buffer;
	GPUBuffer				m_orientations_buffer;
	GPUBuffer				m_color_buffer;
	GPUBuffer				m_rotation_buffer;
	int						m_buffered_shape_count = 0;
	int						m_buffered_vertex_count = 0;


};




#endif // IINSTANCEDDRAWER2D_H
