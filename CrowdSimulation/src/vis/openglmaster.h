#ifndef OPENGLMASTER_H
#define OPENGLMASTER_H

#include <QOpenGLWidget>
#include <thread>
#include <list>
#include "visdeclr.h"
#include "openglincl.h"
#include "tools/resizedlistener.h"
#include "griddrawer2d.h"
#include "shaderloader.h"
#include "CircleDrawer2D.h"
#include "tools/shared_eigen_incl.h"
#include "tools/InputManager.h"
#include "tools/timer.h"


namespace Ui {
class OpenGLMaster;
}

/*
	Opengl master gets click events from QT
	and redirects them to listeners
*/

class Camera2D : public InputListener, public ResizedListener
{
public:
	Camera2D(ISurface&);
	virtual MouseEventReturnValue MousePress(MouseEventInfo) override;
	virtual MouseEventReturnValue MouseMove(MouseEventInfo) override;
	virtual MouseEventReturnValue MouseRelease(MouseEventInfo) override;
	virtual MouseEventReturnValue MouseScroll(MouseEventInfo) override;

	inline Orient2D getOrient() {
		return m_orient2d;
	}

	// get coordinates from a pixel, pos and scale are modifiers of the normal opengl space [-1,1]
	Vector2f pixel_to_world(int x, int y);
	void focusView(AABB2);
	void calculate_orient_scale_rat(Orient2D&);
	void set_enabled(bool state);
	
	virtual void object_resized(ISurface* obj, Tools::Surface2D<int> new_size) override;

private:
	float clamp_zoom(float z);
	bool			m_enabled;

	ISurface&		m_surface;
	Orient2D		m_orient2d;
	float			m_zoom2d = 1;
	float			m_wait_drag_ms;
	Timer			m_io_timer;
};

class OpenGLMaster
	: public QOpenGLWidget, public ISurface
{
	Q_OBJECT
public:
	explicit OpenGLMaster(QWidget *parent = 0);
	~OpenGLMaster();

	inline OpenGLView& getFunc() { return f; }
	void set_render_obj(IRenderable*);
	void stop_render_obj(IRenderable*);

	void step();

	InputManager* get_inputmanager() {
		return &input;
	}

	void render_text(QPainter& p, float x, float y, Color c, const QString &str, const QFont & font = QFont("Courier New"));

	bool verify_opengl_thread_id();

	bool is_initialized();

	void focusView2D(AABB2);

	Tools::Surface2D<int> surface();
	virtual Tools::Surface2D<int> get_size();

	void enable2DCam(bool state);
	inline Camera2D& camera2D() { return m_2d_cam; }

	void snap_screenshot() { m_screenshot_counter++; }
	void wait_till_screenshot_done();

protected:
	// QOpenGLWidget interface
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void wheelEvent(QWheelEvent* e) override;
	void mouseMoveEvent(QMouseEvent* e) override;
	void mousePressEvent(QMouseEvent* e) override;
	void mouseReleaseEvent(QMouseEvent *e) override;
	void keyPressEvent(QKeyEvent* e) override;
	void keyReleaseEvent(QKeyEvent* e) override;

private:

	void update_fbo();
	void draw();


	static bool make_rendertarget(OpenGLView& f, Tools::Surface2D<int> size, bool is_native_size, bool use_depth, GLuint& fbo, GLuint& tex, GLuint& depth, int samples);

	Ui::OpenGLMaster* m_ui = nullptr;
	OpenGLView f;

	std::list<IRenderable*> m_render_obj_stack;

	float m_supersampling;
	bool m_multisample;
	int m_sample_count;

	GLuint m_fbo = 0;
	GLuint m_tex = 0;
	GLuint m_depth = 0;
	GLuint m_fbo_ms = 0;
	GLuint m_tex_ms = 0;
	GLuint m_depth_ms = 0;

	Tools::Surface2D<int> m_offscreen_surface;
	EventBasedInputManager input;

	std::thread::id m_opengl_tid;
	bool			m_opengl_initialized = false;

	Timer			m_render_timer;
	bool			m_recording = false;
	int				m_screenshot_counter = 0;
	int				m_screenshot_done = 0;

	Camera2D		m_2d_cam;
};

#endif // OPENGLMASTER_H
