#include "openglmaster.h"
#include "ui_openglmaster.h"
#include "irenderable.h"
#include "files/ImageLoad.h"
#include "external/bitmap_image.hpp"


#include <QSurfaceFormat>
#include <QDebug>
#include <QGLFormat>
#include <QOpenGLFramebufferObjectFormat>
#include <QWheelEvent>
#include <QColor>
#include <chrono>
#include <thread>

OpenGLMaster::OpenGLMaster(QWidget *parent) :
	QOpenGLWidget(parent),
	m_ui(new Ui::OpenGLMaster),
	m_2d_cam(*this)
{
	m_supersampling = 1;
	m_multisample = true;
	m_sample_count = 4;

	QWidget::setFocusPolicy(Qt::FocusPolicy::WheelFocus);

	setAttribute(Qt::WA_DeleteOnClose);
	m_ui->setupUi(this);

	QSurfaceFormat format;
	format.setSamples(16);
	format.setOption(QSurfaceFormat::StereoBuffers, true);
	setFormat(format);

	input.AddInputListener(&m_2d_cam);
}

OpenGLMaster::~OpenGLMaster()
{
	SafeDeletePtr(m_ui);
}

void OpenGLMaster::set_render_obj(IRenderable* r)
{
	m_render_obj_stack.push_front(r);
}

void OpenGLMaster::stop_render_obj(IRenderable* ptr)
{
	m_render_obj_stack.erase(std::find(m_render_obj_stack.begin(), m_render_obj_stack.end(), ptr));
}

void OpenGLMaster::step()
{
	input.PrepForNextFrame();
}


Vector2f Camera2D::pixel_to_world(int x, int y)
{
	Tools::Surface2D<int> surface = m_surface.get_size();
	Vector2f gl_unit_pos = Vector2f{ -1,-1 } +2 * Vector2f{ x / (float)surface.width, 1.0f - y / (float)surface.height };

	// update a copy for certainly good value:
	Orient2D orient = getOrient();
	calculate_orient_scale_rat(orient);

	Vector2f result = gl_unit_pos / orient.scale;
	result(0) *= orient.ratio;
	return result - orient.pos;
}

void OpenGLMaster::render_text(QPainter& p, float x, float y, Color c, const QString &str, const QFont & font /*= QFont()*/)
{
	float text_x = ((x + 1) / 2) * width();
	float text_y = ((1 - y) / 2) * height();

	QColor qc { c.r, c.g, c.b, c.a };
	p.setPen(qc);
	p.setFont(font);
	p.drawText(text_x, text_y, str);
	p.end();
}

bool OpenGLMaster::verify_opengl_thread_id()
{
	return std::this_thread::get_id() == m_opengl_tid;
}

bool OpenGLMaster::is_initialized()
{
	return m_opengl_initialized;
}

void OpenGLMaster::focusView2D(AABB2 aabb)
{
	m_2d_cam.focusView(aabb);
}

Tools::Surface2D<int> OpenGLMaster::surface()
{
	return { size().width(), size().height() };
}

void OpenGLMaster::draw()
{
	float dt;

	if (m_render_timer.is_running()) {
		float elapsed = m_render_timer.elapsed_ms();
		if (elapsed < 0.5f) dt = elapsed;
	}
	else {
		dt = 0.1f;
	}

	if (m_render_obj_stack.size() > 0) {
		m_opengl_tid = std::this_thread::get_id();


		RenderContext rc{ f, m_2d_cam.getOrient(), dt };
		(*m_render_obj_stack.begin())->render(rc);
	}
	
	Q_ASSERT(f.glGetError() == 0);
}

void Camera2D::calculate_orient_scale_rat(Orient2D& output)
{
	output.scale = m_zoom2d;
	output.ratio = m_surface.ratio();
}

void Camera2D::set_enabled(bool state)
{
	m_enabled = state;
}

void Camera2D::object_resized(ISurface* obj, Tools::Surface2D<int> new_size)
{
	calculate_orient_scale_rat(m_orient2d);
}

float Camera2D::clamp_zoom(float z)
{
	return qBound(0.0001f, z, 100000.0f);
}

void OpenGLMaster::initializeGL()
{
	m_opengl_tid = std::this_thread::get_id();

	if (!f.initializeOpenGLFunctions()) {
		qFatal("Unable to initialize opengl");
	}
	
	f.glClearColor(0.87f, 0.87f, 0.87f, 1.0f);

	f.set_opengl_master(this);
	f.set_shader_loader(new ShaderLoader(f));

	update_fbo();

	f.glEnable(GL_MULTISAMPLE);
	f.glEnable(GL_CULL_FACE);
	f.glFrontFace(GL_CCW);
	f.glDisable(GL_DEPTH_TEST); // should be default

	m_opengl_initialized = true;
}

void OpenGLMaster::resizeGL(int w, int h)
{
	update_fbo();
	shout_resize({ w,h });
}

void OpenGLMaster::paintGL()
{
	//QPainter painter(this);

	//if (false) {
		//painter.beginNativePainting();
		f.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		GLint m_orig_draw = 0, m_origin_read = 0, viewport[4];
		f.glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &m_orig_draw);
		f.glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &m_origin_read);
		f.glGetIntegerv(GL_VIEWPORT, &viewport[0]);

		// GL_SAMPLE_BUFFERS
		Q_ASSERT(f.glGetError() == 0);

		// bind off-screen fbo to render to
		f.glBindFramebuffer(GL_FRAMEBUFFER, m_multisample ? m_fbo_ms : m_fbo);
		f.glViewport(0, 0, m_offscreen_surface.width, m_offscreen_surface.height);
		f.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Q_ASSERT(f.glGetError() == 0);

		draw();

		// if multisampled, resolve to normal
		if (m_multisample) {
			f.glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo_ms);
			f.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);

			int w = m_offscreen_surface.width, h = m_offscreen_surface.height;
			//f.glBlitFramebuffer(0, 0, m_offscreen_surface.width, m_offscreen_surface.height, 0, 0, width(), height(), GL_COLOR_BUFFER_BIT, m_supersampling == 1 ? GL_NEAREST : GL_LINEAR);
			f.glBlitFramebuffer(0, 0, w, h, 0, 0, w, h, GL_COLOR_BUFFER_BIT, GL_NEAREST);

			if (auto err = f.glGetError()) qFatal(QString::number(err).append(" : OpenGL Error").toUtf8());

			
		}

		if (m_recording || m_screenshot_counter > m_screenshot_done)
		{
			if (m_screenshot_counter > m_screenshot_done) m_screenshot_done++;

			f.glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);

			int w = m_offscreen_surface.width, h = m_offscreen_surface.height;

			bitmap_image img(w, h);
			int dim = 4;
			std::vector<uchar> data(w * h * dim);
			f.glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, data.data());

			for (int y = 0; y < h; y++) {
				for (int x = 0; x < w; x++) {
					rgb_t color;
					int pixel_id = (y * w + x) * dim;
					color.red = data[pixel_id + 0];
					color.green = data[pixel_id + 1];
					color.blue = data[pixel_id + 2];
					img.set_pixel(x, (h - y - 1), color);
				}
			}

			static int n;

			img.save_image(std::string("image" + std::to_string(n++) + ".bmp").c_str());

			//ImageLoad::write_image(data, w, h);
		}

		Q_ASSERT(f.glGetError() == 0);

		{ // push to screen
			f.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_orig_draw);
			f.glActiveTexture(GL_TEXTURE0);
			f.glBindTexture(GL_TEXTURE_2D, m_tex);
			f.glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
			f.shader_loader().render_tex0();


		}

		//painter.endNativePainting();
	//}

	//auto f = QFont("Courier New");
	//f.setPixelSize(100);
	//render_text(painter, -1, 1, { 255,255,255,255 }, "Hallo!", f);
}

Tools::Surface2D<int> OpenGLMaster::get_size()
{
	auto size = this->size();
	return { size.width(), size.height() };
}

void OpenGLMaster::enable2DCam(bool state)
{
	m_2d_cam.set_enabled(state);
}

void OpenGLMaster::wait_till_screenshot_done()
{
	while (m_screenshot_counter > m_screenshot_done) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void OpenGLMaster::wheelEvent(QWheelEvent *e)
{
	input.MouseScrollMsg(to_scroll_mouse_event(e));
}

void OpenGLMaster::mouseMoveEvent(QMouseEvent* e)
{
	// the QT version
	input.MouseMoveMsg(to_move_mouse_event(e));
}

void OpenGLMaster::mousePressEvent(QMouseEvent *e)
{
	auto me = to_press_mouse_event(e);
	input.MousePressMsg(me);
}

void OpenGLMaster::mouseReleaseEvent(QMouseEvent *e)
{
	auto me = to_release_mouse_event(e);
	input.MouseReleaseMsg(me);
}

void OpenGLMaster::keyPressEvent(QKeyEvent *e)
{
	if ((Qt::Key) e->key() == Qt::Key::Key_F7) {
		m_recording = !m_recording;
	}
	input.KeyDownMsg((Qt::Key) e->key());
}

void OpenGLMaster::keyReleaseEvent(QKeyEvent *e)
{
	input.KeyUpMsg((Qt::Key) e->key());
}

Camera2D::Camera2D(ISurface& s) : m_surface(s)
{
	m_enabled = true;
	m_wait_drag_ms = 200.0f;
	m_surface.add_resized_listener(this);
}

MouseEventReturnValue Camera2D::MousePress(MouseEventInfo info)
{
	if (!m_enabled) return { };

	// as soon as we press the mouse we wait if its not released for a bit
	m_io_timer.restart();
	this->StartDrag(info.x, info.y);
	return { };
}

MouseEventReturnValue Camera2D::MouseMove(MouseEventInfo info)
{
	if (!m_enabled) return { InputControl::Request::ReleaseAll() };

	bool execute_drag = m_io_timer.is_running() && m_io_timer.elapsed_ms() > m_wait_drag_ms && InputListener::GetAnyManager()->HasDefaultState();

	if (this->HasDrag() && execute_drag) {
		Tools::Direction2D<int> drag = this->GetDragIncr(info.x, info.y);
		auto size = m_surface.get_size();
		Vector2f move = Vector2f{ 
			drag.x / (m_zoom2d * size.width),
			-drag.y / (m_zoom2d * size.height)
		};
		m_orient2d.pos += move;
		return { InputControl::Request::Take_Keep_Mouse() };
	}

	return { InputControl::Request::ReleaseAll() };
}

MouseEventReturnValue Camera2D::MouseRelease(MouseEventInfo info)
{
	if (!m_enabled) return { InputControl::Request::ReleaseAll() };

	qInfo() << "Camera2D mouse release";

	this->EndDrag();
	m_io_timer.stop();
	return { InputControl::Request::ReleaseAll() };
}

MouseEventReturnValue Camera2D::MouseScroll(MouseEventInfo info)
{
	if (!m_enabled) return {};

	float z = 1 + 16.0f / info.delta;
	m_zoom2d = clamp_zoom(z * m_zoom2d);
	//m_zoom2d = 0.0243739;
	calculate_orient_scale_rat(m_orient2d);

	//qInfo() << "New zoom:" << m_zoom2d;
	
	return {};
}


void Camera2D::focusView(AABB2 aabb)
{
	m_orient2d.pos = -aabb.center();
	m_zoom2d = clamp_zoom(1.0f / qMax(aabb.sizes()(0), aabb.sizes()(1)));
	calculate_orient_scale_rat(m_orient2d);
}

void OpenGLMaster::update_fbo()
{
	m_offscreen_surface = { (int)(width() * m_supersampling), (int)(height() * m_supersampling) };

	bool succes = true;

	if (m_multisample && m_sample_count >= 2) {
		succes &= make_rendertarget(f, m_offscreen_surface, true, true, m_fbo_ms, m_tex_ms, m_depth_ms, m_sample_count);
	}
	
	// we need the normal one anyway, either as draw target or as multisample resolution
	succes &= make_rendertarget(f, m_offscreen_surface,
		m_supersampling == 1, // near sampling if equal surface size
		m_multisample == false, // no depth buffer needed if only as intermediate blit
		m_fbo, m_tex, m_depth, 0);
	

	if (!succes) qFatal("Failed to make render target");
}

bool OpenGLMaster::make_rendertarget(OpenGLView& f, Tools::Surface2D<int> size, 
	bool near_sampling, bool use_depth, GLuint& fbo, GLuint& tex, GLuint& depth, int samples)
{
	int w = size.width;
	int h = size.height;
	bool ms = samples > 1;

	f.glGenFramebuffers(1, &fbo);
	f.glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	f.glGenTextures(1, &tex);

	if (!ms || samples < 2) {
		f.glActiveTexture(GL_TEXTURE0);
		f.glBindTexture(GL_TEXTURE_2D, tex);
		f.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		f.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, tex, 0);
		Q_ASSERT(f.glGetError() == 0);

		f.glGenRenderbuffers(1, &depth);
		f.glBindRenderbuffer(GL_RENDERBUFFER, depth);
		f.glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, w, h);
		f.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depth);
		Q_ASSERT(f.glGetError() == 0);
	}
	else {
		f.glActiveTexture(GL_TEXTURE0);
		f.glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, tex);
		f.glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGBA8, w, h, GL_TRUE);
		f.glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);
		Q_ASSERT(f.glGetError() == 0);

		f.glGenRenderbuffers(1, &depth);
		f.glBindRenderbuffer(GL_RENDERBUFFER, depth);
		f.glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH24_STENCIL8, w, h);
		f.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depth);
		Q_ASSERT(f.glGetError() == 0);
	}

	f.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, near_sampling ? GL_NEAREST : GL_LINEAR);
	f.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, near_sampling ? GL_NEAREST : GL_LINEAR);

	//Finally, we configure our framebuffer

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	f.glDrawBuffers(1, DrawBuffers);

	auto status = f.glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT) {
		qInfo() << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
		return false;
	}
	else if (status != GL_FRAMEBUFFER_COMPLETE) {
		qInfo() << "Framebuffer status: " << status;
		return false;
	}
	return true;
}
