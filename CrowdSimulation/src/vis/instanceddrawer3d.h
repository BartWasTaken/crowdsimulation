#ifndef INSTANCEDDRAWER3D_H
#define INSTANCEDDRAWER3D_H

#include "iinstanceddrawer2d.h"
#include "src/vis/meshgen.h"
#include "files/MeshTypes.h"

class InstancedDrawer3D : public IInstancedDrawerBase
{
public:
	InstancedDrawer3D(OpenGLView& func);

	void			render(RenderContext3D);

	void			set_shape(std::vector<Vector3f>& v, std::vector<Vector3f>& n, std::vector<GLuint>& i);
	void			set_shape(MeshGen::IndexedMesh<Vector3f>&);
	void			set_shape(CGMain::IVertexData*);

	void			add(Vector3f position, Vector3f rotation, Vector3f scale, Color color);
	void			set_amount(int n);
	void			move_to(int id, Vector3f position);
	void			change(int i, Vector3f position, Vector3f rotation, Vector3f scale, Color color);
	void			pop();
	//void			

private:
	void			draw(RenderContext3D);
	void			upload_data(RenderContext3D);
	int				provide_shader(ShaderLoader& sl);
	void			prep_shader(RenderContext3D rc);

	std::vector<Vector3f>							m_vertices;
	std::vector<Vector3f>							m_normals;
	std::vector<GLuint>								m_indices;

	std::vector<GLBase::Environment::Orientable>	m_orientations;
	std::vector<Vector4uchar>						m_colors;
	std::vector<Matrix4f>							m_matrices;

	OpenGLView&				m_f;
	GPUBuffer				m_vertex_buffer;
	GPUBuffer				m_normal_buffer;
	GPUBuffer				m_color_buffer;
	GPUBuffer				m_indice_buffer;
	GPUBuffer				m_matrix_buffer;
	int						m_buffered_shape_count = 0;
	int						m_buffered_indice_count = 0;

};

#endif // INSTANCEDDRAWER3D_H