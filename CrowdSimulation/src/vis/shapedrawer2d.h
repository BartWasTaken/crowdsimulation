#ifndef SHAPEDRAWER2D_H
#define SHAPEDRAWER2D_H

#include "openglincl.h"
#include "GPUBuffer.h"
#include "tools/resizedlistener.h"
#include <mutex>
#include <thread>

class ShapeDrawer2D
{
public:
	ShapeDrawer2D(OpenGLView& func);
	ShapeDrawer2D(OpenGLView& func, ColorF c);
	~ShapeDrawer2D();

	void GLDestroy();
	void make_shape(const std::vector<Vector2f>& vertices);
	void remove_shape(bool avoid_lock = false);
	void render(RenderContext);

	inline void render_lines() { m_render_type = GL_LINES; }

private:
	void					setup(OpenGLView& func, ColorF c);
	void					prep_shader(RenderContext);
	int						make_shader(ShaderLoader& sl);

	OpenGLView&				f;

	bool					m_changed = false;
	std::vector<Vector2f>	m_vertices;
	std::vector<GLuint>		m_indices;
	Vector4f				m_color;

	GLuint					m_vao = 0;
	int						m_buffered_v_count = 0;
	int						m_buffered_i_count = 0;
	GPUBuffer				m_vbuffer;
	GPUBuffer				m_ibuffer;

	GLuint					m_shader = 0;
	Vector4f				m_orientation{ 0,0,1,1 };
	GLuint					m_render_type = GL_TRIANGLES;

	static std::recursive_mutex		m_lock;
};

#endif // SHAPEDRAWER2D_H