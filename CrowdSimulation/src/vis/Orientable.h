#pragma once
#include "src/tools/include.h"

namespace GLBase {
namespace Environment {

class Orientable
{
public:
	Orientable();
	~Orientable();

	Matrix4f GetModelMatrix();
	Matrix4f GetTranslateionMatrix();
	Matrix4f GetRotationMatrix();
	Matrix4f GetScaleMatrix();

	void SetTranslation(Vector3f);
	void Translate(Vector3f);

	void SetRotation(Vector3f r);
	void SetRotation(Quaternionf r);
	//void SetRotation(AngleAxis<float>& aa);
	void Rotate(Vector3f r);
	void Rotate(Quaternionf r);

	void SetScale(Vector3f s);
	void SetScale(float s);
	void Scale(Vector3f s);
	void Scale(float s);

	void ResetOrientation();

private:
	Affine3f					gPosition; // Affine3f Translation3f
	Quaternionf					gRotation;
	DiagonalMatrix<float, 4>	gScale;
};

}
}