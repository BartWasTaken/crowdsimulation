#include "Orientable.h"


namespace GLBase {
namespace Environment {

Quaternionf EulerToQuaternion(Vector3f r) {
	Eigen::AngleAxisf rollAngle(r(0), Eigen::Vector3f::UnitX());
	Eigen::AngleAxisf pitchAngle(r(1), Eigen::Vector3f::UnitY());
	Eigen::AngleAxisf yawAngle(r(2), Eigen::Vector3f::UnitZ());

	return rollAngle * pitchAngle * yawAngle;
}

Orientable::Orientable()
{
	ResetOrientation();
}


Orientable::~Orientable()
{
}

Matrix4f ToRotationMatrix(Quaternionf& q) 
{
	q.normalize();
	Matrix4f result;
	result << 
		1.0f - 2.0f * q.y() * q.y() - 2.0f * q.z() * q.z(), 
		2.0f * q.x() * q.y() - 2.0f * q.z() * q.w(), 
		2.0f * q.x() * q.z() + 2.0f * q.y() * q.w(),
		0.0f,
		
		2.0f * q.x() * q.y() + 2.0f*q.z()*q.w(),
		1.0f - 2.0f * q.x() * q.x() - 2.0f * q.z() * q.z(),
		2.0f * q.y() * q.z() - 2.0f * q.x() * q.w(),
		0.0f,

		2.0f * q.x() * q.z() - 2.0f * q.y() * q.w(),
		2.0f * q.y() * q.z() + 2.0f * q.x() * q.w(),
		1.0f - 2.0f * q.x() * q.x() - 2.0f * q.y() * q.y(),
		0.0f,

		0.0f, 0.0f, 0.0f, 1.0f;
	return result;
}

Matrix4f Orientable::GetModelMatrix()
{
	auto positionM = gPosition.matrix();
	return positionM * ToRotationMatrix(gRotation) * gScale;
	// quaternion.toRotationMatrix() gives 3x3 matrix
}

Matrix4f Orientable::GetTranslateionMatrix()
{
	return gPosition.matrix();
}

Matrix4f Orientable::GetRotationMatrix()
{
	return ToRotationMatrix(gRotation);
}

Matrix4f Orientable::GetScaleMatrix()
{
	return gScale;
}

void Orientable::SetTranslation(Vector3f t)
{
	gPosition = Translation3f(t);
}

void Orientable::Translate(Vector3f t)
{
	gPosition.translate(t);
}
 
void Orientable::SetRotation(Vector3f r)
{
	gRotation = EulerToQuaternion(r);
}

void Orientable::SetRotation(Quaternionf r)
{
	gRotation = r;
}

void Orientable::Rotate(Vector3f r)
{
	Rotate(EulerToQuaternion(r));
}

void Orientable::Rotate(Quaternionf q)
{
	gRotation = q * gRotation;
}

void Orientable::SetScale(Vector3f s)
{
	gScale.diagonal()(0) = s(0);
	gScale.diagonal()(1) = s(1);
	gScale.diagonal()(2) = s(2);
}

void Orientable::SetScale(float s)
{
	gScale.diagonal()(0) = s;
	gScale.diagonal()(1) = s;
	gScale.diagonal()(2) = s;
}

void Orientable::Scale(Vector3f s)
{
	gScale.diagonal()(0) *= s(0);
	gScale.diagonal()(1) *= s(1);
	gScale.diagonal()(2) *= s(2);
}

void Orientable::Scale(float s)
{
	gScale.diagonal()(0) *= s;
	gScale.diagonal()(1) *= s;
	gScale.diagonal()(2) *= s;
}

void Orientable::ResetOrientation()
{
	gScale.setIdentity();
	gRotation = AngleAxis<float>(0, Vector3f(0,1,0));
	gPosition = Translation3f(0, 0, 0);
}

}
}