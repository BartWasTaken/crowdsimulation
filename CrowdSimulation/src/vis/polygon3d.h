#ifndef POLYGON3D_H
#define POLYGON3D_H
#include "src/tools/include.h"
#include "src/vis/openglincl.h"
#include "GPUBuffer.h"
#include "Orientable.h"
#include "files/MeshTypes.h"
#include <vector>

class Polygon3D : public GLBase::Environment::Orientable
{
public:
	Polygon3D(OpenGLView&);
	void build(CGMain::IVertexData*);
	void build(std::vector<Vector3f>& vertices, std::vector<GLuint>& indices);
	void render(RenderContext3D rc);
	void destroy();

private:
	void prep_shader(RenderContext3D rc);

	OpenGLView& f;
	GLuint m_vao;
	GLuint m_icount;

	std::vector<GPUBuffer> m_vbuffers;
	GPUBuffer m_indice_buffer;
};

#endif // POLYGON3D_H