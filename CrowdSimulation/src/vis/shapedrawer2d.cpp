#include "shapedrawer2d.h"
#include "openglmaster.h"
#include "src/external/earcut.hpp"
#include <array>

std::recursive_mutex ShapeDrawer2D::m_lock;

ShapeDrawer2D::ShapeDrawer2D(OpenGLView& func)
	: f(func), m_vbuffer(func), m_ibuffer(func)
{
	setup(func, { 1,0,1,1 });
}

ShapeDrawer2D::ShapeDrawer2D(OpenGLView& func, ColorF c)
	: f(func), m_vbuffer(func), m_ibuffer(func)
{
	setup(func, c);
}


void ShapeDrawer2D::setup(OpenGLView& func, ColorF c)
{
	//m_lock = new std::mutex();
	//make_shader(func.shader_loader());
	m_shader = make_shader(func.shader_loader());
	auto s = f.opengl_master().size();;
	m_color = c.as_vector_rgba();
}


ShapeDrawer2D::~ShapeDrawer2D()
{
	//SafeDeletePtr(m_lock);
}

void ShapeDrawer2D::GLDestroy()
{
	// todo
}

void ShapeDrawer2D::make_shape(const std::vector<Vector2f>& vertices)
{
	std::lock_guard<std::recursive_mutex> lock(m_lock);
	//m_vertices = vertices;

	int vcount = vertices.size();

	if (vcount == 0) {
		remove_shape(true);
	}
	else if (vcount == 1) {
		m_vertices = vertices;
		m_indices = std::vector<GLuint>{ 0 };
	}
	else if (vcount == 2) {
		m_vertices = vertices;
		m_indices = std::vector<GLuint>{ 0, 1 };
	}
	else {
		// triangulate polygon

		m_vertices = vertices;

		// The number type to use for tessellation
		using Coord = float;

		// The index type. Defaults to uint32_t, but you can also pass uint16_t if you know that your
		// data won't have more than 65536 vertices.
		using N = GLuint;

		// Create array
		using Point = std::array<Coord, 2>;
		std::vector<std::vector<Point>> polygon;

		// Fill polygon structure with actual data. Any winding order works.
		// The first polyline defines the main polygon.
		polygon.push_back({});
		for (const auto& v : m_vertices) {
			polygon[0].push_back({ v(0), v(1) });
		}

		// Following polylines define holes.
		// polygon.push_back({ { 75, 25 },{ 75, 75 },{ 25, 75 },{ 25, 25 } });
		// Run triangulation
		// Returns array of indices that refer to the vertices of the input polygon.
		// e.g: the index 6 would refer to {25, 75} in this example.
		// Three subsequent indices form a triangle. Output triangles are clockwise.

		m_indices = mapbox::earcut<N>(polygon);
	}

	m_changed = true;
}

void ShapeDrawer2D::remove_shape(bool avoid_lock)
{
	//if (avoid_lock == false) 
		std::lock_guard<std::recursive_mutex> lock(m_lock);
	m_changed = true;
	m_vertices = {};
	m_indices = { };
}

void ShapeDrawer2D::render(RenderContext rc)
{
	bool vao_bound = false;


	if (m_changed) {
		std::lock_guard<std::recursive_mutex> lock(m_lock);

		f.glGenVertexArrays(1, &m_vao);
		f.glBindVertexArray(m_vao);
		vao_bound = true;

		int vbyte_size = m_vertices.size() * sizeof(m_vertices[0]);
		int ibyte_size = m_indices.size() * sizeof(m_indices[0]);

		if (vbyte_size> 0) m_vbuffer.Write(vbyte_size, 0, m_vertices.data(), GL_STATIC_DRAW);
		if (ibyte_size> 0) m_ibuffer.Write(ibyte_size, 0, m_indices.data(), GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER);

		m_changed = false;
		m_buffered_v_count = m_vertices.size();
		m_buffered_i_count = m_indices.size();
	}

	if (m_vao && m_buffered_v_count > 0 && m_buffered_i_count > 0) {
		if (!vao_bound) f.glBindVertexArray(m_vao);

		f.glEnableVertexAttribArray(0);
		f.glBindBuffer(GL_ARRAY_BUFFER, m_vbuffer.ID());
		f.glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			2,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		prep_shader(rc);
		//f.glDrawArrays(GL_TRIANGLE_STRIP, 0, m_vertices.size());

		if (m_buffered_v_count == 1) {
			f.glDrawElements(GL_POINTS, m_indices.size(), GL_UNSIGNED_INT, (void*)0);
		}
		else if (m_buffered_v_count == 2) {
			f.glDrawElements(GL_LINES, m_indices.size(), GL_UNSIGNED_INT, (void*)0);
		}
		else {
			int err;
			switch (m_render_type) {
				case GL_LINES:
					f.glDrawArrays(m_render_type, 0, m_vertices.size());
					break;
				case GL_TRIANGLES:
					f.glDrawElements(m_render_type, m_indices.size(), GL_UNSIGNED_INT, (void*)0);
					break;
			}
		}

		//f.glDisableVertexAttribArray(0);
		f.glBindVertexArray(0);

		Q_ASSERT(f.glGetError() == 0, "ShapeDrawer2D : render", "Failed to render...");
	}

}

void ShapeDrawer2D::prep_shader(RenderContext rc)
{
	m_orientation = rc.orient_2d.to_4_orientation();

	f.glUseProgram(m_shader);
	f.glUniform4fv(0, 1, m_orientation.data());
	f.glUniform4fv(1, 1, m_color.data());

	Q_ASSERT(f.glGetError() == 0, "ShapeDrawer2D : prep_shader", "Failed to prep shader...");
}

int ShapeDrawer2D::make_shader(ShaderLoader& sl)
{
	return sl.shape2d_shader();
}
