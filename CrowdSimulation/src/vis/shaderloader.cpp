#include "shaderloader.h"
#include "openglmaster.h"
#include <QtGlobal>
#include <QFileInfo>
#include <QTextStream>
#include <QByteArray>
#include <QDebug>
#include <iostream>


ShaderLoader::ShaderLoader(OpenGLView& func, QString base_location)
	: f(func), gMapOrient(0.3023f, 0.2339f, 0.3408f, 0.3413f), m_path(base_location)
{
	Q_ASSERT(f.opengl_master().verify_opengl_thread_id());

	Q_ASSERT(f.glGetError() == 0);
	/* Preload shaders */ 
	{
		SHADER_FILE_DESCR descr;
		descr.vert = "fullscreen_uv";
		descr.frag = "texture_copy";
		m_copy_shader = LoadShader(descr);
	} 
	Q_ASSERT(f.glGetError() == 0);
	{
		SHADER_FILE_DESCR descr;
		descr.frag = "poly2d";
		descr.vert = "poly2d";
		m_shape2d_shader = LoadShader(descr);
	}
	Q_ASSERT(f.glGetError() == 0);
	{
		SHADER_FILE_DESCR descr;
		descr.frag = "poly3d";
		descr.vert = "poly3d";
		m_poly3d_shader = LoadShader(descr);
	}
	Q_ASSERT(f.glGetError() == 0);

	make_quad();
	Q_ASSERT(f.glGetError() == 0);
}

ShaderLoader::~ShaderLoader()
{

}

/*
void ShaderLoader::UseCircleShader(OpenGLManager* pmanager, Vector4f orient)
{
	auto vsize = pmanager->GetWindow().GetSize();

	float aa_pixel_range = 2;
	float magic_aa_number = 0.5 * (float)vsize.height / aa_pixel_range; // 0.5 because viewspace size is 2

	DASSERT(glGetError() == 0);
	DASSERT(gCircleShaderId != 0);
	glUseProgram(gCircleShaderId);
	glUniform4fv(0, 1, orient.data());
	glUniform1f(1, magic_aa_number);
	glGetError(); // annoying uniform doesnt exist
}*/

GLuint ShaderLoader::LoadShader(
	const char * vertex_file_path,
	const char * fragment_file_path,
	const char * tesselation_file_path
) {

	bool tes = (tesselation_file_path != nullptr);

	// Create the shaders 1 by 1

	QString vertexPath;
	vertexPath.append(m_path).append(vertex_file_path).append(".vert");
	GLuint VertexShaderID = f.glCreateShader(GL_VERTEX_SHADER);
	bool VSExists = QFileInfo::exists(vertexPath);

	if (!VSExists) {
		qInfo() << "Not a valid path : " << vertexPath << "\n";
		return 0;
	}

	auto VertexShaderCode = read_file(vertexPath);
	compile(VertexShaderID, VertexShaderCode, vertexPath, "Vertex Shader");

	qInfo() << "Read .vert shader code:" << VertexShaderCode;

	GLuint TCShaderID;
	if (tes) {
		QString TCPath;
		TCPath.append(m_path).append(tesselation_file_path).append(".tc");
		TCShaderID = f.glCreateShader(GL_TESS_CONTROL_SHADER);

		bool TCExists = QFileInfo::exists(TCPath);
		if (!TCExists) {
			qInfo() << "Not a valid path : " << m_path << TCPath << "\n";
			return 0;
		}

		auto TCShaderCode = read_file(TCPath);
		compile(TCShaderID, TCShaderCode, TCPath, "TD Shader");
	}

	GLuint TEShaderID;
	if (tes) {
		QString TEPath;
		TEPath.append(m_path).append(tesselation_file_path).append(".te");
		TEShaderID = f.glCreateShader(GL_TESS_EVALUATION_SHADER);

		bool TEExists = QFileInfo::exists(TEPath);
		if (!TEExists) {
			qInfo() << "Not a valid path : " << m_path << VSExists << "\n";
			return 0;
		}

		auto TEShaderCode = read_file(TEPath);
		compile(TEShaderID, TEShaderCode, TEPath, "TE Shader");
	}

	QString fragPath = "";
	fragPath.append(m_path).append(fragment_file_path).append(".frag");
	GLuint FragmentShaderID = f.glCreateShader(GL_FRAGMENT_SHADER);

	bool FMExists = QFileInfo::exists(fragPath);
	if (!FMExists) {
		qInfo() << "Not a valid path : " << m_path << fragPath << "\n";
		return 0;
	}

	auto FragmentShaderCode = read_file(fragPath);
	compile(FragmentShaderID, FragmentShaderCode, fragPath, "Fragment Shader");

	qInfo() << "Read .frag shader code:" << FragmentShaderCode;

	///////////////////////////// LINK PROGRAM //////////////////////////////////

	GLuint ProgramID = f.glCreateProgram();
	f.glAttachShader(ProgramID, VertexShaderID);
	if (tes) {
		f.glAttachShader(ProgramID, TCShaderID);
		f.glAttachShader(ProgramID, TEShaderID);
	}
	f.glAttachShader(ProgramID, FragmentShaderID);
	f.glLinkProgram(ProgramID);

	GLint Result = GL_FALSE;
	int InfoLogLength = 0;

	{ // Check the program
		f.glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
		f.glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		std::vector<char> ProgramErrorMessage(qMax(InfoLogLength, int(1)));
		f.glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, ProgramErrorMessage.data());
		if (ProgramErrorMessage.size() > 1) {
			fprintf(stdout, "Error linking program\n");
			fprintf(stdout, "%s\n", ProgramErrorMessage.data());
		}
	}

	f.glDeleteShader(VertexShaderID);
	f.glDeleteShader(FragmentShaderID);
	if (tes) {
		f.glDeleteShader(TCShaderID);
		f.glDeleteShader(TEShaderID);
	}

	return ProgramID;
}

GLuint ShaderLoader::LoadShader(SHADER_FILE_DESCR& files)
{
	return LoadShader(files.vert.data(),
		files.frag.data(),
		// optional:
		(files.tess.size() > 0) ? files.tess.data() : nullptr);
}

void ShaderLoader::render_tex0()
{
	Q_ASSERT(f.glGetError() == 0);
	f.glUseProgram(m_copy_shader);
	draw_quad();
	Q_ASSERT(f.glGetError() == 0);
}

void ShaderLoader::draw_quad()
{
	f.glBindVertexArray(m_quad_vao);
	f.glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	// reset binding
	f.glBindVertexArray(0);
	Q_ASSERT(f.glGetError() == 0);
}

GLuint ShaderLoader::shape2d_shader()
{
	return m_shape2d_shader;
}

GLuint ShaderLoader::poly3d_shader()
{
	return m_poly3d_shader;
}

void ShaderLoader::make_quad()
{
	static float quad_vertices[] = {
		-1, -1, //0,
		1, -1, //0,
		-1, 1,// 0,
		1, 1, //0,
	};

	//for (float& f : quad_vertices) f *= 0.5f;
	

	f.glGenVertexArrays(1, &m_quad_vao);
	f.glBindVertexArray(m_quad_vao);
	f.glGenBuffers(1, &m_quad_vb);
	f.glBindBuffer(GL_ARRAY_BUFFER, m_quad_vb);
	f.glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*ARRAYSIZE(quad_vertices), &quad_vertices, GL_STATIC_DRAW);
	f.glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	f.glEnableVertexAttribArray(0);
	// reset binding
	f.glBindVertexArray(0);
}

QByteArray ShaderLoader::read_file(QString filename)
{
	QFile f(filename);
	if (!f.open(QFile::ReadOnly | QFile::ReadOnly)) {
		return QByteArray();
	}
	return f.readAll();
}

void ShaderLoader::compile(GLuint ID, QByteArray code, QString path, QString type)
{
	GLint compile_status = GL_FALSE;

	//qInfo().noquote() << "______________________________________________";
	//qInfo().noquote() << code;
	//qInfo().noquote() << "______________________________________________";

	int InfoLogLength = 0;

	const char* sourcePointer = code;

	f.glShaderSource(ID, 1, &sourcePointer, NULL);
	f.glCompileShader(ID);
	{ // ERROR CHECK
		f.glGetShaderiv(ID, GL_COMPILE_STATUS, &compile_status);
		f.glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		std::vector<char> ShaderErrMsg(InfoLogLength);
		f.glGetShaderInfoLog(ID, InfoLogLength, NULL, ShaderErrMsg.data());
		if (InfoLogLength == 1 && ShaderErrMsg.data()[0] == 0) {
			// this seems to be normal on intel graphics, null terminating empty string
			return;
			/*std::cout << "WARNING: single char shader error msg (are you using intel graphics?)\n";
			std::cout << "IN FILE:" << path.data() << '\n';
			std::cout << "COMPILE STATUS: " << compile_status << '\n';
			std::cout << "TOKEN: [" << ShaderErrMsg.data() << "]" << " (value: " << (int)ShaderErrMsg.data()[0] << ")" << "\n";
			*/
		}
		else if (ShaderErrMsg.data() != nullptr) {
			qInfo() << "\n*********** SHADER ERROR (" << type << ") ***********";
			qInfo() << "IN FILE:" << QString(path.data());
			qInfo() << "COMPILE STATUS: " << QString::number(compile_status);
			qInfo().noquote() << "MSG (#" << QString::number(InfoLogLength) << "): " << QString(ShaderErrMsg.data());
		}
	}
}
