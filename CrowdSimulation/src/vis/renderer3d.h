#ifndef RENDERER3D_H
#define RENDERER3D_H
#include "openglincl.h"
#include "src/sim/simdeclr.h"
#include "src/vis/irenderable.h"
#include "Cameras/ICamera.h" 
#include "Cameras/ModelCam.h"
#include "polygon3d.h"
#include "instanceddrawer3d.h"

class Renderer3D 
	: public IRenderable
{
public:
	Renderer3D(OpenGLView& opengl, SimulationCore& sc);

	void reset();
	void update(IColorizer&);
	void render(RenderContext);
	void enable(bool state);
	void set_center(Vector3f);
	inline bool enabled() { return m_enabled; }

private:
	void build_scene();
	void update_agents(IColorizer&);

	bool m_rebuild = false;
	bool m_enabled;

	OpenGLView & f;
	SimulationCore& sc;

	GLBase::Environment::Cameras::ModelCam*	m_pCamera;
	std::vector<Polygon3D>					m_polygons;
	float									m_object_scale_modifier = 1;

	std::vector<InstancedDrawer3D*>			m_instanced_drawers;
};

#endif // RENDERER3D_H