#ifndef CIRCLEDRAWER2D_H
#define CIRCLEDRAWER2D_H
#include "openglincl.h"
#include "tools/include.h"
#include "tools/shared_eigen_incl.h"
#include "tools/resizedlistener.h"
#include "shaderloader.h"
#include "iinstanceddrawer2d.h"
#include "GPUBuffer.h"

class CircleDrawer2D : Tools::INonCopyable
{
public:
	CircleDrawer2D(OpenGLView& func);

	void	add(Color, Vector2f loc, float scale);
	void	move(int i, Vector2f loc);
	void	pop();
	void	change_color(int i, Color c, bool change_alpha = true);
	void	update();
	int		count();
	void	clear();
	void	render(RenderContext);
	Color	get_color(int i);

	void	colorize(IColorizer&);

protected:
	void	write_shape(std::vector<Eigen::Vector2f> &out_data);

private:
	IInstancedDrawer2D m_drawer;
};

class LineDrawer2D : Tools::INonCopyable
{
public:
	LineDrawer2D(OpenGLView& func);

	void	clear();
	void	add(Vector2f a, Vector2f b, Color c);
	void	update();
	void	render(RenderContext);

protected:
	void	write_shape(std::vector<Eigen::Vector2f> &out_data);

private:
	IInstancedDrawer2D m_drawer;
};

#endif // CIRCLEDRAWER2D_H
