#include "meshgen.h"
namespace MeshGen {

template<class T>
static T* DetermineSphereStorage(uint32_t vCount, T* in_storage) {
	T* vertices;
	if (in_storage) vertices = in_storage;
	else vertices = new T[vCount];
	return vertices;
}

template<class T>
void WriteSpherePos(T* vertices, const uint32_t iCount, const uint32_t vCount, const uint32_t hSeg, const uint32_t vSeg) {

	vertices[0] = { 0, -1, 0 };
	vertices[vCount - 1] = { 0, 1, 0 };

	float x, y, z;
	for (uint32_t i = 1; i < vSeg; i++)
	{
		float vProg = (float)i / (float)vSeg;
		y = (float)-cos(M_PI * vProg);

		for (uint32_t j = 0; j < hSeg; j++)
		{
			float hProg = (float)j / (float)hSeg;
			x = (float)(sin(vProg*M_PI) * cos(2 * M_PI * hProg));
			z = (float)(sin(vProg*M_PI) * sin(2 * M_PI * hProg));

			// 1 : inital vertex, then the offset
			uint32_t id = (1 + (i - 1) * hSeg + j);

			vertices[id] = { x, y, z };
			//vertices[id].uv = { hProg, vProg };
			//vertices[id].normal = { x, y, z };
		}
	}
}


quint32* GenSphereIndices(
	const quint32 iCount, const quint32 vCount, const quint32 hSeg, const quint32 vSeg, quint32* in_storage  /*= nullptr*/)
{
	quint32* indices;

	if (in_storage) indices = in_storage;
	else indices = new quint32[iCount];

	for (quint32 i = 0; i < hSeg; i++)
	{
		indices[i * 3 + 0] = 0;
		indices[i * 3 + 1] = 1 + i;
		indices[i * 3 + 2] = 1 + (i + 1) % hSeg;

		indices[iCount - (i + 1) * 3 + 0] = vCount - 1;
		indices[iCount - (i + 1) * 3 + 1] = vCount - 2 - i;
		indices[iCount - (i + 1) * 3 + 2] = vCount - 2 - (i + 1) % hSeg;

		for (quint32 j = 0; j < vSeg - 2; j++) {
			indices[((j * hSeg + i) * 2 + hSeg) * 3 + 0] = 1 + j * hSeg + i;
			indices[((j * hSeg + i) * 2 + hSeg) * 3 + 1] = 1 + (j + 1) * hSeg + (i + 1) % hSeg;
			indices[((j * hSeg + i) * 2 + hSeg) * 3 + 2] = 1 + j * hSeg + (i + 1) % hSeg;

			indices[((j * hSeg + i) * 2 + hSeg) * 3 + 3] = 1 + j * hSeg + i;
			indices[((j * hSeg + i) * 2 + hSeg) * 3 + 4] = 1 + (j + 1)* hSeg + i;
			indices[((j * hSeg + i) * 2 + hSeg) * 3 + 5] = 1 + (j + 1)* hSeg + (i + 1) % hSeg;
		}
	}

	return indices;
}

template<>
Vector3f* GenSphereVertices(
	const uint32_t iCount, const uint32_t vCount, const uint32_t hSeg, const uint32_t vSeg, Vector3f* in_storage /*= nullptr*/)
{
	Vector3f* vertices = DetermineSphereStorage(vCount, in_storage);
	WriteSpherePos(vertices, iCount, vCount, hSeg, vSeg);
	return vertices;
}

}