#include "GPUBuffer.h"

void GPUBuffer::RequireCapacity(int bytesize, GLenum usage, GLenum target)
{
	if (m_capacity < bytesize || m_capacity > 2 * bytesize || usage != m_usage) {
		Build(bytesize, usage, target);
	}
}

GPUBuffer::GPUBuffer(OpenGLView& func) : f(func)
{

}

void GPUBuffer::Write(int bytes, int offset, void* data, GLenum usage, GLenum target)
{
#ifdef _DEBUG
	if (!(bytes > 0)) DebugBreak();
#endif // _DEBUG
	RequireCapacity(offset + bytes, usage, target);
	f.glBindBuffer(target, m_id);
	f.glBufferData(target, m_capacity, NULL, m_usage); // Buffer orphaning, a common way to improve streaming perf. See above link for details.
	f.glBufferSubData(target, offset, bytes, data);
}

void GPUBuffer::Bind()
{
	Q_ASSERT(ID() > 0);
	f.glBindBuffer(m_target, ID());
}

GLuint GPUBuffer::ID() const
{
	return m_id;
}

bool GPUBuffer::is_written()
{
	return ID() > 0;
}

void GPUBuffer::Build(int bytesize, GLenum usage, GLenum target)
{
	if (m_id == 0) f.glGenBuffers(1, &m_id);

	Q_ASSERT(is_valid_usage(usage));
	Q_ASSERT(is_valid_target(target));
	Q_ASSERT(m_id != 0);

	f.glBindBuffer(target, m_id);
	f.glBufferData(target, bytesize, NULL, usage);
	m_usage = usage;
	m_target = target;
	m_capacity = bytesize;
}

bool GPUBuffer::is_valid_usage(GLenum usage)
{
	switch (usage) {
		case GL_STREAM_DRAW:
		case GL_STREAM_READ:
		case GL_STREAM_COPY:
		case GL_STATIC_DRAW:
		case GL_STATIC_READ:
		case GL_STATIC_COPY:
		case GL_DYNAMIC_DRAW:
		case GL_DYNAMIC_READ:
		case GL_DYNAMIC_COPY:
			return true;
		default:
			return false;
	}

}

bool GPUBuffer::is_valid_target(GLenum target)
{
	return (target == GL_ARRAY_BUFFER || target == GL_ELEMENT_ARRAY_BUFFER);
}

void GPUBuffer::Destroy()
{
	if (m_id == 0) {
		f.glDeleteBuffers(1, &m_id);
		m_id = 0;
	}
}