#include "iinstanceddrawer2d.h"
#include <QDebug>
#include "vis/openglmaster.h"

IInstancedDrawer2D::IInstancedDrawer2D(OpenGLView &func, GLenum dt)
	: IInstancedDrawerBase(func), m_color_buffer(func), m_orientations_buffer(func), m_vertex_buffer(func), m_rotation_buffer(func)
{
	m_shader_id = provide_shader(func.shader_loader());
	m_draw_type = dt;
}

void IInstancedDrawer2D::init(std::vector<Vector2f>& vertex_data, std::function<void(int)> sf)
{
	Q_ASSERT(m_vertex_data.size() == 0, "changing of shape not supported");
	m_vertex_data = vertex_data;
	set_changed(true);
	m_prep_shader_func = sf;
}

void IInstancedDrawer2D::upload_data()
{
#ifdef _DEBUG
	if (m_orientations.size() != m_colors.size()) {
		DebugBreak();
	}
#endif
	Q_ASSERT(m_orientations.size() == m_colors.size());
	auto inst_count = m_orientations.size();

	if (inst_count > 0) 
	{
		m_orientations_buffer.Write(m_orientations, GL_STREAM_DRAW);
		m_color_buffer.Write(m_colors, GL_STATIC_DRAW);
		m_rotation_buffer.Write(m_rotations, GL_STREAM_DRAW);

		if (m_vertex_buffer.ID() == 0) // only the first time
		{
			m_vertex_buffer.Write(m_vertex_data, GL_STATIC_DRAW);
		}
	}

	m_buffered_shape_count = inst_count;
	m_buffered_vertex_count = m_vertex_data.size();
}


void IInstancedDrawer2D::render(RenderContext rc)
{
	{
		QMutexLocker locker(&m_mutex);
		if (m_require_update) {
			m_require_update = false;
			upload_data();
			//qInfo() << "Drawer Upload";
		}
	}
	draw(rc);
}

void IInstancedDrawer2D::destroy()
{
	m_vertex_buffer.Destroy();
	m_orientations_buffer.Destroy();
	m_color_buffer.Destroy();
	m_rotation_buffer.Destroy();

	// f.glDeleteProgram(m_shader); // might be shared, use shared pointers for shaders
}

/*Vector2f IInstancedDrawer2D::pixel_to_gl(int x, int y)
{
	Vector2f sspos = Vector2f{-1,-1} + 2 * Vector2f{ x / (float)m_surface.width, 1.0f - y / (float)m_surface.height };
	Vector2f pos = Vector2f(m_orientation(0), m_orientation(1));
	Vector2f scale = Vector2f(m_orientation(3), 1);
	return sspos.cwiseProduct(scale) + pos;
}*/

int IInstancedDrawer2D::provide_shader(ShaderLoader& sl)
{
	SHADER_FILE_DESCR shader;
	shader.frag = "instanced_shape";
	shader.vert = "instanced_shape";
	return sl.LoadShader(shader);
}

void IInstancedDrawer2D::draw(RenderContext rc)
{
	Q_ASSERT(f.opengl_master().verify_opengl_thread_id());

	if (m_buffered_shape_count == 0 || m_buffered_vertex_count == 0) return;

	Q_ASSERT(m_vertex_buffer.ID());
	Q_ASSERT(m_orientations_buffer.ID());
	Q_ASSERT(m_color_buffer.ID());
	Q_ASSERT(f.glGetError() == 0);

	if (m_vao == 0) {
		f.glGenVertexArrays(1, &m_vao);
		f.glBindVertexArray(m_vao);

		f.glEnableVertexAttribArray(0);
		m_vertex_buffer.Bind();
		f.glVertexAttribPointer(
			0, // attribute. No particular reason for 0, but must match the layout in the shader.
			2, // nr of components
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0, // stride
			(void*)0 // array buffer offset
		);

		Q_ASSERT(f.glGetError() == 0);

		// 2nd attribute buffer : positions of particles' centers
		f.glEnableVertexAttribArray(1);
		m_orientations_buffer.Bind();
		f.glVertexAttribPointer(
			1, // attribute. No particular reason for 1, but must match the layout in the shader.
			4, // element count = 4
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0, // stride
			(void*)0 // array buffer offset
		);

		Q_ASSERT(f.glGetError() == 0);

		// 3rd attribute buffer : particles' colors
		f.glEnableVertexAttribArray(2);
		f.glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer.ID());
		f.glVertexAttribPointer(
			2, // attribute. No particular reason for 2, but must match the layout in the shader.
			4, // element count : r + g + b + a => 4
			GL_UNSIGNED_BYTE, // type
			GL_TRUE, // normalized? YES (because its color!), this means that the unsigned char[4] will be accessible with a vec4 (floats) in the shader
			0, // stride
			(void*)0 // array buffer offset
		);

		Q_ASSERT(f.glGetError() == 0);

		// 4th attribute buffer : particles' matrices
		f.glEnableVertexAttribArray(3);
		f.glBindBuffer(GL_ARRAY_BUFFER, m_rotation_buffer.ID());
		f.glVertexAttribPointer(
			3, // attribute. No particular reason for 3, but must match the layout in the shader.
			4, // nr of componenets
			GL_FLOAT, // type
			GL_FALSE, // normalized? YES (because its color!), this means that the unsigned char[4] will be accessible with a vec4 (floats) in the shader
			0, // stride
			(void*)0 // array buffer offset
		);

		Q_ASSERT(f.glGetError() == 0);

		f.glVertexAttribDivisor(0, 0); // particles vertices : always reuse the same vertices -> 0
		f.glVertexAttribDivisor(1, 1); // orientation : one per quad (its center) -> 1
		f.glVertexAttribDivisor(2, 1); // color : one per quad -> 1
		f.glVertexAttribDivisor(3, 1); // rotation : one per quad -> 1

		Q_ASSERT(f.glGetError() == 0);
	}
	else f.glBindVertexArray(m_vao);

	prep_shader(rc);

	f.glDrawArraysInstanced(m_draw_type, 0,
		(GLsizei)m_buffered_vertex_count,
		(GLsizei)m_buffered_shape_count);

	f.glBindVertexArray(0);

	Q_ASSERT(f.glGetError() == 0);
}

void IInstancedDrawer2D::prep_shader(RenderContext rc)
{
	Q_ASSERT(f.glGetError() == 0);

	m_orientation = rc.orient_2d.to_4_orientation();

	f.glUseProgram(m_shader_id);
	f.glUniform4fv(0, 1, m_orientation.data());
	if (m_prep_shader_func) m_prep_shader_func(m_shader_id);

	Q_ASSERT(f.glGetError() == 0, "InstancedDrawerPrepShader", "Failed to make shader...");
}

IInstancedDrawerBase::IInstancedDrawerBase(OpenGLView& func) : f(func)
{

}
