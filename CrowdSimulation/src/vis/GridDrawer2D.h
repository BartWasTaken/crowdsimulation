#ifndef GRIDDRAWER2D_H
#define GRIDDRAWER2D_H
#include "openglincl.h"
#include "tools/shared_eigen_incl.h"
#include "tools/resizedlistener.h"
#include "shaderloader.h"
#include "iinstanceddrawer2d.h"
#include "GPUBuffer.h"
#include "tools/include.h"

class GridDrawer2D
{
public:
	GridDrawer2D(OpenGLView& func);
	~GridDrawer2D();

	void            make_grid(Vector2f orig, Vector2f size, Vector2i count, bool include_block, bool include_dir, bool include_arrow, float split = 0);
	void            set_block_color(int x, int y, Color);
	void			set_dir_alpha(int x, int y, Dir4::DIR dir, float alpha);
	void			set_dir_color(int x, int y, Dir4::DIR dir, ColorF c);
	void			set_arrow_dir(int x, int y, Vector2f dir_n, float mag);

	void			update(bool block = true, bool dir = true, bool arrow = true);

	void			render(RenderContext rc, bool block = true, bool dir = true, bool arrow = true);
	OpenGLView&		get_func();

	Vector2f		position();
	Vector2f		scale();

protected:
	void		write_shape_dir(std::vector<Vector2f>& out_data);
	void		write_shape_square(std::vector<Vector2f>& out_data, float delta);
	void		write_arrow_shape(std::vector<Vector2f>& out_data);

private:
	Color		get_dir_color(Vector2f v_n);

	Vector2f			m_cell_scale;

	Vector2i			m_dim;
	IInstancedDrawer2D	m_grid_drawer;
	IInstancedDrawer2D	m_dir_drawer;
	IInstancedDrawer2D	m_arrow_drawer;
	OpenGLView&			m_func;

	std::vector<ColorF>	m_dir_colors;

	bool				m_has_block = false;
	bool				m_has_dir = false;
	bool				m_has_arrow = false;
};

#endif // GRIDDRAWER2D_H
