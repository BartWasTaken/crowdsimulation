#pragma once

#include <QWindow>
#include <QMainWindow>
#include <QFormLayout>

#include "vis/openglmaster.h"
#include "programmanager.h"
#include "tools/INonCopyable.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow 
	: public QMainWindow, Tools::INonCopyable
{
    Q_OBJECT

public:

	static MainWindow& Create(QWidget *parent = 0);
    ~MainWindow();

	OpenGLView& opengl();
    void simulationFromFile();
    void step();
	QMenu* add_menu(QString name);

	StatsView* getStatsView();
	QFormLayout* getEventForm();

    inline bool is_closed() {return m_closed;}

	static inline MainWindow* gInst() { return s_gInst; }

	InputManager* get_inputmanager() {
		return &input;
	}


protected:
    void closeEvent(QCloseEvent * event) override;;


	virtual void mousePressEvent(QMouseEvent *event) override;


	virtual void mouseReleaseEvent(QMouseEvent *event) override;


	virtual void mouseMoveEvent(QMouseEvent *event) override;


	virtual void wheelEvent(QWheelEvent *event) override;


	virtual void keyPressEvent(QKeyEvent *event) override;


	virtual void keyReleaseEvent(QKeyEvent *event) override;


	virtual void focusInEvent(QFocusEvent *event) override;


	virtual void focusOutEvent(QFocusEvent *event) override;

private:

	MainWindow(QWidget *parent = 0);

	void my_setupui();

	static MainWindow* s_gInst;

	Ui::MainWindow* m_ui = nullptr;
	ProgramManager* m_pm = nullptr;
    bool m_closed = false;

    QString m_last_sim_load_filename = "";
	EventBasedInputManager input;
};
