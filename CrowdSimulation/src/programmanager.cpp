#include "programmanager.h"
#include <QFileDialog>
#include <QDebug>
#include <QtMath>
#include "mainwindow.h"
#include "sim/Engines/testsimulation.h"
#include "sim/Engines/sphengine.h"
#include "sim/Engines/continuumengine.h"
#include "sim/DirectPlanner.h"
#include "src/files/scenefilereader.h"
#include "src/tools/scenebuilder.h"
#include "src/tools/polygon2d.h"

class PerfTester {

	struct Test {
		QString engine_name;
		QString scene_path;
	};

	std::vector<Test> tests;

	PerfTester() {
		tests.push_back({"", "D:/Files/Documents/1AAA_scenes/bulding_1.json"});
	}

	void step(SimulationCore* sc) {
		
	}
};

struct BottomHole {
	bool enabled = false;
	float width;
};

std::vector<std::vector<Vector2f>> make_outline_shapes(Vector2f center, Vector2f range, float size_in, float size_out, BottomHole hole = BottomHole()) {
	range /= 2;
	if (hole.enabled) {
		std::vector<Vector2f> main_square {
			center + range.cwiseProduct(Vector2f{ -hole.width, -size_out}),
			center + range.cwiseProduct(Vector2f{ -size_out, -size_out }),
			center + range.cwiseProduct(Vector2f{ -size_out, size_out }),
			center + range.cwiseProduct(Vector2f{ size_out, size_out }),
			center + range.cwiseProduct(Vector2f{ size_out, -size_out }),
			center + range.cwiseProduct(Vector2f{ hole.width, -size_out}),

			center + range.cwiseProduct(Vector2f{ hole.width, -size_in}),
			center + range.cwiseProduct(Vector2f{ size_in, -size_in }),
			center + range.cwiseProduct(Vector2f{ size_in, size_in }),
			center + range.cwiseProduct(Vector2f{ -size_in, size_in }),
			center + range.cwiseProduct(Vector2f{ -size_in, -size_in }),
			center + range.cwiseProduct(Vector2f{ -hole.width, -size_in}),
		};
		return { main_square };
	}
	else {
		std::vector<Vector2f> main_square_a{
			center + range.cwiseProduct(Vector2f{ -size_out,-size_out }),
			center + range.cwiseProduct(Vector2f{ -size_out, size_out }),
			center + range.cwiseProduct(Vector2f{ size_out, size_out }),
			center + range.cwiseProduct(Vector2f{ size_out, size_in }),
			center + range.cwiseProduct(Vector2f{ -size_in, size_in }),
			center + range.cwiseProduct(Vector2f{ -size_in, -size_out }),
		};
		std::vector<Vector2f> main_square_b{
			center + range.cwiseProduct(Vector2f{ -size_out,-size_out }),
			center + range.cwiseProduct(Vector2f{ -size_out, -size_in }),
			center + range.cwiseProduct(Vector2f{ size_in, -size_in }),
			center + range.cwiseProduct(Vector2f{ size_in, size_out }),
			center + range.cwiseProduct(Vector2f{ size_out, size_out }),
			center + range.cwiseProduct(Vector2f{ size_out, -size_out }),
		};
		return { main_square_a, main_square_b };
	}
}

void make_side_env(SimulationCore& sc, OpenGLView& f, QJsonObject& settings, std::vector<Obstacle>& obstacles, AABB2 bbox, bool add_obstacle)
{
	Vector2f center = bbox.min() + bbox.sizes() * 0.5f;

	float cs_scale = 0.45f;
	std::vector<Vector2f> center_square{
		center + bbox.sizes().cwiseProduct(Vector2f{ -0.5,-0.5f }) * cs_scale,
		center + bbox.sizes().cwiseProduct(Vector2f{ -0.5, 0.5f }) * cs_scale,
		center + bbox.sizes().cwiseProduct(Vector2f{ 0.5f,0.5f }) * cs_scale,
		center + bbox.sizes().cwiseProduct(Vector2f{ 0.5f,-0.5f }) * cs_scale
	};

	QString name;
	name = add_obstacle ? "Side (Obst)" : "Sides";

	{ // Sides environment
		IEnvironment_ptr pEnv = IEnvironment_ptr{ new IEnvironment(f, bbox, name) };

		// agent placer
		/*auto distributor_f = [](SimulationCore& sc, std::function<float()> rgen)
		{
			int nr_agents = sc.nr_active_agents();
			float sides_type_offset = 0.2f;

			for (int i = 0; i < nr_agents; i++)
			{
				auto rx = rgen();
				if (rx < 0.5f) rx = -1.0f + rx * 2 * sides_type_offset;
				else rx = 1.0f - (rx - 0.5f) * 2 * sides_type_offset;
				auto ry = -1 + 2 * rgen();

				if ((int)((ry + 1) * 0.5f * 20.0f) % 2 == 0) {
					ry += 0.1f;
				}

				sc.position(i) = Vector2f{ rx, ry };
			}
		};
		pEnv->set_distributor(distributor_f);
		*/
		{
			float sides_type_offset = 0.1f * bbox.sizes()(0);

			AABB2 left_side = {
				bbox.corner(AABB2::BottomLeft),
				bbox.corner(AABB2::TopLeft) + Vector2f{ sides_type_offset, 0 } };
			AABB2 right_side = {
				bbox.corner(AABB2::BottomRight) - Vector2f{ sides_type_offset, 0 },
				bbox.corner(AABB2::TopRight)
			};

			auto lGoal = IGoal_ptr{ new SquareGoal(left_side) };
			auto rGoal = IGoal_ptr{ new SquareGoal(right_side) };

			lGoal->goal_inner_direction() = Vector2f{ -1,0 };
			rGoal->goal_inner_direction() = Vector2f{ 1,0 };

			pEnv->add_goal(lGoal);
			pEnv->add_goal(rGoal);

			Polygon2D left_area{ bbox_to_polygon(left_side) };
			Polygon2D right_area{ bbox_to_polygon(right_side) };

			SpawnArea a{ left_area };
			SpawnArea b{ right_area };

			pEnv->add_spawn_area(a);
			pEnv->add_spawn_area(b);
		}


		if (add_obstacle) {
			std::vector<Vector2f> vertices{
				center_square[0] * 0.75f,
				center_square[1] * 0.75f,
				center_square[2] * 0.75f,
				center_square[3] * 0.75f
			};
			Obstacle obstacle(sc.settings().agent_rad(), vertices);
			pEnv->add_obstacle(obstacle);
		}

		for (auto& obst : obstacles) {
			pEnv->add_obstacle(obst);
		}

		pEnv->set_settings(settings);
		sc.add_environment(pEnv);
	}
}

void make_smoke_env(SimulationCore& sc, OpenGLView& f)
{
	AABB2 shape = { Vector2f{0,0}, Vector2f{12, 5} };
	Vector2f center = shape.corner(AABB2::BottomLeft) + shape.sizes() / 2;
	Vector2f range = shape.sizes();
	QString name = "Smoke lane";

	float area_ratio_h = 0.1f;
	AABB2 spawn_bbox = {
		shape.corner(AABB2::BottomLeft),
		shape.corner(AABB2::TopLeft) + Vector2f{range(0) * area_ratio_h, 0}
	};
	AABB2 goal_bbox = {
		shape.corner(AABB2::BottomRight) - Vector2f{range(0) * area_ratio_h, 0},
		shape.corner(AABB2::TopRight)
	};

	auto base_polygon = bbox_to_polygon(shape);

	auto pEnv = IEnvironment_ptr{ new IEnvironment(f, shape, name) };

	SimulationSettings_ptr settings_obj = SimulationSettings_ptr::create(); // default
	{
		SimulationSettings& s = *settings_obj;
#ifdef _DEBUG
		s.agent_count(100);
#else
		s.agent_count(3e2);
#endif
		float agents_per_unit = s.agent_count() / (range(0) * range(1));
		auto rad = 0.135f / qSqrt(agents_per_unit);
		s.agent_rad(rad);
		s.agent_speed(rad * 5);
		s.mde_max_iter(5);
		s.sph_stiffness(0.25f);
		s.sph_kernel_rad(0);
		s.mde_max_iter(4);
		s.sph_max_iter_dens(4);
		s.sph_max_dens_coef(0.4f);
	}
	auto settings = settings_obj->to_json();
	pEnv->set_settings(settings);

	auto goal = IGoal_ptr{ new SquareGoal(goal_bbox) };
	goal->goal_inner_direction() = Vector2f{ 1,0 };
	pEnv->add_goal(goal);

	Polygon2D left_area{ bbox_to_polygon(spawn_bbox) };
	SpawnArea spawn_area{ left_area };
	pEnv->add_spawn_area(spawn_area);

	auto outline = make_outline_shapes(center, range, 1.0, 1.1);
	for (auto& o : outline) {
		Obstacle boundary(sc.settings().agent_rad(), o);
		pEnv->add_obstacle(boundary);
	}

	SmokeField::FireEvent fe;
	fe.coordinates = center;
	fe.intensity = 10;
	fe.radius = range(1) * 0.2f;
	fe.falloff = .5;
	fe.time_seconds = 0;
	pEnv->smoke().add_event(fe);

	sc.add_environment(pEnv);
}

void make_multi_level_env(SimulationCore& sc, OpenGLView& f)
{
	// build base shape
	float floor_w = 30, floor_h = 30;
	float inter_space = 1.5;
	int floor_count = 7;
	float goal_width = 0.4f;
	std::vector<AABB2> floors;

	for (int i = 0, n = floor_count; i < n; i++) {
		float left = i * floor_w + i * inter_space;
		float right = left + floor_w;
		floors.push_back(AABB2{ Vector2f{left, 0}, Vector2f{right, floor_h} });
	}

	float total_area = 0;
	for (int i = 0, n = floors.size(); i < n; i++) {
		auto r = floors[i].sizes();
		total_area += r(0) * r(1);
	}

	// compute shape
	AABB2 shape = { floors[0].corner(AABB2::BottomLeft), floors[floors.size() - 1].corner(AABB2::TopRight) };

	// allocate new environment
	QString name = "Multilevel";
	auto pEnv = IEnvironment_ptr{ new IEnvironment(f, shape, name) };

	// set settings
	SimulationSettings_ptr settings_obj = SimulationSettings_ptr::create(); // default
	{
		SimulationSettings& s = *settings_obj;
#ifdef _DEBUG
		s.agent_count(100);
#else
		s.agent_count(5e2);
#endif
		float agents_per_unit = s.agent_count() / total_area;
		auto rad = 0.145f / qSqrt(agents_per_unit);
		s.agent_rad(rad);
		s.agent_speed(rad * 5);
		s.mde_max_iter(6);
		s.sph_stiffness(0.2f);
		s.sph_kernel_rad(0);
		s.mde_max_iter(4);
		s.sph_max_iter_dens(4);
		s.sph_max_dens_coef(0.3f);
	}
	auto settings = settings_obj->to_json();
	pEnv->set_settings(settings);


	float g_left = 0.5f - 0.5f * goal_width;
	float g_right = 0.5f + 0.5f * goal_width;
	std::vector<Vector2f> goal_shape{
		Vector2f{g_left * floor_w, 0},
		Vector2f{g_left * floor_w, 0.03f * floor_h},
		Vector2f{g_right * floor_w, 0.03f * floor_h},
		Vector2f{g_right * floor_w, 0}
	};
	IGoal_ptr og = IGoal_ptr{ new ObstacleGoal(goal_shape) };
	og->goal_inner_direction() = {0, -1};
	pEnv->add_goal(og);

	// staircase bases:
	float width = 0.3f;
	float height = 0.5f;
	float v_low = (0.5 - height / 2) * floor_h;
	float v_high = (0.5 + height / 2) * floor_h;
	std::vector<Vector2f> staircase_l = {
		Vector2f{(0.5f - width / 2) * floor_w, v_low},
		Vector2f{(0.5f - width / 2) * floor_w, v_high},
		Vector2f{0.5f * floor_w, v_high},
		Vector2f{0.5f * floor_w , v_low}
	};
	std::vector<Vector2f> staircase_r = {
		Vector2f{0.5f * floor_w, v_low},
		Vector2f{0.5f * floor_w, v_high},
		Vector2f{(0.5f + width / 2) * floor_w, v_high},
		Vector2f{(0.5f + width / 2) * floor_w, v_low}
	};

	// build floors
	for (int i = 0, n = floors.size(); i < n; i++)
	{
		Vector2f anchor = floors[i].corner(AABB2::BottomLeft);

		// obstacle
		std::vector<std::vector<Vector2f>> outline;
		if (i == 0) outline = make_outline_shapes(floors[i].center(), floors[i].sizes(), 1.0f, 1.0f + 0.2f * inter_space / floor_w, { true, goal_width });
		else outline = make_outline_shapes(floors[i].center(), floors[i].sizes(), 1.0f, 1.0f + 0.2f * inter_space / floor_w);
		for (auto& o : outline) {
			Obstacle boundary(sc.settings().agent_rad(), o);
			pEnv->add_obstacle(boundary);
		}

		// elevation
		AABB2 extended_floor_box{
			floors[i].corner(AABB2::BottomLeft) - 0.4f * Vector2f{inter_space, inter_space},
			floors[i].corner(AABB2::TopRight) + 0.4f * Vector2f{inter_space, inter_space},
		};
		ElevationArea elevation;
		elevation.polygon = bbox_to_polygon(extended_floor_box);
		elevation.height = i * 4;
		elevation.translation = -anchor;
		pEnv->add_elevation(elevation);

		// spawnarea
		SpawnArea spawn_area{ bbox_to_polygon(floors[i]) };
		pEnv->add_spawn_area(spawn_area);

		// staircase
		// two staircase areas, one down one up, unless first or last floor

		// add up stair, if not on the last floor
		if (i != n - 1) {
			// if on an even floor, 
			if (i % 2 == 0) {
				std::vector<Vector2f> area = staircase_l;
				for (auto& v : area) v += anchor;
				StairCaseArea staircase_left_up(area, floor_w * 0.01, i, -1, 1, -1, 3);
				pEnv->add_staircase(staircase_left_up);
			}
			// else on an uneven floor
			else {
				std::vector<Vector2f> area = staircase_r;
				for (auto& v : area) v += anchor;
				StairCaseArea staircase_right_up(area, floor_w * 0.01, i, -1, 3, -1, 1);
				pEnv->add_staircase(staircase_right_up);
			}
		}
		// add down stair, if not on the lowest floor
		if (i != 0) {
			// if on an even floor, 
			if (i % 2 == 0) {
				std::vector<Vector2f> area = staircase_r;
				for (auto& v : area) v += anchor;
				StairCaseArea staircase_right_down(area, floor_w * 0.01, -1, i - 1, -1, 3, -1);
				pEnv->add_staircase(staircase_right_down);
			}
			// else on an uneven floor
			else {
				std::vector<Vector2f> area = staircase_l;
				for (auto& v : area) v += anchor;
				StairCaseArea staircase_left_down(area, floor_w * 0.01, -1, i - 1, -1, 1, -1);
				pEnv->add_staircase(staircase_left_down);
			}
		}


	}


	/////////
	sc.add_environment(pEnv);
}

ProgramManager::ProgramManager(OpenGLMaster& opengl, MainWindow& mw)
	: f(opengl.getFunc())
{
	auto default_settings = SimulationSettings_ptr::create();
	m_sim = new SimulationCore(f, mw, default_settings);
	//m_scene_builder = new SceneBuilder(*this, opengl);
}


ProgramManager::~ProgramManager()
{
	SafeDeletePtr(m_sim);
}

void ProgramManager::simulationFromFile(QString filename)
{
	qInfo() << "Loading scene from" << filename;

	QJsonDocument doc = read_json(filename);

	if (doc.isNull() || doc.isEmpty()) {
		qInfo() << "Could not read json from: ";
		qInfo() << filename;
		qInfo() << ", empty or unreadable\n";
	}
	else {
		SceneFileReader::Read(*m_sim, f, doc, true);

		//SimulationSettings_ptr s = SimulationSettings_ptr::create(doc.object());
		//m_sim->set_settings(s);
	}
}

void ProgramManager::simulationDefault()
{
	Vector2f min{ -40, -40 };
	Vector2f max{ 40, 40 };
	Vector2f range = max - min;
	AABB2 bbox{ min,max };

	Vector2f center = min + range * 0.5f;
	float cs_scale = 0.5f;

	float size_out = 1.1f;
	float size_in = 1.0f;

	auto outline = make_outline_shapes(center, range, size_in, size_out);
	std::vector<Obstacle> boundary;

	for (auto& o : outline) {
		Obstacle obst(m_sim->settings().agent_rad(), o);
		boundary.push_back(obst);
	}

	std::vector<Vector2f> center_square{
		center + range.cwiseProduct(Vector2f{ -0.5,-0.5f }) * cs_scale,
		center + range.cwiseProduct(Vector2f{ -0.5, 0.5f }) * cs_scale,
		center + range.cwiseProduct(Vector2f{ 0.5f,0.5f }) * cs_scale,
		center + range.cwiseProduct(Vector2f{ 0.5f,-0.5f }) * cs_scale
	};

	SimulationSettings_ptr settings_obj = SimulationSettings_ptr::create(); // default
	{
		SimulationSettings& s = *settings_obj;
#ifdef _DEBUG
		s.agent_count(100);
#else
		s.agent_count(3e3);
#endif
		float agents_per_unit = s.agent_count() / (range(0) * range(1));
		auto rad = 0.17f / qSqrt(agents_per_unit);
		s.agent_rad(rad);
		s.agent_speed(rad * 5);
		s.mde_max_iter(5);
		s.sph_stiffness(0.25f);
		s.sph_kernel_rad(0);
		s.mde_max_iter(4);
		s.sph_max_iter_dens(4);
		s.sph_max_dens_coef(0.4f);
	}
	auto settings = settings_obj->to_json();

	auto sph = SPHEngine_ptr::create(f);
	auto test = TestSimulation_ptr::create();
	auto cont = ContinuumEngine_ptr::create(f);
	auto simple_planner = DirectPlanner_ptr::create();

	auto cont_planner_min = ContinuumEngine::MinPotGoalPicker_ptr::create(
		cont->get_continuum_field(), ContinuumEngine::MinPotGoalPicker::PickerType::MIN);

	auto cont_planner_prob = ContinuumEngine::MinPotGoalPicker_ptr::create(
		cont->get_continuum_field(), ContinuumEngine::MinPotGoalPicker::PickerType::PROBABILISTIC);

	m_sim->add_goalpicker(cont_planner_min);
	m_sim->add_goalpicker(cont_planner_prob);
	m_sim->add_goalpicker(simple_planner);

	EngineSetup setup1{
		qSharedPointerDynamicCast<IGlobalPanner>(cont),
		sph,
		"Continuum | SPH"
	};
	m_sim->add_engine(setup1);

	EngineSetup setup2{
		qSharedPointerDynamicCast<IGlobalPanner>(simple_planner),
		sph,
		"Simple | SPH"
	};
	m_sim->add_engine(setup2);

	EngineSetup setup3{
		qSharedPointerDynamicCast<IGlobalPanner>(simple_planner),
		test,
		"Test"
	};
	m_sim->add_engine(setup3);

	// agent goal decider
	auto fartherst_goal_picker_f = [](SimulationCore& sc, VecX<int>& ids)
	{
		// for all agents
		// check dist to all goals
		// pick ferthest

		auto& goals = sc.environment().goals();

		int nr_agents = sc.nr_active_agents();
		int goal_count = goals.size();

		for (int i = 0; i < nr_agents; i++)
		{
			int farthest_gid = 0;
			float farthest_dist = 0;

			auto pos = sc.position(i);

			for (int gid = 0; gid < goal_count; gid++)
			{
				auto surface = goals[gid]->closest_point(pos);
				auto dist = (pos - surface).squaredNorm();
				if (dist > farthest_dist) {
					farthest_dist = dist;
					farthest_gid = gid;
				}
			}

			ids(i) = farthest_gid;
		}
	};

	auto pGoalPicker = GoalPickerDynamic_ptr::create("Farthest", fartherst_goal_picker_f);
	m_sim->add_goalpicker(pGoalPicker);

	{ // Square in the middle environment
		auto pEnv = IEnvironment_ptr{ new IEnvironment(f, { min, max }, "Center") };
		Vector2f center = min + range * 0.5f;
		float cs_scale = 0.5f;
		std::vector<Vector2f> center_square{
			center + range.cwiseProduct(Vector2f{ -0.5,-0.5f }) * cs_scale,
			center + range.cwiseProduct(Vector2f{ -0.5, 0.5f }) * cs_scale,
			center + range.cwiseProduct(Vector2f{ 0.5f,0.5f }) * cs_scale,
			center + range.cwiseProduct(Vector2f{ 0.5f,-0.5f }) * cs_scale
		};
		IGoal_ptr og = IGoal_ptr{ new ObstacleGoal(center_square) };
		pEnv->add_goal(og);
		for (auto & o : boundary) pEnv->add_obstacle(o);
	
		pEnv->set_settings(settings);

		m_sim->add_environment(pEnv);
	}

	{ // Square in the middle environment with obstruction
		auto pEnv = IEnvironment_ptr{ new IEnvironment(f, { min, max }, "Center | obstr") };

		IGoal_ptr og = IGoal_ptr{ new ObstacleGoal(center_square) };

		pEnv->add_goal(og);

		float obst_ratio = 0.75f;

		std::vector<Vector2f> vertices{
			center_square[0] * obst_ratio,
			center_square[1] * obst_ratio,
			center_square[2] * obst_ratio,
			center_square[3] * obst_ratio
		};
		Obstacle obstacle(m_sim->settings().agent_rad(), vertices);
		pEnv->add_obstacle(obstacle);
		for (auto & o : boundary) pEnv->add_obstacle(o);
		pEnv->set_settings(settings);

		m_sim->add_environment(pEnv);
	}


	make_side_env(*m_sim, f, settings, boundary, bbox, true);
	make_side_env(*m_sim, f, settings, boundary, bbox, false);
	make_smoke_env(*m_sim, f);
	make_multi_level_env(*m_sim, f);

	//m_sim->add_environment(IEnvironment_ptr{ new SquareEnvironment({ min, max }, SquareEnvironment::CORNER) });
	//m_sim->add_environment(IEnvironment_ptr{ new SquareEnvironment({ min, max }, SquareEnvironment::SIDES) });

	//m_sim->add_engine(TestSimulation_ptr::create(f));
	//m_sim->add_engine(SPHEngine_ptr::create(f));
	////m_sim->add_engine(TestSim2_ptr::create(f));
	//m_sim->add_engine(ContinuumEngine_ptr::create(f));
}

void ProgramManager::render(RenderContext rc)
{
	m_sim->render(rc);
}



void ProgramManager::step()
{


	m_sim->update();
}
