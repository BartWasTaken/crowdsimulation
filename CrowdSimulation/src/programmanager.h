#ifndef PROGRAMMANAGER_H
#define PROGRAMMANAGER_H
#include <QString>
#include "shared.h"
#include "src/sim/simulationsettings.h"
#include "src/sim/simulationcore.h"
#include "src/vis/irenderable.h"
#include "src/ui/statsview.h"

class SceneBuilder;

class ProgramManager 
	: public IRenderable
{
public:
    ProgramManager(OpenGLMaster&, MainWindow&);
	~ProgramManager();

    void simulationFromFile(QString);
	void simulationDefault();

	virtual void render(RenderContext) override;
	void step();

	inline SimulationCore* get_sim_core() const { return m_sim; }

private:

	SimulationCore* m_sim;
	SceneBuilder* m_scene_builder;
	OpenGLView& f;
};

#endif // PROGRAMMANAGER_H
