#pragma once

#include<QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <type_traits>

bool write_json(QString filename, QJsonArray write_data);
bool write_json(QString filename, QJsonObject write_data);
bool write_string(QString filename, QString txt);

QJsonDocument read_json(QString filename);