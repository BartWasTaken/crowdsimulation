#ifndef STATSVIEW_H
#define STATSVIEW_H

#include "src/sim/simulationcore.h"

#include <QObject>
#include <QTableView>
#include "tools/timer.h"

#include <QJsonObject>
#include <QMenu>
#include <QDialog>
#include <QTableView>
#include <QSharedPointer>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QMutex>

class StatsView : public QTableView
{
	Q_OBJECT
public:
	StatsView(QWidget* parent);
	~StatsView();

	void update(SimulationStats&);

	void renew();

	void update_ui();


private:

	std::vector<std::vector<QString>> get_display_strings(SimulationStats::FrameData& last, SimulationStats::FrameData& avg);

	QStandardItemModel*				m_model = nullptr;

	//QList<QList<QStandardItem*>>	m_table_data;

	QMutex						m_cache_mutex;
	SimulationStats::FrameData	m_last;
	SimulationStats::FrameData	m_avg;
	Timer						m_refresh_timer;

	std::vector<std::vector<QString>>	m_avg_backup;
};

#endif // STATSVIEW_H