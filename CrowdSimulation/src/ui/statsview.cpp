#include "statsview.h"

#include <QDebug>
#include <QFormLayout>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QHeaderView>
#include <QDialogButtonBox>
#include <QtGlobal>

StatsView::StatsView(QWidget* parent)
	: QTableView(parent)
{
	setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

	verticalHeader()->hide();
	//horizontalHeader()->hide();
	//setEditTriggers(QAbstractItemView::NoEditTriggers);
	setShowGrid(false);

	// make the ui model
	m_model = new QStandardItemModel();

	setModel(m_model);
}

StatsView::~StatsView()
{
	SafeDeletePtr(m_model);
}

void StatsView::update(SimulationStats& ss)
{
	QMutexLocker locker(&m_cache_mutex);
	m_last = ss.last();
	m_avg = ss.avg();
}

void StatsView::renew()
{

}

void StatsView::update_ui()
{
	bool do_avg_ui_update = false;
	if (!m_refresh_timer.is_running()) {
		m_refresh_timer.start();
		do_avg_ui_update = true;
	}
	else if (m_refresh_timer.elapsed_sec() > 0.5f) {
		m_refresh_timer.restart();
		do_avg_ui_update = true;
	}

	QMutexLocker locker(&m_cache_mutex);
	auto last = m_last;
	auto avg = m_avg;
	locker.unlock();

	auto new_display_strings = get_display_strings(last, avg);
	if (do_avg_ui_update) m_avg_backup = new_display_strings;

	m_model->clear();
	m_model->setHorizontalHeaderLabels(QStringList() << ("Variable") << ("Value") << ("Recent Avg."));

	for (int i = 0; i < new_display_strings.size(); i++)
	{
		QList<QStandardItem*> row;

		for (int j = 0; j < new_display_strings[i].size(); j++)
		{
			QString* pString = &new_display_strings[i][j];

			if (j == 2 && !do_avg_ui_update) { // average column
				//Q_ASSERT(m_avg_backup.size() > i && m_avg_backup[i].size() > j);
				if (m_avg_backup.size() <= i) {
					m_avg_backup.push_back({});
				}
				else if (m_avg_backup[i].size() <= j) {
					m_avg_backup[i].push_back({"..."});
				}
				else pString = &m_avg_backup[i][j];
			}

			auto item = new QStandardItem(*pString);
			item->setFlags(Qt::ItemIsEnabled);
			row.append(item);
		}

		m_model->appendRow(row);
	}

	resizeColumnsToContents();

	for (int i = 0; i < 3; i++) {
		if (columnWidth(i) < 70) setColumnWidth(i, 70);
	}	
}

std::vector<std::vector<QString>> StatsView::get_display_strings(SimulationStats::FrameData& last, SimulationStats::FrameData& avg)
{
	std::vector<std::vector<QString>> result;

	for (int i = 0; i < StatVars::NR_VARS; i++) {
		result.push_back({
			StatVars::var_names[i],
			QString::number(last.var(i), 'f', StatVars::var_decimals[i]),
			StatVars::do_avg[i] ? QString::number(avg.var(i), 'f', StatVars::var_decimals[i]) : QString("---")
		});

		if (i == StatVars::DT_MS) 
		{
			auto& timers_avg = avg.timers();
			auto& timers_last = last.timers();

			for(int j = 0; j < timers_avg.size(); j++) {
				result.push_back({
				timers_last[j].name,
					QString::number(timers_last[j].elapsed_ms(), 'f', 2),
					QString::number(timers_avg[j].elapsed_ms(), 'f', 2)
				});
			}
		}
	}

	return result;
}
