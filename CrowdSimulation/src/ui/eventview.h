#ifndef EVENTVIEW_H
#define EVENTVIEW_H

#include "sim/IEnvironment.h"
#include "sim/simdeclr.h"
#include <QFormLayout>
#include <QObject>
#include <memory>
#include <QCheckBox>

class EventView : QObject
{
	Q_OBJECT
public:
	EventView(QFormLayout*);
	~EventView();

	void reset_ui(SimulationCore& sc, std::vector<IEnvironment::NewObstacleEvent>& events, bool keep_eventstates);
	void update_ui();

	void checkbox_act(ObstacleEventID::Ref);

private:
	QFormLayout*	m_pForm;

	struct UIEntry {
		QCheckBox* pCheckBox;
		IEnvironment::NewObstacleEvent* pEvent;
	};
	std::map<ObstacleEventID::Ref, UIEntry> m_entries;
};

#endif // EVENTVIEW_H