#include "eventview.h"

#include <QGroupBox>

#include <QLabel>
#include <QLineEdit>

#include "sim/simulationcore.h"
#include "mainwindow.h"

EventView::EventView(QFormLayout* pLayout)
{
	m_pForm = pLayout;
}

EventView::~EventView()
{

}

void clearLayout(QLayout *layout)
{
	QLayoutItem *child;
	while ((child = layout->takeAt(0)) != 0) {
		if (child->layout() != 0)
			clearLayout(child->layout());
		else if (child->widget() != 0)
			delete child->widget();

		delete child;
	}
}

void EventView::reset_ui(SimulationCore& sc, std::vector<IEnvironment::NewObstacleEvent>& events, bool keep_eventstates)
{
	// auto formGroupBox = new QGroupBox(QObject::tr("Form layout"));

	m_entries.clear();
	clearLayout(m_pForm);

	for (auto& e : events) 
	{
		auto id = e.id.make_ref();

		auto* pCheckBox = new QCheckBox();
		{
			QObject::connect(pCheckBox, &QCheckBox::stateChanged, this, [this, id] { checkbox_act(id); });
			m_pForm->addRow(new QLabel(e.get_name()), pCheckBox);
			pCheckBox->setCheckable(e.is_manually_activated());
		}

		m_entries[id] = { pCheckBox, &e };

		if (keep_eventstates) {
			if (e.is_manually_activated()) {
				pCheckBox->setChecked(true);
			}
		}
	}
}

void EventView::update_ui()
{
	// every iteration
}

void EventView::checkbox_act(ObstacleEventID::Ref id)
{
	Q_ASSERT(m_entries.find(id) != m_entries.end());

	auto result = m_entries.find(id);

	if (result != m_entries.end()) {
		UIEntry& e = result->second;
		e.pEvent->activate_manual(e.pCheckBox->isChecked());
		//e.pCheckBox->setChecked(true);
	}
}
