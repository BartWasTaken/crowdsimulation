#include "shared.h"
#include <QDebug>

template<typename T>
bool write_json(QString filename, T write_data)
{
    QFile f(filename);

   if (!f.open(QIODevice::Text | QIODevice::WriteOnly)) {
       return false;
   }

   QJsonDocument jDoc(write_data);

   f.write(jDoc.toJson());
   return true;
}

bool write_json(QString filename, QJsonArray write_data)
{
	return write_json<QJsonArray>(filename, write_data);
}

bool write_json(QString filename, QJsonObject write_data)
{
	return write_json<QJsonObject>(filename, write_data);
}

bool write_string(QString filename, QString txt) {
	QFile f(filename);
	if (!f.open(QIODevice::Text | QIODevice::WriteOnly)) {
		return false;
	}
	f.write(txt.toUtf8().data());
	return true;
}

QJsonDocument read_json(QString filename)
{
    QFile f(filename);

    if (!f.exists() || !f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return QJsonDocument();
    }

	QJsonParseError error;
    QJsonDocument jDoc = QJsonDocument::fromJson(f.readAll(), &error);
	qDebug() << "Read json result:" << error.errorString();

    return jDoc;
}
