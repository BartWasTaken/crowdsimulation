#include "ObjLoader.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <QDebug>
//#include "CGMain/src/CGMain/IO/ObjectWriter.h"
#include "VertexDataInterlaced.h"
#include <unordered_map>

namespace CGMain {
namespace IO {
namespace LoaderTypes {

inline Vector3f NormalFromAdjPointsCCW(const Vector3f& base, const Vector3f& v1, const Vector3f& v2)
{
	return (v1 - base).cross(v2 - base);
}

int exists(std::vector<Vertice>& list, int pID, int uvID, int nID)
{
	for (unsigned int i = 0; i < list.size(); i++)
	{
		if (list[i].pID == pID && list[i].uvID == uvID && list[i].nID == nID) {
			return i;
		}
	}
	return -1;
}
int existsSmooth(std::vector<Vertice>& list, int pID, int uv)
{
	for (unsigned int i = 0; i < list.size(); i++)
	{
		if (list[i].pID == pID && list[i].uvID == uv) {
			return i;
		}
	}
	return -1;
}
int existsVerticeSorted(std::vector<Vertice>& vertices, std::vector<int>& refList, int pID, int uvID, int nID)
{
	for (unsigned int i = 0; i < refList.size(); i++)
	{
		if (/*vertices[refList[i]].pID == pID &&*/ vertices[refList[i]].nID == nID && vertices[refList[i]].uvID == uvID) {
			return refList[i];
		}
	}
	return -1;
}

int existsVerticeSortedSmooth(std::vector<Vertice>& vertices, std::vector<int>& refList, int pID, int uVID)
{
	for (unsigned int i = 0; i < refList.size(); i++)
	{
		if (vertices[refList[i]].uvID == uVID) {
			return refList[i];
		}
	}
	return -1;
}

std::vector<CGMain::IVertexData*> ObjLoader::LoadObject(LoadDescription& ldescr)
{
	std::ifstream fileStream(ldescr.full_path);

	if (!fileStream.good())
	{
		if (ldescr.hint_print_info >= LoadDescription::PrintInfo::Basic) {
			std::cout << "ERROR: cannot read file" << ldescr.full_path << "\n";
		}
		return {};
	}

	LoadState load_state(fileStream, ldescr);

	int read_iteration = 0;

	load_state.NextLine();

	std::vector<CGMain::IVertexData*> load_objects;

	while (SkipToNextObject(load_state))
	{
		if (ldescr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
			std::cout << "INFO | Start reading | object: " << read_iteration << "\n";
			read_iteration++;
		}

		if (IVertexData* obj_load_data = StartObjectRead(load_state)) {
			load_objects.push_back(obj_load_data);
		}
	}
	return load_objects;
}

bool ObjLoader::SkipToNextObject(LoadState& load_state)
{
	if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
		std::cout << "\nChecking for object ... ";
	}

	std::string lineMarker;

	if (!load_state.stream.eof()) do
	{
		std::istringstream ss(load_state.cur_line);
		ss >> lineMarker;

		if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
			std::cout << "\rINFO | looking for object | " << load_state.cur_line;
		}

		Q_ASSERT(lineMarker.compare("f") != 0 && "Impossible to see a vertice before the start of an object");

		// "o" also means object found but not always and want to start at the first vertice
		if (lineMarker.compare("v") == 0) {
			if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
				std::cout << " object found\n";
			}
			return true;
		}
	} while (std::getline(load_state.stream, load_state.cur_line));

	if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
		std::cout << " no next object found\n";
	}

	return false;
}

CGMain::IVertexData* ObjLoader::StartObjectRead(LoadState& load_state)
{
	std::vector<Vector3f> positios;
	std::vector<Vector2f> UVs;
	std::vector<Vector3f> normals;

	ReadVData(load_state, positios, UVs, normals);

	// we need these here buffered because "readfaces" could edit them
	size_t prevBufferV = positios.size();
	size_t prevBufferVT = UVs.size();
	size_t prevBufferVN = normals.size();

	Q_ASSERT(positios.size() != 0 && " no positions ???");

	auto* data = MakeVertexData(load_state, positios, UVs, normals);

	load_state.prevV += prevBufferV;
	load_state.prevVT += prevBufferVT;
	load_state.prevVN += prevBufferVN;

	return data;
}

void ObjLoader::ReadVData(
	LoadState& load_state,
	std::vector<Vector3f>& positions,
	std::vector<Vector2f>& uvs,
	std::vector<Vector3f>& normals)
{
	bool found_normals = false;
	bool found_uvs = false;
	std::string lineMarker;

	do
	{
		std::istringstream ss(load_state.cur_line);

		ss >> lineMarker;
		if (lineMarker.compare("v") == 0)
		{
			if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Medium) {
				if (positions.size() % 500 == 0) std::cout << "\rReading vertices: \t" << positions.size() << "              ";
			}
			Vector3f newV;
			ss >> newV(0) >> newV(1) >> newV(2);
			positions.push_back(newV);
			//std::cout << "V " << curLine << "\n";
		}
		else if (lineMarker.compare("vt") == 0)
		{
			if (!found_uvs) {
				if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Medium) {
					std::cout << "\rReading vertices: \t" << positions.size() << "             \n";
				}
				found_uvs = true;
			}
			if (uvs.size() % 500 == 0) std::cout << "\rReading uvs: \t\t" << uvs.size() << "           ";

			Vector2f newUV;
			ss >> newUV(0) >> newUV(1);
			uvs.push_back(newUV);
			//std::cout << "vt " << curLine << "\n";
		}
		else if (lineMarker.compare("vn") == 0)
		{
			if (!found_normals) {
				if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Medium) {
					if (found_uvs) std::cout << "\rReading uvs: \t\t" << uvs.size() << "           \n";
					else std::cout << "\rReading vertices: \t" << positions.size() << "             \n";
				}
				found_normals = true;
			}
			if (normals.size() % 500 == 0) std::cout << "\rReading normals: \t" << normals.size() << "            ";

			Vector3f newV;
			ss >> newV(0) >> newV(1) >> newV(1);
			normals.push_back(newV);
			//std::cout << "vn " << curLine << "\n";
		}
		else if (lineMarker.compare("f") == 0)
		{
			break;
		}
		//else not a recognized character at this point
	} while (load_state.NextLine());

	if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Medium) {
		if (found_normals) std::cout << "\rReading normals: \t" << normals.size() << "\t\t\n";
		else if (found_uvs) std::cout << "\rReading uvs: \t\t" << uvs.size() << "\t\t\n";
		else std::cout << "\rReading vertices: \t" << positions.size() << "\t\t\n";
	}
}

CGMain::IVertexData* ObjLoader::MakeVertexData(
	LoadState& load_state,
	std::vector<Vector3f>& positions,
	std::vector<Vector2f>& uvs,
	std::vector<Vector3f>& normals)
{
	bool uvs_available = uvs.size() != 0;
	bool norm_available = normals.size() != 0;

	// combos are a set of indices to a position, uv, and normal
	// each is unique
	std::vector<Vertice> vertices;
	// combos-sorted is a sortof hashmap to store vertices based on position ids
	std::vector<std::vector<int>> vertices_sorted(positions.size());
	// each face is 3 combo id's
	std::vector<std::vector<int>> faces;

	// expected sizes
	vertices.reserve(positions.size() * 4);
	faces.reserve(positions.size() * 4);

	enum TBaseNormal {
		Read, Flat, Smooth, None
	} norm_type = None;

	const char* TNormalName[] = {
		"Read", "Flat", "Smooth", "None"
	};

	if (norm_available && (load_state.load_descr.norm_load & NormalLoadFlagBits::TRY_READ)) {
		norm_type = TBaseNormal::Read;
	}
	else if (load_state.load_descr.norm_load & NormalLoadFlagBits::FLAT) {
		norm_type = TBaseNormal::Flat;
	}
	else if (load_state.load_descr.norm_load & NormalLoadFlagBits::SMOOTH) {
		norm_type = TBaseNormal::Smooth;
	}

	if (norm_type == Flat) {
		normals.clear();
	}

	// target normals is how we store the normals to be used if we dont use the in_normals
	/*std::vector<Vector3f> smooth_acculumation_normals;
	if (norm_type == Smooth) {
		smooth_acculumation_normals.resize(positions.size(), { 0,0,0 });
	}*/

	std::string lineMarker;
	std::string partAstr;
	std::string partBstr;
	std::string partCstr;

	bool f1Read = false;
	do
	{
		std::istringstream ss(load_state.cur_line);

		ss >> lineMarker;
		if (lineMarker.compare("f") == 0)
		{
			if (load_state.load_descr.hint_print_info != LoadDescription::PrintInfo::None) {
				if (faces.size() % 500 == 0) std::cout << "\rFaces read: \t\t" << faces.size();
			}
			f1Read = true;

			ss >> partAstr >> partBstr >> partCstr;

			// A B C are the sets that each describe a corner of the face
			// inside each of them there is a some of: position, uv, and normal
			int A[3], B[3], C[3];
			fPartSet(load_state, partAstr, A);
			fPartSet(load_state, partBstr, B);
			fPartSet(load_state, partCstr, C);

			int flat_nID = -1;

			// first decide on the normal
			/*if (norm_type == TBaseNormal::Smooth)
			{
				// vA vB vC are positions for the triangle
				Vector3f& vA = positions[A[0]];
				Vector3f& vB = positions[B[0]];
				Vector3f& vC = positions[C[0]];
				Vector3f face_flat_normal = NormalFromAdjPointsCCW(vA, vB, vC);
				smooth_acculumation_normals[A[0]] += face_flat_normal;
				smooth_acculumation_normals[B[0]] += face_flat_normal;
				smooth_acculumation_normals[C[0]] += face_flat_normal;
			}*/
			//else 
				if (norm_type == TBaseNormal::Flat)
			{
				// in case of flat generated base normals
				// get the triangle points, and calulate the normal
				Vector3f& vA = positions[A[0]];
				Vector3f& vB = positions[B[0]];
				Vector3f& vC = positions[C[0]];

				Vector3f flatNormal = NormalFromAdjPointsCCW(vA, vB, vC);

				// TODO: find existing normals to allow connecting faces?
				flat_nID = (int) normals.size();
				normals.push_back(flatNormal);
			}

			std::vector<int*> ABC = { A, B, C };
			std::vector<int> face_vertice_ids(3);
			for (int i = 0; i < 3; i++)
			{
				// the id of the found vertice, we need to check if it allready exists to share vertices between faces
				int vID = -1;

				// if we use flat normals, override the one from the data (on each vertice i of the face)
				// and if we use smooth normals, the position identity is the same as the normal identity
				if (norm_type == Flat) ABC[i][2] = flat_nID;
				else if (norm_type == Smooth) ABC[i][2] = ABC[i][0];

				// check if this vertice allready exists 
				// in case of smoothing, we dont care about the original normals when comparing vertices
				if (norm_type == Smooth) vID = existsVerticeSortedSmooth(vertices, vertices_sorted[ABC[i][0]], ABC[i][0], ABC[i][1]);
				else vID = existsVerticeSorted(vertices, vertices_sorted[ABC[i][0]], ABC[i][0], ABC[i][1], ABC[i][2]);

				// if we found a combo such that we can share the vertice of the face
				if (vID >= 0) face_vertice_ids[i] = vID;
				else
				{
					vID = (int) vertices.size();
					face_vertice_ids[i] = vID;
					vertices_sorted[ABC[i][0]].push_back(vID);
					vertices.push_back (Vertice { 
						vID, 
						ABC[i][0], 
						ABC[i][1], 
						ABC[i][2]
					});
				}
			}
			faces.push_back(face_vertice_ids);
		}
		else if (lineMarker.compare("s") == 0) continue;
		else if (lineMarker.compare("usemtl") == 0) continue;
		else if (f1Read) break;
	} while (load_state.NextLine());

	if (load_state.load_descr.hint_print_info != LoadDescription::PrintInfo::None) {
		std::cout << "\rFaces read: \t\t" << faces.size();
	}

	if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
		std::cout << "INFO | in.positions   : " << positions.size() << "\n";
		std::cout << "INFO | in.uvs         : " << uvs.size() << "\n";
		std::cout << "INFO | in.normals     : " << normals.size() << "\n"; // (in) means non-generated
		std::cout << "INFO | out.normaltype : " << TNormalName[norm_type] << "\n";
		std::cout << "INFO | out.vertices   : " << vertices.size() << "\n";
		std::cout << "\nObject read, generating meshdata for writing ...";
	}	

	bool output_tangents = false;
	bool generate_tangents = false;
	// TODO: possible to get tangents from an obj file??
	if (false && (load_state.load_descr.tang_load & TangentLoadFlagBits::TRY_READ)) {
		output_tangents = true;
	}
	else if (load_state.load_descr.tang_load & TangentLoadFlagBits::ALT_GENERATE) {
		output_tangents = true;
		generate_tangents = true;
	}

	IVertexData* out_data = new VertexDataInterlaced(
		true, true,
		uvs_available, 
		norm_type != TBaseNormal::None, 
		output_tangents);

	out_data->ReserveMemory((int)vertices.size(), (int)faces.size() * 3);

	for (uint i = 0; i < vertices.size(); i++)
	{
		// positions
		out_data->pos(i) = positions[vertices[i].pID];

		// uvs
		if (uvs_available) out_data->uv(i) = uvs[vertices[i].uvID];

		// normals
		if (norm_type == TBaseNormal::Read) out_data->norm(i) = normals[vertices[i].nID];

		// TODO: tangents...
	}

	auto* out_indices = out_data->indices();
	for (uint i = 0; i < faces.size(); i++)
	{
		out_indices[i * 3] = (uint32_t)faces[i][0];
		out_indices[i * 3 + 1] = (uint32_t)faces[i][1];
		out_indices[i * 3 + 2] = (uint32_t)faces[i][2];
	}

	if (norm_type == TBaseNormal::Smooth) out_data->ConsiderComputeNormals(true);
	else if (norm_type == TBaseNormal::Flat) out_data->ConsiderComputeNormals(false);
	if (generate_tangents) out_data->ConsiderComputeTangents();

	if (load_state.load_descr.hint_print_info >= LoadDescription::PrintInfo::Detail) {
		std::cout << "\rObject read, mesh data generated\t\t\t\n";
	}

	return out_data;
}


void ObjLoader::fPartSet(LoadState& load_state, std::string fPart, int* results)
{
	size_t marker1Pos = fPart.find_first_of('/');
	size_t marker2Pos = fPart.find_last_of('/');

	size_t l1 = marker1Pos;
	size_t l2 = marker2Pos - marker1Pos - 1;
	size_t l3 = fPart.length() - marker2Pos - 1;

	results[0] = atoi(fPart.substr(0, l1).c_str()) - 1 - (int)load_state.prevV;
	results[1] = l2 == 0 ? -1 : atoi(fPart.substr(marker1Pos + 1, l2).c_str()) - 1 - (int) load_state.prevVT;
	results[2] = l3 == 0 ? -1 : atoi(fPart.substr(marker2Pos + 1, l3).c_str()) - 1 - (int) load_state.prevVN;
}

ObjLoader::LoadState::LoadState(std::ifstream& in_stream, LoadDescription& load)
	: stream(in_stream), load_descr(load)
{

}

bool ObjLoader::LoadState::NextLine()
{
	return (bool)std::getline(stream, cur_line);
}

}
}
}