#pragma once
#include "tools/shared_eigen_incl.h"
#include <memory>
#include <vector>
#include <stdint.h>

namespace CGMain {

/*
The IVertexData class is a generic class that can represent all types of vectors
Nono of pos/uv/norm/tang are required to be in the data
Can be checked with has_...
*/

struct VertexBufferDescr {
	int byte_size;
	void* data;
};

class IVertexData {
public:
	enum class MemoryLayout : char {
		Interlaced, Seperated
	};

	virtual ~IVertexData();

	/*
	i_count can be zero for non indexed
	*/
	virtual void ReserveMemory(int v_count, int i_count) = 0;
	virtual bool is_allocated() const = 0;
	virtual MemoryLayout GetMemoryLayout() = 0;

	// if has_tang(), it will compute the tangents, overriding old ones if they exist
	void ConsiderComputeTangents();
	void ConsiderComputeNormals(bool smooth);

	virtual std::vector<VertexBufferDescr> GetLayout() = 0;

	/*
	sizes returns this amount of floats per available element
	this includes pos/uv/norm/tang, but also potential extra elements
	*/
	virtual std::vector<int> sizes() = 0;

	/*
	the pos/uv/norm/tang are have their own direct acces methods
	*/
	virtual Vector3f& pos(int i) = 0;
	virtual Vector2f& uv(int i) = 0;
	virtual Vector3f& norm(int i) = 0;
	virtual Vector3f& tang(int i) = 0;

	virtual int vCount() = 0;
	virtual int iCount() = 0;

	/*
	to support data beyond the basic 4 types, integer based accces is used.
	the void* references the pointer to the data type requested of i'th vertex data
	not including any of pos/uv/norm/tang
	*/
	virtual void* get(int data_index, int vertex_i) = 0;

	/*
	since they are not required to exist in the vertex, a check has to be done
	*/
	virtual bool has_pos() = 0;
	virtual bool has_uv() = 0;
	virtual bool has_norm() = 0;
	virtual bool has_tang() = 0;

	virtual AABB3 GetAABB() = 0;

	virtual uint32_t* indices() = 0;

	virtual bool is_indexed() = 0;
};

typedef std::shared_ptr<IVertexData> IVertexData_ptr;

// Not recommended, use IVertexData(_ptr) instead
template<typename VType>
class IndexedMesh {
public:
	VType * vertices = nullptr;
	uint32_t* indices = nullptr;

	uint32_t vCount;
	uint32_t iCount;

	void InverseOrientation() {
		for (int i = 0; i < iCount; i += 3) {
			int temp = indices[i];
			indices[i] = indices[i + 2];
			indices[i + 2] = temp;
		}
	}

	static void Copy(IndexedMesh& from, IndexedMesh& to) {
		to.vertices = from.vertices;
		to.indices = from.indices;
		to.vCount = from.vCount;
		to.iCount = from.iCount;
	}
};

/*
Helper class to cleanup Indexed mesh objects
Make a shared pointer of this to auto cleanup IndexedMesh objects
IndexedMesh_ptr<Vertex_3> mesh_ptr;
*/
template<typename VType>
class IndexedMeshSelfDelete
	: public IndexedMesh<VType>
{
public:

	IndexedMeshSelfDelete();

	inline static IndexedMeshSelfDelete<VType>* MakeNewFrom(IndexedMesh<VType>& mesh) {
		auto* new_mesh = new IndexedMeshSelfDelete();
		Copy(mesh, new_mesh);
		return new_mesh;
	}

	~IndexedMeshSelfDelete() {
		Tools::SafeDeleteArray(vertices);
		Tools::SafeDeleteArray(indices);
	}

};

template<typename VType>
using IndexedMesh_ptr = typename std::shared_ptr<IndexedMeshSelfDelete<VType>>;


}