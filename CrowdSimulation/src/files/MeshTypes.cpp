#include "MeshTypes.h"
#include <QDebug>

namespace CGMain {

IVertexData::~IVertexData()
{
	// ... (pure) virtual
}

void IVertexData::ConsiderComputeTangents()
{
	if (has_tang()) {

		Q_ASSERT(has_norm() && has_uv() && "Cannot compute tangents without normals and uvs");

		//bitangents = new vec3[vertices.Length];
		Vector3f* tan1 = new Vector3f[vCount()];
		//vec3* tan2 = new vec3[object.gVCount];

		const uint32_t limit = iCount();
		uint32_t* indices = this->indices();
		int vCount = this->vCount();

		for (uint32_t a = 0; a < limit; a += 3)
		{
			long i1 = indices[a];//triangle->index[0];
			long i2 = indices[a + 1];//triangle->index[1];
			long i3 = indices[a + 2];//triangle->index[2];

			Vector3f v1 = pos(i1);
			Vector3f v2 = pos(i2);
			Vector3f v3 = pos(i3);

			Vector2f w1 = uv(i1);
			Vector2f w2 = uv(i2);
			Vector2f w3 = uv(i3);

			float x1 = v2(0) - v1(0);
			float x2 = v3(0) - v1(0);
			float y1 = v2(1) - v1(1);
			float y2 = v3(1) - v1(1);
			float z1 = v2(2) - v1(2);
			float z2 = v3(2) - v1(2);

			float s1 = w2(0) - w1(0);
			float s2 = w3(0) - w1(0);
			float t1 = w2(1) - w1(1);
			float t2 = w3(1) - w1(1);

			float r = 1.0F / (s1 * t2 - s2 * t1);
			Vector3f sdir = Vector3f((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
			Vector3f tdir = Vector3f((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

			tan1[i1] += sdir;
			tan1[i2] += sdir;
			tan1[i3] += sdir;

			//tan2[i1] += tdir;
			//tan2[i2] += tdir;
			//tan2[i3] += tdir;
		}

		for (int a = 0; a < vCount; a++)
		{
			Vector3f n = norm(a);
			//n = VMath.normalize(n);
			Vector3f t = tan1[a];
			//t = VMath.normalize(t);

			// Gram-Schmidt orthogonalize
			tang(a) = (t - n * n.dot(t)).normalized(); // vop::normalize(t - n * glm::dot(n, t));


													   // Calculate handedness
													   //float w =
													   //tangents[a].w = 
													   //(VMath.dot(VMath.cross(n, t), tan2[a]) < 0.0F) ? -1.0F : 1.0F;

													   //tangents[a] *= w;

													   //bitangents[a] = VMath.cross(tangents[a], n);

		}

		delete[] tan1;
		//delete[] tan2;
	}
}

void IVertexData::ConsiderComputeNormals(bool smooth)
{
	if (has_norm() && is_allocated() && has_pos()) {
		if (smooth) {

			uint32_t v_count = vCount();
			std::vector<Vector3f> normal_accumulator;
			normal_accumulator.resize(v_count, { 0,0,0 });
			uint32_t* tris = indices();
			uint32_t* end_tri = tris + iCount();
			for (uint32_t* tri = tris; tri < end_tri; tri += 3) {

				Vector3f a = pos(tri[0]);
				Vector3f b = pos(tri[1]);
				Vector3f c = pos(tri[2]);

				// method 1:
				// equally weighted normals from all triangle
				//Vector3f n = (b - a).cross(c - a);
				//n.normalize();

				// method 2:
				// normals weighted by the surface area of the triangle
				//Vector3f n = (b - a).cross(c - a);

				// method 3:
				// normals weighted by the angle at which the triangle accesses the point
				// gives bad results with small triangles
				/*Vector3f v1 = (b - a);
				Vector3f v2 = (c - a);
				v1.normalize();
				v2.normalize();
				Vector3f n = v1.cross(v2);
				n.normalize();
				float influence = 1 - 0.5 * (1+v1.dot(v2));
				n *= influence;*/

				// method 4, combine method 2 and 3
				Vector3f v1 = (b - a);
				Vector3f v2 = (c - a);
				Vector3f n = v1.cross(v2); // area sized normal
				v1.normalize();
				v2.normalize();
				float influence = 1 - 0.5 * (1 + v1.dot(v2));
				n *= influence; // effect of angle

				normal_accumulator[tri[0]] += n;
				normal_accumulator[tri[1]] += n;
				normal_accumulator[tri[2]] += n;
			}
			for (uint32_t i = 0; i < v_count; i++) norm(i) = normal_accumulator[i].normalized();
		}
		else {
			// compute flat normals
			// loop over faces, and set the three normals
			uint32_t* tris = indices();
			uint32_t* end_tri = tris + iCount();
			for (uint32_t* tri = tris; tri < end_tri; tri += 3) {
				Vector3f a = pos(tri[0]);
				Vector3f b = pos(tri[1]);
				Vector3f c = pos(tri[2]);
				Vector3f n = (b - a).cross(c - a);
				n.normalize();
				norm(tri[0]) = n;
				norm(tri[1]) = n;
				norm(tri[2]) = n;
			}
		}
	}
}

}