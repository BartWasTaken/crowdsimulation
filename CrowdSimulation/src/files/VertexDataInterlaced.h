#pragma once
#include "MeshTypes.h"
#include "tools/include.h"

namespace CGMain {

class VertexDataCommon
	: public IVertexData 
{
public:

	virtual AABB3 GetAABB() override;

	virtual std::vector<int> sizes() override;

	virtual bool has_pos() override;

	virtual bool has_uv() override;

	virtual bool has_norm() override;

	virtual bool has_tang() override;

	virtual int vCount() override;

	virtual int iCount() override;

	virtual uint32_t* indices() override;

	virtual bool is_indexed() override;

protected:

	std::vector<uint32_t>	gIndice_data;
	std::vector<int>		gExtra_sizes;

	int		gVCount = 0;
	int		gICount = 0;
	bool	gIsIndexed;

	bool	gHas_pos;
	bool	gHas_uv;
	bool	gHas_norm;
	bool	gHas_tang;
};

}

namespace CGMain {

class VertexDataInterlaced;

using VertexDataInterlaced_ptr = std::shared_ptr<VertexDataInterlaced>;

class VertexDataInterlaced :
	public VertexDataCommon,
	public Tools::SmartPtrClass<VertexDataInterlaced>
{
public:
	VertexDataInterlaced(bool indexed, bool pos, bool uv, bool norm, bool tang);
	VertexDataInterlaced(bool indexed, bool pos, bool uv, bool norm, bool tang, std::vector<int> extra_vertex_variable_sizes);

	virtual ~VertexDataInterlaced();

	virtual void ReserveMemory(int v_count, int i_count) override;

	virtual Vector3f& pos(int i) override;

	virtual Vector2f& uv(int i) override;

	virtual Vector3f& norm(int i) override;

	virtual Vector3f& tang(int i) override;

	virtual void* get(int data_index, int vertex_i) override;

	virtual std::vector<VertexBufferDescr> GetLayout() override;

	void* data();

	void ReleaseMemory();

	virtual MemoryLayout GetMemoryLayout() override;

	virtual bool is_allocated() const override;

private:

	std::vector<float>		gVertice_data;

	std::vector<int>		gExtra_offsets;

	int		gFloatStride = 0; // the size of per-vertice data

	int		gPosOffset = 0;
	int		gUvOffset = 0;
	int		gNormOffset = 0;
	int		gTangOffset = 0;

	void Setup(bool indexed, bool pos, bool uv, bool norm, bool tang, std::vector<int>& extra_sizes);
};

}

namespace CGMain {

class VertexDataSeperated;

using VertexDataSeperated_ptr = std::shared_ptr<VertexDataSeperated>;

class VertexDataSeperated :
	public VertexDataCommon,
	public Tools::SmartPtrClass<VertexDataInterlaced>
{
public:
	VertexDataSeperated(bool indexed, bool pos, bool uv, bool norm, bool tang);
	VertexDataSeperated(bool indexed, bool pos, bool uv, bool norm, bool tang, std::vector<int> extra_vertex_variable_sizes);

	virtual std::vector<VertexBufferDescr> GetLayout() override;

	virtual MemoryLayout GetMemoryLayout() override;

	virtual void ReserveMemory(int v_count, int i_count) override;

	virtual void* get(int data_index, int vertex_i) override;

	virtual Vector3f& norm(int i) override;
	virtual Vector3f& pos(int i) override;
	virtual Vector3f& tang(int i) override;
	virtual Vector2f& uv(int i) override;

private:
	std::vector<Vector3f>		gPositions_data;
	std::vector<Vector2f>		gUVs_data;
	std::vector<Vector3f>		gNormals_data;
	std::vector<Vector3f>		gTangents_data;
	std::vector<std::vector<uint8_t>> gExtra_data;
};

}