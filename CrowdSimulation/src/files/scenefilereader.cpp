#include "scenefilereader.h"
#include <QJsonArray>
#include <QJsonValueRef>
#include <QJsonObject>
#include <QtDebug>
#include <QFileDialog>
#include <QFileInfo>
#include "src/sim/IEnvironment.h"
#include "src/sim/simulationcore.h"
#include "src/shared.h"

SceneFileReader::SceneFileReader()
{

}

int to_int(double value) {
	if (value < 0) return (int)(value - 0.5f);
	else return (int)(value + 0.5f);
}

void SceneFileReader::Read(SimulationCore& sc, OpenGLView& f, QJsonDocument& json, bool enable)
{
	// file should be:
	// Tope level : array of items
	// each item has a "type" field, which is "scene"
	// each item has a "name" field, the name of the environment

	// each "scene"
	// has 0 or more obstacles
	// has 1 or more goals
	// each obstacle/goal is a named array of "vectors" which are just arrays aswell

	auto item_proc = [&sc, &f, enable](QJsonObject element) {		
		// check if the element has a type (should have!)
		if (element.contains("type"))
		{
			auto type = element["type"];

			// check if the type is a string (should be!)
			if (type.isString())
			{
				if (type.toString().compare("scene") == 0)
				{
					bool success = ReadScene(sc, f, element, enable);
					if (!success) {
						qInfo() << "Unable to load scene";
					}
				}
			}
		}
	};


	QJsonArray main_array;

	// check if the top level is an array
	if (json.isArray())
	{
		QJsonArray main_array = json.array();
		// loop over the items
		for (QJsonValueRef element_ref : main_array)
		{
			if (element_ref.isObject())
			{
				QJsonObject element = element_ref.toObject();
				item_proc(element);
			}
		}
	}
	// if its just one object:
	else if (json.isObject()) {
		auto element = json.object();
		item_proc(element);
	}


}

void SceneFileReader::Write(IEnvironment_ptr pEnv)
{
	// build base object:
	QJsonArray main_array;

	QJsonObject scene; {
		scene["type"] = "scene";
		scene["require outline"] = false;
		scene["pref_goalpicker"] = pEnv->get_pref_goalpicker();
		scene["pref_engine"] = pEnv->get_pref_engine();
		scene["force_pref"] = pEnv->get_force_prefs();
		scene["settings"] = pEnv->get_settings();

		QJsonObject bounds; {
			auto vertice_list = std::vector<Vector2f>{ pEnv->bbox().min(), pEnv->bbox().max() };
			bounds["vertices"] = MakeVertices(vertice_list);
		}
		scene["bounds"] = bounds;

		scene["transformations"]; // add entry
		QJsonArray obstacles; {
			auto obstacle_list = pEnv->get_non_staircase_obstacles();
			for (auto& obst : obstacle_list) {
				const auto& vertice_list = obst.base().vertices();
				QJsonArray v_array = MakeVertices(vertice_list);
				obstacles.push_back(v_array);
			}
		}
		scene["obstacles"] = obstacles;

		scene["objects"];
	}
	main_array.append(scene);

	QFileDialog dialog;
	auto result = dialog.getSaveFileName(nullptr, "Select a simulation file", pEnv->name(), "JSON files (*.json);;All files (*)");

	QString final_string = result;
	{ // try numbering if file exists
		int nr = 0;
		while (QFileInfo::exists(final_string)) {
			final_string = result + "(" + QString::number(nr++) + ")";
		}
	}

	write_json(final_string, main_array);

	qDebug() << "Tried writing file:" << result;
}

QJsonArray SceneFileReader::MakeVertices(const std::vector<Vector2f>& vertice_list)
{
	auto vertices = QJsonArray();

	for (const auto& v : vertice_list) {
		auto json_vertice = QJsonArray();
		json_vertice.push_back(v(0));
		json_vertice.push_back(v(1));
		vertices.push_back(json_vertice);
	}

	return vertices;
}

bool SceneFileReader::ReadScene(SimulationCore& sc, OpenGLView& f, QJsonObject& scene, bool enable)
{
	std::vector<MyObstacle> out_obstacles;
	std::vector<MyGoal> out_goals;
	std::vector<StairCaseArea> out_staircases;
	std::vector<SmokeField::FireEvent> out_fire_events;
	std::vector<ElevationArea> out_elevations;
	std::vector<SpawnArea> out_spawn_areas;
	std::vector<IEnvironment::NewObstacleEvent> out_new_obst_events;

	AABB2 bbox;
	QString name;
	QString pref_goalpicker;
	QString pref_engine;
	bool force_pref = false;

	bool require_outline = false;
	auto keys = scene.keys();

	// weird problem:
	// if we make 'auto obstacles_ref = scene["obstacles"]' to avoid double lookup,
	// it is not recongized as an array for programtically written json files when using obstacles_ref.isArray() later on...

	for (auto key : keys) {
		qInfo() << key << scene[key] << scene[key].isObject() << scene[key].isArray();
	}

	TryReadBool(scene, "require_outline", require_outline);
	TryReadBool(scene, "force_pref", force_pref);
	TryReadString(scene, "pref_engine", pref_engine);
	TryReadString(scene, "pref_goalpicker", pref_goalpicker);

	auto ref_name = scene.find("name");
	if (ref_name != scene.end() && ref_name->isString()) {
		name = ref_name->toString();
	}
	else {
		name = "Nameless";
	}

	auto ref_objects = scene.find("objects");
	if (ref_objects != scene.end() && ref_objects->isArray())
	{
		auto objects = ref_objects->toArray();
		ReadObjects(sc, objects, out_goals, out_staircases);
	}

	auto ref_obst = scene.find("obstacles");
	if (ref_obst != scene.end() && ref_obst->isArray()) {
		auto obstacles = ref_obst->toArray();
		ReadObstacles(sc, obstacles, out_obstacles);
	}

	auto ref_fire_ev = scene.find("fire_events");
	if (ref_fire_ev != scene.end() && ref_fire_ev->isArray()) {
		auto fire_events = ref_fire_ev->toArray();
		ReadFireEvents(sc, fire_events, out_fire_events);
	}

	auto ref_obst_ev = scene.find("new_obstacle_events");
	if (ref_obst_ev != scene.end() && ref_obst_ev->isArray()) {
		auto obst_events = ref_obst_ev->toArray();
		ReadObstacleEvents(sc, obst_events, out_new_obst_events);
	}

	auto ref_elevations = scene.find("elevations");
	if (ref_elevations != scene.end() && ref_elevations->isArray()) {
		auto elevations = ref_elevations->toArray();
		ReadElevations(sc, elevations, out_elevations);
	}

	auto ref_spawn_areas = scene.find("spawn_areas");
	if (ref_spawn_areas != scene.end() && ref_spawn_areas->isArray()) {
		auto spawn_areas = ref_spawn_areas->toArray();
		ReadSpawnAreas(sc, spawn_areas, out_spawn_areas);
	}

	bool bounds_read = false;

	auto ref_bounds = scene.find("bounds");
	if (ref_bounds != scene.end() && ref_bounds->isObject())
	{
		auto bounds = ref_bounds->toObject();
		// weird problem:
		// if we make 'auto obstacles_ref = scene["obstacles"]' to avoid double lookup,
		// it is not recongized as an array for programtically written json files when using obstacles_ref.isArray() later on...
		bounds_read = ReadBounds(sc, bounds, bbox);
	}
	if (!bounds_read) {
		if (out_obstacles.size() + out_goals.size() > 0)
		{
			// make bounding box
			myVec min, max;

			for (int i = 0; i < dim; i++) {
				min(i) = std::numeric_limits<float>::infinity();
				max(i) = -min(i);
			}

			for (auto obst : out_obstacles) {
				for (auto v : obst.vertices) {
					for (int i = 0; i < dim; i++) {
						if (v(i) < min(i)) min(i) = v(i);
						if (v(i) > max(i)) max(i) = v(i);
					}
				}
			}
			for (auto goal : out_goals) {
				for (auto v : goal.vertices) {
					for (int i = 0; i < dim; i++) {
						if (v(i) < min(i)) min(i) = v(i);
						if (v(i) > max(i)) max(i) = v(i);
					}
				}
			}
			for (auto e : out_elevations) {
				for (auto v : e.polygon.vertices()) {
					for (int i = 0; i < dim; i++) {
						if (v(i) < min(i)) min(i) = v(i);
						if (v(i) > max(i)) max(i) = v(i);
					}
				}
			}
			for (auto sa : out_spawn_areas) {
				for (auto v : sa.polygon.vertices()) {
					for (int i = 0; i < dim; i++) {
						if (v(i) < min(i)) min(i) = v(i);
						if (v(i) > max(i)) max(i) = v(i);
					}
				}
			}

			auto range = max - min;
			//min -= range * 0.01;
			//max += range * 0.01;
			bbox = { min, max };
		}
		else {
			qInfo() << "Cannot conclude bounds from file";
			bbox = { Vector2f{ -1,-1 }, Vector2f{ 1,1 } };
		}
	}

	// Verify valid data:

	if (out_goals.size() == 0) {
		qInfo() << "Incomplete scene file: no goals found.";
		return false;
	}

	// build environment:

	auto env = IEnvironment_ptr{ new IEnvironment(f, bbox, name) };

	if (require_outline) {
		AddOutline(sc, env, bbox);
	}

	for (auto& obst : out_obstacles)
	{
		Obstacle o(sc.settings().agent_rad(), obst.vertices);
		env->add_obstacle(o);
	}

	for (auto& goal : out_goals)
	{
		IGoal_ptr g = IGoal_ptr{ new ObstacleGoal(goal.vertices) };
		g->goal_inner_direction() = goal.direction;
		env->add_goal(g);
	}

	for (auto& staircase : out_staircases)
	{
		env->add_staircase(staircase);
	}

	for (auto& fire_event : out_fire_events) {
		env->smoke().add_event(fire_event);
	}

	for (auto& obst_event : out_new_obst_events) {
		env->add_obstacle_event(obst_event);
	}

	for (auto& elev : out_elevations) {
		env->add_elevation(elev);
	}
	for (auto& spawn_area : out_spawn_areas) {
		env->add_spawn_area(spawn_area);
	}
	qInfo() << "Env loaded ";
	qInfo() << "  bounds:" << bbox.min()(0) << bbox.min()(1) << ":" << bbox.max()(0) << bbox.max()(1);
	qInfo() << "  obstacle count:" << out_obstacles.size();
	qInfo() << "  goal count:" << out_goals.size();
	qInfo() << "  stairs count:" << out_staircases.size();

	env->set_preferences(pref_goalpicker, pref_engine, force_pref);

	if (enable)
	{
		auto settings_ref = scene["settings"];

		if (settings_ref.isObject())
		{
			auto settings_obj = settings_ref.toObject();

			SimulationSettings settings;

			auto merge_json = settings.to_json();
			// override defaults with was is in the file
			for (auto key : settings_obj.keys()) {
				merge_json[key] = settings_obj[key];
			}
			settings.from_json(merge_json);

			if (settings.is_valid()) {
				env->set_settings(merge_json);
			}
		}

		sc.set_environment(env);
	}
	else sc.add_environment(env);

	return true;
}

bool SceneFileReader::ReadBounds(SimulationCore&, QJsonObject& bounds, AABB2& out_bbox)
{
	auto vertices_ref = bounds.find("vertices");

	if (vertices_ref != bounds.end() && vertices_ref->isArray())
	{
		auto vertices = vertices_ref->toArray();

		if (IsVerticeArray(vertices))
		{
			std::vector<myVec> out_vert;

			ReadVerticeArray(vertices, out_vert);

			if (out_vert.size() >= 2) {
				out_bbox = { out_vert[0], out_vert[1] };
				return true;
			}
		}
	}

	return false;
}

void SceneFileReader::ReadObjects(SimulationCore&, QJsonArray& objects, std::vector<MyGoal>& out_goals,
	std::vector<StairCaseArea>& out_staircases)
{
	for (const auto& obj_ref : objects)
	{
		if (obj_ref.isObject())
		{
			auto obj = obj_ref.toObject();

			// find the object type:
			auto type_ref = obj["type"];

			if (type_ref.isString())
			{
				auto type = type_ref.toString();

				bool isgoal = type.compare("goal") == 0;
				bool is_staircase = type.compare("staircase") == 0;

				if (isgoal)
				{
					auto vertices_ref = obj.find("vertices");
					auto dir_ref = obj.find("direction");

					myVec direction = { 0,0 };
					if (dir_ref != obj.end() && dir_ref->isArray()) {
						auto qdir = dir_ref->toArray();
						if (qdir.size() >= 2) {
							if (qdir[0].isDouble()) direction(0) = qdir[0].toDouble();
							if (qdir[0].isDouble()) direction(1) = qdir[1].toDouble();
						}
					}

					if (vertices_ref != obj.end() && vertices_ref->isArray())
					{
						auto vertices = vertices_ref->toArray();

						std::vector<myVec> out_vertices;

						ReadVerticeArray(vertices, out_vertices);

						if (out_vertices.size() > 2) {
							out_goals.push_back({ direction, out_vertices });
						}
					}
					else {
						qDebug() << "Invalid type of element";
					}
				}
				else if (is_staircase)
				{
					auto up_id_ref = obj.find("up_id");
					auto down_id_ref = obj.find("down_id");
					auto up_vid_ref = obj.find("up_vid");
					auto down_vid_ref = obj.find("down_vid");
					auto opening_vid_ref = obj.find("opening_vid");
					auto vertices_ref = obj.find("vertices");
					auto thickness_ref = obj.find("thickness");

					if (opening_vid_ref == obj.end()
						|| vertices_ref == obj.end()
						|| thickness_ref == obj.end())
					{
						qInfo() << "Missing fields in staircase";
						continue;
					}

					bool has_up = up_id_ref != obj.end() && up_vid_ref != obj.end()
						&& up_id_ref->isDouble() && up_vid_ref->isDouble();

					bool has_down = down_id_ref != obj.end() && down_vid_ref != obj.end()
						&& down_id_ref->isDouble() && down_vid_ref->isDouble();

					if (vertices_ref->isArray() && thickness_ref->isDouble() && (has_up || has_down) && opening_vid_ref->isDouble())
					{
						//bool direction_up = true;

						int up_id = -1;
						int down_id = -1;
						int up_vid = -1;
						int down_vid = -1;
						int opening_vid = to_int(opening_vid_ref->toDouble());
						float thickness = thickness_ref->toDouble();

						if (has_up) {
							up_id = to_int(up_id_ref->toDouble());
							up_vid = to_int(up_vid_ref->toDouble());
						}
						if (has_down) {
							down_id = to_int(down_id_ref->toDouble());
							down_vid = to_int(down_vid_ref->toDouble());
						}

						Q_ASSERT(up_vid == -1 || opening_vid == -1 || qAbs(up_vid - opening_vid) > 1);

						auto vertices = vertices_ref->toArray();
						bool reversed;


						if (IsVerticeArray(vertices)) {
							std::vector<myVec> out_vertices;
							ReadVerticeArray(vertices, out_vertices, &reversed);

							if (out_vertices.size() > 2) {
								if (reversed) {
									int rev_value = out_vertices.size() - 1;
									int rev_upvid = (up_vid == -1) ? -1 : rev_value - up_vid;
									int rev_downvid = (down_vid == -1) ? -1 : rev_value - down_id;
									int rev_openvid = (opening_vid == -1) ? -1 : rev_value - opening_vid;
									out_staircases.push_back({ out_vertices, thickness, up_id, down_id, rev_upvid, rev_downvid, rev_openvid });
								}
								else {
									out_staircases.push_back({ out_vertices, thickness, up_id, down_id, up_vid, down_vid, opening_vid });
								}
							}
							else qDebug() << "Invalid vertice element";
						}
						else qDebug() << "Invalid vertice element";
					}
					else qDebug() << "Invalid type of element";
				}
				else qDebug() << "Unkown type in object array";
			}
		}
	}
}

void SceneFileReader::ReadObstacles(SimulationCore& sc, QJsonArray& obstacles, std::vector<MyObstacle>& out_obstacles)
{
	for (auto reference : obstacles)
	{
		if (reference.isArray())
		{
			auto json_obstacle = reference.toArray();

			if (IsVerticeArray(json_obstacle))
			{
				std::vector<myVec> arr;
				ReadVerticeArray(json_obstacle, arr);

				if (arr.size() > 2) {
					out_obstacles.push_back({ arr });
				}
			}
		}
	}
}

void SceneFileReader::ReadFireEvents(SimulationCore&, QJsonArray& qj_events, std::vector<SmokeField::FireEvent>& out_events)
{
	for (auto event_ref : qj_events) {
		if (event_ref.isObject()) {
			auto qj_event = event_ref.toObject();
			SmokeField::FireEvent e;
			bool succes = true;
			succes = succes && TryReadCoordinate(qj_event, "coordinates", e.coordinates);
			succes = succes && TryReadFloat(qj_event, "intensity", e.intensity);
			succes = succes && TryReadFloat(qj_event, "radius", e.radius);
			succes = succes && TryReadFloat(qj_event, "falloff", e.falloff);
			succes = succes && TryReadFloat(qj_event, "time_seconds", e.time_seconds);
			if (succes) {
				out_events.push_back(e);
			}
		}
	}
}

void SceneFileReader::ReadElevations(SimulationCore& sc, QJsonArray& qj_array, std::vector<ElevationArea>& out)
{
	for (auto elev_ref : qj_array) {
		if (elev_ref.isObject()) {
			bool succes = true;

			std::vector<Vector2f> vertices;
			float height = 0;
			Vector2f translation{ 0,0 };

			// read elevation height
			auto qj_elev = elev_ref.toObject();
			succes = succes && TryReadFloat(qj_elev, "height", height);

			if (qj_elev["translation"].isArray()) {
				auto translation_qt = qj_elev["translation"].toArray();
				if (translation_qt.size() >= 2 && translation_qt[0].isDouble() && translation_qt[1].isDouble()) {
					translation(0) = translation_qt[0].toDouble();
					translation(1) = translation_qt[1].toDouble();
				}
			}

			// read elevation area
			if (qj_elev["vertices"].isArray()) {
				auto vert_qt = qj_elev["vertices"].toArray();

				if (IsVerticeArray(vert_qt)) {
					ReadVerticeArray(vert_qt, vertices);
					if (vertices.size() == 2) {
						vertices = bbox_to_polygon(AABB2{ vertices[0], vertices[1] });
					}
				}
				else succes = false;
			}
			else succes = false;

			if (succes) {
				out.push_back({ height, translation, {} });
				out.back().polygon.setup(vertices);
			}
		}
	}
}

void SceneFileReader::ReadObstacleEvents(SimulationCore&, QJsonArray& qj_array, std::vector<IEnvironment::NewObstacleEvent>& out)
{
	for (auto o_event : qj_array) {
		if (o_event.isObject()) {
			auto e = o_event.toObject();
			auto vertices_ref = e.find("vertices");
			auto time_ref = e.find("time");
			auto name_ref = e.find("name");

			bool valid = vertices_ref != e.end() && vertices_ref->isArray()
				&& time_ref != e.end() && name_ref != e.end() && name_ref->isString();
			if (!valid) continue;

			bool is_manual = false;
			float time = 0;

			if (time_ref->isDouble())
			{
				time = time_ref->toDouble();
			}
			//if (time_ref->isString())
			//{
			//	is_manual = true;
			//}
			else {
				is_manual = true;
			}

			if (valid) {
				auto qVertices = vertices_ref->toArray();
				QString name = name_ref->toString();
				Polygon2D poly;
				ReadVerticeArray(qVertices, poly.edit_vertices());
				poly.finish();
				out.emplace_back(name, is_manual, time, poly);
			}
		}
	}
}

void SceneFileReader::ReadSpawnAreas(SimulationCore&, QJsonArray& qj_array, std::vector<SpawnArea>& out)
{
	for (auto area_ref : qj_array) {
		if (area_ref.isArray()) {
			auto area_vertices = area_ref.toArray();
			if (IsVerticeArray(area_vertices)) {
				out.push_back({});
				auto& vertices = out.back().polygon.edit_vertices();
				ReadVerticeArray(area_vertices, vertices);
				if (vertices.size() == 2) {
					vertices = bbox_to_polygon(AABB2{ vertices[0], vertices[1] });
				}
				out.back().polygon.finish();
			}
		}
	}
}

void SceneFileReader::ReadVerticeArray(QJsonArray& vertices, std::vector<myVec>& out_vertices, bool* reversed)
{
	out_vertices.clear();



	for (const auto& v_ref : vertices)
	{
		if (v_ref.isArray())
		{
			auto vec = v_ref.toArray();

			if (vec.size() == dim)
			{
				for (int i = 0; i < dim; i++) if (!vec[i].isDouble()) continue;
				myVec out_vec;
				for (int i = 0; i < dim; i++) out_vec(i) = vec[i].toDouble();
				out_vertices.push_back(out_vec);
			}
		}
	}
	

	// consider to change winding
	if (reversed) *reversed = false;

	if (out_vertices.size() > 2) { // do not change bounds winding (where size == 2)
		int n = out_vertices.size();
		int min_id = 0;
		float min_x = out_vertices[min_id][0];

		for (int i = 1; i < n; i++) {
			if (out_vertices[i][0] < min_x) {
				min_x = out_vertices[i][0];
				min_id = i;
			}
		}
		
		int i = (min_id - 1 + n) % n;
		int j = min_id;
		int k = (min_id + 1) % n;
		myVec v1 = (out_vertices[j] - out_vertices[i]).normalized();
		myVec v2 = (out_vertices[k] - out_vertices[j]).normalized();

		float det = v1(0) * v2(1) - v1(1) * v2(0);
		float angle = atan2(det, v1.dot(v2));

		// if wrong order, reverse the vertices
		if (angle > 0) {
			for (int i = 0, h = n / 2; i < h; i++) {
				auto temp = out_vertices[i];
				out_vertices[i] = out_vertices[n - 1 - i];
				out_vertices[n - 1 - i] = temp;
			}
			if (reversed) *reversed = true;
		}	 
	}
}

bool SceneFileReader::IsVerticeArray(QJsonArray& arr)
{
	for (auto vec_ref : arr)
	{
		if (vec_ref.isArray())
		{
			auto vec = vec_ref.toArray();

			if (vec.size() < dim) return false;

			for (int i = 0; i < dim; i++)
			{
				if (!vec[i].isDouble()) return false;
			}
		}
	}
	return true;
}

bool SceneFileReader::TryReadString(QJsonObject& json, QString name, QString& out_value)
{
	auto reference = json[name];
	if (reference.isString()) {
		out_value = reference.toString();
		return true;
	}
	return false;
}

bool SceneFileReader::TryReadBool(QJsonObject& json, QString name, bool& out_value)
{
	auto reference = json[name];
	if (reference.isBool()) {
		out_value = reference.toBool();
		return true;
	}
	return false;
}

bool SceneFileReader::TryReadFloat(QJsonObject& json, QString name, float& out_value)
{
	auto reference = json.find(name);
	if (reference != json.end() && reference->isDouble()) {
		out_value = (float)reference->toDouble();
		return true;
	}
	return false;
}

bool SceneFileReader::TryReadCoordinate(QJsonObject& json, QString name, Vector2f& out_value)
{
	auto reference = json[name];
	if (reference.isArray()) {
		auto qj_arr = reference.toArray();
		if (qj_arr.size() == 2 && qj_arr[0].isDouble() && qj_arr[1].isDouble()) {
			float x = (float)qj_arr[0].toDouble();
			float y = (float)qj_arr[1].toDouble();
			out_value = { x, y };
			return true;
		}
	}
	return false;
}

void SceneFileReader::AddOutline(SimulationCore& sc, IEnvironment_ptr& env, AABB2 bbox)
{
	auto center = (bbox.max() + bbox.min()) * 0.5f;
	auto range = bbox.sizes();

	auto size_out = 1.3f;
	auto size_in = 0.99f;

	std::vector<Vector2f> main_square{
		center + range.cwiseProduct(Vector2f{ -size_out,-size_out }),
		center + range.cwiseProduct(Vector2f{ -size_out, size_out }),
		center + range.cwiseProduct(Vector2f{ size_out, size_out }),
		center + range.cwiseProduct(Vector2f{ size_out,-size_out }),
		center + range.cwiseProduct(Vector2f{ -size_out,-size_out }),

		center + range.cwiseProduct(Vector2f{ -size_out, -size_in }),
		center + range.cwiseProduct(Vector2f{ size_in, -size_in }),
		center + range.cwiseProduct(Vector2f{ size_in, size_in }),
		center + range.cwiseProduct(Vector2f{ -size_in, size_in }),
		center + range.cwiseProduct(Vector2f{ -size_in, -size_out }),
	};
	Obstacle env_boundary(sc.settings().agent_rad(), main_square);
	env->add_obstacle(env_boundary);
}
