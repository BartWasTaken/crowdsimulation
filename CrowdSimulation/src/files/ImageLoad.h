#pragma once

#include <vector>
#include <qglobal.h>

class ImageLoad
{
public:
	ImageLoad();
	~ImageLoad();

	static void write_image(std::vector<uchar>& data_rgb, int w, int h);
};

