#pragma once
#include "src/tools/shared_eigen_incl.h"
#include "MeshTypes.h"
#include <vector>

namespace CGMain {
namespace IO {

namespace NormalLoadFlagBits {
enum Bits : uint32_t {
	NONE = 0x0000,
	TRY_READ = 0x0001,
	SMOOTH = 0x0002,
	FLAT = 0x0004,

	// SMOOTH and FLAT are lower priority when TRY_READ bit is set aswell

	READ_ALT_SMOOTH = TRY_READ | SMOOTH,
	READ_ALT_FLAT = TRY_READ | FLAT
};
}
typedef uint32_t NormalLoadFlags;

namespace TangentLoadFlagBits {
enum Bits : uint32_t {
	NONE = 0x000,
	TRY_READ = 0x0001,
	ALT_GENERATE = 0x0002,
	READ_ALT_GENERATE = TRY_READ | ALT_GENERATE
};
}
typedef uint32_t TangentLoadFlags;


enum class FileType {
	auto_detect, // base on file extention
	rmd, // raw mesh data -> format by myself, easy to load into buffers.
	obj // might come with long load times for big files
};

struct LoadDescription
{
	// auto detect will attempt to find out the type from the extention
	FileType file_type = FileType::auto_detect;

	// simply the full path, including file name and extention
	std::string full_path;

	TangentLoadFlags tang_load = TangentLoadFlagBits::TRY_READ;
	NormalLoadFlags norm_load = NormalLoadFlagBits::READ_ALT_SMOOTH;

	enum PrintInfo {
		None = 0,
		Basic = 1,
		Medium = 2,
		Detail = 3
	} hint_print_info;
};

struct WriteDescription
{
	// choose an file type
	FileType file_type;

	// choose where to write it
	std::string full_path;

	// if the writer will auto-append the extention of the chosen file type, if it's not there yet
	bool auto_append_ext = true;

	// the object to be written
	IVertexData* write_data;

	// field specifation
	bool leaveout_tangents = true;
	bool leavout_normals = false;

	enum class OverwriteRule {
		Cancel, Overwrite, Number
	} overwrite_rule = OverwriteRule::Number;
};

struct ConvertDescription {
	LoadDescription load;
	WriteDescription write;
};

}
}

namespace CGMain {
namespace IO {
namespace LoaderTypes {

struct Vertice {
	int comboID;
	int pID;
	int uvID;
	int nID;
};

struct ComboHasher
{
	inline std::size_t operator()(const Vertice& k) const
	{
		return (int)(k.pID * 73856093) ^ (int)(k.uvID * 19349663) ^ (int)(k.nID * 83492791);
	}
};

class ObjLoader {
public:
	static std::vector<CGMain::IVertexData*> LoadObject(LoadDescription&);

private:
	class LoadState {
	public:
		LoadState(std::ifstream&, LoadDescription&);
		std::ifstream& stream;
		std::string cur_line;
		size_t prevV = 0;
		size_t prevVT = 0;
		size_t prevVN = 0;
		LoadDescription& load_descr;
		bool NextLine();
	};
	struct VHasher
	{
		inline std::size_t operator()(const Vector3f& k) const
		{
			float x = k(0);
			float y = k(1);
			float z = k(2);
			return (int)(x * 73856093) ^ (int)(y * 19349663) ^ (int)(z * 83492791);
		}
	};
	//using VectorHashTable = std::unordered_map<Vector3f, Vector3f, VHasher>;

	static bool SkipToNextObject(LoadState&);
	static IVertexData* StartObjectRead(LoadState&);
	static void ReadVData(LoadState&, 
		std::vector<Vector3f>& vertices, 
		std::vector<Vector2f>& uvs, 
		std::vector<Vector3f>& normals);
	static IVertexData* MakeVertexData(
		LoadState&,
		std::vector<Vector3f>& vertices,
		std::vector<Vector2f>& uvs,
		std::vector<Vector3f>& normals
	);
	static void fPartSet(LoadState& load_state, std::string fPart, int*);
};

}
}
}