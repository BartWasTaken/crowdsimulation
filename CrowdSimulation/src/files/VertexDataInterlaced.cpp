#include "VertexDataInterlaced.h"
#include <Tools/Include.h>

namespace CGMain {

VertexDataInterlaced::VertexDataInterlaced(bool indexed, bool pos, bool uv, bool norm, bool tang, std::vector<int> extra_sizes)
{
	Setup(indexed, pos, uv, norm, tang, extra_sizes);
}

VertexDataInterlaced::VertexDataInterlaced(bool indexed, bool pos, bool uv, bool norm, bool tang)
{
	std::vector<int> empty;
	Setup(indexed, pos, uv, norm, tang, empty);
}

VertexDataInterlaced::~VertexDataInterlaced()
{
	ReleaseMemory();
}

void VertexDataInterlaced::ReserveMemory(int v_count, int i_count)
{
	assert(((i_count > 0) == is_indexed()) && "Cannot reserve indices for non-indexed mesh");
	
	gVCount = v_count;
	gICount = i_count;

	gVertice_data.resize(gFloatStride * v_count);

	if (is_indexed()) {
		gIndice_data.resize(i_count);
	}	
}

Vector3f& VertexDataInterlaced::pos(int i)
{
	return *reinterpret_cast<Vector3f*>(gVertice_data.data() + i*gFloatStride + gPosOffset);
}

Vector2f& VertexDataInterlaced::uv(int i)
{
	return *reinterpret_cast<Vector2f*>(gVertice_data.data() + i*gFloatStride + gUvOffset);
}

Vector3f& VertexDataInterlaced::norm(int i)
{
	return *reinterpret_cast<Vector3f*>(gVertice_data.data() + i*gFloatStride + gNormOffset);
}

Vector3f& VertexDataInterlaced::tang(int i)
{
	return *reinterpret_cast<Vector3f*>(gVertice_data.data() + i*gFloatStride + gTangOffset);
}

void* VertexDataInterlaced::get(int data_type, int i)
{
	if (data_type < gExtra_offsets.size()) {
		return gVertice_data.data() + i * gFloatStride + gExtra_offsets[data_type];
	}
	else return nullptr;
}

bool VertexDataCommon::has_pos()
{
	return gHas_pos;
}

bool VertexDataCommon::has_uv()
{
	return gHas_uv;
}

bool VertexDataCommon::has_norm()
{
	return gHas_norm;
}

bool VertexDataCommon::has_tang()
{
	return gHas_tang;
}

int VertexDataCommon::vCount()
{
	return gVCount;
}

int VertexDataCommon::iCount()
{
	return gICount;
}

uint32_t* VertexDataCommon::indices()
{
	return gIndice_data.data();
}

bool VertexDataCommon::is_indexed()
{
	return gIsIndexed;
}

std::vector<CGMain::VertexBufferDescr> VertexDataInterlaced::GetLayout()
{
	// the interlaced structures uses just a single buffer
	// with size : nr_of_vertices * size_per_vertice
	std::vector<CGMain::VertexBufferDescr> result {
		{ (int) (vCount() * gFloatStride * sizeof(float)), gVertice_data.data() }
	};
	return result;
}

void* VertexDataInterlaced::data()
{
	return gVertice_data.data();
}

void VertexDataInterlaced::ReleaseMemory()
{
	std::vector<float>().swap(gVertice_data);
	std::vector<uint32_t>().swap(gIndice_data);
	gICount = 0;
	gVCount = 0;
}

CGMain::IVertexData::MemoryLayout VertexDataInterlaced::GetMemoryLayout()
{
	return MemoryLayout::Interlaced;
}

bool VertexDataInterlaced::is_allocated() const
{
	return gVertice_data.size() > 0;
}

void VertexDataInterlaced::Setup(bool indexed, bool pos, bool uv, bool norm, bool tang, std::vector<int>& extra_sizes)
{
	Q_ASSERT((tang ? norm && uv : true) && "Cannot have tangents without having normals and uv's");

	gHas_pos = pos;
	gHas_uv = uv;
	gHas_norm = norm;
	gHas_tang = tang;
	gIsIndexed = indexed;

	//gStride = (int)pos + (int)uv + (int)norm + (int)tang;

	gFloatStride = 0;

	if (pos) {
		gPosOffset = gFloatStride;
		gFloatStride += 3;
	}
	if (uv) {
		gUvOffset = gFloatStride;
		gFloatStride += 2;
	}
	if (norm) {
		gNormOffset = gFloatStride;
		gFloatStride += 3;
	}
	if (tang) {
		gTangOffset = gFloatStride;
		gFloatStride += 3;
	}

	auto extra_count = extra_sizes.size();
	for (int i = 0; i < extra_count; i++) {
		gExtra_offsets[i] = gFloatStride;
		gFloatStride += extra_sizes[i];
	}
	gExtra_sizes = extra_sizes;
}

std::vector<CGMain::VertexBufferDescr> VertexDataSeperated::GetLayout()
{
	// data per vertex
	std::vector<CGMain::VertexBufferDescr> result;

	if (has_pos()) result.push_back({ (int)(gPositions_data.size() * sizeof(gPositions_data[0])), gPositions_data.data() });
	if (has_norm()) result.push_back({ (int)(gNormals_data.size() * sizeof(gNormals_data[0])), gNormals_data.data() });
	if (has_uv()) result.push_back({ (int)(gUVs_data.size() * sizeof(gUVs_data[0])), gUVs_data.data() });
	if (has_tang()) result.push_back({ (int)(gTangents_data.size() * sizeof(gTangents_data[0])), gTangents_data.data() });

	for (auto& extra : gExtra_data) {
		result.push_back({ (int)(extra.size() * sizeof(extra[0])), extra.data() });
	}

	return result;
}

CGMain::IVertexData::MemoryLayout VertexDataSeperated::GetMemoryLayout()
{
	return MemoryLayout::Seperated;
}

void VertexDataSeperated::ReserveMemory(int v_count, int i_count)
{
	gIndice_data.resize(i_count);

	if (has_pos()) gPositions_data.resize(v_count);
	if (has_uv()) gUVs_data.resize(v_count);
	if (has_norm())	gNormals_data.resize(v_count);
	if (has_tang()) gTangents_data.resize(v_count);

	for (int i = 0, n = gExtra_data.size(); i < n; i++) {
		gExtra_data[i].resize(v_count * gExtra_sizes[i]);
	}
}

void* VertexDataSeperated::get(int data_type, int i)
{
	return (void*) gExtra_data[data_type][i];
}

Vector3f& VertexDataSeperated::norm(int i)
{
	return gNormals_data[i];
}

Vector3f& VertexDataSeperated::pos(int i)
{
	return gPositions_data[i];
}

std::vector<int> VertexDataCommon::sizes()
{
	std::vector<int> result;
	if (has_pos()) result.push_back(3);
	if (has_uv()) result.push_back(2);
	if (has_norm()) result.push_back(3);
	if (has_tang()) result.push_back(3);
	if (gExtra_sizes.size() > 0) Tools::Append(result, gExtra_sizes);
	return result;
}

Vector3f& VertexDataSeperated::tang(int i)
{
	return gTangents_data[i];
}

Vector2f& VertexDataSeperated::uv(int i)
{
	return gUVs_data[i];
}

AABB3 VertexDataCommon::GetAABB()
{
	assert(has_pos() && "Cannot get AABB on mesh without positions");

	int v_count = vCount();
	Vector3f min = pos(0);
	Vector3f max = pos(0);

	for (int i = 1; i < v_count; i++) {
		auto& v = pos(i);
		for (int j = 0; j < 3; j++) {
			if (v(j) < min(j)) min(j) = v(j);
			if (v(j) > max(j)) max(j) = v(j);
		}
	}

	return { min, max };
}

}