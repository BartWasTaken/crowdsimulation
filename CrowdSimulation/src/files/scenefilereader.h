#ifndef SCENEFILEREADER_H
#define SCENEFILEREADER_H

#include "src/tools/shared_eigen_incl.h"
#include "src/vis/openglincl.h"
#include "src/sim/simdeclr.h"
#include "src/sim/obstacle.h"
#include "src/sim/IEnvironment.h"
#include "src/sim/Engines/smokefield.h"
#include <QJsonDocument>

class SceneFileReader
{
public:
	SceneFileReader();

	static void		Read(SimulationCore&, OpenGLView& f, QJsonDocument&, bool enable);
	static void		Write(IEnvironment_ptr);

private:

	constexpr static int dim = 2;
	using myVec = Matrix<float, dim, 1>;

	struct MyObstacle {
		std::vector<myVec> vertices;
	};
	struct MyGoal {
		myVec direction;
		std::vector<myVec> vertices;
	};

	static QJsonArray MakeVertices(const std::vector<Vector2f>& vertices);

	static bool		ReadScene(SimulationCore&, OpenGLView&, QJsonObject&, bool enable);
	static bool		ReadBounds(SimulationCore& sc, QJsonObject&, AABB2& out_bounds);
	
	static void		ReadObjects(SimulationCore& sc, QJsonArray&, std::vector<MyGoal>& out_goals, std::vector<StairCaseArea>& out_staircases);
	static void		ReadObstacles(SimulationCore& sc, QJsonArray&, std::vector<MyObstacle>& out_obstacles);
	static void		ReadFireEvents(SimulationCore& sc, QJsonArray&, std::vector<SmokeField::FireEvent>&);
	static void		ReadElevations(SimulationCore& sc, QJsonArray&, std::vector<ElevationArea>&);
	static void		ReadObstacleEvents(SimulationCore& sc, QJsonArray&, std::vector<IEnvironment::NewObstacleEvent>&);
	static void		ReadSpawnAreas(SimulationCore& sc, QJsonArray&, std::vector<SpawnArea>&);

	static void		ReadVerticeArray(QJsonArray&, std::vector<myVec>& out_vertices, bool* reversed = nullptr);
	static bool		IsVerticeArray(QJsonArray&);

	static bool		TryReadString(QJsonObject&, QString name, QString& out_value);
	static bool		TryReadBool(QJsonObject&, QString name, bool& out_value);
	static bool		TryReadFloat(QJsonObject&, QString name, float& out_value);
	static bool		TryReadCoordinate(QJsonObject&, QString name, Vector2f& out_value);

	static void		AddOutline(SimulationCore& sc, IEnvironment_ptr& env, AABB2 bbox);
};

#endif // SCENEFILEREADER_H