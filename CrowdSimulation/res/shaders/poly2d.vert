#version 430 core

layout (location = 0) in vec2 a_vertex;

layout (location = 0) uniform vec4 u_orientation;


#define ratio		u_orientation.w
#define map_pos		u_orientation.xy
#define map_scale	u_orientation.z

vec2 translate_global(vec2 v) {
	return v + map_pos;
}
vec2 scale_global(vec2 v) {
	return v * map_scale;
}

void main() 
{
	vec2 pos_ws = translate_global(a_vertex);
	pos_ws = scale_global(pos_ws);
		
	gl_Position = vec4(pos_ws.x / ratio, pos_ws.y, 1, 1);
}