#version 430 core

layout (location = 0) in vec2 a_vertex;
layout (location = 1) in vec4 a_orientation;
layout (location = 2) in vec4 a_color;
layout (location = 3) in vec4 a_rotation;

layout (location = 0) uniform vec4 u_orientation;


out vec4 color;

#define ratio		u_orientation.w
#define map_pos		u_orientation.xy
#define map_scale	u_orientation.z

#define obj_pos		a_orientation.xy
#define obj_scale	a_orientation.zw


vec2 translate_global(vec2 v) {
	return v + map_pos;
}
vec2 translate_local(vec2 v) {
	return v + obj_pos;
}
vec2 rotate_local(vec2 v) {
	return vec2(a_rotation.x * v.x + a_rotation.y * v.y, a_rotation.z * v.x + a_rotation.w * v.y);
}
vec2 scale_local(vec2 v) {
	return v * obj_scale;
}
vec2 scale_global(vec2 v) {
	return v * map_scale;
}

void main() 
{
	//vec2 rot_vertex = vec2(a_rotation.x * a_vertex.x + a_rotation.y * a_vertex.y, a_rotation.z * a_vertex.x + a_rotation.w * a_vertex.y);
	//vec2 pos_ws = ((rot_vertex * obj_scale + obj_pos) + map_pos) * map_scale;

	vec2 pos_ws = scale_local(a_vertex);
	pos_ws = rotate_local(pos_ws);
	pos_ws = translate_local(pos_ws);
	pos_ws = translate_global(pos_ws);
	pos_ws = scale_global(pos_ws);
		
	color = a_color;

	gl_Position = vec4(pos_ws.x / ratio, pos_ws.y, 1, 1);
}