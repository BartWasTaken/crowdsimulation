#version 430 core

layout (location = 1) uniform vec4 u_color;

layout(location = 0) out vec4 out_color;

void main() {
	out_color = u_color;
}