#version 430 core

layout (location = 0) in vec4 a_color;
layout (location = 1) in vec3 a_vertex;
layout (location = 2) in vec3 a_normal;
layout (location = 3) in mat4 a_modelmatrix;

layout (location = 0) uniform mat4 u_vp;

out vec3 ws_normal;
out vec4 color;

void main() 
{	
	vec4 world_space = a_modelmatrix * vec4(a_vertex, 1);
	ws_normal = (a_modelmatrix * vec4(a_normal, 0)).xyz;
	color = a_color;
	//ws_normal = normalize(a_vertex);
	gl_Position = u_vp * world_space;
}