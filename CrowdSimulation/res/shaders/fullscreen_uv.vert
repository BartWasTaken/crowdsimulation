#version 430 core

layout(location = 0) in vec2 in_position_ms;

out vec2 UV;

void main() {
	vec4 vertice = vec4(in_position_ms, 1, 1);
	UV = (vertice.xy + 1) * 0.5;
	gl_Position = vertice;
}