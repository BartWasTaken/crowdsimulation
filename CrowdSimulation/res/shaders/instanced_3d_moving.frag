#version 430 core

layout (location = 0) out vec4 out_color;

in vec4 color;
in vec3 ws_normal;

uniform vec3 light_dir = vec3(.5773,.5773,.5773);

void main() 
{
	vec3 normal = normalize(ws_normal);
	float light_power = 2.0f;
	float ambient_power = 0.9f;
  	float diff_power = light_power * clamp( dot(normal, light_dir), 0.0f, 1.0f);
	out_color = vec4((ambient_power + diff_power) * color.rgb, 1);
}