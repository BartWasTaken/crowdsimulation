#version 430 core

layout (location = 0) in vec3 in_position_ms;

layout (location = 0) uniform mat4 MVP;
//layout (location = 1) uniform mat4 modelMatrix;

//out vec3 gPos_ws;

void main(){
	//gPos_ws = vec3(modelMatrix * vec4(in_position_ms,1));
	gl_Position = MVP * vec4(in_position_ms, 1);
}