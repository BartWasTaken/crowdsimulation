#-------------------------------------------------
#
# Project created by QtCreator 2018-02-07T04:52:39
#
#-------------------------------------------------

QT      += core gui
QT      += gui
QT      += opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CrowdSimulation
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += force_debug_info
QMAKE_CXXFLAGS_RELEASE = $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
QMAKE_LFLAGS_RELEASE = $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO

QMAKE_CXXFLAGS += /std:c++17

#CONFIG += c++17

SOURCES += \
    src/shared.cpp \
    src/programmanager.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/tools/resizedlistener.cpp \
    src/vis/GPUBuffer.cpp \
    src/vis/shaderloader.cpp \
    src/vis/openglmaster.cpp \
    src/sim/simulationsettings.cpp \
    src/sim/simulationcore.cpp \
    src/vis/griddrawer2d.cpp \
    src/vis/iinstanceddrawer2d.cpp \
    src/vis/circledrawer2d.cpp \
    src/tools/Event.cpp \
    src/tools/InputListener.cpp \
    src/tools/InputManager.cpp \
    src/sim/support/neighbourfinder.cpp \
    src/sim/ContinuumField.cpp \
    src/sim/simulationcore.cpp \
    src/sim/simulationsettings.cpp \
    src/sim/Engines/sphengine.cpp \
    src/sim/Engines/testsimulation.cpp \
    src/sim/ContinuumField.cpp \
    src/sim/simulationcore.cpp \
    src/sim/simulationsettings.cpp \
    src/vis/openglincl.cpp \
    src/sim/Engines/continuumengine.cpp \
    src/sim/goals.cpp \
    src/ui/statsview.cpp \
    src/sim/IEnvironment.cpp \
    src/sim/environment2d.cpp \
    src/sim/obstacle.cpp \
    src/vis/shapedrawer2d.cpp \
    src/sim/directplanner.cpp \
    src/sim/interfaces.cpp \
    src/sim/simulationstats.cpp \
    src/tools/griddecorators.cpp \
    src/tools/raster.cpp \
    src/tools/geomfunc.cpp \
    src/tools/timer.cpp \
    src/sim/Engines/smokefield.cpp \
    src/sim/Support/obstaclegridprovider.cpp \
    src/sim/Engines/continuumengine.cpp \
    src/sim/Engines/smokefield.cpp \
    src/sim/Engines/sphengine.cpp \
    src/sim/Engines/testsimulation.cpp \
    src/sim/ContinuumField.cpp \
    src/sim/directplanner.cpp \
    src/sim/environment2d.cpp \
    src/sim/goals.cpp \
    src/sim/IEnvironment.cpp \
    src/sim/interfaces.cpp \
    src/sim/obstacle.cpp \
    src/sim/simulationcore.cpp \
    src/sim/simulationsettings.cpp \
    src/sim/simulationstats.cpp \
    src/tools/Event.cpp \
    src/tools/geomfunc.cpp \
    src/tools/griddecorators.cpp \
    src/tools/InputListener.cpp \
    src/tools/InputManager.cpp \
    src/tools/raster.cpp \
    src/tools/resizedlistener.cpp \
    src/tools/timer.cpp \
    src/ui/statsview.cpp \
    src/vis/circledrawer2d.cpp \
    src/vis/GPUBuffer.cpp \
    src/vis/GridDrawer2D.cpp \
    src/vis/iinstanceddrawer2d.cpp \
    src/vis/openglincl.cpp \
    src/vis/openglmaster.cpp \
    src/vis/shaderloader.cpp \
    src/vis/shapedrawer2d.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/programmanager.cpp \
    src/shared.cpp \
    src/files/scenefilereader.cpp \
    src/tools/linetracer.cpp \
    src/sim/kernels.cpp \
    src/tools/scenebuilder.cpp \
    src/vis/renderer3d.cpp \
    src/vis/polygon3d.cpp \
    src/vis/Cameras/ICamera.cpp \
    src/vis/Cameras/IViewPoint.cpp \
    src/vis/Cameras/ModelCam.cpp \
    src/vis/Cameras/SingleViewPointCamera.cpp \
    src/vis/Cameras/StandardViewPoint.cpp \
    src/vis/Orientable.cpp \
    src/tools/convexhull.cpp \
    src/tools/polygon2d.cpp \
    src/vis/instanceddrawer3d.cpp \
    src/vis/meshgen.cpp \
    src/files/ObjLoader.cpp \
    src/tools/BasicFunctionality.cpp \
    src/files/MeshTypes.cpp \
    src/files/VertexDataInterlaced.cpp \
    src/sim/collisionresolver.cpp \
    src/ui/eventview.cpp

HEADERS += \
    src/sim/support/neighbourfinder.h \
    src/vis/GPUBuffer.h \
    src/shared.h \
    src/programmanager.h \
    src/mainwindow.h \
    src/vis/shaderloader.h \
    src/vis/openglincl.h \
    src/tools/resizedlistener.h \
    src/tools/DataTypes.hpp \
    src/tools/shared_eigen_incl.h \
    src/tools/include.h \
    src/vis/openglmaster.h \
    src/sim/simulationsettings.h \
    src/sim/simulationcore.h \
    src/sim/simdeclr.h \
    src/vis/griddrawer2d.h \
    src/vis/iinstanceddrawer2d.h \
    src/vis/circledrawer2d.h \
    src/vis/irenderable.h \
    src/vis/visdeclr.h \
    src/tools/Event.h \
    src/tools/parrallelforpool.h \
    src/tools/InputListener.h \
    src/tools/manytomanylink.h \
    src/tools/InputManager.h \
    src/tools/changedlistener.h \
    src/sim/kernels.h \
    src/tools/geomfunc.h \
    src/sim/simdeclr.h \
    src/sim/simulationcore.h \
    src/sim/simulationsettings.h \
    src/sim/Engines/sphengine.h \
    src/sim/Engines/testsimulation.h \
    src/sim/ContinuumField.h \
    src/tools/INonCopyable.hpp \
    src/sim/Engines/continuumengine.h \
    src/sim/goals.h \
    src/ui/statsview.h \
    src/sim/IEnvironment.h \
    src/sim/environment2d.h \
    src/sim/obstacle.h \
    src/vis/shapedrawer2d.h \
    src/sim/interfaces.h \
    src/sim/directplanner.h \
    src/sim/simulationstats.h \
    src/tools/griddecorators.h \
    src/tools/raster.h \
    src/tools/timer.h \
    src/sim/Engines/smokefield.h \
    src/tools/macros.h \
    src/sim/Engines/continuumengine.h \
    src/sim/Engines/smokefield.h \
    src/sim/Engines/sphengine.h \
    src/sim/Engines/testsimulation.h \
    src/sim/ContinuumField.h \
    src/sim/directplanner.h \
    src/sim/environment2d.h \
    src/sim/goals.h \
    src/sim/IEnvironment.h \
    src/sim/interfaces.h \
    src/sim/kernels.h \
    src/sim/obstacle.h \
    src/sim/simdeclr.h \
    src/sim/simulationcore.h \
    src/sim/simulationsettings.h \
    src/sim/simulationstats.h \
    src/tools/changedlistener.h \
    src/tools/DataTypes.hpp \
    src/tools/Event.h \
    src/tools/geomfunc.h \
    src/tools/griddecorators.h \
    src/tools/include.h \
    src/tools/INonCopyable.hpp \
    src/tools/InputListener.h \
    src/tools/InputManager.h \
    src/tools/macros.h \
    src/tools/manytomanylink.h \
    src/tools/parrallelforpool.h \
    src/tools/raster.h \
    src/tools/resizedlistener.h \
    src/tools/timer.h \
    src/ui/statsview.h \
    src/vis/CircleDrawer2D.h \
    src/vis/GPUBuffer.h \
    src/vis/GridDrawer2D.h \
    src/vis/iinstanceddrawer2d.h \
    src/vis/irenderable.h \
    src/vis/openglincl.h \
    src/vis/openglmaster.h \
    src/vis/shaderloader.h \
    src/vis/shapedrawer2d.h \
    src/vis/visdeclr.h \
    src/mainwindow.h \
    src/programmanager.h \
    src/shared.h \
    src/files/scenefilereader.h \
    src/tools/linetracer.h \
    src/tools/scenebuilder.h \
    src/vis/renderer3d.h \
    src/vis/polygon3d.h \
    src/vis/Cameras/ICamera.h \
    src/vis/Cameras/IViewPoint.h \
    src/vis/Cameras/ModelCam.h \
    src/vis/Cameras/SingleViewPointCamera.h \
    src/vis/Cameras/StandardViewPoint.h \
    src/vis/Orientable.h \
    src/tools/convexhull.h \
    src/tools/polygon2d.h \
    src/vis/instanceddrawer3d.h \
    src/vis/meshgen.h \
    src/files/ObjLoader.h \
    src/tools/BasicFunctionality.h \
    src/files/MeshTypes.h \
    src/files/VertexDataInterlaced.h \
    src/tools/SmartPtrClass.h \
    src/sim/collisionresolver.h \
    src/ui/eventview.h


FORMS += \
        mainwindow.ui \
    src/vis/openglmaster.ui \
    src/vis/openglmaster.ui

INCLUDEPATH += $$PWD/external/eigen
INCLUDEPATH += $$PWD/external/clipper
INCLUDEPATH += $$PWD/src

RESOURCES += \
    resources.qrc

DISTFILES += \
    shaders
